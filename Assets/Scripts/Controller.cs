﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;


public class Controller : MonoBehaviour
{

	public GameObject Map;
	public GameObject PlayerObj;
	public GameObject BallObj;
	public GameObject RoomGen;
	public GameObject FrontWall;
	public GameObject Room;

	public GameObject UIPanelMenu;
	public GameObject UIPanelGOver;
	public GameObject UIPanelJuego;
	public GameObject UIPanelTransicion;


	public GameObject BotesText;
	public GameObject RoomText;
	public GameObject BricksText;
	public GameObject BotesTextGameOver;
	public GameObject RoomTextGameOver;
	public GameObject BricksTextGameOver;
	public GameObject BotesBuenosText;
	public GameObject TimerTransition;
	public GameObject BricksTextTransicion;
	public GameObject RoomTextTransicion;
	public GameObject TotalRoomMenuText;
	public GameObject TotalBricksMenuText;
	public GameObject Telefono;
	public GameObject MinigameButton;
	public GameObject EnterButton;

	GameObject ball;
	GameObject player;
	GameObject plants;


	public Camera Perspec, Ortho, Iso, Interface;

	public int rollerCamara;
	public float speedCamAnim;

	public bool isEnterButtonReady = true;

	int NRoom;
	public int NBricks;
	public static int NTotalBricks, NTotalRooms;


	float tiempo;
	public float tiempoPausa;
	public int numMultiplyBalls;
		
	public List<GameObject> Multibolas;
	public GameObject audio;
	[SerializeField]
	public static bool autoplay;
	public bool auto;





	public enum State
	{

		MENU = 0,
		GAME = 1,
		PAUSE = 2,
		GAMEOVER = 3,
		TRANSITION = 4,
		COLECTION = 5,
		MINIGAME = 6

	}



	[SerializeField]
	public static State gameState;

	public void Awake ()
	{
		#if UNITY_STANDALONE
//		Screen.SetResolution (410, 656, false);
//		Screen.orientation = ScreenOrientation.Portrait;
		#endif
	}


	public void isTrampa ()
	{
		Application.LoadLevel ("Mapa");
	}

	public void SetAuto ()
	{
		if (auto)
			autoplay = true;
		else
			autoplay = false;
	}

	// Use this for initialization
	public void Start ()
	{
		SetAuto ();
		gameState = State.MENU;
		HideUIPanelGameOver ();
		HideUIPanelJuego ();
		ShowUIPanelMenu ();
		HideUIPanelTransicion ();
		getData ();
		StartCoroutine ("runScore");

		NRoom = 0;
		NBricks = 0;

		UpdateHUD ();

		Perspec.gameObject.SetActive (false);
		Ortho.gameObject.SetActive (false);
		Iso.gameObject.SetActive (true);

	}


	public void StartGame ()
	{
		gameState = State.GAME;

		RemoveAll ();
		StartCoroutine ("CamAnim");
		CreatePlayer ();
		CreateBall ();
		CreateRoom ();

		HideUIPanelMenu ();
		HideUIPanelTransicion ();
		ShowUIPanelJuego ();
		print ("Start game: fase: " + RoomGenerator.fase + " room " + NRoom);
	}



	IEnumerator CamAnim ()
	{

		Perspec.nearClipPlane = 20;
		Ortho.nearClipPlane = 20;
		Iso.nearClipPlane = 20;

		if (Perspec.isActiveAndEnabled) {
			while (Perspec.nearClipPlane > 1) {
				Perspec.nearClipPlane -= 1;

				yield return new WaitForSeconds (speedCamAnim);
			}
		}

		if (Ortho.isActiveAndEnabled) {
			while (Ortho.nearClipPlane > -15) {
				Ortho.nearClipPlane -= 1;

				yield return new WaitForSeconds (speedCamAnim);
			}
		}

		if (Iso.isActiveAndEnabled) {
			while (Iso.nearClipPlane > -15) {
				Iso.nearClipPlane -= 1;
				yield return new WaitForSeconds (speedCamAnim);
			}
		}
	}

	void RemoveAll ()
	{
		Destroy (Room);
		Destroy (player);
		Destroy (ball);
		Destroy (FrontWall);
	}

	public void CreateRoom ()
	{
//		NRoom += 1;
		NTotalRooms += NRoom;
		RoomGen.GetComponent<RoomGenerator> ().setCameras (Perspec, Ortho, Iso, Interface);
		RoomGen.GetComponent<RoomGenerator> ().GenerateRoom ();
		setData ();
		UpdateHUD ();
	}

	void CreatePlayer ()
	{
		player = Instantiate (PlayerObj, transform.position, Quaternion.identity)as GameObject;
		RoomGen.GetComponent<RoomGenerator> ().Player = player;
		player.GetComponent<Move> ().setCameras (Perspec, Ortho, Iso, Interface);
		player.GetComponentInChildren<ColliderPlayer> ().setCameras (Perspec, Ortho, Iso, Interface);
		player.GetComponentInChildren<ColliderPlayer> ().Controller = gameObject;
	}

	#region PANELES

	void HideUIPanelJuego ()
	{
		Vector2 pos = new Vector2 (Screen.width, Screen.height + 8000);
		UIPanelJuego.transform.position = pos;
	}

	void ShowUIPanelJuego ()
	{
		Vector2 pos = new Vector2 (0, 0);
		UIPanelJuego.GetComponent<RectTransform> ().anchoredPosition = pos;
	}

	void HideUIPanelGameOver ()
	{
		Vector2 pos = new Vector2 (Screen.width, Screen.height + 2000);
		UIPanelGOver.transform.position = pos;
	}

	void ShowUIPanelGameOver ()
	{
		Vector2 pos = new Vector2 (0, 0);
		UIPanelGOver.GetComponent<RectTransform> ().anchoredPosition = pos;
		int n = Random.Range (1, 3);
		print ("n: " + n);
		if (NBricks % n == 0)
			ShowMinigameButton ();
	}

	public void ShowMinigameButton ()
	{
		MinigameButton.SetActive (true);
	}

	void HideUIPanelMenu ()
	{
		Vector2 pos = new Vector2 (Screen.width, Screen.height + 2000);
		UIPanelMenu.transform.position = pos;
	}

	void ShowUIPanelMenu ()
	{
		Vector2 pos = new Vector2 (0, 0);
		UIPanelMenu.GetComponent<RectTransform> ().anchoredPosition = pos;
	}

	void HideUIPanelTransicion ()
	{
		Vector2 pos = new Vector2 (Screen.width, Screen.height + 2000);
		UIPanelTransicion.transform.position = pos;
	}

	void ShowUIPanelTransicion ()
	{
		Vector2 pos = new Vector2 (0, 0);
		UIPanelTransicion.GetComponent<RectTransform> ().anchoredPosition = pos;
	}

	#endregion

	void CreateBall ()
	{
		Multibolas = new List<GameObject> ();
		ball = Instantiate (BallObj, transform.position, Quaternion.identity)as GameObject;
		ball.name = "Ball";
		ball.tag = "Ball";
		RoomGen.GetComponent<RoomGenerator> ().Ball = ball;
		ball.transform.GetChild (1).transform.GetComponent<BallCollider> ().Controller = gameObject;
		ball.transform.GetChild (1).transform.GetComponent<BallCollider> ().nBotes = BotesText;
		ball.transform.GetChild (1).GetComponent<BallCollider> ().setCameras (Perspec, Ortho, Iso, Interface);
		Multibolas.Add (ball);
		player.GetComponent<Move> ().SetBall (ball);

	}

	public void RestartGame ()
	{
		StartGame ();
	}
	
	// Update is called once per frame
	void Update ()
	{

		#if UNITY_EDITOR

		if (Input.GetKeyDown (KeyCode.H))
			deleteData ();
		
		//TECLAS DEBUG
		if (Input.GetKey (KeyCode.D))
			MoveRight ();
				
		if (Input.GetKey (KeyCode.A))
			MoveLeft ();

		if (!Input.GetKey (KeyCode.A) && !Input.GetKey (KeyCode.D))
			resetMovimiento ();
		
		#endif
	}

	public void RemoveBrick (GameObject brick)
	{
		FrontWall.GetComponent<FrontWall> ().RemoveBrick (brick);
		if (isWallEmpty () && gameState == State.GAME) {
			gameState = State.TRANSITION;
			PasaFase ();
		}
	}

	#region PASAFASE

	public void PasaFase ()
	{
		UpdateNumberRoom ();
		RoomGen.GetComponent<RoomGenerator> ().increaseFase (NRoom);
		DestroyAllBalls ();
		Destroy (player);
		ShowUIPanelTransicion ();
		HideUIPanelJuego ();
		StartCoroutine ("Cortina");
		print ("Pasa fase: " + RoomGenerator.fase + " room " + NRoom);

	}

	void UpdateNumberRoom ()
	{
		NRoom += 1;
		print ("Número Room: " + NRoom);
	}

	IEnumerator Cortina ()
	{
		tiempo = tiempoPausa;
		DisableEnterButton ();
		while (tiempo > 0) {
			tiempo -= 1 * Time.deltaTime;

			yield return new WaitForSeconds (0);
		}
		if (autoplay)
			RestartGame ();
		else
			EnableEnterButton ();
	}


	void EnableEnterButton ()
	{
		isEnterButtonReady = true;
		EnterButton.SetActive (true);
	}



	void DisableEnterButton ()
	{
		isEnterButtonReady = false;
		EnterButton.SetActive (false);

	}


	#endregion

	bool isWallEmpty ()
	{
		if (FrontWall.GetComponent<FrontWall> ().Bricks.Count == 0)
			return true;
		else
			return false;
	}

	public void GameOver ()
	{
		DestroyAllBalls ();
		Destroy (player);

		gameState = State.GAMEOVER;


		ShowUIPanelGameOver ();
		HideUIPanelMenu ();
		HideUIPanelJuego ();
		UpdateHUD ();
		GameOverStats ();

	}

	void GameOverStats ()
	{

//		NTotalBricks += NBricks;
		UpdateHUD ();
		setData ();

	}

	IEnumerator runScore ()
	{
		int p = NBricks + NTotalBricks;
		while (p > NTotalBricks) {
			NTotalBricks += 1;
			UpdateHUD ();

			yield return new WaitForSeconds (0.0001f);
		}
		setData ();

	}



	void DestroyAllBalls ()
	{
		GameObject[] go = GameObject.FindGameObjectsWithTag ("Ball");
		foreach (GameObject g in go) {
			Destroy (g);
		}
	}

	public void UpdateHUD ()
	{
//		BotesText.GetComponent<Text> ().text = NBotes.ToString ();
		RoomText.GetComponent<Text> ().text = "ROOM " + NRoom;
		BricksText.GetComponent<Text> ().text = "BRICKS " + NBricks;
//		BotesBuenosText.GetComponent<Text> ().text = "BOUNCES" + NBotes;
		BricksTextTransicion.GetComponent<Text> ().text = "BRICKS " + NBricks;
		RoomTextTransicion.GetComponent<Text> ().text = "ROOM " + NRoom;
//		BotesTextGameOver.GetComponent<Text> ().text = "BOUNCES " + NBotes;
		RoomTextGameOver.GetComponent<Text> ().text = "ROOM " + NRoom;
		BricksTextGameOver.GetComponent<Text> ().text = "BRICKS " + NBricks;
		TotalRoomMenuText.GetComponent<Text> ().text = "ROOMS " + NTotalRooms;
		TotalBricksMenuText.GetComponent<Text> ().text = "BRICKS " + NTotalBricks;
				
	}


	//PLAYER BUTTONS
	public void MoveLeft ()
	{

		if (ball && !ball.GetComponent<MoveBall> ().isReady) {
			ball.GetComponent<MoveBall> ().isReady = true;
			ball.GetComponent<MoveBall> ().velocidad += Vector3.left;
		}

		if (player)
			player.GetComponent<Move> ().Left ();
	}

	public void MoveRight ()
	{
		if (ball && !ball.GetComponent<MoveBall> ().isReady) {
			ball.GetComponent<MoveBall> ().isReady = true;
			ball.GetComponent<MoveBall> ().velocidad += Vector3.right;

		}
		if (player)
			player.GetComponent<Move> ().Right ();

	}

	public void resetMovimiento ()
	{
		if (player)
			player.GetComponent<Move> ().resetMove ();
	}


	//TODO: Refactorizar con createBall
	public void CreateMultiBalls (Vector3 pos, Vector3 boundaries, float alturaSombra)
	{
		for (int i = 0; i < numMultiplyBalls; i++) {

			GameObject ballCopy = Instantiate (BallObj, pos, Quaternion.identity)as GameObject;
			ballCopy.name = "BallCopy";
			ballCopy.tag = "Ball";
			ballCopy.GetComponent<MoveBall> ().boundary = boundaries;

			ballCopy.GetComponent<MoveBall> ().velocidad = new Vector3 (Random.Range (-1, 1), 0, Random.Range (0, 2) * 2 - 1);

			ballCopy.GetComponentInChildren<BallCollider> ().setCameras (Perspec, Ortho, Iso, Interface);
			ballCopy.GetComponent<MoveBall> ().isReady = true;
			ballCopy.transform.GetChild (1).transform.GetComponent<BallCollider> ().Controller = gameObject;
			ballCopy.transform.GetChild (0).transform.GetComponent<BallShadow> ().alturaSombra = alturaSombra;//ball.transform.GetChild (0).transform.GetComponent<BallShadow> ().alturaSombra;
			Multibolas.Add (ballCopy);
		}
	}

	public void CheckEndGame (GameObject ball)
	{
		Multibolas.Remove (ball);
//		print ("BOLA " + Multibolas.Count);
		Destroy (ball);

		if (Multibolas.Count < 1) {
			GameOver ();
		}
	}

	public void PlayAudio (float pitch)
	{
		audio.GetComponent<AudioSource> ().pitch = pitch;
//		audio.GetComponent<AudioSource> () = pitch;
		if (!audio.GetComponent<AudioSource> ().isPlaying)
			audio.GetComponent<AudioSource> ().Play ();
//		print (audio.GetComponent<AudioSource> ().clip.);
	}

	//	public void OnApplicationPause ()
	//	{
	//		if (currentState == State.INGAME)
	//			pauseGame ();
	//	}

	#region PLAYER_PREFS

	public static void setData ()
	{

		PlayerPrefs.SetInt ("totalBricks", NTotalBricks);
		PlayerPrefs.SetInt ("totalRooms", NTotalRooms);
		PlayerPrefs.SetInt ("fase", RoomGenerator.fase);
//		setSoundData ();

		print ("SAVING PREF_DATA");

	}


	//	public static void setSoundData ()
	//	{
	//
	//		SetBool ("sfxSwitch", sfxSwitch);
	//		SetBool ("musicSwitch", musicSwitch);
	//
	//	}

	void getData ()
	{

		print ("GETTING DATA");
		if (PlayerPrefs.HasKey ("totalBricks")) {
			NTotalBricks = PlayerPrefs.GetInt ("totalBricks");
			Debug.Log ("loading... totalBricks: " + NTotalBricks);
		}

		if (PlayerPrefs.HasKey ("totalRooms")) {
			NTotalRooms = PlayerPrefs.GetInt ("totalRooms");
			Debug.Log ("loading... totalRooms: " + NTotalRooms);
		}

		if (PlayerPrefs.HasKey ("fase")) {
			RoomGenerator.fase = PlayerPrefs.GetInt ("fase");
			Debug.Log ("loading... fase: " + RoomGenerator.fase);
		}

	}


	public void deleteData ()
	{
		PlayerPrefs.DeleteAll ();

		NTotalBricks = 0;
		NTotalRooms = 0;
		RoomGenerator.fase = 0;
		PlayerPrefs.Save ();
//		isReseted = true;

//		gameObject.GetComponent<gameControl> ().toMenu ();

//		print (" DELETING __ ******** LEVEL " + globales.level + " contadorChallenge " + globales.contadorChallenge + " ChallengeLevel " + globales.currentChallengeIndex + " maxDistance " + globales.maxDistance + " meterScore " + globales.meterScore + " passing " + globales.passStage);
	}


	public void OnApplicationQuit ()
	{
		Destroy (gameObject);
	}


	//herramientas para grabar tipo bool
	static void  SetBool (string name, bool value)
	{

		PlayerPrefs.SetInt (name, value ? 1 : 0);
	}

	static  bool GetBool (string name)
	{
		return PlayerPrefs.GetInt (name) == 1 ? true : false;
	}

	static  bool GetBool (string name, bool defaultValue)
	{
		if (PlayerPrefs.HasKey (name)) {
			return GetBool (name);
		}
		return defaultValue;
	}

	#endregion

	#region MINIGAME

	public void StartMinigame ()
	{
		gameState = State.MINIGAME;
		Telefono.SetActive (true);
	}

	public void EndMinigame ()
	{
		gameState = State.MENU;
		Telefono.SetActive (false);
	}

	#endregion
}
