﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour
{

	bool shaking;
	public float minMovementRange, maxMovementRange, positionRange, maxShakeField;

	public Vector3 initPosP, initPosO, initPosI;
	float shakeSizeP, shakeSizeO, shakeSizeI;

	public Camera Perspec, Ortho, Iso, Interface;



	public void setCameras (Camera Perspe, Camera Ortho, Camera Iso, Camera Interface)
	{
		this.Perspec = Perspe;
		this.Ortho = Ortho;
		this.Iso = Iso;
		this.Interface = Interface;

//		SetInitPos ();
	}

	void Start ()
	{
		initPosP = Perspec.transform.position;
		shakeSizeP = Perspec.fieldOfView;

		initPosO = Ortho.transform.position;
		shakeSizeO = Ortho.orthographicSize;

		initPosI = Iso.transform.position;
		shakeSizeI = Iso.orthographicSize;
	}



	public void ShakeThatCamera (float t)
	{

		if (gameObject.activeSelf && !shaking) {
			


			StartCoroutine ("shake", t);
		}
	}

	public IEnumerator shake (float t)
	{
		shaking = true;

		while (t > 0) {

//			print ("shaking" + t);

			Perspec.fieldOfView = (int)UnityEngine.Random.Range (shakeSizeP, shakeSizeP + maxShakeField);
			Ortho.orthographicSize = (int)UnityEngine.Random.Range (shakeSizeO, shakeSizeO + maxShakeField);
			Iso.orthographicSize = (int)UnityEngine.Random.Range (shakeSizeI, shakeSizeI + maxShakeField);

			Vector3 pP = new Vector3 (Perspec.transform.position.x + Random.Range (-positionRange, positionRange), Perspec.transform.position.y + Random.Range (-positionRange, positionRange), Perspec.transform.position.z);
			Vector3 pO = new Vector3 (Ortho.transform.position.x + Random.Range (-positionRange, positionRange), Ortho.transform.position.y + Random.Range (-positionRange, positionRange), Ortho.transform.position.z);
			Vector3 pI = new Vector3 (Iso.transform.position.x + Random.Range (-positionRange, positionRange), Iso.transform.position.y + Random.Range (-positionRange, positionRange), Iso.transform.position.z);



			Perspec.transform.position = pP;
			Ortho.transform.position = pO;
			Iso.transform.position = pI;


//			GetComponent<Camera> ().rect = new Rect (0, 0, 1 - Random.Range (-minMovementRange, -maxMovementRange), 1 - Random.Range (-minMovementRange, -maxMovementRange));
			t -= 1f;		
			yield return new WaitForSeconds (0.1f);
		}

		Perspec.fieldOfView = shakeSizeP;
		Ortho.orthographicSize = shakeSizeO;
		Iso.orthographicSize = shakeSizeI;
//		Vector3 NP = new Vector3 (initPos.x, initPos.y + 1, initPos.z);

		Perspec.transform.position = initPosP;
		Ortho.transform.position = initPosO;
		Iso.transform.position = initPosI;

//		Perspec.GetComponent<Camera> ().rect = Ortho.GetComponent<Camera> ().rect = Iso.GetComponent<Camera> ().rect = new Rect (0, 0, 1, 1);

		shaking = false;

	}

	//	public IEnumerator shakeOrthographic (float t)
	//	{
	//		shakeSize = Camera.main.orthographicSize;
	//		Vector3 initPos = Camera.main.transform.position;
	//
	//		while (t > 0) {
	//
	//			shaking = true;
	//
	//			Camera.main.orthographicSize = (int)UnityEngine.Random.Range (shakeSize, shakeSize + 6);
	//			Vector3 p = new Vector3 (Camera.main.transform.position.x + Random.Range (-1f, 1f), Camera.main.transform.position.y + Random.Range (-1f, 1f), Camera.main.transform.position.z);
	//			transform.position = p;
	//
	//			// choose the margin randomly
	//			// setup the rectangle
	//			GetComponent<Camera> ().rect = new Rect (0, 0, 1 - Random.Range (-5.10f, -10.1f), 1 - Random.Range (-5.10f, -10.1f));
	//
	//			t -= 1f;
	//			yield return new WaitForSeconds (0.1f * Time.deltaTime);
	//		}
	//
	//		Camera.main.orthographicSize = shakeSize;
	//		transform.position = initPos;
	//		GetComponent<Camera> ().rect = new Rect (0, 0, 1, 1);
	//		shaking = false;
	//
	//	}
}
