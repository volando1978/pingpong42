﻿using UnityEngine;
using System.Collections;

public class RoomGenerator : MonoBehaviour
{



	public GameObject LadrilloObj;
	public GameObject WallObj;
	GameObject Wall;
	public Material Cristal;
	public Material Pared;
	public GameObject Shadow;
	public Camera CameraMan;
	public GameObject Player;
	public GameObject Ball;
	public GameObject Controller;
	public GameObject Plantación;
	public int distanciaZCam, alturaCam;

	public float umbralRebotebounds;

	Vector3 LockCam, LockPlayer, LockBall;
	public Camera Perspec, Ortho, Iso, Interface;


	GameObject Container;
	public Vector3 WallPos;

	public static int fase;
	public static int pasaFaseFactor = 2;
	public int room;
	public int alto, ancho, fondo;

	public int minAlto, minAncho, minFondo;
	public int maxAlto, maxAncho, maxFondo;


	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.G)) {
			Controller.GetComponent<Controller> ().PasaFase ();
		}
	}

	public void setCameras (Camera Perspe, Camera Ortho, Camera Iso, Camera Interface)
	{
		this.Perspec = Perspe;
		this.Ortho = Ortho;
		this.Iso = Iso;
		this.Interface = Interface;
	}

	public Vector3 SetRoom (int fase)
	{

		alto = 4;
		ancho = minAncho + fase % 10;
		fondo = minAlto + fase % 10;

		//		alto = Random.Range (minAlto, maxAlto);
		//		ancho = Random.Range (minAncho, maxAncho);
		//		fondo = Random.Range (minFondo, maxFondo);

		return new Vector3 (alto, ancho, fondo);

	}

	public void increaseFase (int room)
	{
		if (room % RoomGenerator.pasaFaseFactor == 0)
			fase += 1;
		print ("Número de fase " + fase);
	}

	public void GenerateRoom ()
	{

		SetRoom (fase);

		#region POSICIONES
		Vector3 Left = new Vector3 (-ancho / 2, 0, 0);
		Quaternion LeftRotation = Quaternion.Euler (new Vector3 (0, 0, 0));
		Vector3 LeftScale = new Vector3 (1, alto, fondo);

		Vector3 Right = new Vector3 (ancho / 2, 0, 0);
		Quaternion RightRotation = Quaternion.Euler (new Vector3 (0, 0, 0));
		Vector3 RightScale = new Vector3 (1, alto, fondo);


		Vector3 Front = new Vector3 (0, 0, fondo / 2);
		Vector3 FrontScale = new Vector3 (1, alto, ancho);
		Quaternion FrontRotation = Quaternion.Euler (new Vector3 (0, 90, 0));

		Vector3 Back = new Vector3 (0, 0, -fondo / 2);
		Vector3 BackScale = new Vector3 (1, alto, ancho);
		Quaternion BackRotation = Quaternion.Euler (new Vector3 (0, 90, 0));


		Vector3 Bottom = new Vector3 (0, -alto / 2, 0);
		Vector3 BottomScale = new Vector3 (1, ancho, fondo);
		Quaternion BottomRotation = Quaternion.Euler (new Vector3 (0, 0, 90));

		Vector3 Top = new Vector3 (0, alto / 2, 0);
		Vector3 TopScale = new Vector3 (1, ancho, fondo);
		Quaternion TopRotation = Quaternion.Euler (new Vector3 (0, 00, 90));

		#endregion


		Container = new GameObject ();
		Container.name = "Container";
		Controller.GetComponent<Controller> ().Room = Container;


		#region GENERA

		GameObject LadrilloLeft = Instantiate (LadrilloObj, Left, LeftRotation) as GameObject;
		LadrilloLeft.name = "Left";
		LadrilloLeft.transform.localScale = LeftScale;
		LadrilloLeft.transform.SetParent (Container.transform);

//		Vector3 colSizeL = new Vector3 (10, 1, 10);
//		LadrilloLeft.GetComponent<BoxCollider> ().size = colSizeL;
		Vector3 colPosL = new Vector3 (-5, 0, 0);
		LadrilloLeft.GetComponent<BoxCollider> ().center = colPosL;

		GameObject LadrilloRight = Instantiate (LadrilloObj, Right, RightRotation) as GameObject;
		LadrilloRight.name = "Right";
		LadrilloRight.transform.localScale = RightScale;
		LadrilloRight.transform.SetParent (Container.transform);

//		Vector3 colSizeR = new Vector3 (10, 1, 10);
//		LadrilloRight.GetComponent<BoxCollider> ().size = colSizeR;
		Vector3 colPosR = new Vector3 (5, 0, 0);
		LadrilloRight.GetComponent<BoxCollider> ().center = colPosR;

		GameObject LadrilloTop = Instantiate (LadrilloObj, Top, TopRotation) as GameObject;
		LadrilloTop.name = "top";
		LadrilloTop.transform.localScale = TopScale;
		LadrilloTop.transform.SetParent (Container.transform);
		LadrilloTop.GetComponent<MeshRenderer> ().material = Cristal;

		Vector3 colSizeTop = new Vector3 (10, 1, 10);
		LadrilloTop.GetComponent<BoxCollider> ().size = colSizeTop;
		Vector3 colPosTop = new Vector3 (5, 0, 0);
		LadrilloTop.GetComponent<BoxCollider> ().center = colPosTop;


		GameObject LadrilloBottom = Instantiate (LadrilloObj, Bottom, BottomRotation) as GameObject;
		LadrilloBottom.name = "Bottom";
		LadrilloBottom.tag = "Suelo";
		LadrilloBottom.transform.localScale = BottomScale;
		LadrilloBottom.transform.SetParent (Container.transform);

//		Vector3 colSizeBottom = new Vector3 (10, 1, 10);
//		LadrilloBottom.GetComponent<BoxCollider> ().size = colSizeBottom;
		Vector3 colPosBottom = new Vector3 (-5, 0, 0);
		LadrilloBottom.GetComponent<BoxCollider> ().center = colPosBottom;


		GameObject LadrilloFront = Instantiate (LadrilloObj, Front, FrontRotation) as GameObject;
		LadrilloFront.name = "Front";
		LadrilloFront.transform.localScale = FrontScale;
		LadrilloFront.transform.SetParent (Container.transform);
		WallPos = new Vector3 (LadrilloFront.transform.position.x - (ancho / 3), LadrilloFront.transform.position.y - 1, LadrilloFront.transform.position.z - 4);

		Vector3 colSizeF = new Vector3 (10, 1, 10);
		LadrilloFront.GetComponent<BoxCollider> ().size = colSizeF;
		Vector3 colPosF = new Vector3 (-5, 0, 0);
		LadrilloFront.GetComponent<BoxCollider> ().center = colPosF;

		GameObject LadrilloBack = Instantiate (LadrilloObj, Back, BackRotation) as GameObject;
		LadrilloBack.name = "Back";
		LadrilloBack.tag = "BackBrick";
		LadrilloBack.transform.localScale = BackScale;
		LadrilloBack.transform.SetParent (Container.transform);
		LadrilloBack.GetComponent<MeshRenderer> ().material = Cristal;

		Vector3 colSizeB = new Vector3 (10, 1, 10);
		LadrilloBack.GetComponent<BoxCollider> ().size = colSizeB;
		Vector3 colPosB = new Vector3 (5, 0, 0);
		LadrilloBack.GetComponent<BoxCollider> ().center = colPosB;

		#endregion


		LockCam = new Vector3 (0, alturaCam, LadrilloBack.transform.position.z - distanciaZCam);
		LockPlayer = new Vector3 (0, Bottom.y + 3f, Back.z + 4f);
		LockBall = new Vector3 (LockPlayer.x, Bottom.y + 1, LockPlayer.z);

		Shadow = Ball.transform.GetChild (0).gameObject;
		Shadow.GetComponent<BallShadow> ().alturaSombra = Bottom.y;//+ 0.5f
//		Ball.GetComponent<BouncingAnim> ().alturaSueloAnim = Bottom.y;
		CameraMan.transform.position = LockCam;
		Vector3 bounds = new Vector3 (ancho / 2 - Player.transform.localScale.x - umbralRebotebounds, alto / 2, fondo / 2 - umbralRebotebounds); //ojo magico
		Player.transform.position = LockPlayer;
		Player.GetComponent<Move> ().SetBoundary (bounds);
		Player.GetComponent<Move> ().setAncho (ancho);

		Ball.GetComponent<MoveBall> ().SetBoundary (bounds);
		Ball.transform.position = LockBall;


		//BRICKS
		CreateWall (ancho, fondo);

		//PLANTAS
		Vector3 plantPos = new Vector3 (transform.position.x, transform.position.y - 5, transform.position.z); 
		GameObject plantas = Instantiate (Plantación, plantPos, Quaternion.identity)as GameObject;
		plantas.transform.SetParent (Container.transform);

		plantas.GetComponent<Plants> ().setCameras (Perspec, Ortho, Iso, Interface);
		plantas.GetComponent<Plants> ().SetWeather (fase);
		plantas.GetComponent<Plants> ().CreateSet (fase, room);

	}

	void CreateWall (int ancho, int fondo)
	{
		
		Wall = Instantiate (WallObj, transform.position, Quaternion.identity) as GameObject;
//		Wall = Instantiate (WallObj);
		Wall.transform.position = WallPos;
		Controller.GetComponent<Controller> ().FrontWall = Wall;
		Wall.name = "Wall";

//		Wall.GetComponent<FrontWall> ().CreateBrickWallZ (ancho / 2, alto / 2, fondo / 2);
//		Wall.GetComponent<FrontWall> ().CreateBrickWall (ancho / 2, alto / 2);
		Wall.GetComponent<FrontWall> ().CreateBrickWall (ancho - ancho / 3, fondo / 3);


	}

}
