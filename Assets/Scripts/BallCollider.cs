﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallCollider : MonoBehaviour
{
	public GameObject Controller;

	int tocaSuelo = 0;
	public GameObject nBotes;
	public GameObject PlantaParticles;

	public float pitchMod;
	public float clampBolaX;
	public Camera Perspec, Ortho, Iso, Interface;
	public bool isSecondBounce = true;
	GameObject audio;

	public void PlayAudio (float pitch)
	{
		Controller.GetComponent<Controller> ().PlayAudio (pitch);

	}

	public void setCameras (Camera Perspe, Camera Ortho, Camera Iso, Camera Interface)
	{
		this.Perspec = Perspe;
		this.Ortho = Ortho;
		this.Iso = Iso;
		this.Interface = Interface;
	}

	void OnTriggerEnter (Collider other)
	{
		
		if (other.tag == "Brick") {

			//			REBOTE A UNO
			Vector3 velo = new Vector3 (transform.parent.GetComponent<MoveBall> ().velocidad.x,//(Mathf.Clamp (bola.GetComponent<MoveBall> ().velocidad.x, -2, 2),
				               transform.parent.GetComponent<MoveBall> ().velocidad.y,
				               transform.parent.GetComponent<MoveBall> ().velocidad.z);

			if (other.GetComponent<Brick> ().tipo != "BigBall") {
				transform.parent.GetComponent<MoveBall> ().velocidad = new Vector3 (velo.x,
					velo.y,
					-velo.z);
			}
			transform.parent.GetComponentInChildren<BallCollider> ().isSecondBounce = true;

			//DOBLE
			if (other.GetComponent<Brick> ().tipo == "Doble") {
				other.GetComponent<Brick> ().RestaToque ();
				other.GetComponent<Brick> ().checkToques (other.transform.position, Controller);

				//MULTIBALL
			} else if (other.GetComponent<Brick> ().tipo == "Pelotas") {
				Controller.GetComponent<Controller> ().CreateMultiBalls (other.transform.position, transform.parent.GetComponent<MoveBall> ().boundary, transform.parent.GetComponentInChildren <BallShadow> ().alturaSombra);

				if (Iso.isActiveAndEnabled)
					Iso.GetComponent<Shake> ().ShakeThatCamera (0.05f);
				other.GetComponent<Brick> ().kill (other.transform.position, Controller);
			}
			//BIG BALL
			else if (other.GetComponent<Brick> ().tipo == "BigBall") {
				StartCoroutine ("BigBallTime");
				if (Iso.isActiveAndEnabled)
					Iso.GetComponent<Shake> ().ShakeThatCamera (0.05f);
				other.GetComponent<Brick> ().kill (other.transform.position, Controller);
			}

			//NORMAL
			else {
				if (Iso.isActiveAndEnabled)
					Iso.GetComponent<Shake> ().ShakeThatCamera (0.05f);
				other.GetComponent<Brick> ().kill (other.transform.position, Controller);
			}

			PlayAudio (transform.position.z / pitchMod);
//			print (transform.position.z / pitchMod);
		}

		if (other.tag == "Suelo") {
			tocaSuelo++;
		} 

		if (other.tag == "BackBrick") {
			tocaSuelo++;
			Controller.GetComponent<Controller> ().CheckEndGame (this.gameObject.transform.parent.gameObject);
//			Destroy (this.gameObject.transform.parent.gameObject);
			PlayAudio (transform.position.z / pitchMod);

		} 

		if (other.tag == "Muro") {
			isSecondBounce = true;
			if (Iso.isActiveAndEnabled)
//				Iso.GetComponent<Shake> ().ShakeThatCamera (0.05f);
			PlayAudio (transform.position.z / pitchMod);

		} 


//		if (other.tag == "Ball") {
//			Vector3 velo = new Vector3 (transform.parent.GetComponent<MoveBall> ().velocidad.x,//(Mathf.Clamp (bola.GetComponent<MoveBall> ().velocidad.x, -2, 2),
//				               transform.parent.GetComponent<MoveBall> ().velocidad.y,
//				               transform.parent.GetComponent<MoveBall> ().velocidad.z);
//
//			transform.parent.GetComponent<MoveBall> ().velocidad = new Vector3 (velo.x,
//				velo.y,
//				-velo.z);
//
//			transform.parent.GetComponentInChildren<BallCollider> ().isSecondBounce = true;
//			PlayAudio (transform.position.z / pitchMod);
//
//		} 

	}


	IEnumerator BigBallTime ()
	{
		 
		float t = 8f; //Pasarlo desde control
		Vector3 originalScale = this.gameObject.transform.parent.transform.localScale;
		Vector3 bigball = new Vector3 (4, 4, 4);


		while (t > 0) {
			this.gameObject.transform.parent.transform.localScale = bigball;
//			print ("dentro " + t);
			t -= 1 * Time.deltaTime;
			yield return new WaitForSeconds (0);
		}

		this.gameObject.transform.parent.transform.localScale = originalScale;

	}


	IEnumerator HitColor (GameObject other)
	{
		int t = 1;
		GameObject g = other;
		while (t > 0) {
			t -= 1;
			g.GetComponent<Renderer> ().material.color = gameObject.transform.parent.gameObject.GetComponent<Renderer> ().material.color;
			yield return new WaitForSeconds (0.2f);
		}
		g.GetComponent<Renderer> ().material.color = new Color (0.359f, 0.109f, 1.0f, 1.0f);
		yield return new WaitForSeconds (0.2f);
	
	}

}
