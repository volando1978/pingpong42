﻿using UnityEngine;
using System.Collections;

public class MoveBall : MonoBehaviour
{

	public Vector3 velocidad;
	public Vector3 boundary;

	public bool isReady = false;
	public float tiempoAutoStart;



	// Use this for initialization
	void Start ()
	{
		if (Controller.autoplay) {
			StartCoroutine ("AutoGetBallReady");
		}
	}

	IEnumerator AutoGetBallReady ()
	{
		float tiempo = tiempoAutoStart;
		while (tiempo > 0) {
			tiempo -= 1 * Time.deltaTime;

			yield return new WaitForSeconds (0);
		}

		isReady = true;
	}

	public void SetBoundary (Vector3 boundary)
	{
		this.boundary = boundary;
	}

	// Update is called once per frame
	void Update ()
	{
		if (isReady) {
			Vector3 m = transform.position;
			Vector3 movement = new Vector3 ((m.x += Mathf.Clamp (velocidad.x, -0.3f, 0.3f)),
				                   0 
			, (m.z += Mathf.Clamp (velocidad.z, -1, 1)));

			transform.position = movement;

			// boundaries
			if (transform.position.x < -boundary.x) {
				velocidad = new Vector3 (velocidad.x * -1, velocidad.y, velocidad.z);     
			} 
			if (transform.position.x > boundary.x) {
				velocidad = new Vector3 (velocidad.x * -1, velocidad.y, velocidad.z);     
			}

			// boundaries
			if (transform.position.y < -boundary.y) {
				velocidad = new Vector3 (velocidad.x, velocidad.y * -1, velocidad.z);     
			} 
			if (transform.position.y > boundary.y) {
				velocidad = new Vector3 (velocidad.x, velocidad.y * -1, velocidad.z);     
			}

			// boundaries
			if (transform.position.z < -boundary.z) {
				velocidad = new Vector3 (velocidad.x, velocidad.y, velocidad.z * -1);
			} 
			if (transform.position.z > boundary.z) {
				velocidad = new Vector3 (velocidad.x + Random.Range (-2, 2), velocidad.y, velocidad.z * -1);     
			}
		}
	}
}