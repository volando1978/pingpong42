﻿using UnityEngine;
using System.Collections;

public class BouncingAnim : MonoBehaviour
{

	public float stretch;
	public float maxStretch;
	public float alturaSueloAnim;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
//		float dist = Mathf.Pow (transform.position.y - transform.parent.transform.position.y, 2) / 2;
		SetBounce (alturaSueloAnim - transform.position.y);
	}

	public void SetBounce (float dist)
	{
		float value = 1 - Mathf.Abs (dist) * stretch;
		if (value < maxStretch)
			value = maxStretch;

		Vector3 sc = new Vector3 (transform.localScale.x, value / 2, transform.localScale.z);
		transform.localScale = sc;

//		Color32 c = new Color (
//			            GetComponent<Renderer> ().material.color.r,
//			            GetComponent<Renderer> ().material.color.g,
//			            GetComponent<Renderer> ().material.color.b,
//			            value);
//		GetComponent<Renderer> ().material.color = c;
	}
}
