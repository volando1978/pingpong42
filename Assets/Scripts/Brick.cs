﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour
{

	public string tipo;
	public int nToques = 1;
	public bool isReadyToDie = true;
	public Material dobleMaterial, dobleMaterialTocado;

	public GameObject Blanco;
	public GameObject Controller;
	int tocaBrick = 0;

	public void Start ()
	{
		if (tipo == "Doble") {
			GetComponent<Renderer> ().material = dobleMaterial;
			nToques = 2;
		} else if (tipo == "Pelotas") {
			GetComponent<Renderer> ().material = dobleMaterial;
			GetComponent<Renderer> ().material.color = Color.cyan;
		} else if (tipo == "BigBall") {
			GetComponent<Renderer> ().material = dobleMaterial;
			GetComponent<Renderer> ().material.color = Color.green;
		}


	}

	//NORMAL

	public void kill (Vector3 pos, GameObject Controller)
	{
		Instantiate (Blanco, pos, Quaternion.identity);
		Controller.GetComponent<Controller> ().RemoveBrick (this.gameObject);
		tocaBrick++;
		Controller.GetComponent<Controller> ().NBricks += 1;
		Controller.GetComponent<Controller> ().UpdateHUD ();

		Destroy (this.gameObject);
	}

	//DOBLE
	public void RestaToque ()
	{
		nToques -= 1;
		GetComponent<Renderer> ().material = dobleMaterialTocado;

	}

	public void checkToques (Vector3 pos, GameObject Controller)
	{
		if (nToques < 2)
			isReadyToDie = true;
			
		if (nToques < 1)
			kill (pos, Controller);
	}



}
