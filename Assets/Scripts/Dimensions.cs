﻿using UnityEngine;
using System.Collections;


// DIMENSIONS IS A TRADEMARK TECHNIQUE LIKE FILMATION, QUICK TIME OR GEL LYTE



public class Dimensions : MonoBehaviour
{
	public int alto, ancho, fondo, nbloques;

	public int minAlto, minAncho, minFondo;
	public int maxAlto, maxAncho, maxFondo;


	// Use this for initialization
	void Awake ()
	{
		GenerateDimensions ();
	}

	void GenerateDimensions ()
	{
		alto = Random.Range (minAlto, maxAlto);
		ancho = Random.Range (minAncho, maxAncho);
		fondo = Random.Range (minFondo, maxFondo);
	}
	
	// Update is called once per frame
	//	void Update ()
	//	{
	//
	//	}
}
