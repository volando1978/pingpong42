﻿using UnityEngine;
using System.Collections;

public class Plants : MonoBehaviour
{

	public Material[] materiales;
	public GameObject[] plantas;

	public int[] indicesEntrada;
	public int[] indicesSalida;

	public int[] indicesMateriales;

	public Camera Perspec, Ortho, Iso, Interface;
	public Color[] colors;
	public Tiempo currentTiempo;

	public GameObject LluviaObj, NieveObj, NieblaObj, ReflejosMarObj, TruenoObj, LineasFlotacionObj;
	GameObject lluvia, nieve, niebla, reflejosMar, trueno, lineasFlotacion;

	public void setCameras (Camera Perspe, Camera Ortho, Camera Iso, Camera Interface)
	{
		this.Perspec = Perspe;
		this.Ortho = Ortho;
		this.Iso = Iso;
		this.Interface = Interface;
	}

	public enum Tiempo
	{

		BOSQUE = 0,
		DESIERTO = 1,
		CEMENTERIO = 2,
		LLUVIA = 3,
		MAR = 4,
		MONTANA = 5,
		NIEVE = 6,
		MARTE = 7,

	}

	public void SetWeather (int fase)
	{
		GenerateLandscape (fase);

	}

	//crea plantas
	public void CreateSet (int fase, int room)
	{


		for (int x = 0 - (int)GetComponent<Renderer> ().bounds.size.x / 2; x < GetComponent<Renderer> ().bounds.size.x / 2; x += 4) {
			for (int z = 0 - (int)GetComponent<Renderer> ().bounds.size.z / 2; z < GetComponent<Renderer> ().bounds.size.z / 2; z += 4) {
				int r = Random.Range (0, 2);
				if (r > 0) {
					Vector3 pos = new Vector3 (x + Random.Range (-1, 1), transform.position.y + 1, z + Random.Range (-1, 1));
//				Vector3 pos = new Vector3 (x, transform.position.y - 5f, z);
					Quaternion rot = Quaternion.Euler (new Vector3 (0, Random.Range (0, 359), 0));
					GameObject p = CreatePlantsItems (pos, rot, fase);
					p.GetComponent<Renderer> ().material = GetMaterial (fase);
					p.transform.parent = transform;
					p.tag = "Plantas";
//					p.AddComponent<BoxCollider> ();

					RaycastHit hit;

					if (Physics.Raycast (p.transform.position, Vector3.down, out hit)) {
						Debug.DrawRay (p.transform.position, Vector3.down, Color.white);

						if (hit.collider.gameObject.tag == "Suelo" ||
						    hit.collider.gameObject.tag == "Brick" ||
						    hit.collider.gameObject.tag == "Player" ||
						    hit.collider.gameObject.tag == "BackBrick") {
							Destroy (p);
						}
					}
				}
			}
		}
	}

	GameObject CreatePlantsItems (Vector3 pos, Quaternion rot, int fase)
	{

		if (currentTiempo == Tiempo.LLUVIA) {
			GameObject p = Instantiate (plantas [Random.Range (0, 4)], pos, rot) as GameObject;
			return p;
		}

		if (currentTiempo == Tiempo.BOSQUE) {
			GameObject p = Instantiate (plantas [Random.Range (0, 4)], pos, rot) as GameObject;
			return p;
		}

		if (currentTiempo == Tiempo.NIEVE) {
			GameObject p = Instantiate (plantas [Random.Range (0, materiales.Length)], pos, rot) as GameObject;
			return p;
		}

		if (currentTiempo == Tiempo.MAR) {
			GameObject p = Instantiate (plantas [8], pos, rot) as GameObject;
			return p;
		} 

		if (currentTiempo == Tiempo.MONTANA) {
			GameObject p = Instantiate (plantas [Random.Range (2, 4)], pos, rot) as GameObject;
			return p;
		} 

		if (currentTiempo == Tiempo.CEMENTERIO) {
			GameObject p = Instantiate (plantas [Random.Range (0, materiales.Length)], pos, rot) as GameObject;
			return p;
		}

		if (currentTiempo == Tiempo.DESIERTO) {
			GameObject p = Instantiate (plantas [Random.Range (8, 11)], pos, rot) as GameObject;
			return p;
		} 

		if (currentTiempo == Tiempo.MARTE) {
			GameObject p = Instantiate (plantas [Random.Range (4, 5)], pos, rot) as GameObject;
			return p;
		} else {
					
			GameObject p = Instantiate (plantas [Random.Range (indicesEntrada [fase % indicesEntrada.Length], indicesSalida [fase % indicesSalida.Length])], pos, rot) as GameObject;
			return p;

		}
	}

	Material GetMaterial (int fase)
	{

		Material mat = materiales [Random.Range (0, materiales.Length)];

		if (currentTiempo == Tiempo.LLUVIA) {
			mat = materiales [Random.Range (0, fase % materiales.Length)];
			Iso.backgroundColor = colors [6];
			GetComponent<Renderer> ().material.color = colors [6];
					
		}

		if (currentTiempo == Tiempo.NIEVE) {
			mat =	materiales [0];
			Iso.backgroundColor = colors [5];
			GetComponent<Renderer> ().material.color = colors [5];

		}

		if (currentTiempo == Tiempo.MAR) {
			mat =	materiales [0];
						
			int r = Random.Range (-1, 1);
			CreateReflejosMar ();
			if (r > -1) {
				CreateNiebla ();
			}
			Iso.backgroundColor = colors [4];
			GetComponent<Renderer> ().material.color = colors [4];

		}

		if (currentTiempo == Tiempo.MONTANA) {
			mat = materiales [6];
			Iso.backgroundColor = colors [6];
			GetComponent<Renderer> ().material.color = colors [6];

		}

		if (currentTiempo == Tiempo.CEMENTERIO) {
			mat = materiales [8];
			Iso.backgroundColor = colors [0];
			GetComponent<Renderer> ().material.color = colors [0];

			CreateLluvia ();
			CreateTrueno ();
		}

		if (currentTiempo == Tiempo.DESIERTO) {
			mat = materiales [7];

			Iso.backgroundColor = colors [7];
			GetComponent<Renderer> ().material.color = colors [7];

		}

		if (currentTiempo == Tiempo.MARTE) {

			mat = materiales [Random.Range (0, fase % materiales.Length)];

			Iso.backgroundColor = colors [8];
			GetComponent<Renderer> ().material.color = colors [8];

		}

		if (currentTiempo == Tiempo.BOSQUE) {
			mat =	materiales [0];
			Iso.backgroundColor = colors [1];
			GetComponent<Renderer> ().material.color = colors [1];

		}
		return mat;
	}


	void DestroyWeatherElements ()
	{
		Destroy (GameObject.FindGameObjectWithTag ("niebla"));
		Destroy (GameObject.FindGameObjectWithTag ("lluvia"));
//		Destroy (GameObject.FindGameObjectWithTag ("nieve"));
		Destroy (GameObject.FindGameObjectWithTag ("reflejosMar"));
		Destroy (GameObject.FindGameObjectWithTag ("trueno"));
		Destroy (GameObject.FindGameObjectWithTag ("lineasFlotacion"));

		if (nieve != null)
			Destroy (nieve);
	}

	void CreateLineasFlotacion ()
	{
		if (lineasFlotacion == null) {
			lineasFlotacion = Instantiate (LineasFlotacionObj) as GameObject;
			lineasFlotacion.tag = "lineasFlotacion";
		}
	}

	void CreateNiebla ()
	{
		if (niebla == null) {
			niebla = Instantiate (NieblaObj) as GameObject;
			niebla.tag = "niebla";
		}
	}

	void CreateNieve ()
	{
		if (nieve == null) {
			nieve = Instantiate (NieveObj) as GameObject;
		}
	}

	void CreateLluvia ()
	{
		if (lluvia == null) {
			lluvia = Instantiate (LluviaObj) as GameObject;
			lluvia.tag = "lluvia";
		}
	}

	void CreateReflejosMar ()
	{
		if (reflejosMar == null) {
			reflejosMar = Instantiate (ReflejosMarObj) as GameObject;
			reflejosMar.tag = "reflejosMar";
		}
	}

	void CreateTrueno ()
	{
		if (trueno == null) {
			trueno = Instantiate (TruenoObj) as GameObject;
			trueno.tag = "trueno";
		}
	}

	public void GenerateLandscape (int fase)
	{
		Iso.backgroundColor = colors [fase % colors.Length];
		
			
		DestroyWeatherElements ();

		currentTiempo = (Tiempo)(fase % 8);//(Controller.NTotalRooms % 7);

		print ("Tiempo: " + currentTiempo);
		#region AGENTES METEREOLOGICOS
		GameObject[] go = GameObject.FindGameObjectsWithTag ("Weather");

		foreach (GameObject g in go) {
			Destroy (g);
		}
		if (currentTiempo == Tiempo.LLUVIA) {
			CreateLluvia ();
		}

		if (currentTiempo == Tiempo.BOSQUE) {
			CreateNiebla ();

			Iso.backgroundColor = colors [4];

		}

		if (currentTiempo == Tiempo.NIEVE) {
			int r = Random.Range (-1, 1);
			CreateNiebla ();
			if (r > -1) {
				CreateNieve ();
			}
			Iso.backgroundColor = colors [5];

		}

		if (currentTiempo == Tiempo.MAR) {

			CreateLineasFlotacion ();
			int r = Random.Range (-1, 1);
			if (r > -1) {
				CreateNiebla ();
//				CreateLluvia ();
			}
			Iso.backgroundColor = colors [4];

		}

		if (currentTiempo == Tiempo.CEMENTERIO) {
			Iso.backgroundColor = colors [4];
			CreateLluvia ();

		}

		if (currentTiempo == Tiempo.DESIERTO) {
			Iso.backgroundColor = colors [4];

		}

		if (currentTiempo == Tiempo.MARTE) {
			Iso.backgroundColor = colors [4];

		}

		#endregion
	}

	public void CreateMap (int fase, int room)
	{

		for (int f = 0; f < fase; f++) {
			for (int x = 0; x < transform.localScale.x; x += 10) {
				for (int z = 0; z < 100; z += 10) {

					int r = Random.Range (0, 2);
					if (r > 0) {
						Vector3 pos = new Vector3 (x + Random.Range (-1, 1), transform.position.y + 1, z + Random.Range (-1, 1));
						Quaternion rot = Quaternion.Euler (new Vector3 (0, Random.Range (0, 359), 0));
						GameObject p = CreatePlantsItems (pos, rot, f);
						p.GetComponent<Renderer> ().material = GetMaterial (fase);
//						p.transform.parent = transform;
						p.tag = "Plantas"; 
					}
				}
			}
		}
	}

	public void BackToMenu ()
	{
		Application.LoadLevel ("tennisCubo");
	}
}

