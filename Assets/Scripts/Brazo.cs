﻿using UnityEngine;
using System.Collections;

public class Brazo : MonoBehaviour
{

	public float acceleration;

	public Transform brazo;

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey (KeyCode.D)) {
			Vector3 movX = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
			movX.x -= acceleration * Time.deltaTime;
//			brazo.transform.Rotate (new Vector3 (movX.x, 0, 0));
			brazo.transform.Translate (new Vector3 (movX.x, 0, 0));


		}
		if (Input.GetKey (KeyCode.A)) {
			Vector3 movX = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
			movX.x += acceleration * Time.deltaTime;
//			brazo.transform.Rotate (new Vector3 (movX.x, 0, 0));
			brazo.transform.Translate (new Vector3 (movX.x, 0, 0));

		}
		if (Input.GetKey (KeyCode.W)) {
			Vector3 movY = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
			movY.z += acceleration * Time.deltaTime;
//			brazo.transform.Rotate (new Vector3 (0, movY.z, 0));
			brazo.transform.Translate (new Vector3 (0, -movY.z, 0));


		}
		if (Input.GetKey (KeyCode.S)) {
			Vector3 movY = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
			movY.z += acceleration * Time.deltaTime;
//			brazo.transform.Rotate (new Vector3 (0, movY.z, 0));
			brazo.transform.Translate (new Vector3 (0, movY.z, 0));

		}

	}
}
