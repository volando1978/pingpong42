﻿using UnityEngine;
using System.Collections;

public class Thunder : MonoBehaviour
{


	float minTime = .9f;
	float threshold = .5f;
	public Light light;

	float lastTime = 0;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if ((Time.time - lastTime) > minTime) {
			if (Random.value > threshold) {
				light.enabled = true;
			} else {
				light.enabled = false;
				lastTime = Time.time;
			}
		}

	}
}
