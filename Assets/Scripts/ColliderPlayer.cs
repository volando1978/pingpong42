﻿using UnityEngine;
using System.Collections;

public class ColliderPlayer : MonoBehaviour
{


	public float AVG_x, AVG_y, AVG_z, AVG_Multiplier;
	public float fuerza;
	public Vector3 sc;

	public Camera Perspec, Ortho, Iso, Interface;

	public GameObject Controller;


	public void setCameras (Camera Perspe, Camera Ortho, Camera Iso, Camera Interface)
	{
		this.Perspec = Perspe;
		this.Ortho = Ortho;
		this.Iso = Iso;
		this.Interface = Interface;
	}

	// Use this for initialization
	void Start ()
	{
		sc = transform.parent.transform.localScale;
	}

	public void PlayAudio (float pitch)
	{
		Controller.GetComponent<Controller> ().PlayAudio (pitch);

	}

	void OnCollisionEnter (Collision col)
	{
		if (col.collider.tag == "Ball" && col.collider.gameObject.GetComponentInChildren<BallCollider> ().isSecondBounce) {

			Vector3 contactPoint = Vector3.zero;
			foreach (ContactPoint contact in col.contacts) {

				Debug.DrawRay (contact.point, contact.normal, Color.white);
				contactPoint = contact.point;
				Vector3 objectSize = Vector3.Scale (transform.localScale, GetComponent<BoxCollider> ().bounds.size);

				GameObject bola = col.collider.gameObject;
				Vector3 velo = new Vector3 (bola.GetComponent<MoveBall> ().velocidad.x,//(Mathf.Clamp (bola.GetComponent<MoveBall> ().velocidad.x, -2, 2),
					               bola.GetComponent<MoveBall> ().velocidad.y,
					               bola.GetComponent<MoveBall> ().velocidad.z);

				bola.GetComponent<MoveBall> ().velocidad = new Vector3 (Mathf.Clamp (Lado (contactPoint, objectSize).x, -20, 20),
					velo.y,
					-velo.z);

//				print ("velo " + bola.GetComponent<MoveBall> ().velocidad);
				bola.GetComponentInChildren<BallCollider> ().isSecondBounce = false;

				if (Iso.isActiveAndEnabled)
					Iso.GetComponent<Shake> ().ShakeThatCamera (0.05f);
			}
//			PlayAudio (transform.position.x / 100);

		}
	}

	Vector3 Lado (Vector3 cPoint, Vector3 cScale)
	{
		return transform.InverseTransformPoint (cPoint);
	}

	IEnumerator flan ()
	{

		float t = .1f;
		Vector3 sc = transform.parent.transform.localScale;

		while (t >= 0) {

			t -= .1f * Time.deltaTime;
			Vector3 targetScale = transform.parent.transform.localScale / 2;

			float scy = sc.y;
			scy += (targetScale.y - scy) * .1f;

			Vector3 lsc = new Vector3 (sc.x, targetScale.y, sc.z);

			transform.parent.transform.localScale = lsc;

			yield return new WaitForSeconds (0);

		}

		transform.parent.transform.localScale = sc;

	}



}


//			GameObject ball = other.gameObject;
//
//			// The ball holds the ever-increasing speed of my ball, which is to
//			// be updated for each pad-bounce.
////			Ball ballScript = col.gameObject.GetComponent<BallScript> ();
//
//			// The formula for a standard wall-bounce is:
//			// -2*(V dot N)*N + V
//			// where V is the incoming velocity of the ball, and N is the normal-vector
//			// for the wall/pad i.e. upwards/outwards from the pad, when the long side
//			// of it is left-to-right, and top and bottom are up and down respectively.
//			// Because my pad-sprite is like this <======>, I use _transform.up for N.
//			// For other implementations using pad-sprites that are turned differently,
//			// _transform.right or an inversion of either might be used.
//			// First, I find my velocity:
//			Vector3 v = ball.GetComponent<Rigidbody> ().velocity;
////			print ("velocity: " + v);
//
//			// I normalize v, because I want to make a change in speed later (below).
//			// Not normalizing can also introduce weird behavior in the functions, so to
//			// be safe, you can save your v.magnitude, which is the length/speed of the
//			// vector, to a variable, and apply it afterwards.
//			// More on that later.
//
//			v.Normalize ();
////			print ("velocity Norm: " + v);
//
//
//			// We need the dot-product of v and n for the function.
//			float dotOfvn = Vector3.Dot (v, transform.forward);
//
////			print ("dot " + dotOfvn);
//
//			// I make a clean 0,0 vector2, to store my resulting bounce-vector in.
//			// I want to make it so the players can choose which type of bounce
//			// they want, and even mix them, so I've split up the two ("Point 1"
//			// and "Point 2"), so I can add their effects separately.
//			Vector3 R = new Vector3 ();
//
//			// I add the effect of the wall-bounce to my R.
//			// If you ONLY want a wall-bounce, this is the function you want, and in
//			// that case,you can skip the normalization of v above, so you don't need
//			// to manually apply v.magnitude afterwards. But if you want to continue,
//			// I advise you to follow the code as written.
//			R += -2 * dotOfvn * transform.forward + v;
//
//			// Now R represents the precise bounce-vector, that the ball would have
//			// if it had struck a flat wall.
//
//			// Now, on to the Pong-style bounce!
//			// We want to also be able to take into account the Pong/Arkanoid-style of
//			// bouncing from the pad, where the ball shoots in a different direction,
//			// depending on where it hits the pad. The further from the middle, the
//			// sharper the exit-angle.
//
//			// That means we have to be able to interpolate between the normal-vector
//			// for the pad (upwards), and the vector going along the pad (sideways).
//			// The function for getting an interpolated vector between two 2D vectors
//			// is: A*t + (1-t)*B
//			// where t is the unit-based "percentage" you want to be close to A
//			// compared to how close you want to be to B.
//			// That means I get a float of 0.01 for 1%, 1 for 100% and -1 for -100%.
//
//			// To be able to use this, we need to know how far from the middle the ball
//			// struck. We want to find the length from where the ball hit, to the middle
//			// of the pad, but not directly from the position of the ball, but from
//			// where it hit, and it has to be projected to a point on the pad's sideways
//			// normal-vector, to be precise. See the last image of this post for
//			// clarification.
//
//			// We know the ball has hit the pad, so we raycast to see where it hits.
//			// You have to make sure that your raycast can only hit the pad. Do some
//			// layering or something, so it doesn't hit anything else first.
//			RaycastHit hit;
//			if (Physics.Raycast (new Vector3 (ball.transform.position.x,
//				    ball.transform.position.y, ball.transform.position.z), ball.GetComponent<Rigidbody> ().velocity, out hit))
//				;
//
//			// We find the vector from the hit-point to the center of the pad.
//			// The pivot of my pad sprite is in the center of it, so its position
//			// is also its center. If your pivot is at the bottom of the pad, this
//			// should also work fine.
//			Vector3 vectorFromHitPointToPadCenter =
//				hit.point - transform.position;
//
////			print ("hit point : " + vectorFromHitPointToPadCenter);
//
//
//			// Then we can use the Dot-procuct of that vector and our pad's sideways-
//			// vector, to project that vector onto the sideways-vector of the pad, which
//			// gives us the length from the center of the pad, to the red X (see the
//			// last image in this post). If the hitPoint is on the left side of the pad,
//			// this length will be negative, because I use _transform.right as the
//			// sideways-vector.
//			float length = Vector3.Dot (vectorFromHitPointToPadCenter, transform.right);
//
//			// Now, a unit-percentage float, where 1.0f is 100% and -1.0f is -100%.
//			// This gives us a factor of how far from the pad-center the ball has hit.
//			// If it is negative, then it has hit the left side of the pad.
//			// E.g. if it is -1.0f, the ball has hit the far left extremity of the pad.
//			// You'll have to figure out your "halfCurrentPadWidth" variable yourself.
//			// It should be half the width of the pad in units.
//			var percentageOfLengthVSHalfPadLength = length / palaSize / 2;
//
//			// We apply A*t + (1-t)*B
//			// Again, I add this to my existing vector. If you ONLY want this standard
//			// Pong-bounce, just use this, and forget the wall-bounce math above, and
//			// change the += to just =.
//			R += transform.right * percentageOfLengthVSHalfPadLength +
//			(1 - percentageOfLengthVSHalfPadLength) * transform.forward;
//			// Gotta normalize.
//			R.Normalize ();
//
//			// Apply speed to the ball. If you just want the ball to continue at its
//			// present speed, you can replace ballScript.speed with
//			// ball.rigidbody2D.velocity.magnitude.
//			ball.GetComponent<Rigidbody> ().velocity = Vector3.Scale (R, forz + new Vector3 (AVG_Multiplier, AVG_Multiplier, AVG_Multiplier));// * AVG_Multiplier;
////			print ("velocity final : " + ball.GetComponent<Rigidbody> ().velocity);

//			return;