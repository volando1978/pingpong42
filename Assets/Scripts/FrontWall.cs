﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FrontWall : MonoBehaviour
{


	//	public int xWall, yWall, zWall;
	public GameObject BrickObj;
	public List<GameObject> Bricks;
	public float timeCreating;

	void CreateIt (int xWall, int yWall)
	{

		Bricks = new List<GameObject> ();
		gameObject.name = "WallContainer";

		for (int i = 0; i < xWall; i += 2) {
			for (int j = 0; j < yWall; j += 2) {
				Vector3 pos = new Vector3 (transform.position.x + i, transform.position.y, transform.position.z - j);
				GameObject brick = Instantiate (BrickObj, pos, Quaternion.identity)as GameObject;
				brick.name = "brick " + i.ToString () + " " + j.ToString ();
				brick.transform.SetParent (gameObject.transform);
				Color32 c = new Color (i * .1f, i * .1f, j * .1f);
				brick.GetComponent<Renderer> ().material.color = c;


				int rx = Random.Range (0, xWall);
				int ry = Random.Range (0, yWall);

				if (j == rx) {
					brick.GetComponent<Brick> ().tipo = "Pelotas";

				} else if (i == ry) {
					brick.GetComponent<Brick> ().tipo = "BigBall";

				} else {  
					brick.GetComponent<Brick> ().tipo = "Normal";
				}
				
				Bricks.Add (brick);
			}
		}

	}

	public void CreateBrickWall (int xWall, int yWall)
	{
//		StartCoroutine (CreateIt (xWall, yWall));
		CreateIt (xWall, yWall);
	}



	public void RemoveBrick (GameObject brick)
	{
		Bricks.Remove (brick);

	}



}
//	public void CreateBrickWallZ (int xWall, int yWall, int zWall)
//	{
//		StartCoroutine (CreateItZ (xWall, yWall, zWall));
//
//	}
//
//
//	IEnumerator CreateItZ (int xWall, int yWall, int zWall)
//	{
//		Bricks = new List<GameObject> ();
//		gameObject.name = "WallContainer";
//
//		for (int i = 0; i < xWall; i += 2) {
//			for (int j = 0; j < yWall; j += 2) {
//				for (int k = 0; k < zWall; k += 2) {
//
//					Vector3 pos = new Vector3 (transform.position.x + i, transform.position.y + j, transform.position.z - k);
//					GameObject brick = Instantiate (BrickObj, pos, Quaternion.identity)as GameObject;
//					brick.name = "brick " + i.ToString () + " " + j.ToString ();
//					brick.transform.SetParent (gameObject.transform);
//					Color32 c = new Color (1, j * .1f, k * .1f);
//					brick.GetComponent<Renderer> ().material.color = c;
//					Bricks.Add (brick);
//
//					yield return new WaitForSeconds (timeCreating * Time.deltaTime);
//
//				}
//			}
//		}
//
////		for (int i = 0; i < xWall + yWall - 2; i += 2) {
////			for (int j = Mathf.Min(xWall,i+2)-2; j < Mathf.m; j += 2) {
////				for (int k = 0; k < zWall; k += 2) {
////
////					Vector3 pos = new Vector3 (transform.position.x + i, transform.position.y + j, transform.position.z - k);
////					GameObject brick = Instantiate (BrickObj, pos, Quaternion.identity)as GameObject;
////					brick.name = "brick " + i.ToString () + " " + j.ToString ();
////					brick.transform.SetParent (gameObject.transform);
////					Color32 c = new Color (1, j * .1f, k * .1f);
////					brick.GetComponent<Renderer> ().material.color = c;
////					Bricks.Add (brick);
////
////					yield return new WaitForSeconds (timeCreating * Time.deltaTime);
////
////				}
////			}
////		}
//
//
//	}








//		int gridSize = xWall; // Size of the array
//		int animationStep = 0; // Animation progress variable
//
//		int cellCount, x, y;
//
//		for (int j = 0; j < gridSize * 2; j++) {
//			if (animationStep < gridSize) {
//				cellCount = animationStep + 1;
//
//				x = animationStep;
//				y = 0;
//			} else {
//				cellCount = gridSize * 2 - 1 - animationStep;
//
//				x = gridSize - 1;
//				y = animationStep - gridSize + 1;
//			}
//
//			for (int i = 0; i < cellCount; i += 1) {
//
//				int xB = x - i;
//				int yB = y + i;
//				Vector3 pos = new Vector3 (transform.position.x + (xB * 2.5f), transform.position.y, transform.position.z - yB);
//				GameObject brick = Instantiate (BrickObj, pos, Quaternion.identity)as GameObject;
//				brick.name = "brick " + i.ToString () + " " + j.ToString ();
//				brick.transform.SetParent (gameObject.transform);
//
//				if (i == 4)
//					brick.GetComponent<Brick> ().tipo = "Doble";
//				else
//					brick.GetComponent<Brick> ().tipo = "Normal";
//
//
//				Bricks.Add (brick);
//			}
//
//			animationStep++;
//			yield return new WaitForSeconds (timeCreating * Time.deltaTime);
//
//		}
//	}