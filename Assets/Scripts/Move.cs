﻿using UnityEngine;
using System.Collections;
using CnControls;
using UnityEngine.UI;

public class Move : MonoBehaviour
{

	public float acceleration;
	public float currentForzH;
	public float maxCurrentForzH;
	public float currentForzZ;
	public float maxCurrentForzZ;
	public float accelerationCube;
	public float rapidez;
	public float traslationH;

	public GameObject TargetBall;
	public Vector3 boundary;
	public GameObject GotoButton;
	public bool isColliding;
	public Camera Perspec, Ortho, Iso, Interface;

	public int ancho;

	public bool movingRight, movingLeft;
	public bool autoReact = false;




	void Start ()
	{
		accelerationCube = acceleration * rapidez;
	}

	public void setAncho (int ancho)
	{
		this.ancho = ancho;
		print ("Screen " + Screen.width + " ancho " + this.ancho);
	}

	public void SetBoundary (Vector3 boundary)
	{
		this.boundary = boundary;
	}

	public void SetBall (GameObject ball)
	{
		TargetBall = ball;
	}

	void Update ()
	{
		move ();
	}

	public void move ()
	{

		if (Controller.autoplay)
			MoveAuto ();

		//Check if we are running on iOS, Android, Windows Phone 8 or Unity iPhone
		#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE

		if (Input.GetMouseButton (0)) {
			RaycastHit hit;

			Vector3 aver = Interface.ScreenToWorldPoint (new Vector3 (ancho, 0, 0));

			if (Physics.Raycast (Interface.ScreenPointToRay (Input.mousePosition), out hit, 100)) {
				float height = 2f * Interface.orthographicSize;
				float width = height * Interface.aspect;

				float xpoint = hit.point.x * ancho / (width);

			}
		}
		#endif 

		if (movingLeft)
			traslationH -= acceleration;
		
		if (movingRight)
			traslationH += acceleration;

		traslationH *= Time.deltaTime;

		transform.position = new Vector3 (transform.position.x + traslationH, transform.position.y, transform.position.z);

//		 boundaries
		if (transform.position.x < -boundary.x) {
			transform.position = new Vector3 (-boundary.x, transform.position.y, transform.position.z);
		} 
		if (transform.position.x > boundary.x) {
			transform.position = new Vector3 (boundary.x, transform.position.y, transform.position.z);     
		}

		// boundaries
		if (transform.position.z < -boundary.z) {
			transform.position = new Vector3 (transform.position.x, transform.position.y, -boundary.z);
		} 
		if (transform.position.z > boundary.z) {
			transform.position = new Vector3 (transform.position.x, transform.position.y, boundary.z / 2);     
		}

	}

	public void MoveAuto ()
	{
//		float distance = TargetBall.transform.position.x - transform.position.x;
//		print ("Distance " + Mathf.Abs (distance));
//
//		if (distance > Mathf.Abs (1)) {
		if (transform.position.x > TargetBall.transform.position.x) {
			movingLeft = true;
			movingRight = false;

		} else {

			movingRight = true;
			movingLeft = false;
		}
//		}

	}

	public void Left ()
	{
		movingLeft = true;

	}

	public void Right ()
	{

		movingRight = true;
	}

	public void resetMove ()
	{
		traslationH = 0;
		movingRight = false;
		movingLeft = false;
	}


	public void setCameras (Camera Perspe, Camera Ortho, Camera Iso, Camera Interface)
	{
		this.Perspec = Perspe;
		this.Ortho = Ortho;
		this.Iso = Iso;
		this.Interface = Interface;
	}

}
