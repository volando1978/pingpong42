﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class MapController : MonoBehaviour
{

	public int x, y, z;
	public GameObject RoomObj;
	public List<GameObject> Rooms;
	Dimensions dim;

	private RaycastHit hit;
	public float dragSpeed = 2;
	private Vector3 dragOrigin;

	public GameObject Canvas;
	public GameObject Panel;

	// Use this for initialization
	void Start ()
	{
		dim = GetComponent<Dimensions> ();
		CreateMapRoom (x, y, z);
	}

	public void CreateMapRoom (int x, int y, int z)
	{
		Rooms = new List<GameObject> ();
		gameObject.name = "MapRooms";


		for (int i = 0; i < x; i += 2) {
			for (int j = 0; j < y; j += 2) {
				for (int k = 0; k < z; k += 2) {

					int alto = dim.alto;
					int ancho = dim.ancho;
					int fondo = dim.fondo;

//					print (alto + " " + ancho + " " + fondo);

					Vector3 pos = new Vector3 (transform.position.x + i + (int)Random.Range (1, 5), transform.position.y + j + (int)Random.Range (1, 5), transform.position.z - k + (int)Random.Range (1, 5));
					Vector3 sc = new Vector3 (ancho * 0.1f, alto * 0.1f, fondo * 0.1f);
//					Vector3 sc = new Vector3 (ancho * 1f, alto * 1f, fondo * 1f);

					GameObject Room = Instantiate (RoomObj, pos, Quaternion.identity)as GameObject;

					Room.GetComponent<Room> ().x = ancho;
					Room.GetComponent<Room> ().y = alto;
					Room.GetComponent<Room> ().z = fondo;

					Room.transform.position = pos;
					Room.transform.rotation = Quaternion.identity;
					Room.transform.localScale = sc;


					Room.name = "Room " + i.ToString () + " " + j.ToString ();
					Room.transform.SetParent (gameObject.transform);
					Color32 c = new Color (1, j * .1f, k * .1f);
					Room.GetComponent<Renderer> ().material.color = c;

					Rooms.Add (Room);
					Room.GetComponent<Room> ().index = Rooms.LastIndexOf (Room);

				}
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
//		Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray, out hit) && Input.GetMouseButtonDown (0)) {
			print (hit.transform.name + " index " + hit.transform.gameObject.GetComponent<Room> ().index);

			GameObject panel = Instantiate (Panel, hit.transform.position, Quaternion.identity)as GameObject;
			panel.transform.SetParent (Canvas.transform);
			panel.GetComponent<RectTransform> ().anchoredPosition3D = hit.transform.position;

			panel.transform.LookAt (Camera.main.transform);
		}


		//DRAG
		if (Input.GetMouseButtonDown (0)) {
			dragOrigin = Input.mousePosition;
			return;
		}

		if (!Input.GetMouseButton (0))
			return;

		Vector3 pos = Camera.main.ScreenToViewportPoint (Input.mousePosition - dragOrigin);
		Vector3 move = new Vector3 (pos.x * dragSpeed, 0, pos.y * dragSpeed);

		//clamp movement

		transform.Translate (move, Space.World);  
		transform.position = Vector3.ClampMagnitude (transform.position, 10);
	}

}
