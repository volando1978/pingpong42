﻿using UnityEngine;
using System.Collections;

public class Blanco : MonoBehaviour
{


	public float t;
	public Vector3 sc;
	public float diametroInicial;

	// Use this for initialization
	void Start ()
	{
		sc = new Vector3 (diametroInicial, diametroInicial, diametroInicial);
		transform.localScale = sc;
		StartCoroutine ("Plof");
	}

	IEnumerator Plof ()
	{
		t = 1;//time

		while (t > 0) {
			t -= 8 * Time.deltaTime;
			diametroInicial -= 0.1f;
			sc -= new Vector3 (diametroInicial, diametroInicial, diametroInicial);//0.1f, 0.1f, 0.1f);
			transform.localScale = sc;
			yield return new WaitForSeconds (0);

		}
		yield return new WaitForSeconds (0);

		Destroy (gameObject);


	}
}
