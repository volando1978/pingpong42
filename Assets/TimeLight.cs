﻿using UnityEngine;
using System.Collections;
using System;

public class TimeLight : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		float time = (float)System.DateTime.Now.TimeOfDay.TotalMinutes;
		print ("Time: " + time);
		Quaternion rot = Quaternion.Euler ((time * 360) / 1440 + 180, 115, 0);

		transform.localRotation = rot;
//		float intensity = Mathf.Clamp ((time * 360) / 1440, 0, 56);
//
//		GetComponent<Light> ().intensity = intensity;
	}

}
