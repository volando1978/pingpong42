﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Telefono : MonoBehaviour
{

	public GameObject fotoDisplay, button1, button2, button3, button4, button5, button6, button7, button8, button9, button0, buttonast, buttonigual, DialHUD;

	public string currentNumber;

	public bool isDialing = false;

	public Sprite[] pictures;



	// Use this for initialization
	void Start ()
	{
	
		currentNumber = "";
		UpdateDialHUD ();
		buttonigual.GetComponent<Button> ().interactable = false;
		fotoDisplay.GetComponent<Image> ().sprite = pictures [0] as Sprite;

	}

	public void add1 ()
	{
		AddNumber (1);
	}

	public void add2 ()
	{
		AddNumber (2);

	}

	public void add3 ()
	{
		AddNumber (3);

	}

	public void add4 ()
	{
		AddNumber (4);

	}

	public void add5 ()
	{
		AddNumber (5);

	}

	public void add6 ()
	{
		AddNumber (6);

	}

	public void add7 ()
	{
		AddNumber (7);

	}

	public void add8 ()
	{
		AddNumber (8);

	}

	public void add9 ()
	{
		AddNumber (9);

	}

	public void add0 ()
	{
		AddNumber (0);

	}

	public void AddNumber (int i)
	{
		if (!isDialing) {
			if (currentNumber.Length < 7)
				currentNumber = currentNumber + i.ToString ();

			if (currentNumber.Length == 7)
				buttonigual.GetComponent<Button> ().interactable = true;
			print (currentNumber);
			UpdateDialHUD ();
		}
	}

	public void Delete ()
	{
		currentNumber = "";
		UpdateDialHUD ();
		buttonigual.GetComponent<Button> ().interactable = false;
		StopCoroutine ("dialing");
		isDialing = false;
		fotoDisplay.GetComponent<Image> ().sprite = pictures [0] as Sprite;

	}

	public void Call ()
	{
		CallingHUD ();
	}


	public void UpdateDialHUD ()
	{
		DialHUD.GetComponent<Text> ().text = currentNumber;
	}


	public void CallingHUD ()
	{

		StartCoroutine ("dialing");

	}

	IEnumerator dialing ()
	{
		isDialing = true;
		DialHUD.GetComponent<Text> ().text = "CALLING";
		float time = 5;
		while (time > 0) {
			time -= 1;
			if (time % 2 != 0)
				DialHUD.GetComponent<Text> ().text = DialHUD.GetComponent<Text> ().text + " . ";
			else
				DialHUD.GetComponent<Text> ().text = "CALLING";
			yield return new WaitForSeconds (.9f);
		}

		CheckCall (currentNumber);

	}

	public void CheckCall (string currentNumber)
	{
//		currentNumber = "Unavailable";
		print ("check call  " + currentNumber);

		if (string.Equals (currentNumber, "0000000"))
			fotoDisplay.GetComponent<Image> ().sprite = pictures [0] as Sprite;

		if (currentNumber == "1111111")
			fotoDisplay.GetComponent<Image> ().sprite = pictures [1] as Sprite;

		if (currentNumber == "2222222")
			fotoDisplay.GetComponent<Image> ().sprite = pictures [2] as Sprite;

		if (currentNumber == "3333333")
			fotoDisplay.GetComponent<Image> ().sprite = pictures [3] as Sprite;

		if (currentNumber == "4444444")
			fotoDisplay.GetComponent<Image> ().sprite = pictures [4] as Sprite;

		UpdateDialHUD ();
	}

}
