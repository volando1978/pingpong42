﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Controller
struct Controller_t2630893500;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"

// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m3787541599 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::isTrampa()
extern "C"  void Controller_isTrampa_m2013472266 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::Start()
extern "C"  void Controller_Start_m2734679391 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::StartGame()
extern "C"  void Controller_StartGame_m3824060913 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Controller::CamAnim()
extern "C"  Il2CppObject * Controller_CamAnim_m3900757941 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::RemoveAll()
extern "C"  void Controller_RemoveAll_m1435005626 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::CreateRoom()
extern "C"  void Controller_CreateRoom_m119364668 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::CreatePlayer()
extern "C"  void Controller_CreatePlayer_m799294658 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::HideUIPanelJuego()
extern "C"  void Controller_HideUIPanelJuego_m599578937 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::ShowUIPanelJuego()
extern "C"  void Controller_ShowUIPanelJuego_m1208595380 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::HideUIPanelGameOver()
extern "C"  void Controller_HideUIPanelGameOver_m106393041 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::ShowUIPanelGameOver()
extern "C"  void Controller_ShowUIPanelGameOver_m1373388150 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::ShowMinigameButton()
extern "C"  void Controller_ShowMinigameButton_m1217470909 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::HideUIPanelMenu()
extern "C"  void Controller_HideUIPanelMenu_m1476205066 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::ShowUIPanelMenu()
extern "C"  void Controller_ShowUIPanelMenu_m2881324079 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::HideUIPanelTransicion()
extern "C"  void Controller_HideUIPanelTransicion_m3230741105 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::ShowUIPanelTransicion()
extern "C"  void Controller_ShowUIPanelTransicion_m1042328790 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::CreateBall()
extern "C"  void Controller_CreateBall_m3943245920 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::RestartGame()
extern "C"  void Controller_RestartGame_m3205758686 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::Update()
extern "C"  void Controller_Update_m3176534670 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::RemoveBrick(UnityEngine.GameObject)
extern "C"  void Controller_RemoveBrick_m1458410322 (Controller_t2630893500 * __this, GameObject_t3674682005 * ___brick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::PasaFase()
extern "C"  void Controller_PasaFase_m306660593 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::UpdateNumberRoom()
extern "C"  void Controller_UpdateNumberRoom_m1872280882 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Controller::Cortina()
extern "C"  Il2CppObject * Controller_Cortina_m4128456483 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::EnableEnterButton()
extern "C"  void Controller_EnableEnterButton_m3902825572 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::DisableEnterButton()
extern "C"  void Controller_DisableEnterButton_m3804347815 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::isWallEmpty()
extern "C"  bool Controller_isWallEmpty_m1320274370 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::GameOver()
extern "C"  void Controller_GameOver_m4035021163 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::GameOverStats()
extern "C"  void Controller_GameOverStats_m517432214 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Controller::runScore()
extern "C"  Il2CppObject * Controller_runScore_m2752663604 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::DestroyAllBalls()
extern "C"  void Controller_DestroyAllBalls_m2867674762 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::UpdateHUD()
extern "C"  void Controller_UpdateHUD_m1160851307 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::MoveLeft()
extern "C"  void Controller_MoveLeft_m1725249661 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::MoveRight()
extern "C"  void Controller_MoveRight_m3088342408 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::resetMovimiento()
extern "C"  void Controller_resetMovimiento_m1212277985 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::CreateMultiBalls(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  void Controller_CreateMultiBalls_m521653845 (Controller_t2630893500 * __this, Vector3_t4282066566  ___pos0, Vector3_t4282066566  ___boundaries1, float ___alturaSombra2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::CheckEndGame(UnityEngine.GameObject)
extern "C"  void Controller_CheckEndGame_m111272194 (Controller_t2630893500 * __this, GameObject_t3674682005 * ___ball0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::PlayAudio(System.Single)
extern "C"  void Controller_PlayAudio_m1101257740 (Controller_t2630893500 * __this, float ___pitch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::setData()
extern "C"  void Controller_setData_m4071057097 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::getData()
extern "C"  void Controller_getData_m4285674173 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::deleteData()
extern "C"  void Controller_deleteData_m1540580506 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::OnApplicationQuit()
extern "C"  void Controller_OnApplicationQuit_m2648122205 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::SetBool(System.String,System.Boolean)
extern "C"  void Controller_SetBool_m1497303716 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::GetBool(System.String)
extern "C"  bool Controller_GetBool_m3058516473 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::GetBool(System.String,System.Boolean)
extern "C"  bool Controller_GetBool_m3247030372 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::StartMinigame()
extern "C"  void Controller_StartMinigame_m3672498696 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::EndMinigame()
extern "C"  void Controller_EndMinigame_m3932820033 (Controller_t2630893500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
