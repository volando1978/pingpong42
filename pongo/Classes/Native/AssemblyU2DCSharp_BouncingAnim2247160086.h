﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BouncingAnim
struct  BouncingAnim_t2247160086  : public MonoBehaviour_t667441552
{
public:
	// System.Single BouncingAnim::stretch
	float ___stretch_2;
	// System.Single BouncingAnim::maxStretch
	float ___maxStretch_3;
	// System.Single BouncingAnim::alturaSueloAnim
	float ___alturaSueloAnim_4;

public:
	inline static int32_t get_offset_of_stretch_2() { return static_cast<int32_t>(offsetof(BouncingAnim_t2247160086, ___stretch_2)); }
	inline float get_stretch_2() const { return ___stretch_2; }
	inline float* get_address_of_stretch_2() { return &___stretch_2; }
	inline void set_stretch_2(float value)
	{
		___stretch_2 = value;
	}

	inline static int32_t get_offset_of_maxStretch_3() { return static_cast<int32_t>(offsetof(BouncingAnim_t2247160086, ___maxStretch_3)); }
	inline float get_maxStretch_3() const { return ___maxStretch_3; }
	inline float* get_address_of_maxStretch_3() { return &___maxStretch_3; }
	inline void set_maxStretch_3(float value)
	{
		___maxStretch_3 = value;
	}

	inline static int32_t get_offset_of_alturaSueloAnim_4() { return static_cast<int32_t>(offsetof(BouncingAnim_t2247160086, ___alturaSueloAnim_4)); }
	inline float get_alturaSueloAnim_4() const { return ___alturaSueloAnim_4; }
	inline float* get_address_of_alturaSueloAnim_4() { return &___alturaSueloAnim_4; }
	inline void set_alturaSueloAnim_4(float value)
	{
		___alturaSueloAnim_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
