﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Brazo
struct Brazo_t64445670;

#include "codegen/il2cpp-codegen.h"

// System.Void Brazo::.ctor()
extern "C"  void Brazo__ctor_m1688309573 (Brazo_t64445670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Brazo::Start()
extern "C"  void Brazo_Start_m635447365 (Brazo_t64445670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Brazo::Update()
extern "C"  void Brazo_Update_m2524851304 (Brazo_t64445670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
