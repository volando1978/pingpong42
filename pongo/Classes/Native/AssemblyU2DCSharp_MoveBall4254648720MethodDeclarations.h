﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveBall
struct MoveBall_t4254648720;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void MoveBall::.ctor()
extern "C"  void MoveBall__ctor_m3909728011 (MoveBall_t4254648720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveBall::Start()
extern "C"  void MoveBall_Start_m2856865803 (MoveBall_t4254648720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveBall::SetBoundary(UnityEngine.Vector3)
extern "C"  void MoveBall_SetBoundary_m3003401188 (MoveBall_t4254648720 * __this, Vector3_t4282066566  ___boundary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveBall::Update()
extern "C"  void MoveBall_Update_m2669346146 (MoveBall_t4254648720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
