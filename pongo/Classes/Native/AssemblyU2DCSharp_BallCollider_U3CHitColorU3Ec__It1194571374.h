﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// BallCollider
struct BallCollider_t3766478643;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallCollider/<HitColor>c__Iterator1
struct  U3CHitColorU3Ec__Iterator1_t1194571374  : public Il2CppObject
{
public:
	// System.Int32 BallCollider/<HitColor>c__Iterator1::<t>__0
	int32_t ___U3CtU3E__0_0;
	// UnityEngine.GameObject BallCollider/<HitColor>c__Iterator1::other
	GameObject_t3674682005 * ___other_1;
	// UnityEngine.GameObject BallCollider/<HitColor>c__Iterator1::<g>__1
	GameObject_t3674682005 * ___U3CgU3E__1_2;
	// System.Int32 BallCollider/<HitColor>c__Iterator1::$PC
	int32_t ___U24PC_3;
	// System.Object BallCollider/<HitColor>c__Iterator1::$current
	Il2CppObject * ___U24current_4;
	// UnityEngine.GameObject BallCollider/<HitColor>c__Iterator1::<$>other
	GameObject_t3674682005 * ___U3CU24U3Eother_5;
	// BallCollider BallCollider/<HitColor>c__Iterator1::<>f__this
	BallCollider_t3766478643 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CHitColorU3Ec__Iterator1_t1194571374, ___U3CtU3E__0_0)); }
	inline int32_t get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline int32_t* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(int32_t value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_other_1() { return static_cast<int32_t>(offsetof(U3CHitColorU3Ec__Iterator1_t1194571374, ___other_1)); }
	inline GameObject_t3674682005 * get_other_1() const { return ___other_1; }
	inline GameObject_t3674682005 ** get_address_of_other_1() { return &___other_1; }
	inline void set_other_1(GameObject_t3674682005 * value)
	{
		___other_1 = value;
		Il2CppCodeGenWriteBarrier(&___other_1, value);
	}

	inline static int32_t get_offset_of_U3CgU3E__1_2() { return static_cast<int32_t>(offsetof(U3CHitColorU3Ec__Iterator1_t1194571374, ___U3CgU3E__1_2)); }
	inline GameObject_t3674682005 * get_U3CgU3E__1_2() const { return ___U3CgU3E__1_2; }
	inline GameObject_t3674682005 ** get_address_of_U3CgU3E__1_2() { return &___U3CgU3E__1_2; }
	inline void set_U3CgU3E__1_2(GameObject_t3674682005 * value)
	{
		___U3CgU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CHitColorU3Ec__Iterator1_t1194571374, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CHitColorU3Ec__Iterator1_t1194571374, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eother_5() { return static_cast<int32_t>(offsetof(U3CHitColorU3Ec__Iterator1_t1194571374, ___U3CU24U3Eother_5)); }
	inline GameObject_t3674682005 * get_U3CU24U3Eother_5() const { return ___U3CU24U3Eother_5; }
	inline GameObject_t3674682005 ** get_address_of_U3CU24U3Eother_5() { return &___U3CU24U3Eother_5; }
	inline void set_U3CU24U3Eother_5(GameObject_t3674682005 * value)
	{
		___U3CU24U3Eother_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eother_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CHitColorU3Ec__Iterator1_t1194571374, ___U3CU3Ef__this_6)); }
	inline BallCollider_t3766478643 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline BallCollider_t3766478643 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(BallCollider_t3766478643 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
