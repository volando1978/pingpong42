﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CnControls.SimpleButton
struct SimpleButton_t2998894860;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void CnControls.SimpleButton::.ctor()
extern "C"  void SimpleButton__ctor_m552871238 (SimpleButton_t2998894860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.SimpleButton::OnEnable()
extern "C"  void SimpleButton_OnEnable_m1459049600 (SimpleButton_t2998894860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.SimpleButton::OnDisable()
extern "C"  void SimpleButton_OnDisable_m2721801901 (SimpleButton_t2998894860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.SimpleButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SimpleButton_OnPointerUp_m673638817 (SimpleButton_t2998894860 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.SimpleButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SimpleButton_OnPointerDown_m2531365242 (SimpleButton_t2998894860 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
