﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Shake
struct Shake_t79847142;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shake/<shake>c__Iterator7
struct  U3CshakeU3Ec__Iterator7_t570686417  : public Il2CppObject
{
public:
	// System.Single Shake/<shake>c__Iterator7::t
	float ___t_0;
	// UnityEngine.Vector3 Shake/<shake>c__Iterator7::<pP>__0
	Vector3_t4282066566  ___U3CpPU3E__0_1;
	// UnityEngine.Vector3 Shake/<shake>c__Iterator7::<pO>__1
	Vector3_t4282066566  ___U3CpOU3E__1_2;
	// UnityEngine.Vector3 Shake/<shake>c__Iterator7::<pI>__2
	Vector3_t4282066566  ___U3CpIU3E__2_3;
	// System.Int32 Shake/<shake>c__Iterator7::$PC
	int32_t ___U24PC_4;
	// System.Object Shake/<shake>c__Iterator7::$current
	Il2CppObject * ___U24current_5;
	// System.Single Shake/<shake>c__Iterator7::<$>t
	float ___U3CU24U3Et_6;
	// Shake Shake/<shake>c__Iterator7::<>f__this
	Shake_t79847142 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CshakeU3Ec__Iterator7_t570686417, ___t_0)); }
	inline float get_t_0() const { return ___t_0; }
	inline float* get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(float value)
	{
		___t_0 = value;
	}

	inline static int32_t get_offset_of_U3CpPU3E__0_1() { return static_cast<int32_t>(offsetof(U3CshakeU3Ec__Iterator7_t570686417, ___U3CpPU3E__0_1)); }
	inline Vector3_t4282066566  get_U3CpPU3E__0_1() const { return ___U3CpPU3E__0_1; }
	inline Vector3_t4282066566 * get_address_of_U3CpPU3E__0_1() { return &___U3CpPU3E__0_1; }
	inline void set_U3CpPU3E__0_1(Vector3_t4282066566  value)
	{
		___U3CpPU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpOU3E__1_2() { return static_cast<int32_t>(offsetof(U3CshakeU3Ec__Iterator7_t570686417, ___U3CpOU3E__1_2)); }
	inline Vector3_t4282066566  get_U3CpOU3E__1_2() const { return ___U3CpOU3E__1_2; }
	inline Vector3_t4282066566 * get_address_of_U3CpOU3E__1_2() { return &___U3CpOU3E__1_2; }
	inline void set_U3CpOU3E__1_2(Vector3_t4282066566  value)
	{
		___U3CpOU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CpIU3E__2_3() { return static_cast<int32_t>(offsetof(U3CshakeU3Ec__Iterator7_t570686417, ___U3CpIU3E__2_3)); }
	inline Vector3_t4282066566  get_U3CpIU3E__2_3() const { return ___U3CpIU3E__2_3; }
	inline Vector3_t4282066566 * get_address_of_U3CpIU3E__2_3() { return &___U3CpIU3E__2_3; }
	inline void set_U3CpIU3E__2_3(Vector3_t4282066566  value)
	{
		___U3CpIU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CshakeU3Ec__Iterator7_t570686417, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CshakeU3Ec__Iterator7_t570686417, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Et_6() { return static_cast<int32_t>(offsetof(U3CshakeU3Ec__Iterator7_t570686417, ___U3CU24U3Et_6)); }
	inline float get_U3CU24U3Et_6() const { return ___U3CU24U3Et_6; }
	inline float* get_address_of_U3CU24U3Et_6() { return &___U3CU24U3Et_6; }
	inline void set_U3CU24U3Et_6(float value)
	{
		___U3CU24U3Et_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CshakeU3Ec__Iterator7_t570686417, ___U3CU3Ef__this_7)); }
	inline Shake_t79847142 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline Shake_t79847142 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(Shake_t79847142 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
