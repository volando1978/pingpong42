﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shake/<shake>c__Iterator7
struct U3CshakeU3Ec__Iterator7_t570686417;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Shake/<shake>c__Iterator7::.ctor()
extern "C"  void U3CshakeU3Ec__Iterator7__ctor_m4289725626 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Shake/<shake>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CshakeU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m828775192 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Shake/<shake>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CshakeU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m4171321004 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Shake/<shake>c__Iterator7::MoveNext()
extern "C"  bool U3CshakeU3Ec__Iterator7_MoveNext_m2349314042 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shake/<shake>c__Iterator7::Dispose()
extern "C"  void U3CshakeU3Ec__Iterator7_Dispose_m3450972791 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shake/<shake>c__Iterator7::Reset()
extern "C"  void U3CshakeU3Ec__Iterator7_Reset_m1936158567 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
