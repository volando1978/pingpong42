﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColliderPlayer/<flan>c__Iterator3
struct U3CflanU3Ec__Iterator3_t3689796459;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ColliderPlayer/<flan>c__Iterator3::.ctor()
extern "C"  void U3CflanU3Ec__Iterator3__ctor_m2224992864 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ColliderPlayer/<flan>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CflanU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1259654706 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ColliderPlayer/<flan>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CflanU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3091035590 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ColliderPlayer/<flan>c__Iterator3::MoveNext()
extern "C"  bool U3CflanU3Ec__Iterator3_MoveNext_m4199599252 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColliderPlayer/<flan>c__Iterator3::Dispose()
extern "C"  void U3CflanU3Ec__Iterator3_Dispose_m3517679261 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColliderPlayer/<flan>c__Iterator3::Reset()
extern "C"  void U3CflanU3Ec__Iterator3_Reset_m4166393101 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
