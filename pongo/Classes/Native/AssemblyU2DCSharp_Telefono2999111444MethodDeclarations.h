﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Telefono
struct Telefono_t2999111444;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Telefono::.ctor()
extern "C"  void Telefono__ctor_m3358828039 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::Start()
extern "C"  void Telefono_Start_m2305965831 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add1()
extern "C"  void Telefono_add1_m1291726317 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add2()
extern "C"  void Telefono_add2_m1291727278 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add3()
extern "C"  void Telefono_add3_m1291728239 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add4()
extern "C"  void Telefono_add4_m1291729200 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add5()
extern "C"  void Telefono_add5_m1291730161 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add6()
extern "C"  void Telefono_add6_m1291731122 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add7()
extern "C"  void Telefono_add7_m1291732083 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add8()
extern "C"  void Telefono_add8_m1291733044 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add9()
extern "C"  void Telefono_add9_m1291734005 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::add0()
extern "C"  void Telefono_add0_m1291725356 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::AddNumber(System.Int32)
extern "C"  void Telefono_AddNumber_m2955638976 (Telefono_t2999111444 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::Delete()
extern "C"  void Telefono_Delete_m2268432968 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::Call()
extern "C"  void Telefono_Call_m430376251 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::UpdateDialHUD()
extern "C"  void Telefono_UpdateDialHUD_m2371369475 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::CallingHUD()
extern "C"  void Telefono_CallingHUD_m112899632 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Telefono::dialing()
extern "C"  Il2CppObject * Telefono_dialing_m411091183 (Telefono_t2999111444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono::CheckCall(System.String)
extern "C"  void Telefono_CheckCall_m2092514743 (Telefono_t2999111444 * __this, String_t* ___currentNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
