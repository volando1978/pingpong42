﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CreateMap
struct CreateMap_t2602318496;

#include "codegen/il2cpp-codegen.h"

// System.Void CreateMap::.ctor()
extern "C"  void CreateMap__ctor_m4077309643 (CreateMap_t2602318496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CreateMap::Start()
extern "C"  void CreateMap_Start_m3024447435 (CreateMap_t2602318496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
