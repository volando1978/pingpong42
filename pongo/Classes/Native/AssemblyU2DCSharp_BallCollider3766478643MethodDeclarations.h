﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BallCollider
struct BallCollider_t3766478643;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Collider
struct Collider_t2939674232;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void BallCollider::.ctor()
extern "C"  void BallCollider__ctor_m3051189448 (BallCollider_t3766478643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallCollider::PlayAudio(System.Single)
extern "C"  void BallCollider_PlayAudio_m338850499 (BallCollider_t3766478643 * __this, float ___pitch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallCollider::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void BallCollider_setCameras_m1923159934 (BallCollider_t3766478643 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallCollider::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void BallCollider_OnTriggerEnter_m2106133424 (BallCollider_t3766478643 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BallCollider::BigBallTime()
extern "C"  Il2CppObject * BallCollider_BigBallTime_m644600426 (BallCollider_t3766478643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BallCollider::HitColor(UnityEngine.GameObject)
extern "C"  Il2CppObject * BallCollider_HitColor_m1095455820 (BallCollider_t3766478643 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
