﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Telefono/<dialing>c__Iterator8
struct U3CdialingU3Ec__Iterator8_t3806020820;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Telefono/<dialing>c__Iterator8::.ctor()
extern "C"  void U3CdialingU3Ec__Iterator8__ctor_m1365083719 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Telefono/<dialing>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdialingU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3838339893 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Telefono/<dialing>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdialingU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m442071241 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Telefono/<dialing>c__Iterator8::MoveNext()
extern "C"  bool U3CdialingU3Ec__Iterator8_MoveNext_m82314037 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono/<dialing>c__Iterator8::Dispose()
extern "C"  void U3CdialingU3Ec__Iterator8_Dispose_m1778711748 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Telefono/<dialing>c__Iterator8::Reset()
extern "C"  void U3CdialingU3Ec__Iterator8_Reset_m3306483956 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
