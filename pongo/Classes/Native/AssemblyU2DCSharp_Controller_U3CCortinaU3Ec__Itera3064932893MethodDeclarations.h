﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Controller/<Cortina>c__Iterator5
struct U3CCortinaU3Ec__Iterator5_t3064932893;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Controller/<Cortina>c__Iterator5::.ctor()
extern "C"  void U3CCortinaU3Ec__Iterator5__ctor_m1381489950 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Controller/<Cortina>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCortinaU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m745223038 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Controller/<Cortina>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCortinaU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m4190106386 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller/<Cortina>c__Iterator5::MoveNext()
extern "C"  bool U3CCortinaU3Ec__Iterator5_MoveNext_m533400574 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller/<Cortina>c__Iterator5::Dispose()
extern "C"  void U3CCortinaU3Ec__Iterator5_Dispose_m365230555 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller/<Cortina>c__Iterator5::Reset()
extern "C"  void U3CCortinaU3Ec__Iterator5_Reset_m3322890187 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
