﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Copy._2D.PlatformerCharacter2D
struct PlatformerCharacter2D_t2561026825;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::.ctor()
extern "C"  void PlatformerCharacter2D__ctor_m3368050705 (PlatformerCharacter2D_t2561026825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Awake()
extern "C"  void PlatformerCharacter2D_Awake_m3605655924 (PlatformerCharacter2D_t2561026825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::FixedUpdate()
extern "C"  void PlatformerCharacter2D_FixedUpdate_m3897306316 (PlatformerCharacter2D_t2561026825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean)
extern "C"  void PlatformerCharacter2D_Move_m98163478 (PlatformerCharacter2D_t2561026825 * __this, float ___move0, bool ___jump1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Flip()
extern "C"  void PlatformerCharacter2D_Flip_m1219371072 (PlatformerCharacter2D_t2561026825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
