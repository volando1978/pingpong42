﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapController
struct MapController_t1754731000;

#include "codegen/il2cpp-codegen.h"

// System.Void MapController::.ctor()
extern "C"  void MapController__ctor_m3215722099 (MapController_t1754731000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapController::Start()
extern "C"  void MapController_Start_m2162859891 (MapController_t1754731000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapController::CreateMapRoom(System.Int32,System.Int32,System.Int32)
extern "C"  void MapController_CreateMapRoom_m112154365 (MapController_t1754731000 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapController::Update()
extern "C"  void MapController_Update_m2629999354 (MapController_t1754731000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
