﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Brick
struct  Brick_t64452641  : public MonoBehaviour_t667441552
{
public:
	// System.String Brick::tipo
	String_t* ___tipo_2;
	// System.Int32 Brick::nToques
	int32_t ___nToques_3;
	// System.Boolean Brick::isReadyToDie
	bool ___isReadyToDie_4;
	// UnityEngine.Material Brick::dobleMaterial
	Material_t3870600107 * ___dobleMaterial_5;
	// UnityEngine.Material Brick::dobleMaterialTocado
	Material_t3870600107 * ___dobleMaterialTocado_6;
	// UnityEngine.GameObject Brick::Blanco
	GameObject_t3674682005 * ___Blanco_7;
	// UnityEngine.GameObject Brick::Controller
	GameObject_t3674682005 * ___Controller_8;
	// System.Int32 Brick::tocaBrick
	int32_t ___tocaBrick_9;

public:
	inline static int32_t get_offset_of_tipo_2() { return static_cast<int32_t>(offsetof(Brick_t64452641, ___tipo_2)); }
	inline String_t* get_tipo_2() const { return ___tipo_2; }
	inline String_t** get_address_of_tipo_2() { return &___tipo_2; }
	inline void set_tipo_2(String_t* value)
	{
		___tipo_2 = value;
		Il2CppCodeGenWriteBarrier(&___tipo_2, value);
	}

	inline static int32_t get_offset_of_nToques_3() { return static_cast<int32_t>(offsetof(Brick_t64452641, ___nToques_3)); }
	inline int32_t get_nToques_3() const { return ___nToques_3; }
	inline int32_t* get_address_of_nToques_3() { return &___nToques_3; }
	inline void set_nToques_3(int32_t value)
	{
		___nToques_3 = value;
	}

	inline static int32_t get_offset_of_isReadyToDie_4() { return static_cast<int32_t>(offsetof(Brick_t64452641, ___isReadyToDie_4)); }
	inline bool get_isReadyToDie_4() const { return ___isReadyToDie_4; }
	inline bool* get_address_of_isReadyToDie_4() { return &___isReadyToDie_4; }
	inline void set_isReadyToDie_4(bool value)
	{
		___isReadyToDie_4 = value;
	}

	inline static int32_t get_offset_of_dobleMaterial_5() { return static_cast<int32_t>(offsetof(Brick_t64452641, ___dobleMaterial_5)); }
	inline Material_t3870600107 * get_dobleMaterial_5() const { return ___dobleMaterial_5; }
	inline Material_t3870600107 ** get_address_of_dobleMaterial_5() { return &___dobleMaterial_5; }
	inline void set_dobleMaterial_5(Material_t3870600107 * value)
	{
		___dobleMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___dobleMaterial_5, value);
	}

	inline static int32_t get_offset_of_dobleMaterialTocado_6() { return static_cast<int32_t>(offsetof(Brick_t64452641, ___dobleMaterialTocado_6)); }
	inline Material_t3870600107 * get_dobleMaterialTocado_6() const { return ___dobleMaterialTocado_6; }
	inline Material_t3870600107 ** get_address_of_dobleMaterialTocado_6() { return &___dobleMaterialTocado_6; }
	inline void set_dobleMaterialTocado_6(Material_t3870600107 * value)
	{
		___dobleMaterialTocado_6 = value;
		Il2CppCodeGenWriteBarrier(&___dobleMaterialTocado_6, value);
	}

	inline static int32_t get_offset_of_Blanco_7() { return static_cast<int32_t>(offsetof(Brick_t64452641, ___Blanco_7)); }
	inline GameObject_t3674682005 * get_Blanco_7() const { return ___Blanco_7; }
	inline GameObject_t3674682005 ** get_address_of_Blanco_7() { return &___Blanco_7; }
	inline void set_Blanco_7(GameObject_t3674682005 * value)
	{
		___Blanco_7 = value;
		Il2CppCodeGenWriteBarrier(&___Blanco_7, value);
	}

	inline static int32_t get_offset_of_Controller_8() { return static_cast<int32_t>(offsetof(Brick_t64452641, ___Controller_8)); }
	inline GameObject_t3674682005 * get_Controller_8() const { return ___Controller_8; }
	inline GameObject_t3674682005 ** get_address_of_Controller_8() { return &___Controller_8; }
	inline void set_Controller_8(GameObject_t3674682005 * value)
	{
		___Controller_8 = value;
		Il2CppCodeGenWriteBarrier(&___Controller_8, value);
	}

	inline static int32_t get_offset_of_tocaBrick_9() { return static_cast<int32_t>(offsetof(Brick_t64452641, ___tocaBrick_9)); }
	inline int32_t get_tocaBrick_9() const { return ___tocaBrick_9; }
	inline int32_t* get_address_of_tocaBrick_9() { return &___tocaBrick_9; }
	inline void set_tocaBrick_9(int32_t value)
	{
		___tocaBrick_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
