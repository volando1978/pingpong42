﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Telefono
struct  Telefono_t2999111444  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject Telefono::fotoDisplay
	GameObject_t3674682005 * ___fotoDisplay_2;
	// UnityEngine.GameObject Telefono::button1
	GameObject_t3674682005 * ___button1_3;
	// UnityEngine.GameObject Telefono::button2
	GameObject_t3674682005 * ___button2_4;
	// UnityEngine.GameObject Telefono::button3
	GameObject_t3674682005 * ___button3_5;
	// UnityEngine.GameObject Telefono::button4
	GameObject_t3674682005 * ___button4_6;
	// UnityEngine.GameObject Telefono::button5
	GameObject_t3674682005 * ___button5_7;
	// UnityEngine.GameObject Telefono::button6
	GameObject_t3674682005 * ___button6_8;
	// UnityEngine.GameObject Telefono::button7
	GameObject_t3674682005 * ___button7_9;
	// UnityEngine.GameObject Telefono::button8
	GameObject_t3674682005 * ___button8_10;
	// UnityEngine.GameObject Telefono::button9
	GameObject_t3674682005 * ___button9_11;
	// UnityEngine.GameObject Telefono::button0
	GameObject_t3674682005 * ___button0_12;
	// UnityEngine.GameObject Telefono::buttonast
	GameObject_t3674682005 * ___buttonast_13;
	// UnityEngine.GameObject Telefono::buttonigual
	GameObject_t3674682005 * ___buttonigual_14;
	// UnityEngine.GameObject Telefono::DialHUD
	GameObject_t3674682005 * ___DialHUD_15;
	// System.String Telefono::currentNumber
	String_t* ___currentNumber_16;
	// System.Boolean Telefono::isDialing
	bool ___isDialing_17;
	// UnityEngine.Sprite[] Telefono::pictures
	SpriteU5BU5D_t2761310900* ___pictures_18;

public:
	inline static int32_t get_offset_of_fotoDisplay_2() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___fotoDisplay_2)); }
	inline GameObject_t3674682005 * get_fotoDisplay_2() const { return ___fotoDisplay_2; }
	inline GameObject_t3674682005 ** get_address_of_fotoDisplay_2() { return &___fotoDisplay_2; }
	inline void set_fotoDisplay_2(GameObject_t3674682005 * value)
	{
		___fotoDisplay_2 = value;
		Il2CppCodeGenWriteBarrier(&___fotoDisplay_2, value);
	}

	inline static int32_t get_offset_of_button1_3() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button1_3)); }
	inline GameObject_t3674682005 * get_button1_3() const { return ___button1_3; }
	inline GameObject_t3674682005 ** get_address_of_button1_3() { return &___button1_3; }
	inline void set_button1_3(GameObject_t3674682005 * value)
	{
		___button1_3 = value;
		Il2CppCodeGenWriteBarrier(&___button1_3, value);
	}

	inline static int32_t get_offset_of_button2_4() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button2_4)); }
	inline GameObject_t3674682005 * get_button2_4() const { return ___button2_4; }
	inline GameObject_t3674682005 ** get_address_of_button2_4() { return &___button2_4; }
	inline void set_button2_4(GameObject_t3674682005 * value)
	{
		___button2_4 = value;
		Il2CppCodeGenWriteBarrier(&___button2_4, value);
	}

	inline static int32_t get_offset_of_button3_5() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button3_5)); }
	inline GameObject_t3674682005 * get_button3_5() const { return ___button3_5; }
	inline GameObject_t3674682005 ** get_address_of_button3_5() { return &___button3_5; }
	inline void set_button3_5(GameObject_t3674682005 * value)
	{
		___button3_5 = value;
		Il2CppCodeGenWriteBarrier(&___button3_5, value);
	}

	inline static int32_t get_offset_of_button4_6() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button4_6)); }
	inline GameObject_t3674682005 * get_button4_6() const { return ___button4_6; }
	inline GameObject_t3674682005 ** get_address_of_button4_6() { return &___button4_6; }
	inline void set_button4_6(GameObject_t3674682005 * value)
	{
		___button4_6 = value;
		Il2CppCodeGenWriteBarrier(&___button4_6, value);
	}

	inline static int32_t get_offset_of_button5_7() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button5_7)); }
	inline GameObject_t3674682005 * get_button5_7() const { return ___button5_7; }
	inline GameObject_t3674682005 ** get_address_of_button5_7() { return &___button5_7; }
	inline void set_button5_7(GameObject_t3674682005 * value)
	{
		___button5_7 = value;
		Il2CppCodeGenWriteBarrier(&___button5_7, value);
	}

	inline static int32_t get_offset_of_button6_8() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button6_8)); }
	inline GameObject_t3674682005 * get_button6_8() const { return ___button6_8; }
	inline GameObject_t3674682005 ** get_address_of_button6_8() { return &___button6_8; }
	inline void set_button6_8(GameObject_t3674682005 * value)
	{
		___button6_8 = value;
		Il2CppCodeGenWriteBarrier(&___button6_8, value);
	}

	inline static int32_t get_offset_of_button7_9() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button7_9)); }
	inline GameObject_t3674682005 * get_button7_9() const { return ___button7_9; }
	inline GameObject_t3674682005 ** get_address_of_button7_9() { return &___button7_9; }
	inline void set_button7_9(GameObject_t3674682005 * value)
	{
		___button7_9 = value;
		Il2CppCodeGenWriteBarrier(&___button7_9, value);
	}

	inline static int32_t get_offset_of_button8_10() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button8_10)); }
	inline GameObject_t3674682005 * get_button8_10() const { return ___button8_10; }
	inline GameObject_t3674682005 ** get_address_of_button8_10() { return &___button8_10; }
	inline void set_button8_10(GameObject_t3674682005 * value)
	{
		___button8_10 = value;
		Il2CppCodeGenWriteBarrier(&___button8_10, value);
	}

	inline static int32_t get_offset_of_button9_11() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button9_11)); }
	inline GameObject_t3674682005 * get_button9_11() const { return ___button9_11; }
	inline GameObject_t3674682005 ** get_address_of_button9_11() { return &___button9_11; }
	inline void set_button9_11(GameObject_t3674682005 * value)
	{
		___button9_11 = value;
		Il2CppCodeGenWriteBarrier(&___button9_11, value);
	}

	inline static int32_t get_offset_of_button0_12() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___button0_12)); }
	inline GameObject_t3674682005 * get_button0_12() const { return ___button0_12; }
	inline GameObject_t3674682005 ** get_address_of_button0_12() { return &___button0_12; }
	inline void set_button0_12(GameObject_t3674682005 * value)
	{
		___button0_12 = value;
		Il2CppCodeGenWriteBarrier(&___button0_12, value);
	}

	inline static int32_t get_offset_of_buttonast_13() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___buttonast_13)); }
	inline GameObject_t3674682005 * get_buttonast_13() const { return ___buttonast_13; }
	inline GameObject_t3674682005 ** get_address_of_buttonast_13() { return &___buttonast_13; }
	inline void set_buttonast_13(GameObject_t3674682005 * value)
	{
		___buttonast_13 = value;
		Il2CppCodeGenWriteBarrier(&___buttonast_13, value);
	}

	inline static int32_t get_offset_of_buttonigual_14() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___buttonigual_14)); }
	inline GameObject_t3674682005 * get_buttonigual_14() const { return ___buttonigual_14; }
	inline GameObject_t3674682005 ** get_address_of_buttonigual_14() { return &___buttonigual_14; }
	inline void set_buttonigual_14(GameObject_t3674682005 * value)
	{
		___buttonigual_14 = value;
		Il2CppCodeGenWriteBarrier(&___buttonigual_14, value);
	}

	inline static int32_t get_offset_of_DialHUD_15() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___DialHUD_15)); }
	inline GameObject_t3674682005 * get_DialHUD_15() const { return ___DialHUD_15; }
	inline GameObject_t3674682005 ** get_address_of_DialHUD_15() { return &___DialHUD_15; }
	inline void set_DialHUD_15(GameObject_t3674682005 * value)
	{
		___DialHUD_15 = value;
		Il2CppCodeGenWriteBarrier(&___DialHUD_15, value);
	}

	inline static int32_t get_offset_of_currentNumber_16() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___currentNumber_16)); }
	inline String_t* get_currentNumber_16() const { return ___currentNumber_16; }
	inline String_t** get_address_of_currentNumber_16() { return &___currentNumber_16; }
	inline void set_currentNumber_16(String_t* value)
	{
		___currentNumber_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentNumber_16, value);
	}

	inline static int32_t get_offset_of_isDialing_17() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___isDialing_17)); }
	inline bool get_isDialing_17() const { return ___isDialing_17; }
	inline bool* get_address_of_isDialing_17() { return &___isDialing_17; }
	inline void set_isDialing_17(bool value)
	{
		___isDialing_17 = value;
	}

	inline static int32_t get_offset_of_pictures_18() { return static_cast<int32_t>(offsetof(Telefono_t2999111444, ___pictures_18)); }
	inline SpriteU5BU5D_t2761310900* get_pictures_18() const { return ___pictures_18; }
	inline SpriteU5BU5D_t2761310900** get_address_of_pictures_18() { return &___pictures_18; }
	inline void set_pictures_18(SpriteU5BU5D_t2761310900* value)
	{
		___pictures_18 = value;
		Il2CppCodeGenWriteBarrier(&___pictures_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
