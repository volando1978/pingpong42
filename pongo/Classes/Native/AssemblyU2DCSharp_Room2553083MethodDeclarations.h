﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Room
struct Room_t2553083;

#include "codegen/il2cpp-codegen.h"

// System.Void Room::.ctor()
extern "C"  void Room__ctor_m1273823232 (Room_t2553083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
