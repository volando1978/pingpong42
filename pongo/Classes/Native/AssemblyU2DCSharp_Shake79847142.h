﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shake
struct  Shake_t79847142  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean Shake::shaking
	bool ___shaking_2;
	// System.Single Shake::minMovementRange
	float ___minMovementRange_3;
	// System.Single Shake::maxMovementRange
	float ___maxMovementRange_4;
	// System.Single Shake::positionRange
	float ___positionRange_5;
	// System.Single Shake::maxShakeField
	float ___maxShakeField_6;
	// UnityEngine.Vector3 Shake::initPosP
	Vector3_t4282066566  ___initPosP_7;
	// UnityEngine.Vector3 Shake::initPosO
	Vector3_t4282066566  ___initPosO_8;
	// UnityEngine.Vector3 Shake::initPosI
	Vector3_t4282066566  ___initPosI_9;
	// System.Single Shake::shakeSizeP
	float ___shakeSizeP_10;
	// System.Single Shake::shakeSizeO
	float ___shakeSizeO_11;
	// System.Single Shake::shakeSizeI
	float ___shakeSizeI_12;
	// UnityEngine.Camera Shake::Perspec
	Camera_t2727095145 * ___Perspec_13;
	// UnityEngine.Camera Shake::Ortho
	Camera_t2727095145 * ___Ortho_14;
	// UnityEngine.Camera Shake::Iso
	Camera_t2727095145 * ___Iso_15;
	// UnityEngine.Camera Shake::Interface
	Camera_t2727095145 * ___Interface_16;

public:
	inline static int32_t get_offset_of_shaking_2() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___shaking_2)); }
	inline bool get_shaking_2() const { return ___shaking_2; }
	inline bool* get_address_of_shaking_2() { return &___shaking_2; }
	inline void set_shaking_2(bool value)
	{
		___shaking_2 = value;
	}

	inline static int32_t get_offset_of_minMovementRange_3() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___minMovementRange_3)); }
	inline float get_minMovementRange_3() const { return ___minMovementRange_3; }
	inline float* get_address_of_minMovementRange_3() { return &___minMovementRange_3; }
	inline void set_minMovementRange_3(float value)
	{
		___minMovementRange_3 = value;
	}

	inline static int32_t get_offset_of_maxMovementRange_4() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___maxMovementRange_4)); }
	inline float get_maxMovementRange_4() const { return ___maxMovementRange_4; }
	inline float* get_address_of_maxMovementRange_4() { return &___maxMovementRange_4; }
	inline void set_maxMovementRange_4(float value)
	{
		___maxMovementRange_4 = value;
	}

	inline static int32_t get_offset_of_positionRange_5() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___positionRange_5)); }
	inline float get_positionRange_5() const { return ___positionRange_5; }
	inline float* get_address_of_positionRange_5() { return &___positionRange_5; }
	inline void set_positionRange_5(float value)
	{
		___positionRange_5 = value;
	}

	inline static int32_t get_offset_of_maxShakeField_6() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___maxShakeField_6)); }
	inline float get_maxShakeField_6() const { return ___maxShakeField_6; }
	inline float* get_address_of_maxShakeField_6() { return &___maxShakeField_6; }
	inline void set_maxShakeField_6(float value)
	{
		___maxShakeField_6 = value;
	}

	inline static int32_t get_offset_of_initPosP_7() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___initPosP_7)); }
	inline Vector3_t4282066566  get_initPosP_7() const { return ___initPosP_7; }
	inline Vector3_t4282066566 * get_address_of_initPosP_7() { return &___initPosP_7; }
	inline void set_initPosP_7(Vector3_t4282066566  value)
	{
		___initPosP_7 = value;
	}

	inline static int32_t get_offset_of_initPosO_8() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___initPosO_8)); }
	inline Vector3_t4282066566  get_initPosO_8() const { return ___initPosO_8; }
	inline Vector3_t4282066566 * get_address_of_initPosO_8() { return &___initPosO_8; }
	inline void set_initPosO_8(Vector3_t4282066566  value)
	{
		___initPosO_8 = value;
	}

	inline static int32_t get_offset_of_initPosI_9() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___initPosI_9)); }
	inline Vector3_t4282066566  get_initPosI_9() const { return ___initPosI_9; }
	inline Vector3_t4282066566 * get_address_of_initPosI_9() { return &___initPosI_9; }
	inline void set_initPosI_9(Vector3_t4282066566  value)
	{
		___initPosI_9 = value;
	}

	inline static int32_t get_offset_of_shakeSizeP_10() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___shakeSizeP_10)); }
	inline float get_shakeSizeP_10() const { return ___shakeSizeP_10; }
	inline float* get_address_of_shakeSizeP_10() { return &___shakeSizeP_10; }
	inline void set_shakeSizeP_10(float value)
	{
		___shakeSizeP_10 = value;
	}

	inline static int32_t get_offset_of_shakeSizeO_11() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___shakeSizeO_11)); }
	inline float get_shakeSizeO_11() const { return ___shakeSizeO_11; }
	inline float* get_address_of_shakeSizeO_11() { return &___shakeSizeO_11; }
	inline void set_shakeSizeO_11(float value)
	{
		___shakeSizeO_11 = value;
	}

	inline static int32_t get_offset_of_shakeSizeI_12() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___shakeSizeI_12)); }
	inline float get_shakeSizeI_12() const { return ___shakeSizeI_12; }
	inline float* get_address_of_shakeSizeI_12() { return &___shakeSizeI_12; }
	inline void set_shakeSizeI_12(float value)
	{
		___shakeSizeI_12 = value;
	}

	inline static int32_t get_offset_of_Perspec_13() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___Perspec_13)); }
	inline Camera_t2727095145 * get_Perspec_13() const { return ___Perspec_13; }
	inline Camera_t2727095145 ** get_address_of_Perspec_13() { return &___Perspec_13; }
	inline void set_Perspec_13(Camera_t2727095145 * value)
	{
		___Perspec_13 = value;
		Il2CppCodeGenWriteBarrier(&___Perspec_13, value);
	}

	inline static int32_t get_offset_of_Ortho_14() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___Ortho_14)); }
	inline Camera_t2727095145 * get_Ortho_14() const { return ___Ortho_14; }
	inline Camera_t2727095145 ** get_address_of_Ortho_14() { return &___Ortho_14; }
	inline void set_Ortho_14(Camera_t2727095145 * value)
	{
		___Ortho_14 = value;
		Il2CppCodeGenWriteBarrier(&___Ortho_14, value);
	}

	inline static int32_t get_offset_of_Iso_15() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___Iso_15)); }
	inline Camera_t2727095145 * get_Iso_15() const { return ___Iso_15; }
	inline Camera_t2727095145 ** get_address_of_Iso_15() { return &___Iso_15; }
	inline void set_Iso_15(Camera_t2727095145 * value)
	{
		___Iso_15 = value;
		Il2CppCodeGenWriteBarrier(&___Iso_15, value);
	}

	inline static int32_t get_offset_of_Interface_16() { return static_cast<int32_t>(offsetof(Shake_t79847142, ___Interface_16)); }
	inline Camera_t2727095145 * get_Interface_16() const { return ___Interface_16; }
	inline Camera_t2727095145 ** get_address_of_Interface_16() { return &___Interface_16; }
	inline void set_Interface_16(Camera_t2727095145 * value)
	{
		___Interface_16 = value;
		Il2CppCodeGenWriteBarrier(&___Interface_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
