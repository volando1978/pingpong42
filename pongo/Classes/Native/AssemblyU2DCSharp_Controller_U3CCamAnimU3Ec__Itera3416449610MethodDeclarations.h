﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Controller/<CamAnim>c__Iterator4
struct U3CCamAnimU3Ec__Iterator4_t3416449610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Controller/<CamAnim>c__Iterator4::.ctor()
extern "C"  void U3CCamAnimU3Ec__Iterator4__ctor_m3640395665 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Controller/<CamAnim>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCamAnimU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3244763883 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Controller/<CamAnim>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCamAnimU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3479856255 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller/<CamAnim>c__Iterator4::MoveNext()
extern "C"  bool U3CCamAnimU3Ec__Iterator4_MoveNext_m2045962411 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller/<CamAnim>c__Iterator4::Dispose()
extern "C"  void U3CCamAnimU3Ec__Iterator4_Dispose_m2215138190 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller/<CamAnim>c__Iterator4::Reset()
extern "C"  void U3CCamAnimU3Ec__Iterator4_Reset_m1286828606 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
