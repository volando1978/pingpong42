﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThidPersonExampleController
struct ThidPersonExampleController_t1811656226;

#include "codegen/il2cpp-codegen.h"

// System.Void ThidPersonExampleController::.ctor()
extern "C"  void ThidPersonExampleController__ctor_m20243541 (ThidPersonExampleController_t1811656226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThidPersonExampleController::OnEnable()
extern "C"  void ThidPersonExampleController_OnEnable_m3851486993 (ThidPersonExampleController_t1811656226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ThidPersonExampleController::Update()
extern "C"  void ThidPersonExampleController_Update_m2354411864 (ThidPersonExampleController_t1811656226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
