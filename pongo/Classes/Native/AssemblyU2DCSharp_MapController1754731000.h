﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// Dimensions
struct Dimensions_t2407799789;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapController
struct  MapController_t1754731000  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 MapController::x
	int32_t ___x_2;
	// System.Int32 MapController::y
	int32_t ___y_3;
	// System.Int32 MapController::z
	int32_t ___z_4;
	// UnityEngine.GameObject MapController::RoomObj
	GameObject_t3674682005 * ___RoomObj_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MapController::Rooms
	List_1_t747900261 * ___Rooms_6;
	// Dimensions MapController::dim
	Dimensions_t2407799789 * ___dim_7;
	// UnityEngine.RaycastHit MapController::hit
	RaycastHit_t4003175726  ___hit_8;
	// System.Single MapController::dragSpeed
	float ___dragSpeed_9;
	// UnityEngine.Vector3 MapController::dragOrigin
	Vector3_t4282066566  ___dragOrigin_10;
	// UnityEngine.GameObject MapController::Canvas
	GameObject_t3674682005 * ___Canvas_11;
	// UnityEngine.GameObject MapController::Panel
	GameObject_t3674682005 * ___Panel_12;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___z_4)); }
	inline int32_t get_z_4() const { return ___z_4; }
	inline int32_t* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(int32_t value)
	{
		___z_4 = value;
	}

	inline static int32_t get_offset_of_RoomObj_5() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___RoomObj_5)); }
	inline GameObject_t3674682005 * get_RoomObj_5() const { return ___RoomObj_5; }
	inline GameObject_t3674682005 ** get_address_of_RoomObj_5() { return &___RoomObj_5; }
	inline void set_RoomObj_5(GameObject_t3674682005 * value)
	{
		___RoomObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___RoomObj_5, value);
	}

	inline static int32_t get_offset_of_Rooms_6() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___Rooms_6)); }
	inline List_1_t747900261 * get_Rooms_6() const { return ___Rooms_6; }
	inline List_1_t747900261 ** get_address_of_Rooms_6() { return &___Rooms_6; }
	inline void set_Rooms_6(List_1_t747900261 * value)
	{
		___Rooms_6 = value;
		Il2CppCodeGenWriteBarrier(&___Rooms_6, value);
	}

	inline static int32_t get_offset_of_dim_7() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___dim_7)); }
	inline Dimensions_t2407799789 * get_dim_7() const { return ___dim_7; }
	inline Dimensions_t2407799789 ** get_address_of_dim_7() { return &___dim_7; }
	inline void set_dim_7(Dimensions_t2407799789 * value)
	{
		___dim_7 = value;
		Il2CppCodeGenWriteBarrier(&___dim_7, value);
	}

	inline static int32_t get_offset_of_hit_8() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___hit_8)); }
	inline RaycastHit_t4003175726  get_hit_8() const { return ___hit_8; }
	inline RaycastHit_t4003175726 * get_address_of_hit_8() { return &___hit_8; }
	inline void set_hit_8(RaycastHit_t4003175726  value)
	{
		___hit_8 = value;
	}

	inline static int32_t get_offset_of_dragSpeed_9() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___dragSpeed_9)); }
	inline float get_dragSpeed_9() const { return ___dragSpeed_9; }
	inline float* get_address_of_dragSpeed_9() { return &___dragSpeed_9; }
	inline void set_dragSpeed_9(float value)
	{
		___dragSpeed_9 = value;
	}

	inline static int32_t get_offset_of_dragOrigin_10() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___dragOrigin_10)); }
	inline Vector3_t4282066566  get_dragOrigin_10() const { return ___dragOrigin_10; }
	inline Vector3_t4282066566 * get_address_of_dragOrigin_10() { return &___dragOrigin_10; }
	inline void set_dragOrigin_10(Vector3_t4282066566  value)
	{
		___dragOrigin_10 = value;
	}

	inline static int32_t get_offset_of_Canvas_11() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___Canvas_11)); }
	inline GameObject_t3674682005 * get_Canvas_11() const { return ___Canvas_11; }
	inline GameObject_t3674682005 ** get_address_of_Canvas_11() { return &___Canvas_11; }
	inline void set_Canvas_11(GameObject_t3674682005 * value)
	{
		___Canvas_11 = value;
		Il2CppCodeGenWriteBarrier(&___Canvas_11, value);
	}

	inline static int32_t get_offset_of_Panel_12() { return static_cast<int32_t>(offsetof(MapController_t1754731000, ___Panel_12)); }
	inline GameObject_t3674682005 * get_Panel_12() const { return ___Panel_12; }
	inline GameObject_t3674682005 ** get_address_of_Panel_12() { return &___Panel_12; }
	inline void set_Panel_12(GameObject_t3674682005 * value)
	{
		___Panel_12 = value;
		Il2CppCodeGenWriteBarrier(&___Panel_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
