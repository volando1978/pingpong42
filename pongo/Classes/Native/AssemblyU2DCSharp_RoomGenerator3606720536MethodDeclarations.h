﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RoomGenerator
struct RoomGenerator_t3606720536;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void RoomGenerator::.ctor()
extern "C"  void RoomGenerator__ctor_m4112043091 (RoomGenerator_t3606720536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomGenerator::.cctor()
extern "C"  void RoomGenerator__cctor_m2437188026 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomGenerator::Update()
extern "C"  void RoomGenerator_Update_m351179034 (RoomGenerator_t3606720536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomGenerator::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void RoomGenerator_setCameras_m2717393161 (RoomGenerator_t3606720536 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 RoomGenerator::SetRoom(System.Int32)
extern "C"  Vector3_t4282066566  RoomGenerator_SetRoom_m4237980505 (RoomGenerator_t3606720536 * __this, int32_t ___fase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomGenerator::increaseFase(System.Int32)
extern "C"  void RoomGenerator_increaseFase_m279346609 (RoomGenerator_t3606720536 * __this, int32_t ___room0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomGenerator::GenerateRoom()
extern "C"  void RoomGenerator_GenerateRoom_m2374567553 (RoomGenerator_t3606720536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomGenerator::CreateWall(System.Int32,System.Int32)
extern "C"  void RoomGenerator_CreateWall_m4117356815 (RoomGenerator_t3606720536 * __this, int32_t ___ancho0, int32_t ___fondo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
