﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Plants_Tiempo1322696995.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Plants
struct  Plants_t2393071496  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Material[] Plants::materiales
	MaterialU5BU5D_t170856778* ___materiales_2;
	// UnityEngine.GameObject[] Plants::plantas
	GameObjectU5BU5D_t2662109048* ___plantas_3;
	// System.Int32[] Plants::indicesEntrada
	Int32U5BU5D_t3230847821* ___indicesEntrada_4;
	// System.Int32[] Plants::indicesSalida
	Int32U5BU5D_t3230847821* ___indicesSalida_5;
	// System.Int32[] Plants::indicesMateriales
	Int32U5BU5D_t3230847821* ___indicesMateriales_6;
	// UnityEngine.Camera Plants::Perspec
	Camera_t2727095145 * ___Perspec_7;
	// UnityEngine.Camera Plants::Ortho
	Camera_t2727095145 * ___Ortho_8;
	// UnityEngine.Camera Plants::Iso
	Camera_t2727095145 * ___Iso_9;
	// UnityEngine.Camera Plants::Interface
	Camera_t2727095145 * ___Interface_10;
	// UnityEngine.Color[] Plants::colors
	ColorU5BU5D_t2441545636* ___colors_11;
	// Plants/Tiempo Plants::currentTiempo
	int32_t ___currentTiempo_12;
	// UnityEngine.GameObject Plants::LluviaObj
	GameObject_t3674682005 * ___LluviaObj_13;
	// UnityEngine.GameObject Plants::NieveObj
	GameObject_t3674682005 * ___NieveObj_14;
	// UnityEngine.GameObject Plants::NieblaObj
	GameObject_t3674682005 * ___NieblaObj_15;
	// UnityEngine.GameObject Plants::ReflejosMarObj
	GameObject_t3674682005 * ___ReflejosMarObj_16;
	// UnityEngine.GameObject Plants::TruenoObj
	GameObject_t3674682005 * ___TruenoObj_17;
	// UnityEngine.GameObject Plants::LineasFlotacionObj
	GameObject_t3674682005 * ___LineasFlotacionObj_18;
	// UnityEngine.GameObject Plants::lluvia
	GameObject_t3674682005 * ___lluvia_19;
	// UnityEngine.GameObject Plants::nieve
	GameObject_t3674682005 * ___nieve_20;
	// UnityEngine.GameObject Plants::niebla
	GameObject_t3674682005 * ___niebla_21;
	// UnityEngine.GameObject Plants::reflejosMar
	GameObject_t3674682005 * ___reflejosMar_22;
	// UnityEngine.GameObject Plants::trueno
	GameObject_t3674682005 * ___trueno_23;
	// UnityEngine.GameObject Plants::lineasFlotacion
	GameObject_t3674682005 * ___lineasFlotacion_24;

public:
	inline static int32_t get_offset_of_materiales_2() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___materiales_2)); }
	inline MaterialU5BU5D_t170856778* get_materiales_2() const { return ___materiales_2; }
	inline MaterialU5BU5D_t170856778** get_address_of_materiales_2() { return &___materiales_2; }
	inline void set_materiales_2(MaterialU5BU5D_t170856778* value)
	{
		___materiales_2 = value;
		Il2CppCodeGenWriteBarrier(&___materiales_2, value);
	}

	inline static int32_t get_offset_of_plantas_3() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___plantas_3)); }
	inline GameObjectU5BU5D_t2662109048* get_plantas_3() const { return ___plantas_3; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_plantas_3() { return &___plantas_3; }
	inline void set_plantas_3(GameObjectU5BU5D_t2662109048* value)
	{
		___plantas_3 = value;
		Il2CppCodeGenWriteBarrier(&___plantas_3, value);
	}

	inline static int32_t get_offset_of_indicesEntrada_4() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___indicesEntrada_4)); }
	inline Int32U5BU5D_t3230847821* get_indicesEntrada_4() const { return ___indicesEntrada_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_indicesEntrada_4() { return &___indicesEntrada_4; }
	inline void set_indicesEntrada_4(Int32U5BU5D_t3230847821* value)
	{
		___indicesEntrada_4 = value;
		Il2CppCodeGenWriteBarrier(&___indicesEntrada_4, value);
	}

	inline static int32_t get_offset_of_indicesSalida_5() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___indicesSalida_5)); }
	inline Int32U5BU5D_t3230847821* get_indicesSalida_5() const { return ___indicesSalida_5; }
	inline Int32U5BU5D_t3230847821** get_address_of_indicesSalida_5() { return &___indicesSalida_5; }
	inline void set_indicesSalida_5(Int32U5BU5D_t3230847821* value)
	{
		___indicesSalida_5 = value;
		Il2CppCodeGenWriteBarrier(&___indicesSalida_5, value);
	}

	inline static int32_t get_offset_of_indicesMateriales_6() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___indicesMateriales_6)); }
	inline Int32U5BU5D_t3230847821* get_indicesMateriales_6() const { return ___indicesMateriales_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_indicesMateriales_6() { return &___indicesMateriales_6; }
	inline void set_indicesMateriales_6(Int32U5BU5D_t3230847821* value)
	{
		___indicesMateriales_6 = value;
		Il2CppCodeGenWriteBarrier(&___indicesMateriales_6, value);
	}

	inline static int32_t get_offset_of_Perspec_7() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___Perspec_7)); }
	inline Camera_t2727095145 * get_Perspec_7() const { return ___Perspec_7; }
	inline Camera_t2727095145 ** get_address_of_Perspec_7() { return &___Perspec_7; }
	inline void set_Perspec_7(Camera_t2727095145 * value)
	{
		___Perspec_7 = value;
		Il2CppCodeGenWriteBarrier(&___Perspec_7, value);
	}

	inline static int32_t get_offset_of_Ortho_8() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___Ortho_8)); }
	inline Camera_t2727095145 * get_Ortho_8() const { return ___Ortho_8; }
	inline Camera_t2727095145 ** get_address_of_Ortho_8() { return &___Ortho_8; }
	inline void set_Ortho_8(Camera_t2727095145 * value)
	{
		___Ortho_8 = value;
		Il2CppCodeGenWriteBarrier(&___Ortho_8, value);
	}

	inline static int32_t get_offset_of_Iso_9() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___Iso_9)); }
	inline Camera_t2727095145 * get_Iso_9() const { return ___Iso_9; }
	inline Camera_t2727095145 ** get_address_of_Iso_9() { return &___Iso_9; }
	inline void set_Iso_9(Camera_t2727095145 * value)
	{
		___Iso_9 = value;
		Il2CppCodeGenWriteBarrier(&___Iso_9, value);
	}

	inline static int32_t get_offset_of_Interface_10() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___Interface_10)); }
	inline Camera_t2727095145 * get_Interface_10() const { return ___Interface_10; }
	inline Camera_t2727095145 ** get_address_of_Interface_10() { return &___Interface_10; }
	inline void set_Interface_10(Camera_t2727095145 * value)
	{
		___Interface_10 = value;
		Il2CppCodeGenWriteBarrier(&___Interface_10, value);
	}

	inline static int32_t get_offset_of_colors_11() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___colors_11)); }
	inline ColorU5BU5D_t2441545636* get_colors_11() const { return ___colors_11; }
	inline ColorU5BU5D_t2441545636** get_address_of_colors_11() { return &___colors_11; }
	inline void set_colors_11(ColorU5BU5D_t2441545636* value)
	{
		___colors_11 = value;
		Il2CppCodeGenWriteBarrier(&___colors_11, value);
	}

	inline static int32_t get_offset_of_currentTiempo_12() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___currentTiempo_12)); }
	inline int32_t get_currentTiempo_12() const { return ___currentTiempo_12; }
	inline int32_t* get_address_of_currentTiempo_12() { return &___currentTiempo_12; }
	inline void set_currentTiempo_12(int32_t value)
	{
		___currentTiempo_12 = value;
	}

	inline static int32_t get_offset_of_LluviaObj_13() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___LluviaObj_13)); }
	inline GameObject_t3674682005 * get_LluviaObj_13() const { return ___LluviaObj_13; }
	inline GameObject_t3674682005 ** get_address_of_LluviaObj_13() { return &___LluviaObj_13; }
	inline void set_LluviaObj_13(GameObject_t3674682005 * value)
	{
		___LluviaObj_13 = value;
		Il2CppCodeGenWriteBarrier(&___LluviaObj_13, value);
	}

	inline static int32_t get_offset_of_NieveObj_14() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___NieveObj_14)); }
	inline GameObject_t3674682005 * get_NieveObj_14() const { return ___NieveObj_14; }
	inline GameObject_t3674682005 ** get_address_of_NieveObj_14() { return &___NieveObj_14; }
	inline void set_NieveObj_14(GameObject_t3674682005 * value)
	{
		___NieveObj_14 = value;
		Il2CppCodeGenWriteBarrier(&___NieveObj_14, value);
	}

	inline static int32_t get_offset_of_NieblaObj_15() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___NieblaObj_15)); }
	inline GameObject_t3674682005 * get_NieblaObj_15() const { return ___NieblaObj_15; }
	inline GameObject_t3674682005 ** get_address_of_NieblaObj_15() { return &___NieblaObj_15; }
	inline void set_NieblaObj_15(GameObject_t3674682005 * value)
	{
		___NieblaObj_15 = value;
		Il2CppCodeGenWriteBarrier(&___NieblaObj_15, value);
	}

	inline static int32_t get_offset_of_ReflejosMarObj_16() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___ReflejosMarObj_16)); }
	inline GameObject_t3674682005 * get_ReflejosMarObj_16() const { return ___ReflejosMarObj_16; }
	inline GameObject_t3674682005 ** get_address_of_ReflejosMarObj_16() { return &___ReflejosMarObj_16; }
	inline void set_ReflejosMarObj_16(GameObject_t3674682005 * value)
	{
		___ReflejosMarObj_16 = value;
		Il2CppCodeGenWriteBarrier(&___ReflejosMarObj_16, value);
	}

	inline static int32_t get_offset_of_TruenoObj_17() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___TruenoObj_17)); }
	inline GameObject_t3674682005 * get_TruenoObj_17() const { return ___TruenoObj_17; }
	inline GameObject_t3674682005 ** get_address_of_TruenoObj_17() { return &___TruenoObj_17; }
	inline void set_TruenoObj_17(GameObject_t3674682005 * value)
	{
		___TruenoObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___TruenoObj_17, value);
	}

	inline static int32_t get_offset_of_LineasFlotacionObj_18() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___LineasFlotacionObj_18)); }
	inline GameObject_t3674682005 * get_LineasFlotacionObj_18() const { return ___LineasFlotacionObj_18; }
	inline GameObject_t3674682005 ** get_address_of_LineasFlotacionObj_18() { return &___LineasFlotacionObj_18; }
	inline void set_LineasFlotacionObj_18(GameObject_t3674682005 * value)
	{
		___LineasFlotacionObj_18 = value;
		Il2CppCodeGenWriteBarrier(&___LineasFlotacionObj_18, value);
	}

	inline static int32_t get_offset_of_lluvia_19() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___lluvia_19)); }
	inline GameObject_t3674682005 * get_lluvia_19() const { return ___lluvia_19; }
	inline GameObject_t3674682005 ** get_address_of_lluvia_19() { return &___lluvia_19; }
	inline void set_lluvia_19(GameObject_t3674682005 * value)
	{
		___lluvia_19 = value;
		Il2CppCodeGenWriteBarrier(&___lluvia_19, value);
	}

	inline static int32_t get_offset_of_nieve_20() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___nieve_20)); }
	inline GameObject_t3674682005 * get_nieve_20() const { return ___nieve_20; }
	inline GameObject_t3674682005 ** get_address_of_nieve_20() { return &___nieve_20; }
	inline void set_nieve_20(GameObject_t3674682005 * value)
	{
		___nieve_20 = value;
		Il2CppCodeGenWriteBarrier(&___nieve_20, value);
	}

	inline static int32_t get_offset_of_niebla_21() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___niebla_21)); }
	inline GameObject_t3674682005 * get_niebla_21() const { return ___niebla_21; }
	inline GameObject_t3674682005 ** get_address_of_niebla_21() { return &___niebla_21; }
	inline void set_niebla_21(GameObject_t3674682005 * value)
	{
		___niebla_21 = value;
		Il2CppCodeGenWriteBarrier(&___niebla_21, value);
	}

	inline static int32_t get_offset_of_reflejosMar_22() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___reflejosMar_22)); }
	inline GameObject_t3674682005 * get_reflejosMar_22() const { return ___reflejosMar_22; }
	inline GameObject_t3674682005 ** get_address_of_reflejosMar_22() { return &___reflejosMar_22; }
	inline void set_reflejosMar_22(GameObject_t3674682005 * value)
	{
		___reflejosMar_22 = value;
		Il2CppCodeGenWriteBarrier(&___reflejosMar_22, value);
	}

	inline static int32_t get_offset_of_trueno_23() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___trueno_23)); }
	inline GameObject_t3674682005 * get_trueno_23() const { return ___trueno_23; }
	inline GameObject_t3674682005 ** get_address_of_trueno_23() { return &___trueno_23; }
	inline void set_trueno_23(GameObject_t3674682005 * value)
	{
		___trueno_23 = value;
		Il2CppCodeGenWriteBarrier(&___trueno_23, value);
	}

	inline static int32_t get_offset_of_lineasFlotacion_24() { return static_cast<int32_t>(offsetof(Plants_t2393071496, ___lineasFlotacion_24)); }
	inline GameObject_t3674682005 * get_lineasFlotacion_24() const { return ___lineasFlotacion_24; }
	inline GameObject_t3674682005 ** get_address_of_lineasFlotacion_24() { return &___lineasFlotacion_24; }
	inline void set_lineasFlotacion_24(GameObject_t3674682005 * value)
	{
		___lineasFlotacion_24 = value;
		Il2CppCodeGenWriteBarrier(&___lineasFlotacion_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
