﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CnControls.SensitiveJoystick
struct SensitiveJoystick_t1697973888;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void CnControls.SensitiveJoystick::.ctor()
extern "C"  void SensitiveJoystick__ctor_m3620960106 (SensitiveJoystick_t1697973888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.SensitiveJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SensitiveJoystick_OnDrag_m1401735889 (SensitiveJoystick_t1697973888 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
