﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomGenerator
struct  RoomGenerator_t3606720536  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject RoomGenerator::LadrilloObj
	GameObject_t3674682005 * ___LadrilloObj_2;
	// UnityEngine.GameObject RoomGenerator::WallObj
	GameObject_t3674682005 * ___WallObj_3;
	// UnityEngine.GameObject RoomGenerator::Wall
	GameObject_t3674682005 * ___Wall_4;
	// UnityEngine.Material RoomGenerator::Cristal
	Material_t3870600107 * ___Cristal_5;
	// UnityEngine.Material RoomGenerator::Pared
	Material_t3870600107 * ___Pared_6;
	// UnityEngine.GameObject RoomGenerator::Shadow
	GameObject_t3674682005 * ___Shadow_7;
	// UnityEngine.Camera RoomGenerator::CameraMan
	Camera_t2727095145 * ___CameraMan_8;
	// UnityEngine.GameObject RoomGenerator::Player
	GameObject_t3674682005 * ___Player_9;
	// UnityEngine.GameObject RoomGenerator::Ball
	GameObject_t3674682005 * ___Ball_10;
	// UnityEngine.GameObject RoomGenerator::Controller
	GameObject_t3674682005 * ___Controller_11;
	// UnityEngine.GameObject RoomGenerator::Plantación
	GameObject_t3674682005 * ___PlantaciUF3n_12;
	// System.Int32 RoomGenerator::distanciaZCam
	int32_t ___distanciaZCam_13;
	// System.Int32 RoomGenerator::alturaCam
	int32_t ___alturaCam_14;
	// System.Single RoomGenerator::umbralRebotebounds
	float ___umbralRebotebounds_15;
	// UnityEngine.Vector3 RoomGenerator::LockCam
	Vector3_t4282066566  ___LockCam_16;
	// UnityEngine.Vector3 RoomGenerator::LockPlayer
	Vector3_t4282066566  ___LockPlayer_17;
	// UnityEngine.Vector3 RoomGenerator::LockBall
	Vector3_t4282066566  ___LockBall_18;
	// UnityEngine.Camera RoomGenerator::Perspec
	Camera_t2727095145 * ___Perspec_19;
	// UnityEngine.Camera RoomGenerator::Ortho
	Camera_t2727095145 * ___Ortho_20;
	// UnityEngine.Camera RoomGenerator::Iso
	Camera_t2727095145 * ___Iso_21;
	// UnityEngine.Camera RoomGenerator::Interface
	Camera_t2727095145 * ___Interface_22;
	// UnityEngine.GameObject RoomGenerator::Container
	GameObject_t3674682005 * ___Container_23;
	// UnityEngine.Vector3 RoomGenerator::WallPos
	Vector3_t4282066566  ___WallPos_24;
	// System.Int32 RoomGenerator::room
	int32_t ___room_27;
	// System.Int32 RoomGenerator::alto
	int32_t ___alto_28;
	// System.Int32 RoomGenerator::ancho
	int32_t ___ancho_29;
	// System.Int32 RoomGenerator::fondo
	int32_t ___fondo_30;
	// System.Int32 RoomGenerator::minAlto
	int32_t ___minAlto_31;
	// System.Int32 RoomGenerator::minAncho
	int32_t ___minAncho_32;
	// System.Int32 RoomGenerator::minFondo
	int32_t ___minFondo_33;
	// System.Int32 RoomGenerator::maxAlto
	int32_t ___maxAlto_34;
	// System.Int32 RoomGenerator::maxAncho
	int32_t ___maxAncho_35;
	// System.Int32 RoomGenerator::maxFondo
	int32_t ___maxFondo_36;

public:
	inline static int32_t get_offset_of_LadrilloObj_2() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___LadrilloObj_2)); }
	inline GameObject_t3674682005 * get_LadrilloObj_2() const { return ___LadrilloObj_2; }
	inline GameObject_t3674682005 ** get_address_of_LadrilloObj_2() { return &___LadrilloObj_2; }
	inline void set_LadrilloObj_2(GameObject_t3674682005 * value)
	{
		___LadrilloObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___LadrilloObj_2, value);
	}

	inline static int32_t get_offset_of_WallObj_3() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___WallObj_3)); }
	inline GameObject_t3674682005 * get_WallObj_3() const { return ___WallObj_3; }
	inline GameObject_t3674682005 ** get_address_of_WallObj_3() { return &___WallObj_3; }
	inline void set_WallObj_3(GameObject_t3674682005 * value)
	{
		___WallObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___WallObj_3, value);
	}

	inline static int32_t get_offset_of_Wall_4() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Wall_4)); }
	inline GameObject_t3674682005 * get_Wall_4() const { return ___Wall_4; }
	inline GameObject_t3674682005 ** get_address_of_Wall_4() { return &___Wall_4; }
	inline void set_Wall_4(GameObject_t3674682005 * value)
	{
		___Wall_4 = value;
		Il2CppCodeGenWriteBarrier(&___Wall_4, value);
	}

	inline static int32_t get_offset_of_Cristal_5() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Cristal_5)); }
	inline Material_t3870600107 * get_Cristal_5() const { return ___Cristal_5; }
	inline Material_t3870600107 ** get_address_of_Cristal_5() { return &___Cristal_5; }
	inline void set_Cristal_5(Material_t3870600107 * value)
	{
		___Cristal_5 = value;
		Il2CppCodeGenWriteBarrier(&___Cristal_5, value);
	}

	inline static int32_t get_offset_of_Pared_6() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Pared_6)); }
	inline Material_t3870600107 * get_Pared_6() const { return ___Pared_6; }
	inline Material_t3870600107 ** get_address_of_Pared_6() { return &___Pared_6; }
	inline void set_Pared_6(Material_t3870600107 * value)
	{
		___Pared_6 = value;
		Il2CppCodeGenWriteBarrier(&___Pared_6, value);
	}

	inline static int32_t get_offset_of_Shadow_7() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Shadow_7)); }
	inline GameObject_t3674682005 * get_Shadow_7() const { return ___Shadow_7; }
	inline GameObject_t3674682005 ** get_address_of_Shadow_7() { return &___Shadow_7; }
	inline void set_Shadow_7(GameObject_t3674682005 * value)
	{
		___Shadow_7 = value;
		Il2CppCodeGenWriteBarrier(&___Shadow_7, value);
	}

	inline static int32_t get_offset_of_CameraMan_8() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___CameraMan_8)); }
	inline Camera_t2727095145 * get_CameraMan_8() const { return ___CameraMan_8; }
	inline Camera_t2727095145 ** get_address_of_CameraMan_8() { return &___CameraMan_8; }
	inline void set_CameraMan_8(Camera_t2727095145 * value)
	{
		___CameraMan_8 = value;
		Il2CppCodeGenWriteBarrier(&___CameraMan_8, value);
	}

	inline static int32_t get_offset_of_Player_9() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Player_9)); }
	inline GameObject_t3674682005 * get_Player_9() const { return ___Player_9; }
	inline GameObject_t3674682005 ** get_address_of_Player_9() { return &___Player_9; }
	inline void set_Player_9(GameObject_t3674682005 * value)
	{
		___Player_9 = value;
		Il2CppCodeGenWriteBarrier(&___Player_9, value);
	}

	inline static int32_t get_offset_of_Ball_10() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Ball_10)); }
	inline GameObject_t3674682005 * get_Ball_10() const { return ___Ball_10; }
	inline GameObject_t3674682005 ** get_address_of_Ball_10() { return &___Ball_10; }
	inline void set_Ball_10(GameObject_t3674682005 * value)
	{
		___Ball_10 = value;
		Il2CppCodeGenWriteBarrier(&___Ball_10, value);
	}

	inline static int32_t get_offset_of_Controller_11() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Controller_11)); }
	inline GameObject_t3674682005 * get_Controller_11() const { return ___Controller_11; }
	inline GameObject_t3674682005 ** get_address_of_Controller_11() { return &___Controller_11; }
	inline void set_Controller_11(GameObject_t3674682005 * value)
	{
		___Controller_11 = value;
		Il2CppCodeGenWriteBarrier(&___Controller_11, value);
	}

	inline static int32_t get_offset_of_PlantaciUF3n_12() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___PlantaciUF3n_12)); }
	inline GameObject_t3674682005 * get_PlantaciUF3n_12() const { return ___PlantaciUF3n_12; }
	inline GameObject_t3674682005 ** get_address_of_PlantaciUF3n_12() { return &___PlantaciUF3n_12; }
	inline void set_PlantaciUF3n_12(GameObject_t3674682005 * value)
	{
		___PlantaciUF3n_12 = value;
		Il2CppCodeGenWriteBarrier(&___PlantaciUF3n_12, value);
	}

	inline static int32_t get_offset_of_distanciaZCam_13() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___distanciaZCam_13)); }
	inline int32_t get_distanciaZCam_13() const { return ___distanciaZCam_13; }
	inline int32_t* get_address_of_distanciaZCam_13() { return &___distanciaZCam_13; }
	inline void set_distanciaZCam_13(int32_t value)
	{
		___distanciaZCam_13 = value;
	}

	inline static int32_t get_offset_of_alturaCam_14() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___alturaCam_14)); }
	inline int32_t get_alturaCam_14() const { return ___alturaCam_14; }
	inline int32_t* get_address_of_alturaCam_14() { return &___alturaCam_14; }
	inline void set_alturaCam_14(int32_t value)
	{
		___alturaCam_14 = value;
	}

	inline static int32_t get_offset_of_umbralRebotebounds_15() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___umbralRebotebounds_15)); }
	inline float get_umbralRebotebounds_15() const { return ___umbralRebotebounds_15; }
	inline float* get_address_of_umbralRebotebounds_15() { return &___umbralRebotebounds_15; }
	inline void set_umbralRebotebounds_15(float value)
	{
		___umbralRebotebounds_15 = value;
	}

	inline static int32_t get_offset_of_LockCam_16() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___LockCam_16)); }
	inline Vector3_t4282066566  get_LockCam_16() const { return ___LockCam_16; }
	inline Vector3_t4282066566 * get_address_of_LockCam_16() { return &___LockCam_16; }
	inline void set_LockCam_16(Vector3_t4282066566  value)
	{
		___LockCam_16 = value;
	}

	inline static int32_t get_offset_of_LockPlayer_17() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___LockPlayer_17)); }
	inline Vector3_t4282066566  get_LockPlayer_17() const { return ___LockPlayer_17; }
	inline Vector3_t4282066566 * get_address_of_LockPlayer_17() { return &___LockPlayer_17; }
	inline void set_LockPlayer_17(Vector3_t4282066566  value)
	{
		___LockPlayer_17 = value;
	}

	inline static int32_t get_offset_of_LockBall_18() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___LockBall_18)); }
	inline Vector3_t4282066566  get_LockBall_18() const { return ___LockBall_18; }
	inline Vector3_t4282066566 * get_address_of_LockBall_18() { return &___LockBall_18; }
	inline void set_LockBall_18(Vector3_t4282066566  value)
	{
		___LockBall_18 = value;
	}

	inline static int32_t get_offset_of_Perspec_19() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Perspec_19)); }
	inline Camera_t2727095145 * get_Perspec_19() const { return ___Perspec_19; }
	inline Camera_t2727095145 ** get_address_of_Perspec_19() { return &___Perspec_19; }
	inline void set_Perspec_19(Camera_t2727095145 * value)
	{
		___Perspec_19 = value;
		Il2CppCodeGenWriteBarrier(&___Perspec_19, value);
	}

	inline static int32_t get_offset_of_Ortho_20() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Ortho_20)); }
	inline Camera_t2727095145 * get_Ortho_20() const { return ___Ortho_20; }
	inline Camera_t2727095145 ** get_address_of_Ortho_20() { return &___Ortho_20; }
	inline void set_Ortho_20(Camera_t2727095145 * value)
	{
		___Ortho_20 = value;
		Il2CppCodeGenWriteBarrier(&___Ortho_20, value);
	}

	inline static int32_t get_offset_of_Iso_21() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Iso_21)); }
	inline Camera_t2727095145 * get_Iso_21() const { return ___Iso_21; }
	inline Camera_t2727095145 ** get_address_of_Iso_21() { return &___Iso_21; }
	inline void set_Iso_21(Camera_t2727095145 * value)
	{
		___Iso_21 = value;
		Il2CppCodeGenWriteBarrier(&___Iso_21, value);
	}

	inline static int32_t get_offset_of_Interface_22() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Interface_22)); }
	inline Camera_t2727095145 * get_Interface_22() const { return ___Interface_22; }
	inline Camera_t2727095145 ** get_address_of_Interface_22() { return &___Interface_22; }
	inline void set_Interface_22(Camera_t2727095145 * value)
	{
		___Interface_22 = value;
		Il2CppCodeGenWriteBarrier(&___Interface_22, value);
	}

	inline static int32_t get_offset_of_Container_23() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___Container_23)); }
	inline GameObject_t3674682005 * get_Container_23() const { return ___Container_23; }
	inline GameObject_t3674682005 ** get_address_of_Container_23() { return &___Container_23; }
	inline void set_Container_23(GameObject_t3674682005 * value)
	{
		___Container_23 = value;
		Il2CppCodeGenWriteBarrier(&___Container_23, value);
	}

	inline static int32_t get_offset_of_WallPos_24() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___WallPos_24)); }
	inline Vector3_t4282066566  get_WallPos_24() const { return ___WallPos_24; }
	inline Vector3_t4282066566 * get_address_of_WallPos_24() { return &___WallPos_24; }
	inline void set_WallPos_24(Vector3_t4282066566  value)
	{
		___WallPos_24 = value;
	}

	inline static int32_t get_offset_of_room_27() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___room_27)); }
	inline int32_t get_room_27() const { return ___room_27; }
	inline int32_t* get_address_of_room_27() { return &___room_27; }
	inline void set_room_27(int32_t value)
	{
		___room_27 = value;
	}

	inline static int32_t get_offset_of_alto_28() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___alto_28)); }
	inline int32_t get_alto_28() const { return ___alto_28; }
	inline int32_t* get_address_of_alto_28() { return &___alto_28; }
	inline void set_alto_28(int32_t value)
	{
		___alto_28 = value;
	}

	inline static int32_t get_offset_of_ancho_29() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___ancho_29)); }
	inline int32_t get_ancho_29() const { return ___ancho_29; }
	inline int32_t* get_address_of_ancho_29() { return &___ancho_29; }
	inline void set_ancho_29(int32_t value)
	{
		___ancho_29 = value;
	}

	inline static int32_t get_offset_of_fondo_30() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___fondo_30)); }
	inline int32_t get_fondo_30() const { return ___fondo_30; }
	inline int32_t* get_address_of_fondo_30() { return &___fondo_30; }
	inline void set_fondo_30(int32_t value)
	{
		___fondo_30 = value;
	}

	inline static int32_t get_offset_of_minAlto_31() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___minAlto_31)); }
	inline int32_t get_minAlto_31() const { return ___minAlto_31; }
	inline int32_t* get_address_of_minAlto_31() { return &___minAlto_31; }
	inline void set_minAlto_31(int32_t value)
	{
		___minAlto_31 = value;
	}

	inline static int32_t get_offset_of_minAncho_32() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___minAncho_32)); }
	inline int32_t get_minAncho_32() const { return ___minAncho_32; }
	inline int32_t* get_address_of_minAncho_32() { return &___minAncho_32; }
	inline void set_minAncho_32(int32_t value)
	{
		___minAncho_32 = value;
	}

	inline static int32_t get_offset_of_minFondo_33() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___minFondo_33)); }
	inline int32_t get_minFondo_33() const { return ___minFondo_33; }
	inline int32_t* get_address_of_minFondo_33() { return &___minFondo_33; }
	inline void set_minFondo_33(int32_t value)
	{
		___minFondo_33 = value;
	}

	inline static int32_t get_offset_of_maxAlto_34() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___maxAlto_34)); }
	inline int32_t get_maxAlto_34() const { return ___maxAlto_34; }
	inline int32_t* get_address_of_maxAlto_34() { return &___maxAlto_34; }
	inline void set_maxAlto_34(int32_t value)
	{
		___maxAlto_34 = value;
	}

	inline static int32_t get_offset_of_maxAncho_35() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___maxAncho_35)); }
	inline int32_t get_maxAncho_35() const { return ___maxAncho_35; }
	inline int32_t* get_address_of_maxAncho_35() { return &___maxAncho_35; }
	inline void set_maxAncho_35(int32_t value)
	{
		___maxAncho_35 = value;
	}

	inline static int32_t get_offset_of_maxFondo_36() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536, ___maxFondo_36)); }
	inline int32_t get_maxFondo_36() const { return ___maxFondo_36; }
	inline int32_t* get_address_of_maxFondo_36() { return &___maxFondo_36; }
	inline void set_maxFondo_36(int32_t value)
	{
		___maxFondo_36 = value;
	}
};

struct RoomGenerator_t3606720536_StaticFields
{
public:
	// System.Int32 RoomGenerator::fase
	int32_t ___fase_25;
	// System.Int32 RoomGenerator::pasaFaseFactor
	int32_t ___pasaFaseFactor_26;

public:
	inline static int32_t get_offset_of_fase_25() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536_StaticFields, ___fase_25)); }
	inline int32_t get_fase_25() const { return ___fase_25; }
	inline int32_t* get_address_of_fase_25() { return &___fase_25; }
	inline void set_fase_25(int32_t value)
	{
		___fase_25 = value;
	}

	inline static int32_t get_offset_of_pasaFaseFactor_26() { return static_cast<int32_t>(offsetof(RoomGenerator_t3606720536_StaticFields, ___pasaFaseFactor_26)); }
	inline int32_t get_pasaFaseFactor_26() const { return ___pasaFaseFactor_26; }
	inline int32_t* get_address_of_pasaFaseFactor_26() { return &___pasaFaseFactor_26; }
	inline void set_pasaFaseFactor_26(int32_t value)
	{
		___pasaFaseFactor_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
