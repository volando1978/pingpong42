﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BallCollider/<BigBallTime>c__Iterator0
struct U3CBigBallTimeU3Ec__Iterator0_t1350130161;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BallCollider/<BigBallTime>c__Iterator0::.ctor()
extern "C"  void U3CBigBallTimeU3Ec__Iterator0__ctor_m3073329610 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BallCollider/<BigBallTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBigBallTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m525618834 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BallCollider/<BigBallTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBigBallTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1833547302 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BallCollider/<BigBallTime>c__Iterator0::MoveNext()
extern "C"  bool U3CBigBallTimeU3Ec__Iterator0_MoveNext_m2782394578 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallCollider/<BigBallTime>c__Iterator0::Dispose()
extern "C"  void U3CBigBallTimeU3Ec__Iterator0_Dispose_m2725505927 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallCollider/<BigBallTime>c__Iterator0::Reset()
extern "C"  void U3CBigBallTimeU3Ec__Iterator0_Reset_m719762551 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
