﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CnControls.VirtualButton
struct VirtualButton_t3335323691;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CnControls.VirtualButton::.ctor(System.String)
extern "C"  void VirtualButton__ctor_m2558924099 (VirtualButton_t3335323691 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CnControls.VirtualButton::get_Name()
extern "C"  String_t* VirtualButton_get_Name_m3237498300 (VirtualButton_t3335323691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.VirtualButton::set_Name(System.String)
extern "C"  void VirtualButton_set_Name_m1959416469 (VirtualButton_t3335323691 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.VirtualButton::get_IsPressed()
extern "C"  bool VirtualButton_get_IsPressed_m3924498768 (VirtualButton_t3335323691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.VirtualButton::set_IsPressed(System.Boolean)
extern "C"  void VirtualButton_set_IsPressed_m711185903 (VirtualButton_t3335323691 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.VirtualButton::Press()
extern "C"  void VirtualButton_Press_m961444032 (VirtualButton_t3335323691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.VirtualButton::Release()
extern "C"  void VirtualButton_Release_m1530526852 (VirtualButton_t3335323691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.VirtualButton::get_GetButton()
extern "C"  bool VirtualButton_get_GetButton_m330304480 (VirtualButton_t3335323691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.VirtualButton::get_GetButtonDown()
extern "C"  bool VirtualButton_get_GetButtonDown_m2531325090 (VirtualButton_t3335323691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.VirtualButton::get_GetButtonUp()
extern "C"  bool VirtualButton_get_GetButtonUp_m3891402779 (VirtualButton_t3335323691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
