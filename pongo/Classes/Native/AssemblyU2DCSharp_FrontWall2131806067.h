﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FrontWall
struct  FrontWall_t2131806067  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject FrontWall::BrickObj
	GameObject_t3674682005 * ___BrickObj_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> FrontWall::Bricks
	List_1_t747900261 * ___Bricks_3;
	// System.Single FrontWall::timeCreating
	float ___timeCreating_4;

public:
	inline static int32_t get_offset_of_BrickObj_2() { return static_cast<int32_t>(offsetof(FrontWall_t2131806067, ___BrickObj_2)); }
	inline GameObject_t3674682005 * get_BrickObj_2() const { return ___BrickObj_2; }
	inline GameObject_t3674682005 ** get_address_of_BrickObj_2() { return &___BrickObj_2; }
	inline void set_BrickObj_2(GameObject_t3674682005 * value)
	{
		___BrickObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___BrickObj_2, value);
	}

	inline static int32_t get_offset_of_Bricks_3() { return static_cast<int32_t>(offsetof(FrontWall_t2131806067, ___Bricks_3)); }
	inline List_1_t747900261 * get_Bricks_3() const { return ___Bricks_3; }
	inline List_1_t747900261 ** get_address_of_Bricks_3() { return &___Bricks_3; }
	inline void set_Bricks_3(List_1_t747900261 * value)
	{
		___Bricks_3 = value;
		Il2CppCodeGenWriteBarrier(&___Bricks_3, value);
	}

	inline static int32_t get_offset_of_timeCreating_4() { return static_cast<int32_t>(offsetof(FrontWall_t2131806067, ___timeCreating_4)); }
	inline float get_timeCreating_4() const { return ___timeCreating_4; }
	inline float* get_address_of_timeCreating_4() { return &___timeCreating_4; }
	inline void set_timeCreating_4(float value)
	{
		___timeCreating_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
