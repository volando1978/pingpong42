﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Brick
struct Brick_t64452641;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void Brick::.ctor()
extern "C"  void Brick__ctor_m1887233642 (Brick_t64452641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Brick::Start()
extern "C"  void Brick_Start_m834371434 (Brick_t64452641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Brick::kill(UnityEngine.Vector3,UnityEngine.GameObject)
extern "C"  void Brick_kill_m2419377885 (Brick_t64452641 * __this, Vector3_t4282066566  ___pos0, GameObject_t3674682005 * ___Controller1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Brick::RestaToque()
extern "C"  void Brick_RestaToque_m2346085363 (Brick_t64452641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Brick::checkToques(UnityEngine.Vector3,UnityEngine.GameObject)
extern "C"  void Brick_checkToques_m2912556642 (Brick_t64452641 * __this, Vector3_t4282066566  ___pos0, GameObject_t3674682005 * ___Controller1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
