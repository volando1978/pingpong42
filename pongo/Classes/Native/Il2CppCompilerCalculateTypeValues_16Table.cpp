﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic_Cull2290505109.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskUtilities3879688356.h"
#include "UnityEngine_UI_UnityEngine_UI_Misc8834360.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation1108456480.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode356329147.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage821930207.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D3357079374.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar2601556940.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction522766867.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEven3541123425.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis4294105229.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRepe99988271.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect3606982749.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementTy300513412.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarV184977789.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRec1643322606.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1885181538.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transitio1922345195.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection1293548283.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility1171612705.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction94975348.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2627072750.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis3565360268.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState2895308594.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial639665897.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE1574154081.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle110812896.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit2757337633.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent2331340366.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1990156785.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1074114320.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping1257491342.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli1294793591.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter436718473.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As2149445162.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2777732396.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMo2493957633.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1153512176.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit1837657360.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1285073872.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fit909765868.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup169317941.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corne284493240.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1399125956.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons1640775616.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou1336501463.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical2052396382.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement1596995480.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup352294875.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder1942933988.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility3144854024.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup423167365.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper3377436606.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect3555037586.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2306480155.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline3745177896.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV14062429115.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow75537580.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3379220348.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_CreateMap2602318496.h"
#include "AssemblyU2DCSharp_BallShadow3766914079.h"
#include "AssemblyU2DCSharp_BallCollider3766478643.h"
#include "AssemblyU2DCSharp_BallCollider_U3CBigBallTimeU3Ec_1350130161.h"
#include "AssemblyU2DCSharp_BallCollider_U3CHitColorU3Ec__It1194571374.h"
#include "AssemblyU2DCSharp_Blanco1992262851.h"
#include "AssemblyU2DCSharp_Blanco_U3CPlofU3Ec__Iterator21800312444.h"
#include "AssemblyU2DCSharp_BouncingAnim2247160086.h"
#include "AssemblyU2DCSharp_Brazo64445670.h"
#include "AssemblyU2DCSharp_Brick64452641.h"
#include "AssemblyU2DCSharp_ColliderPlayer3441697813.h"
#include "AssemblyU2DCSharp_ColliderPlayer_U3CflanU3Ec__Iter3689796459.h"
#include "AssemblyU2DCSharp_Controller2630893500.h"
#include "AssemblyU2DCSharp_Controller_State2540969054.h"
#include "AssemblyU2DCSharp_Controller_U3CCamAnimU3Ec__Itera3416449610.h"
#include "AssemblyU2DCSharp_Controller_U3CCortinaU3Ec__Itera3064932893.h"
#include "AssemblyU2DCSharp_Controller_U3CrunScoreU3Ec__Iter3971101203.h"
#include "AssemblyU2DCSharp_Dimensions2407799789.h"
#include "AssemblyU2DCSharp_FrontWall2131806067.h"
#include "AssemblyU2DCSharp_MapController1754731000.h"
#include "AssemblyU2DCSharp_Move2404337.h"
#include "AssemblyU2DCSharp_MoveBall4254648720.h"
#include "AssemblyU2DCSharp_Plants2393071496.h"
#include "AssemblyU2DCSharp_Plants_Tiempo1322696995.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (CullStateChangedEvent_t2290505109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (MaskUtilities_t3879688356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (Misc_t8834360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (Navigation_t1108456480)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[5] = 
{
	Navigation_t1108456480::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1108456480::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1108456480::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1108456480::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1108456480::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (Mode_t356329147)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1604[6] = 
{
	Mode_t356329147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (RawImage_t821930207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[2] = 
{
	RawImage_t821930207::get_offset_of_m_Texture_28(),
	RawImage_t821930207::get_offset_of_m_UVRect_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (RectMask2D_t3357079374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1606[8] = 
{
	RectMask2D_t3357079374::get_offset_of_m_VertexClipper_2(),
	RectMask2D_t3357079374::get_offset_of_m_RectTransform_3(),
	RectMask2D_t3357079374::get_offset_of_m_ClipTargets_4(),
	RectMask2D_t3357079374::get_offset_of_m_ShouldRecalculateClipRects_5(),
	RectMask2D_t3357079374::get_offset_of_m_Clippers_6(),
	RectMask2D_t3357079374::get_offset_of_m_LastClipRectCanvasSpace_7(),
	RectMask2D_t3357079374::get_offset_of_m_LastValidClipRect_8(),
	RectMask2D_t3357079374::get_offset_of_m_ForceClip_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (Scrollbar_t2601556940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[11] = 
{
	Scrollbar_t2601556940::get_offset_of_m_HandleRect_16(),
	Scrollbar_t2601556940::get_offset_of_m_Direction_17(),
	Scrollbar_t2601556940::get_offset_of_m_Value_18(),
	Scrollbar_t2601556940::get_offset_of_m_Size_19(),
	Scrollbar_t2601556940::get_offset_of_m_NumberOfSteps_20(),
	Scrollbar_t2601556940::get_offset_of_m_OnValueChanged_21(),
	Scrollbar_t2601556940::get_offset_of_m_ContainerRect_22(),
	Scrollbar_t2601556940::get_offset_of_m_Offset_23(),
	Scrollbar_t2601556940::get_offset_of_m_Tracker_24(),
	Scrollbar_t2601556940::get_offset_of_m_PointerDownRepeat_25(),
	Scrollbar_t2601556940::get_offset_of_isPointerDownAndNotDragging_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (Direction_t522766867)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1608[5] = 
{
	Direction_t522766867::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (ScrollEvent_t3541123425), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (Axis_t4294105229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1610[3] = 
{
	Axis_t4294105229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (U3CClickRepeatU3Ec__Iterator5_t99988271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1611[7] = 
{
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3ClocalMousePosU3E__0_1(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CaxisCoordinateU3E__1_2(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U24PC_3(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U24current_4(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CU24U3EeventData_5(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (ScrollRect_t3606982749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[36] = 
{
	ScrollRect_t3606982749::get_offset_of_m_Content_2(),
	ScrollRect_t3606982749::get_offset_of_m_Horizontal_3(),
	ScrollRect_t3606982749::get_offset_of_m_Vertical_4(),
	ScrollRect_t3606982749::get_offset_of_m_MovementType_5(),
	ScrollRect_t3606982749::get_offset_of_m_Elasticity_6(),
	ScrollRect_t3606982749::get_offset_of_m_Inertia_7(),
	ScrollRect_t3606982749::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t3606982749::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t3606982749::get_offset_of_m_Viewport_10(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t3606982749::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t3606982749::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t3606982749::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t3606982749::get_offset_of_m_ViewRect_20(),
	ScrollRect_t3606982749::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t3606982749::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t3606982749::get_offset_of_m_Velocity_23(),
	ScrollRect_t3606982749::get_offset_of_m_Dragging_24(),
	ScrollRect_t3606982749::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t3606982749::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t3606982749::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t3606982749::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t3606982749::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t3606982749::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t3606982749::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t3606982749::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t3606982749::get_offset_of_m_Rect_33(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t3606982749::get_offset_of_m_Tracker_36(),
	ScrollRect_t3606982749::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (MovementType_t300513412)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1613[4] = 
{
	MovementType_t300513412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (ScrollbarVisibility_t184977789)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1614[4] = 
{
	ScrollbarVisibility_t184977789::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (ScrollRectEvent_t1643322606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (Selectable_t1885181538), -1, sizeof(Selectable_t1885181538_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1616[14] = 
{
	Selectable_t1885181538_StaticFields::get_offset_of_s_List_2(),
	Selectable_t1885181538::get_offset_of_m_Navigation_3(),
	Selectable_t1885181538::get_offset_of_m_Transition_4(),
	Selectable_t1885181538::get_offset_of_m_Colors_5(),
	Selectable_t1885181538::get_offset_of_m_SpriteState_6(),
	Selectable_t1885181538::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t1885181538::get_offset_of_m_Interactable_8(),
	Selectable_t1885181538::get_offset_of_m_TargetGraphic_9(),
	Selectable_t1885181538::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t1885181538::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t1885181538::get_offset_of_m_CanvasGroupCache_12(),
	Selectable_t1885181538::get_offset_of_U3CisPointerInsideU3Ek__BackingField_13(),
	Selectable_t1885181538::get_offset_of_U3CisPointerDownU3Ek__BackingField_14(),
	Selectable_t1885181538::get_offset_of_U3ChasSelectionU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (Transition_t1922345195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1617[5] = 
{
	Transition_t1922345195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (SelectionState_t1293548283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1618[5] = 
{
	SelectionState_t1293548283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (SetPropertyUtility_t1171612705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (Slider_t79469677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1620[15] = 
{
	Slider_t79469677::get_offset_of_m_FillRect_16(),
	Slider_t79469677::get_offset_of_m_HandleRect_17(),
	Slider_t79469677::get_offset_of_m_Direction_18(),
	Slider_t79469677::get_offset_of_m_MinValue_19(),
	Slider_t79469677::get_offset_of_m_MaxValue_20(),
	Slider_t79469677::get_offset_of_m_WholeNumbers_21(),
	Slider_t79469677::get_offset_of_m_Value_22(),
	Slider_t79469677::get_offset_of_m_OnValueChanged_23(),
	Slider_t79469677::get_offset_of_m_FillImage_24(),
	Slider_t79469677::get_offset_of_m_FillTransform_25(),
	Slider_t79469677::get_offset_of_m_FillContainerRect_26(),
	Slider_t79469677::get_offset_of_m_HandleTransform_27(),
	Slider_t79469677::get_offset_of_m_HandleContainerRect_28(),
	Slider_t79469677::get_offset_of_m_Offset_29(),
	Slider_t79469677::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (Direction_t94975348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1621[5] = 
{
	Direction_t94975348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (SliderEvent_t2627072750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (Axis_t3565360268)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1623[3] = 
{
	Axis_t3565360268::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (SpriteState_t2895308594)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[3] = 
{
	SpriteState_t2895308594::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t2895308594::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t2895308594::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (StencilMaterial_t639665897), -1, sizeof(StencilMaterial_t639665897_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1625[1] = 
{
	StencilMaterial_t639665897_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (MatEntry_t1574154081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[10] = 
{
	MatEntry_t1574154081::get_offset_of_baseMat_0(),
	MatEntry_t1574154081::get_offset_of_customMat_1(),
	MatEntry_t1574154081::get_offset_of_count_2(),
	MatEntry_t1574154081::get_offset_of_stencilId_3(),
	MatEntry_t1574154081::get_offset_of_operation_4(),
	MatEntry_t1574154081::get_offset_of_compareFunction_5(),
	MatEntry_t1574154081::get_offset_of_readMask_6(),
	MatEntry_t1574154081::get_offset_of_writeMask_7(),
	MatEntry_t1574154081::get_offset_of_useAlphaClip_8(),
	MatEntry_t1574154081::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (Text_t9039225), -1, sizeof(Text_t9039225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1627[7] = 
{
	Text_t9039225::get_offset_of_m_FontData_28(),
	Text_t9039225::get_offset_of_m_Text_29(),
	Text_t9039225::get_offset_of_m_TextCache_30(),
	Text_t9039225::get_offset_of_m_TextCacheForLayout_31(),
	Text_t9039225_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t9039225::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t9039225::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (Toggle_t110812896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[5] = 
{
	Toggle_t110812896::get_offset_of_toggleTransition_16(),
	Toggle_t110812896::get_offset_of_graphic_17(),
	Toggle_t110812896::get_offset_of_m_Group_18(),
	Toggle_t110812896::get_offset_of_onValueChanged_19(),
	Toggle_t110812896::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (ToggleTransition_t2757337633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1629[3] = 
{
	ToggleTransition_t2757337633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (ToggleEvent_t2331340366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (ToggleGroup_t1990156785), -1, sizeof(ToggleGroup_t1990156785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1631[4] = 
{
	ToggleGroup_t1990156785::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1990156785::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1990156785_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ToggleGroup_t1990156785_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (ClipperRegistry_t1074114320), -1, sizeof(ClipperRegistry_t1074114320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1632[2] = 
{
	ClipperRegistry_t1074114320_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1074114320::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (Clipping_t1257491342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (RectangularVertexClipper_t1294793591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1636[2] = 
{
	RectangularVertexClipper_t1294793591::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t1294793591::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (AspectRatioFitter_t436718473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[4] = 
{
	AspectRatioFitter_t436718473::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t436718473::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t436718473::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t436718473::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (AspectMode_t2149445162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1638[6] = 
{
	AspectMode_t2149445162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (CanvasScaler_t2777732396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[14] = 
{
	0,
	CanvasScaler_t2777732396::get_offset_of_m_UiScaleMode_3(),
	CanvasScaler_t2777732396::get_offset_of_m_ReferencePixelsPerUnit_4(),
	CanvasScaler_t2777732396::get_offset_of_m_ScaleFactor_5(),
	CanvasScaler_t2777732396::get_offset_of_m_ReferenceResolution_6(),
	CanvasScaler_t2777732396::get_offset_of_m_ScreenMatchMode_7(),
	CanvasScaler_t2777732396::get_offset_of_m_MatchWidthOrHeight_8(),
	CanvasScaler_t2777732396::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2777732396::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2777732396::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2777732396::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2777732396::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2777732396::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2777732396::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (ScaleMode_t2493957633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1640[4] = 
{
	ScaleMode_t2493957633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (ScreenMatchMode_t1153512176)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1641[4] = 
{
	ScreenMatchMode_t1153512176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (Unit_t1837657360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1642[6] = 
{
	Unit_t1837657360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (ContentSizeFitter_t1285073872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[4] = 
{
	ContentSizeFitter_t1285073872::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1285073872::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1285073872::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1285073872::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (FitMode_t909765868)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1644[4] = 
{
	FitMode_t909765868::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (GridLayoutGroup_t169317941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[6] = 
{
	GridLayoutGroup_t169317941::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t169317941::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t169317941::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t169317941::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t169317941::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t169317941::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (Corner_t284493240)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1646[5] = 
{
	Corner_t284493240::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (Axis_t1399125956)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1647[3] = 
{
	Axis_t1399125956::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (Constraint_t1640775616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1648[4] = 
{
	Constraint_t1640775616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (HorizontalLayoutGroup_t1336501463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (HorizontalOrVerticalLayoutGroup_t2052396382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[3] = 
{
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (LayoutElement_t1596995480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[7] = 
{
	LayoutElement_t1596995480::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1596995480::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1596995480::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (LayoutGroup_t352294875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[8] = 
{
	LayoutGroup_t352294875::get_offset_of_m_Padding_2(),
	LayoutGroup_t352294875::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t352294875::get_offset_of_m_Rect_4(),
	LayoutGroup_t352294875::get_offset_of_m_Tracker_5(),
	LayoutGroup_t352294875::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t352294875::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t352294875::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t352294875::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (LayoutRebuilder_t1942933988), -1, sizeof(LayoutRebuilder_t1942933988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1658[9] = 
{
	LayoutRebuilder_t1942933988::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t1942933988::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (LayoutUtility_t3144854024), -1, sizeof(LayoutUtility_t3144854024_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1659[8] = 
{
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (VerticalLayoutGroup_t423167365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (VertexHelper_t3377436606), -1, sizeof(VertexHelper_t3377436606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1665[9] = 
{
	VertexHelper_t3377436606::get_offset_of_m_Positions_0(),
	VertexHelper_t3377436606::get_offset_of_m_Colors_1(),
	VertexHelper_t3377436606::get_offset_of_m_Uv0S_2(),
	VertexHelper_t3377436606::get_offset_of_m_Uv1S_3(),
	VertexHelper_t3377436606::get_offset_of_m_Normals_4(),
	VertexHelper_t3377436606::get_offset_of_m_Tangents_5(),
	VertexHelper_t3377436606::get_offset_of_m_Indices_6(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (BaseVertexEffect_t3555037586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (BaseMeshEffect_t2306480155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[1] = 
{
	BaseMeshEffect_t2306480155::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (Outline_t3745177896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (PositionAsUV1_t4062429115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (Shadow_t75537580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[4] = 
{
	0,
	Shadow_t75537580::get_offset_of_m_EffectColor_4(),
	Shadow_t75537580::get_offset_of_m_EffectDistance_5(),
	Shadow_t75537580::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238937), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1673[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (U24ArrayTypeU2412_t3379220351)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220351_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (U3CModuleU3E_t86524796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (CreateMap_t2602318496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[1] = 
{
	CreateMap_t2602318496::get_offset_of_elementos_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (BallShadow_t3766914079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[3] = 
{
	BallShadow_t3766914079::get_offset_of_sombraBola_2(),
	BallShadow_t3766914079::get_offset_of_maxSombraBola_3(),
	BallShadow_t3766914079::get_offset_of_alturaSombra_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (BallCollider_t3766478643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1678[12] = 
{
	BallCollider_t3766478643::get_offset_of_Controller_2(),
	BallCollider_t3766478643::get_offset_of_tocaSuelo_3(),
	BallCollider_t3766478643::get_offset_of_nBotes_4(),
	BallCollider_t3766478643::get_offset_of_PlantaParticles_5(),
	BallCollider_t3766478643::get_offset_of_pitchMod_6(),
	BallCollider_t3766478643::get_offset_of_clampBolaX_7(),
	BallCollider_t3766478643::get_offset_of_Perspec_8(),
	BallCollider_t3766478643::get_offset_of_Ortho_9(),
	BallCollider_t3766478643::get_offset_of_Iso_10(),
	BallCollider_t3766478643::get_offset_of_Interface_11(),
	BallCollider_t3766478643::get_offset_of_isSecondBounce_12(),
	BallCollider_t3766478643::get_offset_of_audio_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (U3CBigBallTimeU3Ec__Iterator0_t1350130161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[6] = 
{
	U3CBigBallTimeU3Ec__Iterator0_t1350130161::get_offset_of_U3CtU3E__0_0(),
	U3CBigBallTimeU3Ec__Iterator0_t1350130161::get_offset_of_U3CoriginalScaleU3E__1_1(),
	U3CBigBallTimeU3Ec__Iterator0_t1350130161::get_offset_of_U3CbigballU3E__2_2(),
	U3CBigBallTimeU3Ec__Iterator0_t1350130161::get_offset_of_U24PC_3(),
	U3CBigBallTimeU3Ec__Iterator0_t1350130161::get_offset_of_U24current_4(),
	U3CBigBallTimeU3Ec__Iterator0_t1350130161::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (U3CHitColorU3Ec__Iterator1_t1194571374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[7] = 
{
	U3CHitColorU3Ec__Iterator1_t1194571374::get_offset_of_U3CtU3E__0_0(),
	U3CHitColorU3Ec__Iterator1_t1194571374::get_offset_of_other_1(),
	U3CHitColorU3Ec__Iterator1_t1194571374::get_offset_of_U3CgU3E__1_2(),
	U3CHitColorU3Ec__Iterator1_t1194571374::get_offset_of_U24PC_3(),
	U3CHitColorU3Ec__Iterator1_t1194571374::get_offset_of_U24current_4(),
	U3CHitColorU3Ec__Iterator1_t1194571374::get_offset_of_U3CU24U3Eother_5(),
	U3CHitColorU3Ec__Iterator1_t1194571374::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (Blanco_t1992262851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1681[3] = 
{
	Blanco_t1992262851::get_offset_of_t_2(),
	Blanco_t1992262851::get_offset_of_sc_3(),
	Blanco_t1992262851::get_offset_of_diametroInicial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (U3CPlofU3Ec__Iterator2_t1800312444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[3] = 
{
	U3CPlofU3Ec__Iterator2_t1800312444::get_offset_of_U24PC_0(),
	U3CPlofU3Ec__Iterator2_t1800312444::get_offset_of_U24current_1(),
	U3CPlofU3Ec__Iterator2_t1800312444::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (BouncingAnim_t2247160086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[3] = 
{
	BouncingAnim_t2247160086::get_offset_of_stretch_2(),
	BouncingAnim_t2247160086::get_offset_of_maxStretch_3(),
	BouncingAnim_t2247160086::get_offset_of_alturaSueloAnim_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (Brazo_t64445670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[2] = 
{
	Brazo_t64445670::get_offset_of_acceleration_2(),
	Brazo_t64445670::get_offset_of_brazo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (Brick_t64452641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1685[8] = 
{
	Brick_t64452641::get_offset_of_tipo_2(),
	Brick_t64452641::get_offset_of_nToques_3(),
	Brick_t64452641::get_offset_of_isReadyToDie_4(),
	Brick_t64452641::get_offset_of_dobleMaterial_5(),
	Brick_t64452641::get_offset_of_dobleMaterialTocado_6(),
	Brick_t64452641::get_offset_of_Blanco_7(),
	Brick_t64452641::get_offset_of_Controller_8(),
	Brick_t64452641::get_offset_of_tocaBrick_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (ColliderPlayer_t3441697813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[11] = 
{
	ColliderPlayer_t3441697813::get_offset_of_AVG_x_2(),
	ColliderPlayer_t3441697813::get_offset_of_AVG_y_3(),
	ColliderPlayer_t3441697813::get_offset_of_AVG_z_4(),
	ColliderPlayer_t3441697813::get_offset_of_AVG_Multiplier_5(),
	ColliderPlayer_t3441697813::get_offset_of_fuerza_6(),
	ColliderPlayer_t3441697813::get_offset_of_sc_7(),
	ColliderPlayer_t3441697813::get_offset_of_Perspec_8(),
	ColliderPlayer_t3441697813::get_offset_of_Ortho_9(),
	ColliderPlayer_t3441697813::get_offset_of_Iso_10(),
	ColliderPlayer_t3441697813::get_offset_of_Interface_11(),
	ColliderPlayer_t3441697813::get_offset_of_Controller_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (U3CflanU3Ec__Iterator3_t3689796459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[8] = 
{
	U3CflanU3Ec__Iterator3_t3689796459::get_offset_of_U3CtU3E__0_0(),
	U3CflanU3Ec__Iterator3_t3689796459::get_offset_of_U3CscU3E__1_1(),
	U3CflanU3Ec__Iterator3_t3689796459::get_offset_of_U3CtargetScaleU3E__2_2(),
	U3CflanU3Ec__Iterator3_t3689796459::get_offset_of_U3CscyU3E__3_3(),
	U3CflanU3Ec__Iterator3_t3689796459::get_offset_of_U3ClscU3E__4_4(),
	U3CflanU3Ec__Iterator3_t3689796459::get_offset_of_U24PC_5(),
	U3CflanU3Ec__Iterator3_t3689796459::get_offset_of_U24current_6(),
	U3CflanU3Ec__Iterator3_t3689796459::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (Controller_t2630893500), -1, sizeof(Controller_t2630893500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1688[46] = 
{
	Controller_t2630893500::get_offset_of_Map_2(),
	Controller_t2630893500::get_offset_of_PlayerObj_3(),
	Controller_t2630893500::get_offset_of_BallObj_4(),
	Controller_t2630893500::get_offset_of_RoomGen_5(),
	Controller_t2630893500::get_offset_of_FrontWall_6(),
	Controller_t2630893500::get_offset_of_Room_7(),
	Controller_t2630893500::get_offset_of_UIPanelMenu_8(),
	Controller_t2630893500::get_offset_of_UIPanelGOver_9(),
	Controller_t2630893500::get_offset_of_UIPanelJuego_10(),
	Controller_t2630893500::get_offset_of_UIPanelTransicion_11(),
	Controller_t2630893500::get_offset_of_BotesText_12(),
	Controller_t2630893500::get_offset_of_RoomText_13(),
	Controller_t2630893500::get_offset_of_BricksText_14(),
	Controller_t2630893500::get_offset_of_BotesTextGameOver_15(),
	Controller_t2630893500::get_offset_of_RoomTextGameOver_16(),
	Controller_t2630893500::get_offset_of_BricksTextGameOver_17(),
	Controller_t2630893500::get_offset_of_BotesBuenosText_18(),
	Controller_t2630893500::get_offset_of_TimerTransition_19(),
	Controller_t2630893500::get_offset_of_BricksTextTransicion_20(),
	Controller_t2630893500::get_offset_of_RoomTextTransicion_21(),
	Controller_t2630893500::get_offset_of_TotalRoomMenuText_22(),
	Controller_t2630893500::get_offset_of_TotalBricksMenuText_23(),
	Controller_t2630893500::get_offset_of_Telefono_24(),
	Controller_t2630893500::get_offset_of_MinigameButton_25(),
	Controller_t2630893500::get_offset_of_EnterButton_26(),
	Controller_t2630893500::get_offset_of_ball_27(),
	Controller_t2630893500::get_offset_of_player_28(),
	Controller_t2630893500::get_offset_of_plants_29(),
	Controller_t2630893500::get_offset_of_Perspec_30(),
	Controller_t2630893500::get_offset_of_Ortho_31(),
	Controller_t2630893500::get_offset_of_Iso_32(),
	Controller_t2630893500::get_offset_of_Interface_33(),
	Controller_t2630893500::get_offset_of_rollerCamara_34(),
	Controller_t2630893500::get_offset_of_speedCamAnim_35(),
	Controller_t2630893500::get_offset_of_gameRulesOn_36(),
	Controller_t2630893500::get_offset_of_isEnterButtonReady_37(),
	Controller_t2630893500::get_offset_of_NRoom_38(),
	Controller_t2630893500::get_offset_of_NBricks_39(),
	Controller_t2630893500_StaticFields::get_offset_of_NTotalBricks_40(),
	Controller_t2630893500_StaticFields::get_offset_of_NTotalRooms_41(),
	Controller_t2630893500::get_offset_of_tiempo_42(),
	Controller_t2630893500::get_offset_of_tiempoPausa_43(),
	Controller_t2630893500::get_offset_of_numMultiplyBalls_44(),
	Controller_t2630893500::get_offset_of_Multibolas_45(),
	Controller_t2630893500::get_offset_of_audio_46(),
	Controller_t2630893500_StaticFields::get_offset_of_gameState_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (State_t2540969054)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1689[8] = 
{
	State_t2540969054::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (U3CCamAnimU3Ec__Iterator4_t3416449610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[3] = 
{
	U3CCamAnimU3Ec__Iterator4_t3416449610::get_offset_of_U24PC_0(),
	U3CCamAnimU3Ec__Iterator4_t3416449610::get_offset_of_U24current_1(),
	U3CCamAnimU3Ec__Iterator4_t3416449610::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (U3CCortinaU3Ec__Iterator5_t3064932893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[3] = 
{
	U3CCortinaU3Ec__Iterator5_t3064932893::get_offset_of_U24PC_0(),
	U3CCortinaU3Ec__Iterator5_t3064932893::get_offset_of_U24current_1(),
	U3CCortinaU3Ec__Iterator5_t3064932893::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (U3CrunScoreU3Ec__Iterator6_t3971101203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[4] = 
{
	U3CrunScoreU3Ec__Iterator6_t3971101203::get_offset_of_U3CpU3E__0_0(),
	U3CrunScoreU3Ec__Iterator6_t3971101203::get_offset_of_U24PC_1(),
	U3CrunScoreU3Ec__Iterator6_t3971101203::get_offset_of_U24current_2(),
	U3CrunScoreU3Ec__Iterator6_t3971101203::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (Dimensions_t2407799789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1693[10] = 
{
	Dimensions_t2407799789::get_offset_of_alto_2(),
	Dimensions_t2407799789::get_offset_of_ancho_3(),
	Dimensions_t2407799789::get_offset_of_fondo_4(),
	Dimensions_t2407799789::get_offset_of_nbloques_5(),
	Dimensions_t2407799789::get_offset_of_minAlto_6(),
	Dimensions_t2407799789::get_offset_of_minAncho_7(),
	Dimensions_t2407799789::get_offset_of_minFondo_8(),
	Dimensions_t2407799789::get_offset_of_maxAlto_9(),
	Dimensions_t2407799789::get_offset_of_maxAncho_10(),
	Dimensions_t2407799789::get_offset_of_maxFondo_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (FrontWall_t2131806067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[3] = 
{
	FrontWall_t2131806067::get_offset_of_BrickObj_2(),
	FrontWall_t2131806067::get_offset_of_Bricks_3(),
	FrontWall_t2131806067::get_offset_of_timeCreating_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (MapController_t1754731000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[11] = 
{
	MapController_t1754731000::get_offset_of_x_2(),
	MapController_t1754731000::get_offset_of_y_3(),
	MapController_t1754731000::get_offset_of_z_4(),
	MapController_t1754731000::get_offset_of_RoomObj_5(),
	MapController_t1754731000::get_offset_of_Rooms_6(),
	MapController_t1754731000::get_offset_of_dim_7(),
	MapController_t1754731000::get_offset_of_hit_8(),
	MapController_t1754731000::get_offset_of_dragSpeed_9(),
	MapController_t1754731000::get_offset_of_dragOrigin_10(),
	MapController_t1754731000::get_offset_of_Canvas_11(),
	MapController_t1754731000::get_offset_of_Panel_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (Move_t2404337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[19] = 
{
	Move_t2404337::get_offset_of_acceleration_2(),
	Move_t2404337::get_offset_of_currentForzH_3(),
	Move_t2404337::get_offset_of_maxCurrentForzH_4(),
	Move_t2404337::get_offset_of_currentForzZ_5(),
	Move_t2404337::get_offset_of_maxCurrentForzZ_6(),
	Move_t2404337::get_offset_of_accelerationCube_7(),
	Move_t2404337::get_offset_of_rapidez_8(),
	Move_t2404337::get_offset_of_traslationH_9(),
	Move_t2404337::get_offset_of_TargetBall_10(),
	Move_t2404337::get_offset_of_boundary_11(),
	Move_t2404337::get_offset_of_GotoButton_12(),
	Move_t2404337::get_offset_of_isColliding_13(),
	Move_t2404337::get_offset_of_Perspec_14(),
	Move_t2404337::get_offset_of_Ortho_15(),
	Move_t2404337::get_offset_of_Iso_16(),
	Move_t2404337::get_offset_of_Interface_17(),
	Move_t2404337::get_offset_of_ancho_18(),
	Move_t2404337::get_offset_of_movingRight_19(),
	Move_t2404337::get_offset_of_movingLeft_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (MoveBall_t4254648720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1697[3] = 
{
	MoveBall_t4254648720::get_offset_of_velocidad_2(),
	MoveBall_t4254648720::get_offset_of_boundary_3(),
	MoveBall_t4254648720::get_offset_of_isReady_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (Plants_t2393071496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[23] = 
{
	Plants_t2393071496::get_offset_of_materiales_2(),
	Plants_t2393071496::get_offset_of_plantas_3(),
	Plants_t2393071496::get_offset_of_indicesEntrada_4(),
	Plants_t2393071496::get_offset_of_indicesSalida_5(),
	Plants_t2393071496::get_offset_of_indicesMateriales_6(),
	Plants_t2393071496::get_offset_of_Perspec_7(),
	Plants_t2393071496::get_offset_of_Ortho_8(),
	Plants_t2393071496::get_offset_of_Iso_9(),
	Plants_t2393071496::get_offset_of_Interface_10(),
	Plants_t2393071496::get_offset_of_colors_11(),
	Plants_t2393071496::get_offset_of_currentTiempo_12(),
	Plants_t2393071496::get_offset_of_LluviaObj_13(),
	Plants_t2393071496::get_offset_of_NieveObj_14(),
	Plants_t2393071496::get_offset_of_NieblaObj_15(),
	Plants_t2393071496::get_offset_of_ReflejosMarObj_16(),
	Plants_t2393071496::get_offset_of_TruenoObj_17(),
	Plants_t2393071496::get_offset_of_LineasFlotacionObj_18(),
	Plants_t2393071496::get_offset_of_lluvia_19(),
	Plants_t2393071496::get_offset_of_nieve_20(),
	Plants_t2393071496::get_offset_of_niebla_21(),
	Plants_t2393071496::get_offset_of_reflejosMar_22(),
	Plants_t2393071496::get_offset_of_trueno_23(),
	Plants_t2393071496::get_offset_of_lineasFlotacion_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (Tiempo_t1322696995)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1699[9] = 
{
	Tiempo_t1322696995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
