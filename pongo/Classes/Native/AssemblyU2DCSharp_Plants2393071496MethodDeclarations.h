﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Plants
struct Plants_t2393071496;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void Plants::.ctor()
extern "C"  void Plants__ctor_m2365783571 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Plants_setCameras_m1784206025 (Plants_t2393071496 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::SetWeather(System.Int32)
extern "C"  void Plants_SetWeather_m4171024980 (Plants_t2393071496 * __this, int32_t ___fase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::CreateSet(System.Int32,System.Int32)
extern "C"  void Plants_CreateSet_m585282447 (Plants_t2393071496 * __this, int32_t ___fase0, int32_t ___room1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Plants::CreatePlantsItems(UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  GameObject_t3674682005 * Plants_CreatePlantsItems_m3446816263 (Plants_t2393071496 * __this, Vector3_t4282066566  ___pos0, Quaternion_t1553702882  ___rot1, int32_t ___fase2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material Plants::GetMaterial()
extern "C"  Material_t3870600107 * Plants_GetMaterial_m1752811087 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::DestroyWeatherElements()
extern "C"  void Plants_DestroyWeatherElements_m997798466 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::CreateLineasFlotacion()
extern "C"  void Plants_CreateLineasFlotacion_m3334133290 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::CreateNiebla()
extern "C"  void Plants_CreateNiebla_m3669633466 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::CreateNieve()
extern "C"  void Plants_CreateNieve_m3582645902 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::CreateLluvia()
extern "C"  void Plants_CreateLluvia_m3322971302 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::CreateReflejosMar()
extern "C"  void Plants_CreateReflejosMar_m2151477137 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::CreateTrueno()
extern "C"  void Plants_CreateTrueno_m1100102140 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::GenerateLandscape(System.Int32)
extern "C"  void Plants_GenerateLandscape_m3010235720 (Plants_t2393071496 * __this, int32_t ___fase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::CreateMap(System.Int32,System.Int32)
extern "C"  void Plants_CreateMap_m3058892373 (Plants_t2393071496 * __this, int32_t ___fase0, int32_t ___room1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plants::BackToMenu()
extern "C"  void Plants_BackToMenu_m2087043794 (Plants_t2393071496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
