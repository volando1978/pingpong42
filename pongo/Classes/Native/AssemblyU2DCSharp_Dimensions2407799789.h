﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dimensions
struct  Dimensions_t2407799789  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 Dimensions::alto
	int32_t ___alto_2;
	// System.Int32 Dimensions::ancho
	int32_t ___ancho_3;
	// System.Int32 Dimensions::fondo
	int32_t ___fondo_4;
	// System.Int32 Dimensions::nbloques
	int32_t ___nbloques_5;
	// System.Int32 Dimensions::minAlto
	int32_t ___minAlto_6;
	// System.Int32 Dimensions::minAncho
	int32_t ___minAncho_7;
	// System.Int32 Dimensions::minFondo
	int32_t ___minFondo_8;
	// System.Int32 Dimensions::maxAlto
	int32_t ___maxAlto_9;
	// System.Int32 Dimensions::maxAncho
	int32_t ___maxAncho_10;
	// System.Int32 Dimensions::maxFondo
	int32_t ___maxFondo_11;

public:
	inline static int32_t get_offset_of_alto_2() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___alto_2)); }
	inline int32_t get_alto_2() const { return ___alto_2; }
	inline int32_t* get_address_of_alto_2() { return &___alto_2; }
	inline void set_alto_2(int32_t value)
	{
		___alto_2 = value;
	}

	inline static int32_t get_offset_of_ancho_3() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___ancho_3)); }
	inline int32_t get_ancho_3() const { return ___ancho_3; }
	inline int32_t* get_address_of_ancho_3() { return &___ancho_3; }
	inline void set_ancho_3(int32_t value)
	{
		___ancho_3 = value;
	}

	inline static int32_t get_offset_of_fondo_4() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___fondo_4)); }
	inline int32_t get_fondo_4() const { return ___fondo_4; }
	inline int32_t* get_address_of_fondo_4() { return &___fondo_4; }
	inline void set_fondo_4(int32_t value)
	{
		___fondo_4 = value;
	}

	inline static int32_t get_offset_of_nbloques_5() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___nbloques_5)); }
	inline int32_t get_nbloques_5() const { return ___nbloques_5; }
	inline int32_t* get_address_of_nbloques_5() { return &___nbloques_5; }
	inline void set_nbloques_5(int32_t value)
	{
		___nbloques_5 = value;
	}

	inline static int32_t get_offset_of_minAlto_6() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___minAlto_6)); }
	inline int32_t get_minAlto_6() const { return ___minAlto_6; }
	inline int32_t* get_address_of_minAlto_6() { return &___minAlto_6; }
	inline void set_minAlto_6(int32_t value)
	{
		___minAlto_6 = value;
	}

	inline static int32_t get_offset_of_minAncho_7() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___minAncho_7)); }
	inline int32_t get_minAncho_7() const { return ___minAncho_7; }
	inline int32_t* get_address_of_minAncho_7() { return &___minAncho_7; }
	inline void set_minAncho_7(int32_t value)
	{
		___minAncho_7 = value;
	}

	inline static int32_t get_offset_of_minFondo_8() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___minFondo_8)); }
	inline int32_t get_minFondo_8() const { return ___minFondo_8; }
	inline int32_t* get_address_of_minFondo_8() { return &___minFondo_8; }
	inline void set_minFondo_8(int32_t value)
	{
		___minFondo_8 = value;
	}

	inline static int32_t get_offset_of_maxAlto_9() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___maxAlto_9)); }
	inline int32_t get_maxAlto_9() const { return ___maxAlto_9; }
	inline int32_t* get_address_of_maxAlto_9() { return &___maxAlto_9; }
	inline void set_maxAlto_9(int32_t value)
	{
		___maxAlto_9 = value;
	}

	inline static int32_t get_offset_of_maxAncho_10() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___maxAncho_10)); }
	inline int32_t get_maxAncho_10() const { return ___maxAncho_10; }
	inline int32_t* get_address_of_maxAncho_10() { return &___maxAncho_10; }
	inline void set_maxAncho_10(int32_t value)
	{
		___maxAncho_10 = value;
	}

	inline static int32_t get_offset_of_maxFondo_11() { return static_cast<int32_t>(offsetof(Dimensions_t2407799789, ___maxFondo_11)); }
	inline int32_t get_maxFondo_11() const { return ___maxFondo_11; }
	inline int32_t* get_address_of_maxFondo_11() { return &___maxFondo_11; }
	inline void set_maxFondo_11(int32_t value)
	{
		___maxFondo_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
