﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Blanco
struct  Blanco_t1992262851  : public MonoBehaviour_t667441552
{
public:
	// System.Single Blanco::t
	float ___t_2;
	// UnityEngine.Vector3 Blanco::sc
	Vector3_t4282066566  ___sc_3;
	// System.Single Blanco::diametroInicial
	float ___diametroInicial_4;

public:
	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(Blanco_t1992262851, ___t_2)); }
	inline float get_t_2() const { return ___t_2; }
	inline float* get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(float value)
	{
		___t_2 = value;
	}

	inline static int32_t get_offset_of_sc_3() { return static_cast<int32_t>(offsetof(Blanco_t1992262851, ___sc_3)); }
	inline Vector3_t4282066566  get_sc_3() const { return ___sc_3; }
	inline Vector3_t4282066566 * get_address_of_sc_3() { return &___sc_3; }
	inline void set_sc_3(Vector3_t4282066566  value)
	{
		___sc_3 = value;
	}

	inline static int32_t get_offset_of_diametroInicial_4() { return static_cast<int32_t>(offsetof(Blanco_t1992262851, ___diametroInicial_4)); }
	inline float get_diametroInicial_4() const { return ___diametroInicial_4; }
	inline float* get_address_of_diametroInicial_4() { return &___diametroInicial_4; }
	inline void set_diametroInicial_4(float value)
	{
		___diametroInicial_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
