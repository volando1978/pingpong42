﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CustomJoystick.FourWayController
struct FourWayController_t1827771971;

#include "codegen/il2cpp-codegen.h"

// System.Void CustomJoystick.FourWayController::.ctor()
extern "C"  void FourWayController__ctor_m394611019 (FourWayController_t1827771971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomJoystick.FourWayController::Awake()
extern "C"  void FourWayController_Awake_m632216238 (FourWayController_t1827771971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CustomJoystick.FourWayController::Update()
extern "C"  void FourWayController_Update_m1074901794 (FourWayController_t1827771971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
