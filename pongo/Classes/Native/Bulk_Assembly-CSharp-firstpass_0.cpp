﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// CnControls.CnInputManager
struct CnInputManager_t540789462;
// System.String
struct String_t;
// CnControls.VirtualAxis
struct VirtualAxis_t2395187226;
// CnControls.VirtualButton
struct VirtualButton_t3335323691;
// System.Collections.Generic.List`1<CnControls.VirtualAxis>
struct List_1_t3763372778;
// System.Collections.Generic.List`1<CnControls.VirtualButton>
struct List_1_t408541947;
// CnControls.Dpad
struct Dpad_t3878341687;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;
// CnControls.DpadAxis
struct DpadAxis_t235416056;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// System.Object
struct Il2CppObject;
// CnControls.SensitiveJoystick
struct SensitiveJoystick_t1697973888;
// CnControls.SimpleButton
struct SimpleButton_t2998894860;
// CnControls.SimpleJoystick
struct SimpleJoystick_t3821479766;
// CnControls.Touchpad
struct Touchpad_t2035887356;
// CommonOnScreenControl
struct CommonOnScreenControl_t1743026727;
// CustomJoystick.FourWayController
struct FourWayController_t1827771971;
// Examples.Scenes.TouchpadCamera.RotateCamera
struct RotateCamera_t1717774348;
// Examples.Scenes.TouchpadCamera.RotationConstraint
struct RotationConstraint_t178863463;
// ThidPersonExampleController
struct ThidPersonExampleController_t1811656226;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;
// UnityStandardAssets._2D.Camera2DFollow
struct Camera2DFollow_t1184786310;
// UnityStandardAssets._2D.Restarter
struct Restarter_t2816782862;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityStandardAssets.Cameras.AbstractTargetFollower
struct AbstractTargetFollower_t2600961761;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityStandardAssets.Cameras.AutoCam
struct AutoCam_t3033465374;
// UnityStandardAssets.Cameras.PivotBasedCameraRig
struct PivotBasedCameraRig_t2501384728;
// UnityStandardAssets.Copy._2D.Platformer2DUserControl
struct Platformer2DUserControl_t3012254862;
// UnityStandardAssets.Copy._2D.PlatformerCharacter2D
struct PlatformerCharacter2D_t2561026825;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_CnInputMan540789462.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_CnInputMan540789462MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge288823852MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1228960317MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge288823852.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1228960317.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3763372778.h"
#include "mscorlib_System_Collections_Generic_List_1_gen408541947.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualAx2395187226.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualAx2395187226MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3763372778MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualBu3335323691.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualBu3335323691MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen408541947MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_ControlMo3285416379.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_ControlMo3285416379MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Dpad3878341687.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Dpad3878341687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_DpadAxis235416056MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_DpadAxis235416056.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Sensitive1697973888.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Sensitive1697973888MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_SimpleJoy3821479766MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_SimpleJoy3821479766.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_SimpleBut2998894860.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_SimpleBut2998894860MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Touchpad2035887356.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Touchpad2035887356MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CommonOnScreenContro1743026727.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CommonOnScreenContro1743026727MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CustomJoystick_FourW1827771971.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CustomJoystick_FourW1827771971MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Examples_Scenes_Touc1717774348.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Examples_Scenes_Touc1717774348MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Examples_Scenes_Touch178863463.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Examples_Scenes_Touch178863463MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ThidPersonExampleCon1811656226.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ThidPersonExampleCon1811656226MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1184786310.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1184786310MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2816782862.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2816782862MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2600961761.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2600961761MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C181978984.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C181978984MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3033465374.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3033465374MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2501384728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2501384728.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3012254862.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3012254862MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2561026825.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2561026825MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D9846735MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t972643934_m1940403147(__this, method) ((  RectTransform_t972643934 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t1659122786_m811718087(__this, method) ((  Transform_t1659122786 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
#define Component_GetComponent_TisCharacterController_t1618060635_m3352851511(__this, method) ((  CharacterController_t1618060635 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3346577219_m2213165263(__this, method) ((  Rigidbody_t3346577219 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Camera>()
#define Component_GetComponentInChildren_TisCamera_t2727095145_m2632836353(__this, method) ((  Camera_t2727095145 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Copy._2D.PlatformerCharacter2D>()
#define Component_GetComponent_TisPlatformerCharacter2D_t2561026825_m598360315(__this, method) ((  PlatformerCharacter2D_t2561026825 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t2776330603_m4147395588(__this, method) ((  Animator_t2776330603 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t1743771669_m832779581(__this, method) ((  Rigidbody2D_t1743771669 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CnControls.CnInputManager::.ctor()
extern Il2CppClass* Dictionary_2_t288823852_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1228960317_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m231195537_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m657770400_MethodInfo_var;
extern const uint32_t CnInputManager__ctor_m525640124_MetadataUsageId;
extern "C"  void CnInputManager__ctor_m525640124 (CnInputManager_t540789462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager__ctor_m525640124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t288823852 * L_0 = (Dictionary_2_t288823852 *)il2cpp_codegen_object_new(Dictionary_2_t288823852_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m231195537(L_0, /*hidden argument*/Dictionary_2__ctor_m231195537_MethodInfo_var);
		__this->set__virtualAxisDictionary_1(L_0);
		Dictionary_2_t1228960317 * L_1 = (Dictionary_2_t1228960317 *)il2cpp_codegen_object_new(Dictionary_2_t1228960317_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m657770400(L_1, /*hidden argument*/Dictionary_2__ctor_m657770400_MethodInfo_var);
		__this->set__virtualButtonsDictionary_2(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// CnControls.CnInputManager CnControls.CnInputManager::get_Instance()
extern Il2CppClass* CnInputManager_t540789462_il2cpp_TypeInfo_var;
extern const uint32_t CnInputManager_get_Instance_m1958956814_MetadataUsageId;
extern "C"  CnInputManager_t540789462 * CnInputManager_get_Instance_m1958956814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_get_Instance_m1958956814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CnInputManager_t540789462 * G_B2_0 = NULL;
	CnInputManager_t540789462 * G_B1_0 = NULL;
	{
		CnInputManager_t540789462 * L_0 = ((CnInputManager_t540789462_StaticFields*)CnInputManager_t540789462_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		CnInputManager_t540789462 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0017;
		}
	}
	{
		CnInputManager_t540789462 * L_2 = (CnInputManager_t540789462 *)il2cpp_codegen_object_new(CnInputManager_t540789462_il2cpp_TypeInfo_var);
		CnInputManager__ctor_m525640124(L_2, /*hidden argument*/NULL);
		CnInputManager_t540789462 * L_3 = L_2;
		((CnInputManager_t540789462_StaticFields*)CnInputManager_t540789462_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_3);
		G_B2_0 = L_3;
	}

IL_0017:
	{
		return G_B2_0;
	}
}
// System.Int32 CnControls.CnInputManager::get_TouchCount()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t CnInputManager_get_TouchCount_m2484571883_MetadataUsageId;
extern "C"  int32_t CnInputManager_get_TouchCount_m2484571883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_get_TouchCount_m2484571883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Touch CnControls.CnInputManager::GetTouch(System.Int32)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t CnInputManager_GetTouch_m1019003927_MetadataUsageId;
extern "C"  Touch_t4210255029  CnInputManager_GetTouch_m1019003927 (Il2CppObject * __this /* static, unused */, int32_t ___touchIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetTouch_m1019003927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___touchIndex0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Touch_t4210255029  L_1 = Input_GetTouch_m2282421092(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single CnControls.CnInputManager::GetAxis(System.String)
extern "C"  float CnInputManager_GetAxis_m2810903397 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___axisName0;
		float L_1 = CnInputManager_GetAxis_m562856312(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single CnControls.CnInputManager::GetAxisRaw(System.String)
extern "C"  float CnInputManager_GetAxisRaw_m692810837 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___axisName0;
		float L_1 = CnInputManager_GetAxis_m562856312(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single CnControls.CnInputManager::GetAxis(System.String,System.Boolean)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m189145749_MethodInfo_var;
extern const uint32_t CnInputManager_GetAxis_m562856312_MetadataUsageId;
extern "C"  float CnInputManager_GetAxis_m562856312 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, bool ___isRaw1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetAxis_m562856312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float G_B5_0 = 0.0f;
	{
		String_t* L_0 = ___axisName0;
		bool L_1 = CnInputManager_AxisExists_m2616522713(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		CnInputManager_t540789462 * L_2 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Dictionary_2_t288823852 * L_3 = L_2->get__virtualAxisDictionary_1();
		String_t* L_4 = ___axisName0;
		NullCheck(L_3);
		List_1_t3763372778 * L_5 = Dictionary_2_get_Item_m189145749(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m189145749_MethodInfo_var);
		String_t* L_6 = ___axisName0;
		bool L_7 = ___isRaw1;
		float L_8 = CnInputManager_GetVirtualAxisValue_m4142473329(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0023:
	{
		bool L_9 = ___isRaw1;
		if (!L_9)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_10 = ___axisName0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_11 = Input_GetAxisRaw_m1900207208(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		G_B5_0 = L_11;
		goto IL_003a;
	}

IL_0034:
	{
		String_t* L_12 = ___axisName0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_13 = Input_GetAxis_m2027668530(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		G_B5_0 = L_13;
	}

IL_003a:
	{
		return G_B5_0;
	}
}
// System.Boolean CnControls.CnInputManager::GetButton(System.String)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3625649830_MethodInfo_var;
extern const uint32_t CnInputManager_GetButton_m2809242788_MetadataUsageId;
extern "C"  bool CnInputManager_GetButton_m2809242788 (Il2CppObject * __this /* static, unused */, String_t* ___buttonName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetButton_m2809242788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ___buttonName0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButton_m4226175975(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)1;
	}

IL_000f:
	{
		String_t* L_3 = ___buttonName0;
		bool L_4 = CnInputManager_ButtonExists_m2499826600(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		CnInputManager_t540789462 * L_5 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t1228960317 * L_6 = L_5->get__virtualButtonsDictionary_2();
		String_t* L_7 = ___buttonName0;
		NullCheck(L_6);
		List_1_t408541947 * L_8 = Dictionary_2_get_Item_m3625649830(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m3625649830_MethodInfo_var);
		bool L_9 = CnInputManager_GetAnyVirtualButton_m3717308741(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::GetButtonDown(System.String)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3625649830_MethodInfo_var;
extern const uint32_t CnInputManager_GetButtonDown_m3486119458_MetadataUsageId;
extern "C"  bool CnInputManager_GetButtonDown_m3486119458 (Il2CppObject * __this /* static, unused */, String_t* ___buttonName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetButtonDown_m3486119458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ___buttonName0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonDown_m1879002085(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)1;
	}

IL_000f:
	{
		String_t* L_3 = ___buttonName0;
		bool L_4 = CnInputManager_ButtonExists_m2499826600(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		CnInputManager_t540789462 * L_5 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t1228960317 * L_6 = L_5->get__virtualButtonsDictionary_2();
		String_t* L_7 = ___buttonName0;
		NullCheck(L_6);
		List_1_t408541947 * L_8 = Dictionary_2_get_Item_m3625649830(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m3625649830_MethodInfo_var);
		bool L_9 = CnInputManager_GetAnyVirtualButtonDown_m706362819(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::GetButtonUp(System.String)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3625649830_MethodInfo_var;
extern const uint32_t CnInputManager_GetButtonUp_m2544187337_MetadataUsageId;
extern "C"  bool CnInputManager_GetButtonUp_m2544187337 (Il2CppObject * __this /* static, unused */, String_t* ___buttonName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetButtonUp_m2544187337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ___buttonName0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonUp_m2712347212(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)1;
	}

IL_000f:
	{
		String_t* L_3 = ___buttonName0;
		bool L_4 = CnInputManager_ButtonExists_m2499826600(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		CnInputManager_t540789462 * L_5 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t1228960317 * L_6 = L_5->get__virtualButtonsDictionary_2();
		String_t* L_7 = ___buttonName0;
		NullCheck(L_6);
		List_1_t408541947 * L_8 = Dictionary_2_get_Item_m3625649830(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m3625649830_MethodInfo_var);
		bool L_9 = CnInputManager_GetAnyVirtualButtonUp_m3586789226(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::AxisExists(System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m3307480068_MethodInfo_var;
extern const uint32_t CnInputManager_AxisExists_m2616522713_MetadataUsageId;
extern "C"  bool CnInputManager_AxisExists_m2616522713 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_AxisExists_m2616522713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CnInputManager_t540789462 * L_0 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t288823852 * L_1 = L_0->get__virtualAxisDictionary_1();
		String_t* L_2 = ___axisName0;
		NullCheck(L_1);
		bool L_3 = Dictionary_2_ContainsKey_m3307480068(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m3307480068_MethodInfo_var);
		return L_3;
	}
}
// System.Boolean CnControls.CnInputManager::ButtonExists(System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m2679750675_MethodInfo_var;
extern const uint32_t CnInputManager_ButtonExists_m2499826600_MetadataUsageId;
extern "C"  bool CnInputManager_ButtonExists_m2499826600 (Il2CppObject * __this /* static, unused */, String_t* ___buttonName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_ButtonExists_m2499826600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CnInputManager_t540789462 * L_0 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t1228960317 * L_1 = L_0->get__virtualButtonsDictionary_2();
		String_t* L_2 = ___buttonName0;
		NullCheck(L_1);
		bool L_3 = Dictionary_2_ContainsKey_m2679750675(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m2679750675_MethodInfo_var);
		return L_3;
	}
}
// System.Void CnControls.CnInputManager::RegisterVirtualAxis(CnControls.VirtualAxis)
extern Il2CppClass* List_1_t3763372778_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3307480068_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3061809494_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m435531778_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m189145749_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2756170310_MethodInfo_var;
extern const uint32_t CnInputManager_RegisterVirtualAxis_m2285669572_MetadataUsageId;
extern "C"  void CnInputManager_RegisterVirtualAxis_m2285669572 (Il2CppObject * __this /* static, unused */, VirtualAxis_t2395187226 * ___virtualAxis0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_RegisterVirtualAxis_m2285669572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CnInputManager_t540789462 * L_0 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t288823852 * L_1 = L_0->get__virtualAxisDictionary_1();
		VirtualAxis_t2395187226 * L_2 = ___virtualAxis0;
		NullCheck(L_2);
		String_t* L_3 = VirtualAxis_get_Name_m1171714283(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4 = Dictionary_2_ContainsKey_m3307480068(L_1, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m3307480068_MethodInfo_var);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		CnInputManager_t540789462 * L_5 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t288823852 * L_6 = L_5->get__virtualAxisDictionary_1();
		VirtualAxis_t2395187226 * L_7 = ___virtualAxis0;
		NullCheck(L_7);
		String_t* L_8 = VirtualAxis_get_Name_m1171714283(L_7, /*hidden argument*/NULL);
		List_1_t3763372778 * L_9 = (List_1_t3763372778 *)il2cpp_codegen_object_new(List_1_t3763372778_il2cpp_TypeInfo_var);
		List_1__ctor_m3061809494(L_9, /*hidden argument*/List_1__ctor_m3061809494_MethodInfo_var);
		NullCheck(L_6);
		Dictionary_2_set_Item_m435531778(L_6, L_8, L_9, /*hidden argument*/Dictionary_2_set_Item_m435531778_MethodInfo_var);
	}

IL_0034:
	{
		CnInputManager_t540789462 * L_10 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_t288823852 * L_11 = L_10->get__virtualAxisDictionary_1();
		VirtualAxis_t2395187226 * L_12 = ___virtualAxis0;
		NullCheck(L_12);
		String_t* L_13 = VirtualAxis_get_Name_m1171714283(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_t3763372778 * L_14 = Dictionary_2_get_Item_m189145749(L_11, L_13, /*hidden argument*/Dictionary_2_get_Item_m189145749_MethodInfo_var);
		VirtualAxis_t2395187226 * L_15 = ___virtualAxis0;
		NullCheck(L_14);
		List_1_Add_m2756170310(L_14, L_15, /*hidden argument*/List_1_Add_m2756170310_MethodInfo_var);
		return;
	}
}
// System.Void CnControls.CnInputManager::UnregisterVirtualAxis(CnControls.VirtualAxis)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3307480068_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m189145749_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m1630851271_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1022993741;
extern Il2CppCodeGenString* _stringLiteral1716871397;
extern Il2CppCodeGenString* _stringLiteral2864362756;
extern Il2CppCodeGenString* _stringLiteral1395099046;
extern const uint32_t CnInputManager_UnregisterVirtualAxis_m1023264331_MetadataUsageId;
extern "C"  void CnInputManager_UnregisterVirtualAxis_m1023264331 (Il2CppObject * __this /* static, unused */, VirtualAxis_t2395187226 * ___virtualAxis0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_UnregisterVirtualAxis_m1023264331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CnInputManager_t540789462 * L_0 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t288823852 * L_1 = L_0->get__virtualAxisDictionary_1();
		VirtualAxis_t2395187226 * L_2 = ___virtualAxis0;
		NullCheck(L_2);
		String_t* L_3 = VirtualAxis_get_Name_m1171714283(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4 = Dictionary_2_ContainsKey_m3307480068(L_1, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m3307480068_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0059;
		}
	}
	{
		CnInputManager_t540789462 * L_5 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t288823852 * L_6 = L_5->get__virtualAxisDictionary_1();
		VirtualAxis_t2395187226 * L_7 = ___virtualAxis0;
		NullCheck(L_7);
		String_t* L_8 = VirtualAxis_get_Name_m1171714283(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		List_1_t3763372778 * L_9 = Dictionary_2_get_Item_m189145749(L_6, L_8, /*hidden argument*/Dictionary_2_get_Item_m189145749_MethodInfo_var);
		VirtualAxis_t2395187226 * L_10 = ___virtualAxis0;
		NullCheck(L_9);
		bool L_11 = List_1_Remove_m1630851271(L_9, L_10, /*hidden argument*/List_1_Remove_m1630851271_MethodInfo_var);
		if (L_11)
		{
			goto IL_0054;
		}
	}
	{
		VirtualAxis_t2395187226 * L_12 = ___virtualAxis0;
		NullCheck(L_12);
		String_t* L_13 = VirtualAxis_get_Name_m1171714283(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1022993741, L_13, _stringLiteral1716871397, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0054:
	{
		goto IL_0073;
	}

IL_0059:
	{
		VirtualAxis_t2395187226 * L_15 = ___virtualAxis0;
		NullCheck(L_15);
		String_t* L_16 = VirtualAxis_get_Name_m1171714283(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2864362756, L_16, _stringLiteral1395099046, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void CnControls.CnInputManager::RegisterVirtualButton(CnControls.VirtualButton)
extern Il2CppClass* List_1_t408541947_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2679750675_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3644703655_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m941700369_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3625649830_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3339064471_MethodInfo_var;
extern const uint32_t CnInputManager_RegisterVirtualButton_m3391168612_MetadataUsageId;
extern "C"  void CnInputManager_RegisterVirtualButton_m3391168612 (Il2CppObject * __this /* static, unused */, VirtualButton_t3335323691 * ___virtualButton0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_RegisterVirtualButton_m3391168612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CnInputManager_t540789462 * L_0 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t1228960317 * L_1 = L_0->get__virtualButtonsDictionary_2();
		VirtualButton_t3335323691 * L_2 = ___virtualButton0;
		NullCheck(L_2);
		String_t* L_3 = VirtualButton_get_Name_m3237498300(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4 = Dictionary_2_ContainsKey_m2679750675(L_1, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m2679750675_MethodInfo_var);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		CnInputManager_t540789462 * L_5 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t1228960317 * L_6 = L_5->get__virtualButtonsDictionary_2();
		VirtualButton_t3335323691 * L_7 = ___virtualButton0;
		NullCheck(L_7);
		String_t* L_8 = VirtualButton_get_Name_m3237498300(L_7, /*hidden argument*/NULL);
		List_1_t408541947 * L_9 = (List_1_t408541947 *)il2cpp_codegen_object_new(List_1_t408541947_il2cpp_TypeInfo_var);
		List_1__ctor_m3644703655(L_9, /*hidden argument*/List_1__ctor_m3644703655_MethodInfo_var);
		NullCheck(L_6);
		Dictionary_2_set_Item_m941700369(L_6, L_8, L_9, /*hidden argument*/Dictionary_2_set_Item_m941700369_MethodInfo_var);
	}

IL_0034:
	{
		CnInputManager_t540789462 * L_10 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_t1228960317 * L_11 = L_10->get__virtualButtonsDictionary_2();
		VirtualButton_t3335323691 * L_12 = ___virtualButton0;
		NullCheck(L_12);
		String_t* L_13 = VirtualButton_get_Name_m3237498300(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_t408541947 * L_14 = Dictionary_2_get_Item_m3625649830(L_11, L_13, /*hidden argument*/Dictionary_2_get_Item_m3625649830_MethodInfo_var);
		VirtualButton_t3335323691 * L_15 = ___virtualButton0;
		NullCheck(L_14);
		List_1_Add_m3339064471(L_14, L_15, /*hidden argument*/List_1_Add_m3339064471_MethodInfo_var);
		return;
	}
}
// System.Void CnControls.CnInputManager::UnregisterVirtualButton(CnControls.VirtualButton)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2679750675_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3625649830_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m932886486_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2024883569;
extern Il2CppCodeGenString* _stringLiteral3180212815;
extern const uint32_t CnInputManager_UnregisterVirtualButton_m1628192363_MetadataUsageId;
extern "C"  void CnInputManager_UnregisterVirtualButton_m1628192363 (Il2CppObject * __this /* static, unused */, VirtualButton_t3335323691 * ___virtualButton0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_UnregisterVirtualButton_m1628192363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CnInputManager_t540789462 * L_0 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dictionary_2_t1228960317 * L_1 = L_0->get__virtualButtonsDictionary_2();
		VirtualButton_t3335323691 * L_2 = ___virtualButton0;
		NullCheck(L_2);
		String_t* L_3 = VirtualButton_get_Name_m3237498300(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4 = Dictionary_2_ContainsKey_m2679750675(L_1, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m2679750675_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		CnInputManager_t540789462 * L_5 = CnInputManager_get_Instance_m1958956814(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Dictionary_2_t1228960317 * L_6 = L_5->get__virtualButtonsDictionary_2();
		VirtualButton_t3335323691 * L_7 = ___virtualButton0;
		NullCheck(L_7);
		String_t* L_8 = VirtualButton_get_Name_m3237498300(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		List_1_t408541947 * L_9 = Dictionary_2_get_Item_m3625649830(L_6, L_8, /*hidden argument*/Dictionary_2_get_Item_m3625649830_MethodInfo_var);
		VirtualButton_t3335323691 * L_10 = ___virtualButton0;
		NullCheck(L_9);
		bool L_11 = List_1_Remove_m932886486(L_9, L_10, /*hidden argument*/List_1_Remove_m932886486_MethodInfo_var);
		if (L_11)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2024883569, /*hidden argument*/NULL);
	}

IL_0044:
	{
		goto IL_0053;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral3180212815, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Single CnControls.CnInputManager::GetVirtualAxisValue(System.Collections.Generic.List`1<CnControls.VirtualAxis>,System.String,System.Boolean)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1197850629_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2758802384_MethodInfo_var;
extern const uint32_t CnInputManager_GetVirtualAxisValue_m4142473329_MetadataUsageId;
extern "C"  float CnInputManager_GetVirtualAxisValue_m4142473329 (Il2CppObject * __this /* static, unused */, List_1_t3763372778 * ___virtualAxisList0, String_t* ___axisName1, bool ___isRaw2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetVirtualAxisValue_m4142473329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___isRaw2;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___axisName1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxisRaw_m1900207208(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0011:
	{
		String_t* L_3 = ___axisName1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_4 = Input_GetAxis_m2027668530(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_6 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_5, (0.0f), /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002a;
		}
	}
	{
		float L_7 = V_0;
		return L_7;
	}

IL_002a:
	{
		V_1 = 0;
		goto IL_0054;
	}

IL_0031:
	{
		List_1_t3763372778 * L_8 = ___virtualAxisList0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		VirtualAxis_t2395187226 * L_10 = List_1_get_Item_m1197850629(L_8, L_9, /*hidden argument*/List_1_get_Item_m1197850629_MethodInfo_var);
		NullCheck(L_10);
		float L_11 = VirtualAxis_get_Value_m954379946(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_13 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_12, (0.0f), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0050;
		}
	}
	{
		float L_14 = V_2;
		return L_14;
	}

IL_0050:
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_16 = V_1;
		List_1_t3763372778 * L_17 = ___virtualAxisList0;
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2758802384(L_17, /*hidden argument*/List_1_get_Count_m2758802384_MethodInfo_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0031;
		}
	}
	{
		return (0.0f);
	}
}
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonDown(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern const MethodInfo* List_1_get_Item_m853421844_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m18163745_MethodInfo_var;
extern const uint32_t CnInputManager_GetAnyVirtualButtonDown_m706362819_MetadataUsageId;
extern "C"  bool CnInputManager_GetAnyVirtualButtonDown_m706362819 (Il2CppObject * __this /* static, unused */, List_1_t408541947 * ___virtualButtons0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetAnyVirtualButtonDown_m706362819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001e;
	}

IL_0007:
	{
		List_1_t408541947 * L_0 = ___virtualButtons0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		VirtualButton_t3335323691 * L_2 = List_1_get_Item_m853421844(L_0, L_1, /*hidden argument*/List_1_get_Item_m853421844_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = VirtualButton_get_GetButtonDown_m2531325090(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_5 = V_0;
		List_1_t408541947 * L_6 = ___virtualButtons0;
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m18163745(L_6, /*hidden argument*/List_1_get_Count_m18163745_MethodInfo_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonUp(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern const MethodInfo* List_1_get_Item_m853421844_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m18163745_MethodInfo_var;
extern const uint32_t CnInputManager_GetAnyVirtualButtonUp_m3586789226_MetadataUsageId;
extern "C"  bool CnInputManager_GetAnyVirtualButtonUp_m3586789226 (Il2CppObject * __this /* static, unused */, List_1_t408541947 * ___virtualButtons0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetAnyVirtualButtonUp_m3586789226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001e;
	}

IL_0007:
	{
		List_1_t408541947 * L_0 = ___virtualButtons0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		VirtualButton_t3335323691 * L_2 = List_1_get_Item_m853421844(L_0, L_1, /*hidden argument*/List_1_get_Item_m853421844_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = VirtualButton_get_GetButtonUp_m3891402779(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_5 = V_0;
		List_1_t408541947 * L_6 = ___virtualButtons0;
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m18163745(L_6, /*hidden argument*/List_1_get_Count_m18163745_MethodInfo_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButton(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern const MethodInfo* List_1_get_Item_m853421844_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m18163745_MethodInfo_var;
extern const uint32_t CnInputManager_GetAnyVirtualButton_m3717308741_MetadataUsageId;
extern "C"  bool CnInputManager_GetAnyVirtualButton_m3717308741 (Il2CppObject * __this /* static, unused */, List_1_t408541947 * ___virtualButtons0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CnInputManager_GetAnyVirtualButton_m3717308741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001e;
	}

IL_0007:
	{
		List_1_t408541947 * L_0 = ___virtualButtons0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		VirtualButton_t3335323691 * L_2 = List_1_get_Item_m853421844(L_0, L_1, /*hidden argument*/List_1_get_Item_m853421844_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = VirtualButton_get_GetButton_m330304480(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_5 = V_0;
		List_1_t408541947 * L_6 = ___virtualButtons0;
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m18163745(L_6, /*hidden argument*/List_1_get_Count_m18163745_MethodInfo_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void CnControls.Dpad::.ctor()
extern "C"  void Dpad__ctor_m1778790011 (Dpad_t3878341687 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Camera CnControls.Dpad::get_CurrentEventCamera()
extern "C"  Camera_t2727095145 * Dpad_get_CurrentEventCamera_m3691154095 (Dpad_t3878341687 * __this, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = __this->get_U3CCurrentEventCameraU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void CnControls.Dpad::set_CurrentEventCamera(UnityEngine.Camera)
extern "C"  void Dpad_set_CurrentEventCamera_m2950020208 (Dpad_t3878341687 * __this, Camera_t2727095145 * ___value0, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___value0;
		__this->set_U3CCurrentEventCameraU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void CnControls.Dpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t Dpad_OnPointerDown_m453381349_MetadataUsageId;
extern "C"  void Dpad_OnPointerDown_m453381349 (Dpad_t3878341687 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dpad_OnPointerDown_m453381349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DpadAxis_t235416056 * V_0 = NULL;
	DpadAxisU5BU5D_t1203427113* V_1 = NULL;
	int32_t V_2 = 0;
	Camera_t2727095145 * G_B2_0 = NULL;
	Dpad_t3878341687 * G_B2_1 = NULL;
	Camera_t2727095145 * G_B1_0 = NULL;
	Dpad_t3878341687 * G_B1_1 = NULL;
	{
		PointerEventData_t1848751023 * L_0 = ___eventData0;
		NullCheck(L_0);
		Camera_t2727095145 * L_1 = PointerEventData_get_pressEventCamera_m2764092724(L_0, /*hidden argument*/NULL);
		Camera_t2727095145 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = __this;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = __this;
			goto IL_0014;
		}
	}
	{
		Camera_t2727095145 * L_3 = Dpad_get_CurrentEventCamera_m3691154095(__this, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0014:
	{
		NullCheck(G_B2_1);
		Dpad_set_CurrentEventCamera_m2950020208(G_B2_1, G_B2_0, /*hidden argument*/NULL);
		DpadAxisU5BU5D_t1203427113* L_4 = __this->get_DpadAxis_2();
		V_1 = L_4;
		V_2 = 0;
		goto IL_0063;
	}

IL_0027:
	{
		DpadAxisU5BU5D_t1203427113* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		DpadAxis_t235416056 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = L_8;
		DpadAxis_t235416056 * L_9 = V_0;
		NullCheck(L_9);
		RectTransform_t972643934 * L_10 = DpadAxis_get_RectTransform_m108508377(L_9, /*hidden argument*/NULL);
		PointerEventData_t1848751023 * L_11 = ___eventData0;
		NullCheck(L_11);
		Vector2_t4282066565  L_12 = PointerEventData_get_position_m2263123361(L_11, /*hidden argument*/NULL);
		Camera_t2727095145 * L_13 = Dpad_get_CurrentEventCamera_m3691154095(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		bool L_14 = RectTransformUtility_RectangleContainsScreenPoint_m1460676684(NULL /*static, unused*/, L_10, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_005f;
		}
	}
	{
		DpadAxis_t235416056 * L_15 = V_0;
		PointerEventData_t1848751023 * L_16 = ___eventData0;
		NullCheck(L_16);
		Vector2_t4282066565  L_17 = PointerEventData_get_position_m2263123361(L_16, /*hidden argument*/NULL);
		Camera_t2727095145 * L_18 = Dpad_get_CurrentEventCamera_m3691154095(__this, /*hidden argument*/NULL);
		PointerEventData_t1848751023 * L_19 = ___eventData0;
		NullCheck(L_19);
		int32_t L_20 = PointerEventData_get_pointerId_m315063471(L_19, /*hidden argument*/NULL);
		NullCheck(L_15);
		DpadAxis_Press_m531132620(L_15, L_17, L_18, L_20, /*hidden argument*/NULL);
	}

IL_005f:
	{
		int32_t L_21 = V_2;
		V_2 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0063:
	{
		int32_t L_22 = V_2;
		DpadAxisU5BU5D_t1203427113* L_23 = V_1;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		return;
	}
}
// System.Void CnControls.Dpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Dpad_OnPointerUp_m930694092 (Dpad_t3878341687 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	DpadAxis_t235416056 * V_0 = NULL;
	DpadAxisU5BU5D_t1203427113* V_1 = NULL;
	int32_t V_2 = 0;
	{
		DpadAxisU5BU5D_t1203427113* L_0 = __this->get_DpadAxis_2();
		V_1 = L_0;
		V_2 = 0;
		goto IL_0022;
	}

IL_000e:
	{
		DpadAxisU5BU5D_t1203427113* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		DpadAxis_t235416056 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		DpadAxis_t235416056 * L_5 = V_0;
		PointerEventData_t1848751023 * L_6 = ___eventData0;
		NullCheck(L_6);
		int32_t L_7 = PointerEventData_get_pointerId_m315063471(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		DpadAxis_TryRelease_m3480600199(L_5, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_9 = V_2;
		DpadAxisU5BU5D_t1203427113* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void CnControls.DpadAxis::.ctor()
extern "C"  void DpadAxis__ctor_m3232219098 (DpadAxis_t235416056 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform CnControls.DpadAxis::get_RectTransform()
extern "C"  RectTransform_t972643934 * DpadAxis_get_RectTransform_m108508377 (DpadAxis_t235416056 * __this, const MethodInfo* method)
{
	{
		RectTransform_t972643934 * L_0 = __this->get_U3CRectTransformU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void CnControls.DpadAxis::set_RectTransform(UnityEngine.RectTransform)
extern "C"  void DpadAxis_set_RectTransform_m1413112094 (DpadAxis_t235416056 * __this, RectTransform_t972643934 * ___value0, const MethodInfo* method)
{
	{
		RectTransform_t972643934 * L_0 = ___value0;
		__this->set_U3CRectTransformU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Int32 CnControls.DpadAxis::get_LastFingerId()
extern "C"  int32_t DpadAxis_get_LastFingerId_m428322455 (DpadAxis_t235416056 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CLastFingerIdU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void CnControls.DpadAxis::set_LastFingerId(System.Int32)
extern "C"  void DpadAxis_set_LastFingerId_m1979767442 (DpadAxis_t235416056 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CLastFingerIdU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void CnControls.DpadAxis::Awake()
extern const MethodInfo* Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var;
extern const uint32_t DpadAxis_Awake_m3469824317_MetadataUsageId;
extern "C"  void DpadAxis_Awake_m3469824317 (DpadAxis_t235416056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DpadAxis_Awake_m3469824317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t972643934 * L_0 = Component_GetComponent_TisRectTransform_t972643934_m1940403147(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var);
		DpadAxis_set_RectTransform_m1413112094(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.DpadAxis::OnEnable()
extern Il2CppClass* VirtualAxis_t2395187226_il2cpp_TypeInfo_var;
extern const uint32_t DpadAxis_OnEnable_m4238917996_MetadataUsageId;
extern "C"  void DpadAxis_OnEnable_m4238917996 (DpadAxis_t235416056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DpadAxis_OnEnable_m4238917996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VirtualAxis_t2395187226 * G_B2_0 = NULL;
	DpadAxis_t235416056 * G_B2_1 = NULL;
	VirtualAxis_t2395187226 * G_B1_0 = NULL;
	DpadAxis_t235416056 * G_B1_1 = NULL;
	{
		VirtualAxis_t2395187226 * L_0 = __this->get__virtualAxis_4();
		VirtualAxis_t2395187226 * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0019;
		}
	}
	{
		String_t* L_2 = __this->get_AxisName_2();
		VirtualAxis_t2395187226 * L_3 = (VirtualAxis_t2395187226 *)il2cpp_codegen_object_new(VirtualAxis_t2395187226_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m2367204594(L_3, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0019:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__virtualAxis_4(G_B2_0);
		DpadAxis_set_LastFingerId_m1979767442(__this, (-1), /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_4 = __this->get__virtualAxis_4();
		CnInputManager_RegisterVirtualAxis_m2285669572(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.DpadAxis::OnDisable()
extern "C"  void DpadAxis_OnDisable_m2998376257 (DpadAxis_t235416056 * __this, const MethodInfo* method)
{
	{
		VirtualAxis_t2395187226 * L_0 = __this->get__virtualAxis_4();
		CnInputManager_UnregisterVirtualAxis_m1023264331(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.DpadAxis::Press(UnityEngine.Vector2,UnityEngine.Camera,System.Int32)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t DpadAxis_Press_m531132620_MetadataUsageId;
extern "C"  void DpadAxis_Press_m531132620 (DpadAxis_t235416056 * __this, Vector2_t4282066565  ___screenPoint0, Camera_t2727095145 * ___eventCamera1, int32_t ___pointerId2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DpadAxis_Press_m531132620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		VirtualAxis_t2395187226 * L_0 = __this->get__virtualAxis_4();
		float L_1 = __this->get_AxisMultiplier_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_1, (-1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtualAxis_set_Value_m3165149161(L_0, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___pointerId2;
		DpadAxis_set_LastFingerId_m1979767442(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.DpadAxis::TryRelease(System.Int32)
extern "C"  void DpadAxis_TryRelease_m3480600199 (DpadAxis_t235416056 * __this, int32_t ___pointerId0, const MethodInfo* method)
{
	{
		int32_t L_0 = DpadAxis_get_LastFingerId_m428322455(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___pointerId0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0023;
		}
	}
	{
		VirtualAxis_t2395187226 * L_2 = __this->get__virtualAxis_4();
		NullCheck(L_2);
		VirtualAxis_set_Value_m3165149161(L_2, (0.0f), /*hidden argument*/NULL);
		DpadAxis_set_LastFingerId_m1979767442(__this, (-1), /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void CnControls.SensitiveJoystick::.ctor()
extern Il2CppClass* KeyframeU5BU5D_t3589549831_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3667593487_il2cpp_TypeInfo_var;
extern const uint32_t SensitiveJoystick__ctor_m3620960106_MetadataUsageId;
extern "C"  void SensitiveJoystick__ctor_m3620960106 (SensitiveJoystick_t1697973888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SensitiveJoystick__ctor_m3620960106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		KeyframeU5BU5D_t3589549831* L_0 = ((KeyframeU5BU5D_t3589549831*)SZArrayNew(KeyframeU5BU5D_t3589549831_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Keyframe_t4079056114  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m3412708539(&L_1, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t4079056114 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		KeyframeU5BU5D_t3589549831* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Keyframe_t4079056114  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m3412708539(&L_3, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t4079056114 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		AnimationCurve_t3667593487 * L_4 = (AnimationCurve_t3667593487 *)il2cpp_codegen_object_new(AnimationCurve_t3667593487_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2436282331(L_4, L_2, /*hidden argument*/NULL);
		__this->set_SensitivityCurve_21(L_4);
		SimpleJoystick__ctor_m2978567484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SensitiveJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SensitiveJoystick_OnDrag_m1401735889_MetadataUsageId;
extern "C"  void SensitiveJoystick_OnDrag_m1401735889 (SensitiveJoystick_t1697973888 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SensitiveJoystick_OnDrag_m1401735889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		PointerEventData_t1848751023 * L_0 = ___eventData0;
		SimpleJoystick_OnDrag_m3343598115(__this, L_0, /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_1 = ((SimpleJoystick_t3821479766 *)__this)->get_HorizintalAxis_18();
		NullCheck(L_1);
		float L_2 = VirtualAxis_get_Value_m954379946(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		VirtualAxis_t2395187226 * L_3 = ((SimpleJoystick_t3821479766 *)__this)->get_VerticalAxis_19();
		NullCheck(L_3);
		float L_4 = VirtualAxis_get_Value_m954379946(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = V_1;
		float L_8 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		VirtualAxis_t2395187226 * L_9 = ((SimpleJoystick_t3821479766 *)__this)->get_HorizintalAxis_18();
		float L_10 = V_2;
		AnimationCurve_t3667593487 * L_11 = __this->get_SensitivityCurve_21();
		float L_12 = V_2;
		float L_13 = V_0;
		NullCheck(L_11);
		float L_14 = AnimationCurve_Evaluate_m547727012(L_11, ((float)((float)L_12*(float)L_13)), /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtualAxis_set_Value_m3165149161(L_9, ((float)((float)L_10*(float)L_14)), /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_15 = ((SimpleJoystick_t3821479766 *)__this)->get_VerticalAxis_19();
		float L_16 = V_3;
		AnimationCurve_t3667593487 * L_17 = __this->get_SensitivityCurve_21();
		float L_18 = V_3;
		float L_19 = V_1;
		NullCheck(L_17);
		float L_20 = AnimationCurve_Evaluate_m547727012(L_17, ((float)((float)L_18*(float)L_19)), /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtualAxis_set_Value_m3165149161(L_15, ((float)((float)L_16*(float)L_20)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleButton::.ctor()
extern Il2CppCodeGenString* _stringLiteral2320462;
extern const uint32_t SimpleButton__ctor_m552871238_MetadataUsageId;
extern "C"  void SimpleButton__ctor_m552871238 (SimpleButton_t2998894860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleButton__ctor_m552871238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_ButtonName_2(_stringLiteral2320462);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleButton::OnEnable()
extern Il2CppClass* VirtualButton_t3335323691_il2cpp_TypeInfo_var;
extern const uint32_t SimpleButton_OnEnable_m1459049600_MetadataUsageId;
extern "C"  void SimpleButton_OnEnable_m1459049600 (SimpleButton_t2998894860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleButton_OnEnable_m1459049600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VirtualButton_t3335323691 * G_B2_0 = NULL;
	SimpleButton_t2998894860 * G_B2_1 = NULL;
	VirtualButton_t3335323691 * G_B1_0 = NULL;
	SimpleButton_t2998894860 * G_B1_1 = NULL;
	{
		VirtualButton_t3335323691 * L_0 = __this->get__virtualButton_3();
		VirtualButton_t3335323691 * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0019;
		}
	}
	{
		String_t* L_2 = __this->get_ButtonName_2();
		VirtualButton_t3335323691 * L_3 = (VirtualButton_t3335323691 *)il2cpp_codegen_object_new(VirtualButton_t3335323691_il2cpp_TypeInfo_var);
		VirtualButton__ctor_m2558924099(L_3, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0019:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__virtualButton_3(G_B2_0);
		VirtualButton_t3335323691 * L_4 = __this->get__virtualButton_3();
		CnInputManager_RegisterVirtualButton_m3391168612(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleButton::OnDisable()
extern "C"  void SimpleButton_OnDisable_m2721801901 (SimpleButton_t2998894860 * __this, const MethodInfo* method)
{
	{
		VirtualButton_t3335323691 * L_0 = __this->get__virtualButton_3();
		CnInputManager_UnregisterVirtualButton_m1628192363(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SimpleButton_OnPointerUp_m673638817 (SimpleButton_t2998894860 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	{
		VirtualButton_t3335323691 * L_0 = __this->get__virtualButton_3();
		NullCheck(L_0);
		VirtualButton_Release_m1530526852(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SimpleButton_OnPointerDown_m2531365242 (SimpleButton_t2998894860 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	{
		VirtualButton_t3335323691 * L_0 = __this->get__virtualButton_3();
		NullCheck(L_0);
		VirtualButton_Press_m961444032(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleJoystick::.ctor()
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern Il2CppCodeGenString* _stringLiteral2375469974;
extern const uint32_t SimpleJoystick__ctor_m2978567484_MetadataUsageId;
extern "C"  void SimpleJoystick__ctor_m2978567484 (SimpleJoystick_t3821479766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJoystick__ctor_m2978567484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_MovementRange_2((50.0f));
		__this->set_HorizontalAxisName_3(_stringLiteral3381094468);
		__this->set_VerticalAxisName_4(_stringLiteral2375469974);
		__this->set_MoveBase_6((bool)1);
		__this->set_SnapsToFinger_7((bool)1);
		__this->set_JoystickMoveAxis_8(3);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Camera CnControls.SimpleJoystick::get_CurrentEventCamera()
extern "C"  Camera_t2727095145 * SimpleJoystick_get_CurrentEventCamera_m2847661390 (SimpleJoystick_t3821479766 * __this, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = __this->get_U3CCurrentEventCameraU3Ek__BackingField_20();
		return L_0;
	}
}
// System.Void CnControls.SimpleJoystick::set_CurrentEventCamera(UnityEngine.Camera)
extern "C"  void SimpleJoystick_set_CurrentEventCamera_m3883554319 (SimpleJoystick_t3821479766 * __this, Camera_t2727095145 * ___value0, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___value0;
		__this->set_U3CCurrentEventCameraU3Ek__BackingField_20(L_0);
		return;
	}
}
// System.Void CnControls.SimpleJoystick::Awake()
extern const MethodInfo* Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var;
extern const uint32_t SimpleJoystick_Awake_m3216172703_MetadataUsageId;
extern "C"  void SimpleJoystick_Awake_m3216172703 (SimpleJoystick_t3821479766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJoystick_Awake_m3216172703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Image_t538875265 * L_0 = __this->get_Stick_10();
		NullCheck(L_0);
		RectTransform_t972643934 * L_1 = Component_GetComponent_TisRectTransform_t972643934_m1940403147(L_0, /*hidden argument*/Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var);
		__this->set__stickTransform_16(L_1);
		Image_t538875265 * L_2 = __this->get_JoystickBase_9();
		NullCheck(L_2);
		RectTransform_t972643934 * L_3 = Component_GetComponent_TisRectTransform_t972643934_m1940403147(L_2, /*hidden argument*/Component_GetComponent_TisRectTransform_t972643934_m1940403147_MethodInfo_var);
		__this->set__baseTransform_15(L_3);
		RectTransform_t972643934 * L_4 = __this->get__stickTransform_16();
		NullCheck(L_4);
		Vector2_t4282066565  L_5 = RectTransform_get_anchoredPosition_m2318455998(L_4, /*hidden argument*/NULL);
		__this->set__initialStickPosition_12(L_5);
		Vector2_t4282066565  L_6 = __this->get__initialStickPosition_12();
		__this->set__intermediateStickPosition_13(L_6);
		RectTransform_t972643934 * L_7 = __this->get__baseTransform_15();
		NullCheck(L_7);
		Vector2_t4282066565  L_8 = RectTransform_get_anchoredPosition_m2318455998(L_7, /*hidden argument*/NULL);
		__this->set__initialBasePosition_14(L_8);
		RectTransform_t972643934 * L_9 = __this->get__stickTransform_16();
		Vector2_t4282066565  L_10 = __this->get__initialStickPosition_12();
		NullCheck(L_9);
		RectTransform_set_anchoredPosition_m1498949997(L_9, L_10, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_11 = __this->get__baseTransform_15();
		Vector2_t4282066565  L_12 = __this->get__initialBasePosition_14();
		NullCheck(L_11);
		RectTransform_set_anchoredPosition_m1498949997(L_11, L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_MovementRange_2();
		__this->set__oneOverMovementRange_17(((float)((float)(1.0f)/(float)L_13)));
		bool L_14 = __this->get_HideOnRelease_5();
		if (!L_14)
		{
			goto IL_0096;
		}
	}
	{
		SimpleJoystick_Hide_m1296302401(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0096:
	{
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnEnable()
extern Il2CppClass* VirtualAxis_t2395187226_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJoystick_OnEnable_m2551158986_MetadataUsageId;
extern "C"  void SimpleJoystick_OnEnable_m2551158986 (SimpleJoystick_t3821479766 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJoystick_OnEnable_m2551158986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VirtualAxis_t2395187226 * G_B2_0 = NULL;
	SimpleJoystick_t3821479766 * G_B2_1 = NULL;
	VirtualAxis_t2395187226 * G_B1_0 = NULL;
	SimpleJoystick_t3821479766 * G_B1_1 = NULL;
	VirtualAxis_t2395187226 * G_B4_0 = NULL;
	SimpleJoystick_t3821479766 * G_B4_1 = NULL;
	VirtualAxis_t2395187226 * G_B3_0 = NULL;
	SimpleJoystick_t3821479766 * G_B3_1 = NULL;
	{
		VirtualAxis_t2395187226 * L_0 = __this->get_HorizintalAxis_18();
		VirtualAxis_t2395187226 * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0019;
		}
	}
	{
		String_t* L_2 = __this->get_HorizontalAxisName_3();
		VirtualAxis_t2395187226 * L_3 = (VirtualAxis_t2395187226 *)il2cpp_codegen_object_new(VirtualAxis_t2395187226_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m2367204594(L_3, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0019:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_HorizintalAxis_18(G_B2_0);
		VirtualAxis_t2395187226 * L_4 = __this->get_VerticalAxis_19();
		VirtualAxis_t2395187226 * L_5 = L_4;
		G_B3_0 = L_5;
		G_B3_1 = __this;
		if (L_5)
		{
			G_B4_0 = L_5;
			G_B4_1 = __this;
			goto IL_0037;
		}
	}
	{
		String_t* L_6 = __this->get_VerticalAxisName_4();
		VirtualAxis_t2395187226 * L_7 = (VirtualAxis_t2395187226 *)il2cpp_codegen_object_new(VirtualAxis_t2395187226_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m2367204594(L_7, L_6, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B3_1;
	}

IL_0037:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_VerticalAxis_19(G_B4_0);
		VirtualAxis_t2395187226 * L_8 = __this->get_HorizintalAxis_18();
		CnInputManager_RegisterVirtualAxis_m2285669572(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_9 = __this->get_VerticalAxis_19();
		CnInputManager_RegisterVirtualAxis_m2285669572(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnDisable()
extern "C"  void SimpleJoystick_OnDisable_m2217454499 (SimpleJoystick_t3821479766 * __this, const MethodInfo* method)
{
	{
		VirtualAxis_t2395187226 * L_0 = __this->get_HorizintalAxis_18();
		CnInputManager_UnregisterVirtualAxis_m1023264331(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_1 = __this->get_VerticalAxis_19();
		CnInputManager_UnregisterVirtualAxis_m1023264331(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJoystick_OnDrag_m3343598115_MetadataUsageId;
extern "C"  void SimpleJoystick_OnDrag_m3343598115 (SimpleJoystick_t3821479766 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJoystick_OnDrag_m3343598115_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector2_t4282066565  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t4282066565  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t4282066565  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Camera_t2727095145 * G_B2_0 = NULL;
	SimpleJoystick_t3821479766 * G_B2_1 = NULL;
	Camera_t2727095145 * G_B1_0 = NULL;
	SimpleJoystick_t3821479766 * G_B1_1 = NULL;
	{
		PointerEventData_t1848751023 * L_0 = ___eventData0;
		NullCheck(L_0);
		Camera_t2727095145 * L_1 = PointerEventData_get_pressEventCamera_m2764092724(L_0, /*hidden argument*/NULL);
		Camera_t2727095145 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = __this;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = __this;
			goto IL_0014;
		}
	}
	{
		Camera_t2727095145 * L_3 = SimpleJoystick_get_CurrentEventCamera_m2847661390(__this, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0014:
	{
		NullCheck(G_B2_1);
		SimpleJoystick_set_CurrentEventCamera_m3883554319(G_B2_1, G_B2_0, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_4 = __this->get__stickTransform_16();
		PointerEventData_t1848751023 * L_5 = ___eventData0;
		NullCheck(L_5);
		Vector2_t4282066565  L_6 = PointerEventData_get_position_m2263123361(L_5, /*hidden argument*/NULL);
		Camera_t2727095145 * L_7 = SimpleJoystick_get_CurrentEventCamera_m2847661390(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718(NULL /*static, unused*/, L_4, L_6, L_7, (&V_0), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_8 = __this->get__stickTransform_16();
		Vector3_t4282066566  L_9 = V_0;
		NullCheck(L_8);
		Transform_set_position_m3111394108(L_8, L_9, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_10 = __this->get__stickTransform_16();
		NullCheck(L_10);
		Vector2_t4282066565  L_11 = RectTransform_get_anchoredPosition_m2318455998(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		int32_t L_12 = __this->get_JoystickMoveAxis_8();
		if (((int32_t)((int32_t)L_12&(int32_t)1)))
		{
			goto IL_006a;
		}
	}
	{
		Vector2_t4282066565 * L_13 = __this->get_address_of__intermediateStickPosition_13();
		float L_14 = L_13->get_x_1();
		(&V_1)->set_x_1(L_14);
	}

IL_006a:
	{
		int32_t L_15 = __this->get_JoystickMoveAxis_8();
		if (((int32_t)((int32_t)L_15&(int32_t)2)))
		{
			goto IL_0089;
		}
	}
	{
		Vector2_t4282066565 * L_16 = __this->get_address_of__intermediateStickPosition_13();
		float L_17 = L_16->get_y_2();
		(&V_1)->set_y_2(L_17);
	}

IL_0089:
	{
		RectTransform_t972643934 * L_18 = __this->get__stickTransform_16();
		Vector2_t4282066565  L_19 = V_1;
		NullCheck(L_18);
		RectTransform_set_anchoredPosition_m1498949997(L_18, L_19, /*hidden argument*/NULL);
		float L_20 = (&V_1)->get_x_1();
		float L_21 = (&V_1)->get_y_2();
		Vector2_t4282066565  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector2__ctor_m1517109030(&L_22, L_20, L_21, /*hidden argument*/NULL);
		Vector2_t4282066565  L_23 = __this->get__intermediateStickPosition_13();
		Vector2_t4282066565  L_24 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		float L_25 = Vector2_get_magnitude_m1987058139((&V_2), /*hidden argument*/NULL);
		V_3 = L_25;
		Vector2_t4282066565  L_26 = V_2;
		float L_27 = V_3;
		Vector2_t4282066565  L_28 = Vector2_op_Division_m747627697(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		float L_29 = V_3;
		float L_30 = __this->get_MovementRange_2();
		if ((!(((float)L_29) > ((float)L_30))))
		{
			goto IL_0155;
		}
	}
	{
		bool L_31 = __this->get_MoveBase_6();
		if (!L_31)
		{
			goto IL_0132;
		}
	}
	{
		bool L_32 = __this->get_SnapsToFinger_7();
		if (!L_32)
		{
			goto IL_0132;
		}
	}
	{
		float L_33 = Vector2_get_magnitude_m1987058139((&V_2), /*hidden argument*/NULL);
		float L_34 = __this->get_MovementRange_2();
		V_5 = ((float)((float)L_33-(float)L_34));
		Vector2_t4282066565  L_35 = V_4;
		float L_36 = V_5;
		Vector2_t4282066565  L_37 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_6 = L_37;
		RectTransform_t972643934 * L_38 = __this->get__baseTransform_15();
		RectTransform_t972643934 * L_39 = L_38;
		NullCheck(L_39);
		Vector2_t4282066565  L_40 = RectTransform_get_anchoredPosition_m2318455998(L_39, /*hidden argument*/NULL);
		Vector2_t4282066565  L_41 = V_6;
		Vector2_t4282066565  L_42 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_39);
		RectTransform_set_anchoredPosition_m1498949997(L_39, L_42, /*hidden argument*/NULL);
		Vector2_t4282066565  L_43 = __this->get__intermediateStickPosition_13();
		Vector2_t4282066565  L_44 = V_6;
		Vector2_t4282066565  L_45 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		__this->set__intermediateStickPosition_13(L_45);
		goto IL_0155;
	}

IL_0132:
	{
		RectTransform_t972643934 * L_46 = __this->get__stickTransform_16();
		Vector2_t4282066565  L_47 = __this->get__intermediateStickPosition_13();
		Vector2_t4282066565  L_48 = V_4;
		float L_49 = __this->get_MovementRange_2();
		Vector2_t4282066565  L_50 = Vector2_op_Multiply_m1738245082(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		Vector2_t4282066565  L_51 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_47, L_50, /*hidden argument*/NULL);
		NullCheck(L_46);
		RectTransform_set_anchoredPosition_m1498949997(L_46, L_51, /*hidden argument*/NULL);
	}

IL_0155:
	{
		RectTransform_t972643934 * L_52 = __this->get__stickTransform_16();
		NullCheck(L_52);
		Vector2_t4282066565  L_53 = RectTransform_get_anchoredPosition_m2318455998(L_52, /*hidden argument*/NULL);
		V_7 = L_53;
		float L_54 = (&V_7)->get_x_1();
		float L_55 = (&V_7)->get_y_2();
		Vector2_t4282066565  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Vector2__ctor_m1517109030(&L_56, L_54, L_55, /*hidden argument*/NULL);
		Vector2_t4282066565  L_57 = __this->get__intermediateStickPosition_13();
		Vector2_t4282066565  L_58 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_56, L_57, /*hidden argument*/NULL);
		V_8 = L_58;
		float L_59 = (&V_8)->get_x_1();
		float L_60 = __this->get__oneOverMovementRange_17();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_61 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_59*(float)L_60)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_9 = L_61;
		float L_62 = (&V_8)->get_y_2();
		float L_63 = __this->get__oneOverMovementRange_17();
		float L_64 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)L_62*(float)L_63)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_10 = L_64;
		VirtualAxis_t2395187226 * L_65 = __this->get_HorizintalAxis_18();
		float L_66 = V_9;
		NullCheck(L_65);
		VirtualAxis_set_Value_m3165149161(L_65, L_66, /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_67 = __this->get_VerticalAxis_19();
		float L_68 = V_10;
		NullCheck(L_67);
		VirtualAxis_set_Value_m3165149161(L_67, L_68, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SimpleJoystick_OnPointerUp_m2338416363 (SimpleJoystick_t3821479766 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		RectTransform_t972643934 * L_0 = __this->get__baseTransform_15();
		Vector2_t4282066565  L_1 = __this->get__initialBasePosition_14();
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m1498949997(L_0, L_1, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_2 = __this->get__stickTransform_16();
		Vector2_t4282066565  L_3 = __this->get__initialStickPosition_12();
		NullCheck(L_2);
		RectTransform_set_anchoredPosition_m1498949997(L_2, L_3, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = __this->get__initialStickPosition_12();
		__this->set__intermediateStickPosition_13(L_4);
		VirtualAxis_t2395187226 * L_5 = __this->get_HorizintalAxis_18();
		V_0 = (0.0f);
		VirtualAxis_t2395187226 * L_6 = __this->get_VerticalAxis_19();
		float L_7 = V_0;
		NullCheck(L_6);
		VirtualAxis_set_Value_m3165149161(L_6, L_7, /*hidden argument*/NULL);
		float L_8 = V_0;
		NullCheck(L_5);
		VirtualAxis_set_Value_m3165149161(L_5, L_8, /*hidden argument*/NULL);
		bool L_9 = __this->get_HideOnRelease_5();
		if (!L_9)
		{
			goto IL_005e;
		}
	}
	{
		SimpleJoystick_Hide_m1296302401(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void CnControls.SimpleJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJoystick_OnPointerDown_m359785540_MetadataUsageId;
extern "C"  void SimpleJoystick_OnPointerDown_m359785540 (SimpleJoystick_t3821479766 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJoystick_OnPointerDown_m359785540_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Camera_t2727095145 * G_B3_0 = NULL;
	SimpleJoystick_t3821479766 * G_B3_1 = NULL;
	Camera_t2727095145 * G_B2_0 = NULL;
	SimpleJoystick_t3821479766 * G_B2_1 = NULL;
	{
		bool L_0 = __this->get_SnapsToFinger_7();
		if (!L_0)
		{
			goto IL_0086;
		}
	}
	{
		PointerEventData_t1848751023 * L_1 = ___eventData0;
		NullCheck(L_1);
		Camera_t2727095145 * L_2 = PointerEventData_get_pressEventCamera_m2764092724(L_1, /*hidden argument*/NULL);
		Camera_t2727095145 * L_3 = L_2;
		G_B2_0 = L_3;
		G_B2_1 = __this;
		if (L_3)
		{
			G_B3_0 = L_3;
			G_B3_1 = __this;
			goto IL_001f;
		}
	}
	{
		Camera_t2727095145 * L_4 = SimpleJoystick_get_CurrentEventCamera_m2847661390(__this, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_1;
	}

IL_001f:
	{
		NullCheck(G_B3_1);
		SimpleJoystick_set_CurrentEventCamera_m3883554319(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_5 = __this->get__stickTransform_16();
		PointerEventData_t1848751023 * L_6 = ___eventData0;
		NullCheck(L_6);
		Vector2_t4282066565  L_7 = PointerEventData_get_position_m2263123361(L_6, /*hidden argument*/NULL);
		Camera_t2727095145 * L_8 = SimpleJoystick_get_CurrentEventCamera_m2847661390(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718(NULL /*static, unused*/, L_5, L_7, L_8, (&V_0), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_9 = __this->get__baseTransform_15();
		PointerEventData_t1848751023 * L_10 = ___eventData0;
		NullCheck(L_10);
		Vector2_t4282066565  L_11 = PointerEventData_get_position_m2263123361(L_10, /*hidden argument*/NULL);
		Camera_t2727095145 * L_12 = SimpleJoystick_get_CurrentEventCamera_m2847661390(__this, /*hidden argument*/NULL);
		RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718(NULL /*static, unused*/, L_9, L_11, L_12, (&V_1), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_13 = __this->get__baseTransform_15();
		Vector3_t4282066566  L_14 = V_1;
		NullCheck(L_13);
		Transform_set_position_m3111394108(L_13, L_14, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_15 = __this->get__stickTransform_16();
		Vector3_t4282066566  L_16 = V_0;
		NullCheck(L_15);
		Transform_set_position_m3111394108(L_15, L_16, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_17 = __this->get__stickTransform_16();
		NullCheck(L_17);
		Vector2_t4282066565  L_18 = RectTransform_get_anchoredPosition_m2318455998(L_17, /*hidden argument*/NULL);
		__this->set__intermediateStickPosition_13(L_18);
		goto IL_008d;
	}

IL_0086:
	{
		PointerEventData_t1848751023 * L_19 = ___eventData0;
		VirtActionInvoker1< PointerEventData_t1848751023 * >::Invoke(7 /* System.Void CnControls.SimpleJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_19);
	}

IL_008d:
	{
		bool L_20 = __this->get_HideOnRelease_5();
		if (!L_20)
		{
			goto IL_009f;
		}
	}
	{
		SimpleJoystick_Hide_m1296302401(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_009f:
	{
		return;
	}
}
// System.Void CnControls.SimpleJoystick::Hide(System.Boolean)
extern "C"  void SimpleJoystick_Hide_m1296302401 (SimpleJoystick_t3821479766 * __this, bool ___isHidden0, const MethodInfo* method)
{
	{
		Image_t538875265 * L_0 = __this->get_JoystickBase_9();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		bool L_2 = ___isHidden0;
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Image_t538875265 * L_3 = __this->get_Stick_10();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		bool L_5 = ___isHidden0;
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.Touchpad::.ctor()
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern Il2CppCodeGenString* _stringLiteral2375469974;
extern const uint32_t Touchpad__ctor_m770695510_MetadataUsageId;
extern "C"  void Touchpad__ctor_m770695510 (Touchpad_t2035887356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Touchpad__ctor_m770695510_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_HorizontalAxisName_2(_stringLiteral3381094468);
		__this->set_VerticalAxisName_3(_stringLiteral2375469974);
		__this->set_PreserveInertia_4((bool)1);
		__this->set_Friction_5((3.0f));
		__this->set_ControlMoveAxis_10(3);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Camera CnControls.Touchpad::get_CurrentEventCamera()
extern "C"  Camera_t2727095145 * Touchpad_get_CurrentEventCamera_m63735220 (Touchpad_t2035887356 * __this, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = __this->get_U3CCurrentEventCameraU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void CnControls.Touchpad::set_CurrentEventCamera(UnityEngine.Camera)
extern "C"  void Touchpad_set_CurrentEventCamera_m3264480181 (Touchpad_t2035887356 * __this, Camera_t2727095145 * ___value0, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___value0;
		__this->set_U3CCurrentEventCameraU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void CnControls.Touchpad::OnEnable()
extern Il2CppClass* VirtualAxis_t2395187226_il2cpp_TypeInfo_var;
extern const uint32_t Touchpad_OnEnable_m966352496_MetadataUsageId;
extern "C"  void Touchpad_OnEnable_m966352496 (Touchpad_t2035887356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Touchpad_OnEnable_m966352496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VirtualAxis_t2395187226 * G_B2_0 = NULL;
	Touchpad_t2035887356 * G_B2_1 = NULL;
	VirtualAxis_t2395187226 * G_B1_0 = NULL;
	Touchpad_t2035887356 * G_B1_1 = NULL;
	VirtualAxis_t2395187226 * G_B4_0 = NULL;
	Touchpad_t2035887356 * G_B4_1 = NULL;
	VirtualAxis_t2395187226 * G_B3_0 = NULL;
	Touchpad_t2035887356 * G_B3_1 = NULL;
	{
		VirtualAxis_t2395187226 * L_0 = __this->get__horizintalAxis_6();
		VirtualAxis_t2395187226 * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0019;
		}
	}
	{
		String_t* L_2 = __this->get_HorizontalAxisName_2();
		VirtualAxis_t2395187226 * L_3 = (VirtualAxis_t2395187226 *)il2cpp_codegen_object_new(VirtualAxis_t2395187226_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m2367204594(L_3, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_0019:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__horizintalAxis_6(G_B2_0);
		VirtualAxis_t2395187226 * L_4 = __this->get__verticalAxis_7();
		VirtualAxis_t2395187226 * L_5 = L_4;
		G_B3_0 = L_5;
		G_B3_1 = __this;
		if (L_5)
		{
			G_B4_0 = L_5;
			G_B4_1 = __this;
			goto IL_0037;
		}
	}
	{
		String_t* L_6 = __this->get_VerticalAxisName_3();
		VirtualAxis_t2395187226 * L_7 = (VirtualAxis_t2395187226 *)il2cpp_codegen_object_new(VirtualAxis_t2395187226_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m2367204594(L_7, L_6, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B3_1;
	}

IL_0037:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__verticalAxis_7(G_B4_0);
		VirtualAxis_t2395187226 * L_8 = __this->get__horizintalAxis_6();
		CnInputManager_RegisterVirtualAxis_m2285669572(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_9 = __this->get__verticalAxis_7();
		CnInputManager_RegisterVirtualAxis_m2285669572(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.Touchpad::OnDisable()
extern "C"  void Touchpad_OnDisable_m333093565 (Touchpad_t2035887356 * __this, const MethodInfo* method)
{
	{
		VirtualAxis_t2395187226 * L_0 = __this->get__horizintalAxis_6();
		CnInputManager_UnregisterVirtualAxis_m1023264331(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_1 = __this->get__verticalAxis_7();
		CnInputManager_UnregisterVirtualAxis_m1023264331(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CnControls.Touchpad::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Touchpad_OnDrag_m1969753533 (Touchpad_t2035887356 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_ControlMoveAxis_10();
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_0026;
		}
	}
	{
		VirtualAxis_t2395187226 * L_1 = __this->get__horizintalAxis_6();
		PointerEventData_t1848751023 * L_2 = ___eventData0;
		NullCheck(L_2);
		Vector2_t4282066565  L_3 = PointerEventData_get_delta_m4112242786(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_x_1();
		NullCheck(L_1);
		VirtualAxis_set_Value_m3165149161(L_1, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		int32_t L_5 = __this->get_ControlMoveAxis_10();
		if (!((int32_t)((int32_t)L_5&(int32_t)2)))
		{
			goto IL_004c;
		}
	}
	{
		VirtualAxis_t2395187226 * L_6 = __this->get__verticalAxis_7();
		PointerEventData_t1848751023 * L_7 = ___eventData0;
		NullCheck(L_7);
		Vector2_t4282066565  L_8 = PointerEventData_get_delta_m4112242786(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = (&V_1)->get_y_2();
		NullCheck(L_6);
		VirtualAxis_set_Value_m3165149161(L_6, L_9, /*hidden argument*/NULL);
	}

IL_004c:
	{
		int32_t L_10 = Time_get_renderedFrameCount_m504365668(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__lastDragFrameNumber_8(L_10);
		return;
	}
}
// System.Void CnControls.Touchpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Touchpad_OnPointerUp_m3176516497 (Touchpad_t2035887356 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	{
		__this->set__isCurrentlyTweaking_9((bool)0);
		bool L_0 = __this->get_PreserveInertia_4();
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		VirtualAxis_t2395187226 * L_1 = __this->get__horizintalAxis_6();
		NullCheck(L_1);
		VirtualAxis_set_Value_m3165149161(L_1, (0.0f), /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_2 = __this->get__verticalAxis_7();
		NullCheck(L_2);
		VirtualAxis_set_Value_m3165149161(L_2, (0.0f), /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void CnControls.Touchpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Touchpad_OnPointerDown_m2615129962 (Touchpad_t2035887356 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	{
		__this->set__isCurrentlyTweaking_9((bool)1);
		PointerEventData_t1848751023 * L_0 = ___eventData0;
		VirtActionInvoker1< PointerEventData_t1848751023 * >::Invoke(7 /* System.Void CnControls.Touchpad::OnDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_0);
		return;
	}
}
// System.Void CnControls.Touchpad::Update()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Touchpad_Update_m4143586423_MetadataUsageId;
extern "C"  void Touchpad_Update_m4143586423 (Touchpad_t2035887356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Touchpad_Update_m4143586423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get__isCurrentlyTweaking_9();
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_1 = __this->get__lastDragFrameNumber_8();
		int32_t L_2 = Time_get_renderedFrameCount_m504365668(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)((int32_t)L_2-(int32_t)2)))))
		{
			goto IL_003d;
		}
	}
	{
		VirtualAxis_t2395187226 * L_3 = __this->get__horizintalAxis_6();
		NullCheck(L_3);
		VirtualAxis_set_Value_m3165149161(L_3, (0.0f), /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_4 = __this->get__verticalAxis_7();
		NullCheck(L_4);
		VirtualAxis_set_Value_m3165149161(L_4, (0.0f), /*hidden argument*/NULL);
	}

IL_003d:
	{
		bool L_5 = __this->get_PreserveInertia_4();
		if (!L_5)
		{
			goto IL_00ab;
		}
	}
	{
		bool L_6 = __this->get__isCurrentlyTweaking_9();
		if (L_6)
		{
			goto IL_00ab;
		}
	}
	{
		VirtualAxis_t2395187226 * L_7 = __this->get__horizintalAxis_6();
		VirtualAxis_t2395187226 * L_8 = __this->get__horizintalAxis_6();
		NullCheck(L_8);
		float L_9 = VirtualAxis_get_Value_m954379946(L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_Friction_5();
		float L_11 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_9, (0.0f), ((float)((float)L_10*(float)L_11)), /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtualAxis_set_Value_m3165149161(L_7, L_12, /*hidden argument*/NULL);
		VirtualAxis_t2395187226 * L_13 = __this->get__verticalAxis_7();
		VirtualAxis_t2395187226 * L_14 = __this->get__verticalAxis_7();
		NullCheck(L_14);
		float L_15 = VirtualAxis_get_Value_m954379946(L_14, /*hidden argument*/NULL);
		float L_16 = __this->get_Friction_5();
		float L_17 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_15, (0.0f), ((float)((float)L_16*(float)L_17)), /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtualAxis_set_Value_m3165149161(L_13, L_18, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		return;
	}
}
// System.Void CnControls.VirtualAxis::.ctor(System.String)
extern "C"  void VirtualAxis__ctor_m2367204594 (VirtualAxis_t2395187226 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		VirtualAxis_set_Name_m2750146694(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String CnControls.VirtualAxis::get_Name()
extern "C"  String_t* VirtualAxis_get_Name_m1171714283 (VirtualAxis_t2395187226 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CnControls.VirtualAxis::set_Name(System.String)
extern "C"  void VirtualAxis_set_Name_m2750146694 (VirtualAxis_t2395187226 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Single CnControls.VirtualAxis::get_Value()
extern "C"  float VirtualAxis_get_Value_m954379946 (VirtualAxis_t2395187226 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void CnControls.VirtualAxis::set_Value(System.Single)
extern "C"  void VirtualAxis_set_Value_m3165149161 (VirtualAxis_t2395187226 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void CnControls.VirtualButton::.ctor(System.String)
extern "C"  void VirtualButton__ctor_m2558924099 (VirtualButton_t3335323691 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		__this->set__lastPressedFrame_0((-1));
		__this->set__lastReleasedFrame_1((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		VirtualButton_set_Name_m1959416469(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String CnControls.VirtualButton::get_Name()
extern "C"  String_t* VirtualButton_get_Name_m3237498300 (VirtualButton_t3335323691 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void CnControls.VirtualButton::set_Name(System.String)
extern "C"  void VirtualButton_set_Name_m1959416469 (VirtualButton_t3335323691 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean CnControls.VirtualButton::get_IsPressed()
extern "C"  bool VirtualButton_get_IsPressed_m3924498768 (VirtualButton_t3335323691 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsPressedU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void CnControls.VirtualButton::set_IsPressed(System.Boolean)
extern "C"  void VirtualButton_set_IsPressed_m711185903 (VirtualButton_t3335323691 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsPressedU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void CnControls.VirtualButton::Press()
extern "C"  void VirtualButton_Press_m961444032 (VirtualButton_t3335323691 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtualButton_get_IsPressed_m3924498768(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		VirtualButton_set_IsPressed_m711185903(__this, (bool)1, /*hidden argument*/NULL);
		int32_t L_1 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__lastPressedFrame_0(L_1);
		return;
	}
}
// System.Void CnControls.VirtualButton::Release()
extern "C"  void VirtualButton_Release_m1530526852 (VirtualButton_t3335323691 * __this, const MethodInfo* method)
{
	{
		VirtualButton_set_IsPressed_m711185903(__this, (bool)0, /*hidden argument*/NULL);
		int32_t L_0 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__lastReleasedFrame_1(L_0);
		return;
	}
}
// System.Boolean CnControls.VirtualButton::get_GetButton()
extern "C"  bool VirtualButton_get_GetButton_m330304480 (VirtualButton_t3335323691 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtualButton_get_IsPressed_m3924498768(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean CnControls.VirtualButton::get_GetButtonDown()
extern "C"  bool VirtualButton_get_GetButtonDown_m2531325090 (VirtualButton_t3335323691 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get__lastPressedFrame_0();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_1 = __this->get__lastPressedFrame_0();
		int32_t L_2 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2))) == ((int32_t)(-1)))? 1 : 0);
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean CnControls.VirtualButton::get_GetButtonUp()
extern "C"  bool VirtualButton_get_GetButtonUp_m3891402779 (VirtualButton_t3335323691 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = __this->get__lastReleasedFrame_1();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_1 = __this->get__lastReleasedFrame_1();
		int32_t L_2 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)((int32_t)((int32_t)L_2-(int32_t)1))))? 1 : 0);
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Void CommonOnScreenControl::.ctor()
extern "C"  void CommonOnScreenControl__ctor_m574268656 (CommonOnScreenControl_t1743026727 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CustomJoystick.FourWayController::.ctor()
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern const uint32_t FourWayController__ctor_m394611019_MetadataUsageId;
extern "C"  void FourWayController__ctor_m394611019 (FourWayController_t1827771971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FourWayController__ctor_m394611019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3U5BU5D_t215400611* L_0 = ((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Vector3_t4282066566  L_1 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		Vector3U5BU5D_t215400611* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Vector3_t4282066566  L_3 = Vector3_get_back_m1326515313(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		Vector3U5BU5D_t215400611* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Vector3_t4282066566  L_5 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		Vector3U5BU5D_t215400611* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		Vector3_t4282066566  L_7 = Vector3_get_left_m1616598929(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_7;
		__this->set_directionalVectors_2(L_6);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CustomJoystick.FourWayController::Awake()
extern "C"  void FourWayController_Awake_m632216238 (FourWayController_t1827771971 * __this, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(L_0, /*hidden argument*/NULL);
		__this->set__mainCameraTransform_3(L_1);
		return;
	}
}
// System.Void CustomJoystick.FourWayController::Update()
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern Il2CppCodeGenString* _stringLiteral2375469974;
extern const uint32_t FourWayController_Update_m1074901794_MetadataUsageId;
extern "C"  void FourWayController_Update_m1074901794 (FourWayController_t1827771971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FourWayController_Update_m1074901794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		float L_0 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		float L_1 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral2375469974, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_0), L_0, (0.0f), L_1, /*hidden argument*/NULL);
		float L_2 = Vector3_get_sqrMagnitude_m1207423764((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)(1.0E-05f)))))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0032:
	{
		Vector3U5BU5D_t215400611* L_3 = __this->get_directionalVectors_2();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		V_1 = (*(Vector3_t4282066566 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector3_t4282066566  L_4 = V_0;
		Vector3_t4282066566  L_5 = V_1;
		float L_6 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		V_3 = 1;
		goto IL_008d;
	}

IL_0053:
	{
		Vector3_t4282066566  L_7 = V_0;
		Vector3U5BU5D_t215400611* L_8 = __this->get_directionalVectors_2();
		int32_t L_9 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		float L_10 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_7, (*(Vector3_t4282066566 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))), /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = V_4;
		float L_12 = V_2;
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_0089;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_13 = __this->get_directionalVectors_2();
		int32_t L_14 = V_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		V_1 = (*(Vector3_t4282066566 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14))));
		float L_15 = V_4;
		V_2 = L_15;
	}

IL_0089:
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_008d:
	{
		int32_t L_17 = V_3;
		Vector3U5BU5D_t215400611* L_18 = __this->get_directionalVectors_2();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0053;
		}
	}
	{
		Transform_t1659122786 * L_19 = __this->get__mainCameraTransform_3();
		Vector3_t4282066566  L_20 = V_1;
		NullCheck(L_19);
		Vector3_t4282066566  L_21 = Transform_InverseTransformDirection_m416562129(L_19, L_20, /*hidden argument*/NULL);
		V_5 = L_21;
		(&V_5)->set_y_2((0.0f));
		Vector3_Normalize_m3984983796((&V_5), /*hidden argument*/NULL);
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_23 = L_22;
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = Transform_get_position_m2211398607(L_23, /*hidden argument*/NULL);
		Vector3_t4282066566  L_25 = V_5;
		float L_26 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_27 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		Vector3_t4282066566  L_28 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_24, L_27, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m3111394108(L_23, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Examples.Scenes.TouchpadCamera.RotateCamera::.ctor()
extern "C"  void RotateCamera__ctor_m2159699684 (RotateCamera_t1717774348 * __this, const MethodInfo* method)
{
	{
		__this->set_RotationSpeed_2((15.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Examples.Scenes.TouchpadCamera.RotateCamera::Update()
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern const uint32_t RotateCamera_Update_m4253042857_MetadataUsageId;
extern "C"  void RotateCamera_Update_m4253042857 (RotateCamera_t1717774348 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RotateCamera_Update_m4253042857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t1659122786 * L_1 = __this->get_OriginTransform_3();
		Vector3_t4282066566  L_2 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = V_0;
		float L_4 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_RotationSpeed_2();
		NullCheck(L_1);
		Transform_Rotate_m4090005356(L_1, L_2, ((float)((float)((float)((float)L_3*(float)L_4))*(float)L_5)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::.ctor()
extern "C"  void RotationConstraint__ctor_m1279824105 (RotationConstraint_t178863463 * __this, const MethodInfo* method)
{
	{
		__this->set_Min_2((-15.0f));
		__this->set_Max_3((15.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::Awake()
extern "C"  void RotationConstraint_Awake_m1517429324 (RotationConstraint_t178863463 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set__transformCache_4(L_0);
		Vector3_t4282066566  L_1 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__rotateAround_7(L_1);
		Transform_t1659122786 * L_2 = __this->get__transformCache_4();
		NullCheck(L_2);
		Quaternion_t1553702882  L_3 = Transform_get_localRotation_m3343229381(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t4282066566  L_4 = Quaternion_get_eulerAngles_m997303795((&V_1), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Vector3_get_Item_m108333500((&V_2), 0, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6 = __this->get__rotateAround_7();
		Quaternion_t1553702882  L_7 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Quaternion_t1553702882  L_8 = V_0;
		float L_9 = __this->get_Min_2();
		Vector3_t4282066566  L_10 = __this->get__rotateAround_7();
		Quaternion_t1553702882  L_11 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_12 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_8, L_11, /*hidden argument*/NULL);
		__this->set__minQuaternion_5(L_12);
		Quaternion_t1553702882  L_13 = V_0;
		float L_14 = __this->get_Max_3();
		Vector3_t4282066566  L_15 = __this->get__rotateAround_7();
		Quaternion_t1553702882  L_16 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_17 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		__this->set__maxQuaternion_6(L_17);
		float L_18 = __this->get_Max_3();
		float L_19 = __this->get_Min_2();
		__this->set__range_8(((float)((float)L_18-(float)L_19)));
		return;
	}
}
// System.Void Examples.Scenes.TouchpadCamera.RotationConstraint::LateUpdate()
extern "C"  void RotationConstraint_LateUpdate_m3774866186 (RotationConstraint_t178863463 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		Transform_t1659122786 * L_0 = __this->get__transformCache_4();
		NullCheck(L_0);
		Quaternion_t1553702882  L_1 = Transform_get_localRotation_m3343229381(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t4282066566  L_2 = Quaternion_get_eulerAngles_m997303795((&V_0), /*hidden argument*/NULL);
		V_5 = L_2;
		float L_3 = Vector3_get_Item_m108333500((&V_5), 0, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = __this->get__rotateAround_7();
		Quaternion_t1553702882  L_5 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Quaternion_t1553702882  L_6 = V_1;
		Quaternion_t1553702882  L_7 = __this->get__minQuaternion_5();
		float L_8 = Quaternion_Angle_m835424754(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Quaternion_t1553702882  L_9 = V_1;
		Quaternion_t1553702882  L_10 = __this->get__maxQuaternion_6();
		float L_11 = Quaternion_Angle_m835424754(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = V_2;
		float L_13 = __this->get__range_8();
		if ((!(((float)L_12) <= ((float)L_13))))
		{
			goto IL_005c;
		}
	}
	{
		float L_14 = V_3;
		float L_15 = __this->get__range_8();
		if ((!(((float)L_14) <= ((float)L_15))))
		{
			goto IL_005c;
		}
	}
	{
		return;
	}

IL_005c:
	{
		Vector3_t4282066566  L_16 = Quaternion_get_eulerAngles_m997303795((&V_0), /*hidden argument*/NULL);
		V_4 = L_16;
		float L_17 = V_2;
		float L_18 = V_3;
		if ((!(((float)L_17) > ((float)L_18))))
		{
			goto IL_008e;
		}
	}
	{
		Quaternion_t1553702882 * L_19 = __this->get_address_of__maxQuaternion_6();
		Vector3_t4282066566  L_20 = Quaternion_get_eulerAngles_m997303795(L_19, /*hidden argument*/NULL);
		V_6 = L_20;
		float L_21 = Vector3_get_Item_m108333500((&V_6), 0, /*hidden argument*/NULL);
		Vector3_set_Item_m1844835745((&V_4), 0, L_21, /*hidden argument*/NULL);
		goto IL_00ab;
	}

IL_008e:
	{
		Quaternion_t1553702882 * L_22 = __this->get_address_of__minQuaternion_5();
		Vector3_t4282066566  L_23 = Quaternion_get_eulerAngles_m997303795(L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		float L_24 = Vector3_get_Item_m108333500((&V_7), 0, /*hidden argument*/NULL);
		Vector3_set_Item_m1844835745((&V_4), 0, L_24, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		Transform_t1659122786 * L_25 = __this->get__transformCache_4();
		Vector3_t4282066566  L_26 = V_4;
		NullCheck(L_25);
		Transform_set_localEulerAngles_m3898859559(L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThidPersonExampleController::.ctor()
extern "C"  void ThidPersonExampleController__ctor_m20243541 (ThidPersonExampleController_t1811656226 * __this, const MethodInfo* method)
{
	{
		__this->set_MovementSpeed_2((10.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThidPersonExampleController::OnEnable()
extern const MethodInfo* Component_GetComponent_TisTransform_t1659122786_m811718087_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCharacterController_t1618060635_m3352851511_MethodInfo_var;
extern const uint32_t ThidPersonExampleController_OnEnable_m3851486993_MetadataUsageId;
extern "C"  void ThidPersonExampleController_OnEnable_m3851486993 (ThidPersonExampleController_t1811656226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThidPersonExampleController_OnEnable_m3851486993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Component_GetComponent_TisTransform_t1659122786_m811718087(L_0, /*hidden argument*/Component_GetComponent_TisTransform_t1659122786_m811718087_MethodInfo_var);
		__this->set__mainCameraTransform_3(L_1);
		CharacterController_t1618060635 * L_2 = Component_GetComponent_TisCharacterController_t1618060635_m3352851511(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_t1618060635_m3352851511_MethodInfo_var);
		__this->set__characterController_5(L_2);
		Transform_t1659122786 * L_3 = Component_GetComponent_TisTransform_t1659122786_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t1659122786_m811718087_MethodInfo_var);
		__this->set__transform_4(L_3);
		return;
	}
}
// System.Void ThidPersonExampleController::Update()
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern Il2CppCodeGenString* _stringLiteral2375469974;
extern const uint32_t ThidPersonExampleController_Update_m2354411864_MetadataUsageId;
extern "C"  void ThidPersonExampleController_Update_m2354411864 (ThidPersonExampleController_t1811656226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThidPersonExampleController_Update_m2354411864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		float L_1 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral2375469974, /*hidden argument*/NULL);
		Vector3__ctor_m1846874791((&V_0), L_0, L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Vector3_get_sqrMagnitude_m1207423764((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)(0.001f)))))
		{
			goto IL_005e;
		}
	}
	{
		Transform_t1659122786 * L_4 = __this->get__mainCameraTransform_3();
		Vector3_t4282066566  L_5 = V_0;
		NullCheck(L_4);
		Vector3_t4282066566  L_6 = Transform_TransformDirection_m83001769(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		(&V_1)->set_y_2((0.0f));
		Vector3_Normalize_m3984983796((&V_1), /*hidden argument*/NULL);
		Transform_t1659122786 * L_7 = __this->get__transform_4();
		Vector3_t4282066566  L_8 = V_1;
		NullCheck(L_7);
		Transform_set_forward_m907359846(L_7, L_8, /*hidden argument*/NULL);
	}

IL_005e:
	{
		Vector3_t4282066566  L_9 = V_1;
		Vector3_t4282066566  L_10 = Physics_get_gravity_m2907531023(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_11 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		CharacterController_t1618060635 * L_12 = __this->get__characterController_5();
		Vector3_t4282066566  L_13 = V_1;
		float L_14 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		CharacterController_Move_m3043020731(L_12, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Camera2DFollow::.ctor()
extern "C"  void Camera2DFollow__ctor_m2804771665 (Camera2DFollow_t1184786310 * __this, const MethodInfo* method)
{
	{
		__this->set_damping_3((1.0f));
		__this->set_lookAheadFactor_4((3.0f));
		__this->set_lookAheadReturnSpeed_5((0.5f));
		__this->set_lookAheadMoveThreshold_6((0.1f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Camera2DFollow::Start()
extern "C"  void Camera2DFollow_Start_m1751909457 (Camera2DFollow_t1184786310 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t1659122786 * L_0 = __this->get_target_2();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		__this->set_m_LastTargetPosition_8(L_1);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_4 = __this->get_target_2();
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (&V_0)->get_z_3();
		__this->set_m_OffsetZ_7(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_parent_m3231272063(L_8, (Transform_t1659122786 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Camera2DFollow::Update()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Camera2DFollow_Update_m2775437788_MetadataUsageId;
extern "C"  void Camera2DFollow_Update_m2775437788 (Camera2DFollow_t1184786310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Camera2DFollow_Update_m2775437788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Transform_t1659122786 * L_0 = __this->get_target_2();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = __this->get_m_LastTargetPosition_8();
		Vector3_t4282066566  L_3 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_4 = L_3;
		float L_4 = (&V_4)->get_x_1();
		V_0 = L_4;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = fabsf(L_5);
		float L_7 = __this->get_lookAheadMoveThreshold_6();
		V_1 = (bool)((((float)L_6) > ((float)L_7))? 1 : 0);
		bool L_8 = V_1;
		if (!L_8)
		{
			goto IL_005b;
		}
	}
	{
		float L_9 = __this->get_lookAheadFactor_4();
		Vector3_t4282066566  L_10 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_11 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_13 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		__this->set_m_LookAheadPos_10(L_14);
		goto IL_007d;
	}

IL_005b:
	{
		Vector3_t4282066566  L_15 = __this->get_m_LookAheadPos_10();
		Vector3_t4282066566  L_16 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = __this->get_lookAheadReturnSpeed_5();
		Vector3_t4282066566  L_19 = Vector3_MoveTowards_m2405650085(NULL /*static, unused*/, L_15, L_16, ((float)((float)L_17*(float)L_18)), /*hidden argument*/NULL);
		__this->set_m_LookAheadPos_10(L_19);
	}

IL_007d:
	{
		Transform_t1659122786 * L_20 = __this->get_target_2();
		NullCheck(L_20);
		Vector3_t4282066566  L_21 = Transform_get_position_m2211398607(L_20, /*hidden argument*/NULL);
		Vector3_t4282066566  L_22 = __this->get_m_LookAheadPos_10();
		Vector3_t4282066566  L_23 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		Vector3_t4282066566  L_24 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_25 = __this->get_m_OffsetZ_7();
		Vector3_t4282066566  L_26 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		Vector3_t4282066566  L_27 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		V_2 = L_27;
		Transform_t1659122786 * L_28 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_position_m2211398607(L_28, /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = V_2;
		Vector3_t4282066566 * L_31 = __this->get_address_of_m_CurrentVelocity_9();
		float L_32 = __this->get_damping_3();
		Vector3_t4282066566  L_33 = Vector3_SmoothDamp_m3121209633(NULL /*static, unused*/, L_29, L_30, L_31, L_32, /*hidden argument*/NULL);
		V_3 = L_33;
		Transform_t1659122786 * L_34 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_35 = V_3;
		NullCheck(L_34);
		Transform_set_position_m3111394108(L_34, L_35, /*hidden argument*/NULL);
		Transform_t1659122786 * L_36 = __this->get_target_2();
		NullCheck(L_36);
		Vector3_t4282066566  L_37 = Transform_get_position_m2211398607(L_36, /*hidden argument*/NULL);
		__this->set_m_LastTargetPosition_8(L_37);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Restarter::.ctor()
extern "C"  void Restarter__ctor_m1240728241 (Restarter_t2816782862 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets._2D.Restarter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t Restarter_OnTriggerEnter2D_m3306157799_MetadataUsageId;
extern "C"  void Restarter_OnTriggerEnter2D_m3306157799 (Restarter_t2816782862 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Restarter_OnTriggerEnter2D_m3306157799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Collider2D_t1552025098 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m217485006(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		Scene_t1080795294  L_3 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = Scene_get_name_m894591657((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern "C"  void AbstractTargetFollower__ctor_m374659589 (AbstractTargetFollower_t2600961761 * __this, const MethodInfo* method)
{
	{
		__this->set_m_AutoTargetPlayer_3((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t3346577219_m2213165263_MethodInfo_var;
extern const uint32_t AbstractTargetFollower_Start_m3616764677_MetadataUsageId;
extern "C"  void AbstractTargetFollower_Start_m3616764677 (AbstractTargetFollower_t2600961761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractTargetFollower_Start_m3616764677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_AutoTargetPlayer_3();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		AbstractTargetFollower_FindAndTargetPlayer_m760024051(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Transform_t1659122786 * L_1 = __this->get_m_Target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		return;
	}

IL_0023:
	{
		Transform_t1659122786 * L_3 = __this->get_m_Target_2();
		NullCheck(L_3);
		Rigidbody_t3346577219 * L_4 = Component_GetComponent_TisRigidbody_t3346577219_m2213165263(L_3, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m2213165263_MethodInfo_var);
		__this->set_targetRigidbody_5(L_4);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t AbstractTargetFollower_FixedUpdate_m4099039168_MetadataUsageId;
extern "C"  void AbstractTargetFollower_FixedUpdate_m4099039168 (AbstractTargetFollower_t2600961761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractTargetFollower_FixedUpdate_m4099039168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_AutoTargetPlayer_3();
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t1659122786 * L_1 = __this->get_m_Target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t1659122786 * L_3 = __this->get_m_Target_2();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_m3858025161(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0037;
		}
	}

IL_0031:
	{
		AbstractTargetFollower_FindAndTargetPlayer_m760024051(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_6 = __this->get_m_UpdateType_4();
		if (L_6)
		{
			goto IL_004d;
		}
	}
	{
		float L_7 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t AbstractTargetFollower_LateUpdate_m1073802094_MetadataUsageId;
extern "C"  void AbstractTargetFollower_LateUpdate_m1073802094 (AbstractTargetFollower_t2600961761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractTargetFollower_LateUpdate_m1073802094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_AutoTargetPlayer_3();
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t1659122786 * L_1 = __this->get_m_Target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t1659122786 * L_3 = __this->get_m_Target_2();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_m3858025161(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0037;
		}
	}

IL_0031:
	{
		AbstractTargetFollower_FindAndTargetPlayer_m760024051(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_6 = __this->get_m_UpdateType_4();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_7 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t AbstractTargetFollower_ManualUpdate_m1337708814_MetadataUsageId;
extern "C"  void AbstractTargetFollower_ManualUpdate_m1337708814 (AbstractTargetFollower_t2600961761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractTargetFollower_ManualUpdate_m1337708814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_AutoTargetPlayer_3();
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t1659122786 * L_1 = __this->get_m_Target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t1659122786 * L_3 = __this->get_m_Target_2();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_m3858025161(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0037;
		}
	}

IL_0031:
	{
		AbstractTargetFollower_FindAndTargetPlayer_m760024051(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_6 = __this->get_m_UpdateType_4();
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_004e;
		}
	}
	{
		float L_7 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< float >::Invoke(5 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single) */, __this, L_7);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t AbstractTargetFollower_FindAndTargetPlayer_m760024051_MetadataUsageId;
extern "C"  void AbstractTargetFollower_FindAndTargetPlayer_m760024051 (AbstractTargetFollower_t2600961761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractTargetFollower_FindAndTargetPlayer_m760024051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral2393081601, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t3674682005 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t3674682005 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< Transform_t1659122786 * >::Invoke(6 /* System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform) */, __this, L_4);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern "C"  void AbstractTargetFollower_SetTarget_m2029942535 (AbstractTargetFollower_t2600961761 * __this, Transform_t1659122786 * ___newTransform0, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = ___newTransform0;
		__this->set_m_Target_2(L_0);
		return;
	}
}
// UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern "C"  Transform_t1659122786 * AbstractTargetFollower_get_Target_m3249270167 (AbstractTargetFollower_t2600961761 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = __this->get_m_Target_2();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Cameras.AutoCam::.ctor()
extern "C"  void AutoCam__ctor_m2090348880 (AutoCam_t3033465374 * __this, const MethodInfo* method)
{
	{
		__this->set_m_MoveSpeed_9((3.0f));
		__this->set_m_TurnSpeed_10((1.0f));
		__this->set_m_RollSpeed_11((0.2f));
		__this->set_m_FollowTilt_13((bool)1);
		__this->set_m_SpinTurnLimit_14((90.0f));
		__this->set_m_TargetVelocityLowerLimit_15((4.0f));
		__this->set_m_SmoothTurnTime_16((0.2f));
		Vector3_t4282066566  L_0 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_RollUp_20(L_0);
		PivotBasedCameraRig__ctor_m678659222(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.AutoCam::FollowTarget(System.Single)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t AutoCam_FollowTarget_m238523989_MetadataUsageId;
extern "C"  void AutoCam_FollowTarget_m238523989 (AutoCam_t3033465374 * __this, float ___deltaTime0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoCam_FollowTarget_m238523989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Quaternion_t1553702882  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float G_B13_0 = 0.0f;
	AutoCam_t3033465374 * G_B24_0 = NULL;
	AutoCam_t3033465374 * G_B23_0 = NULL;
	Vector3_t4282066566  G_B25_0;
	memset(&G_B25_0, 0, sizeof(G_B25_0));
	AutoCam_t3033465374 * G_B25_1 = NULL;
	{
		float L_0 = ___deltaTime0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_001c;
		}
	}
	{
		Transform_t1659122786 * L_1 = ((AbstractTargetFollower_t2600961761 *)__this)->get_m_Target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}

IL_001c:
	{
		return;
	}

IL_001d:
	{
		Transform_t1659122786 * L_3 = ((AbstractTargetFollower_t2600961761 *)__this)->get_m_Target_2();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Transform_get_forward_m877665793(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Transform_t1659122786 * L_5 = ((AbstractTargetFollower_t2600961761 *)__this)->get_m_Target_2();
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_up_m297874561(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		bool L_7 = __this->get_m_FollowVelocity_12();
		if (!L_7)
		{
			goto IL_00b6;
		}
	}
	{
		bool L_8 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00b6;
		}
	}
	{
		Rigidbody_t3346577219 * L_9 = ((AbstractTargetFollower_t2600961761 *)__this)->get_targetRigidbody_5();
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Rigidbody_get_velocity_m2696244068(L_9, /*hidden argument*/NULL);
		V_7 = L_10;
		float L_11 = Vector3_get_magnitude_m989985786((&V_7), /*hidden argument*/NULL);
		float L_12 = __this->get_m_TargetVelocityLowerLimit_15();
		if ((!(((float)L_11) > ((float)L_12))))
		{
			goto IL_0089;
		}
	}
	{
		Rigidbody_t3346577219 * L_13 = ((AbstractTargetFollower_t2600961761 *)__this)->get_targetRigidbody_5();
		NullCheck(L_13);
		Vector3_t4282066566  L_14 = Rigidbody_get_velocity_m2696244068(L_13, /*hidden argument*/NULL);
		V_8 = L_14;
		Vector3_t4282066566  L_15 = Vector3_get_normalized_m2650940353((&V_8), /*hidden argument*/NULL);
		V_0 = L_15;
		Vector3_t4282066566  L_16 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_16;
		goto IL_008f;
	}

IL_0089:
	{
		Vector3_t4282066566  L_17 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_17;
	}

IL_008f:
	{
		float L_18 = __this->get_m_CurrentTurnAmount_18();
		float* L_19 = __this->get_address_of_m_TurnSpeedVelocityChange_19();
		float L_20 = __this->get_m_SmoothTurnTime_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_21 = Mathf_SmoothDamp_m488292903(NULL /*static, unused*/, L_18, (1.0f), L_19, L_20, /*hidden argument*/NULL);
		__this->set_m_CurrentTurnAmount_18(L_21);
		goto IL_0175;
	}

IL_00b6:
	{
		float L_22 = (&V_0)->get_x_1();
		float L_23 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = atan2f(L_22, L_23);
		V_2 = ((float)((float)L_24*(float)(57.29578f)));
		float L_25 = __this->get_m_SpinTurnLimit_14();
		if ((!(((float)L_25) > ((float)(0.0f)))))
		{
			goto IL_0163;
		}
	}
	{
		float L_26 = __this->get_m_LastFlatAngle_17();
		float L_27 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_28 = Mathf_DeltaAngle_m226689272(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		float L_29 = fabsf(L_28);
		float L_30 = ___deltaTime0;
		V_3 = ((float)((float)L_29/(float)L_30));
		float L_31 = __this->get_m_SpinTurnLimit_14();
		float L_32 = __this->get_m_SpinTurnLimit_14();
		float L_33 = V_3;
		float L_34 = Mathf_InverseLerp_m152689993(NULL /*static, unused*/, L_31, ((float)((float)L_32*(float)(0.75f))), L_33, /*hidden argument*/NULL);
		V_4 = L_34;
		float L_35 = __this->get_m_CurrentTurnAmount_18();
		float L_36 = V_4;
		if ((!(((float)L_35) > ((float)L_36))))
		{
			goto IL_0125;
		}
	}
	{
		G_B13_0 = (0.1f);
		goto IL_012a;
	}

IL_0125:
	{
		G_B13_0 = (1.0f);
	}

IL_012a:
	{
		V_5 = G_B13_0;
		bool L_37 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0156;
		}
	}
	{
		float L_38 = __this->get_m_CurrentTurnAmount_18();
		float L_39 = V_4;
		float* L_40 = __this->get_address_of_m_TurnSpeedVelocityChange_19();
		float L_41 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_42 = Mathf_SmoothDamp_m488292903(NULL /*static, unused*/, L_38, L_39, L_40, L_41, /*hidden argument*/NULL);
		__this->set_m_CurrentTurnAmount_18(L_42);
		goto IL_015e;
	}

IL_0156:
	{
		float L_43 = V_4;
		__this->set_m_CurrentTurnAmount_18(L_43);
	}

IL_015e:
	{
		goto IL_016e;
	}

IL_0163:
	{
		__this->set_m_CurrentTurnAmount_18((1.0f));
	}

IL_016e:
	{
		float L_44 = V_2;
		__this->set_m_LastFlatAngle_17(L_44);
	}

IL_0175:
	{
		Transform_t1659122786 * L_45 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_46 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t4282066566  L_47 = Transform_get_position_m2211398607(L_46, /*hidden argument*/NULL);
		Transform_t1659122786 * L_48 = ((AbstractTargetFollower_t2600961761 *)__this)->get_m_Target_2();
		NullCheck(L_48);
		Vector3_t4282066566  L_49 = Transform_get_position_m2211398607(L_48, /*hidden argument*/NULL);
		float L_50 = ___deltaTime0;
		float L_51 = __this->get_m_MoveSpeed_9();
		Vector3_t4282066566  L_52 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_47, L_49, ((float)((float)L_50*(float)L_51)), /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_position_m3111394108(L_45, L_52, /*hidden argument*/NULL);
		bool L_53 = __this->get_m_FollowTilt_13();
		if (L_53)
		{
			goto IL_01d7;
		}
	}
	{
		(&V_0)->set_y_2((0.0f));
		float L_54 = Vector3_get_sqrMagnitude_m1207423764((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_54) < ((float)(1.401298E-45f)))))
		{
			goto IL_01d7;
		}
	}
	{
		Transform_t1659122786 * L_55 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Vector3_t4282066566  L_56 = Transform_get_forward_m877665793(L_55, /*hidden argument*/NULL);
		V_0 = L_56;
	}

IL_01d7:
	{
		Vector3_t4282066566  L_57 = V_0;
		Vector3_t4282066566  L_58 = __this->get_m_RollUp_20();
		Quaternion_t1553702882  L_59 = Quaternion_LookRotation_m2869326048(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		V_6 = L_59;
		float L_60 = __this->get_m_RollSpeed_11();
		G_B23_0 = __this;
		if ((!(((float)L_60) > ((float)(0.0f)))))
		{
			G_B24_0 = __this;
			goto IL_020f;
		}
	}
	{
		Vector3_t4282066566  L_61 = __this->get_m_RollUp_20();
		Vector3_t4282066566  L_62 = V_1;
		float L_63 = __this->get_m_RollSpeed_11();
		float L_64 = ___deltaTime0;
		Vector3_t4282066566  L_65 = Vector3_Slerp_m1274749478(NULL /*static, unused*/, L_61, L_62, ((float)((float)L_63*(float)L_64)), /*hidden argument*/NULL);
		G_B25_0 = L_65;
		G_B25_1 = G_B23_0;
		goto IL_0214;
	}

IL_020f:
	{
		Vector3_t4282066566  L_66 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B25_0 = L_66;
		G_B25_1 = G_B24_0;
	}

IL_0214:
	{
		NullCheck(G_B25_1);
		G_B25_1->set_m_RollUp_20(G_B25_0);
		Transform_t1659122786 * L_67 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_68 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_68);
		Quaternion_t1553702882  L_69 = Transform_get_rotation_m11483428(L_68, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_70 = V_6;
		float L_71 = __this->get_m_TurnSpeed_10();
		float L_72 = __this->get_m_CurrentTurnAmount_18();
		float L_73 = ___deltaTime0;
		Quaternion_t1553702882  L_74 = Quaternion_Lerp_m1693481477(NULL /*static, unused*/, L_69, L_70, ((float)((float)((float)((float)L_71*(float)L_72))*(float)L_73)), /*hidden argument*/NULL);
		NullCheck(L_67);
		Transform_set_rotation_m1525803229(L_67, L_74, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern "C"  void PivotBasedCameraRig__ctor_m678659222 (PivotBasedCameraRig_t2501384728 * __this, const MethodInfo* method)
{
	{
		AbstractTargetFollower__ctor_m374659589(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisCamera_t2727095145_m2632836353_MethodInfo_var;
extern const uint32_t PivotBasedCameraRig_Awake_m916264441_MetadataUsageId;
extern "C"  void PivotBasedCameraRig_Awake_m916264441 (PivotBasedCameraRig_t2501384728 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PivotBasedCameraRig_Awake_m916264441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2727095145 * L_0 = Component_GetComponentInChildren_TisCamera_t2727095145_m2632836353(__this, /*hidden argument*/Component_GetComponentInChildren_TisCamera_t2727095145_m2632836353_MethodInfo_var);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(L_0, /*hidden argument*/NULL);
		__this->set_m_Cam_6(L_1);
		Transform_t1659122786 * L_2 = __this->get_m_Cam_6();
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Transform_get_parent_m2236876972(L_2, /*hidden argument*/NULL);
		__this->set_m_Pivot_7(L_3);
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::.ctor()
extern "C"  void Platformer2DUserControl__ctor_m2694631724 (Platformer2DUserControl_t3012254862 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::Awake()
extern const MethodInfo* Component_GetComponent_TisPlatformerCharacter2D_t2561026825_m598360315_MethodInfo_var;
extern const uint32_t Platformer2DUserControl_Awake_m2932236943_MetadataUsageId;
extern "C"  void Platformer2DUserControl_Awake_m2932236943 (Platformer2DUserControl_t3012254862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Platformer2DUserControl_Awake_m2932236943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlatformerCharacter2D_t2561026825 * L_0 = Component_GetComponent_TisPlatformerCharacter2D_t2561026825_m598360315(__this, /*hidden argument*/Component_GetComponent_TisPlatformerCharacter2D_t2561026825_m598360315_MethodInfo_var);
		__this->set_m_Character_2(L_0);
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::Update()
extern Il2CppCodeGenString* _stringLiteral2320462;
extern const uint32_t Platformer2DUserControl_Update_m3656066913_MetadataUsageId;
extern "C"  void Platformer2DUserControl_Update_m3656066913 (Platformer2DUserControl_t3012254862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Platformer2DUserControl_Update_m3656066913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_Jump_3();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		bool L_1 = CnInputManager_GetButtonDown_m3486119458(NULL /*static, unused*/, _stringLiteral2320462, /*hidden argument*/NULL);
		__this->set_m_Jump_3(L_1);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.Platformer2DUserControl::FixedUpdate()
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern const uint32_t Platformer2DUserControl_FixedUpdate_m2677546407_MetadataUsageId;
extern "C"  void Platformer2DUserControl_FixedUpdate_m2677546407 (Platformer2DUserControl_t3012254862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Platformer2DUserControl_FixedUpdate_m2677546407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = CnInputManager_GetAxis_m2810903397(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		V_0 = L_0;
		PlatformerCharacter2D_t2561026825 * L_1 = __this->get_m_Character_2();
		float L_2 = V_0;
		bool L_3 = __this->get_m_Jump_3();
		NullCheck(L_1);
		PlatformerCharacter2D_Move_m98163478(L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Jump_3((bool)0);
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::.ctor()
extern "C"  void PlatformerCharacter2D__ctor_m3368050705 (PlatformerCharacter2D_t2561026825 * __this, const MethodInfo* method)
{
	{
		__this->set_m_MaxSpeed_3((10.0f));
		__this->set_m_JumpForce_4((400.0f));
		__this->set_m_FacingRight_11((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Awake()
extern const MethodInfo* Component_GetComponent_TisAnimator_t2776330603_m4147395588_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t1743771669_m832779581_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1093312417;
extern const uint32_t PlatformerCharacter2D_Awake_m3605655924_MetadataUsageId;
extern "C"  void PlatformerCharacter2D_Awake_m3605655924 (PlatformerCharacter2D_t2561026825 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlatformerCharacter2D_Awake_m3605655924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Transform_Find_m3950449392(L_0, _stringLiteral1093312417, /*hidden argument*/NULL);
		__this->set_m_GroundCheck_7(L_1);
		Animator_t2776330603 * L_2 = Component_GetComponent_TisAnimator_t2776330603_m4147395588(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t2776330603_m4147395588_MethodInfo_var);
		__this->set_m_Anim_9(L_2);
		Rigidbody2D_t1743771669 * L_3 = Component_GetComponent_TisRigidbody2D_t1743771669_m832779581(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t1743771669_m832779581_MethodInfo_var);
		__this->set_m_Rigidbody2D_10(L_3);
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::FixedUpdate()
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2141373863;
extern const uint32_t PlatformerCharacter2D_FixedUpdate_m3897306316_MetadataUsageId;
extern "C"  void PlatformerCharacter2D_FixedUpdate_m3897306316 (PlatformerCharacter2D_t2561026825 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlatformerCharacter2D_FixedUpdate_m3897306316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Collider2DU5BU5D_t1758559887* V_0 = NULL;
	int32_t V_1 = 0;
	{
		__this->set_m_Grounded_8((bool)0);
		Transform_t1659122786 * L_0 = __this->get_m_GroundCheck_7();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		LayerMask_t3236759763  L_3 = __this->get_m_WhatIsGround_6();
		int32_t L_4 = LayerMask_op_Implicit_m1595580047(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_5 = Physics2D_OverlapCircleAll_m1000299574(NULL /*static, unused*/, L_2, (0.2f), L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		V_1 = 0;
		goto IL_0057;
	}

IL_0034:
	{
		Collider2DU5BU5D_t1758559887* L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Collider2D_t1552025098 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(L_9, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0053;
		}
	}
	{
		__this->set_m_Grounded_8((bool)1);
	}

IL_0053:
	{
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_14 = V_1;
		Collider2DU5BU5D_t1758559887* L_15 = V_0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0034;
		}
	}
	{
		Animator_t2776330603 * L_16 = __this->get_m_Anim_9();
		bool L_17 = __this->get_m_Grounded_8();
		NullCheck(L_16);
		Animator_SetBool_m2336836203(L_16, _stringLiteral2141373863, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral80089127;
extern Il2CppCodeGenString* _stringLiteral2141373863;
extern const uint32_t PlatformerCharacter2D_Move_m98163478_MetadataUsageId;
extern "C"  void PlatformerCharacter2D_Move_m98163478 (PlatformerCharacter2D_t2561026825 * __this, float ___move0, bool ___jump1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlatformerCharacter2D_Move_m98163478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_m_Grounded_8();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_m_AirControl_5();
		if (!L_1)
		{
			goto IL_0094;
		}
	}

IL_0016:
	{
		Animator_t2776330603 * L_2 = __this->get_m_Anim_9();
		float L_3 = ___move0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		NullCheck(L_2);
		Animator_SetFloat_m1775105839(L_2, _stringLiteral80089127, L_4, /*hidden argument*/NULL);
		Rigidbody2D_t1743771669 * L_5 = __this->get_m_Rigidbody2D_10();
		float L_6 = ___move0;
		float L_7 = __this->get_m_MaxSpeed_3();
		Rigidbody2D_t1743771669 * L_8 = __this->get_m_Rigidbody2D_10();
		NullCheck(L_8);
		Vector2_t4282066565  L_9 = Rigidbody2D_get_velocity_m416159605(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = (&V_0)->get_y_2();
		Vector2_t4282066565  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector2__ctor_m1517109030(&L_11, ((float)((float)L_6*(float)L_7)), L_10, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody2D_set_velocity_m100625302(L_5, L_11, /*hidden argument*/NULL);
		float L_12 = ___move0;
		if ((!(((float)L_12) > ((float)(0.0f)))))
		{
			goto IL_0078;
		}
	}
	{
		bool L_13 = __this->get_m_FacingRight_11();
		if (L_13)
		{
			goto IL_0078;
		}
	}
	{
		PlatformerCharacter2D_Flip_m1219371072(__this, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_0078:
	{
		float L_14 = ___move0;
		if ((!(((float)L_14) < ((float)(0.0f)))))
		{
			goto IL_0094;
		}
	}
	{
		bool L_15 = __this->get_m_FacingRight_11();
		if (!L_15)
		{
			goto IL_0094;
		}
	}
	{
		PlatformerCharacter2D_Flip_m1219371072(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		bool L_16 = __this->get_m_Grounded_8();
		if (!L_16)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_17 = ___jump1;
		if (!L_17)
		{
			goto IL_00ed;
		}
	}
	{
		Animator_t2776330603 * L_18 = __this->get_m_Anim_9();
		NullCheck(L_18);
		bool L_19 = Animator_GetBool_m436748612(L_18, _stringLiteral2141373863, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00ed;
		}
	}
	{
		__this->set_m_Grounded_8((bool)0);
		Animator_t2776330603 * L_20 = __this->get_m_Anim_9();
		NullCheck(L_20);
		Animator_SetBool_m2336836203(L_20, _stringLiteral2141373863, (bool)0, /*hidden argument*/NULL);
		Rigidbody2D_t1743771669 * L_21 = __this->get_m_Rigidbody2D_10();
		float L_22 = __this->get_m_JumpForce_4();
		Vector2_t4282066565  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector2__ctor_m1517109030(&L_23, (0.0f), L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rigidbody2D_AddForce_m312397382(L_21, L_23, /*hidden argument*/NULL);
	}

IL_00ed:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Copy._2D.PlatformerCharacter2D::Flip()
extern "C"  void PlatformerCharacter2D_Flip_m1219371072 (PlatformerCharacter2D_t2561026825 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_m_FacingRight_11();
		__this->set_m_FacingRight_11((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_localScale_m3886572677(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566 * L_3 = (&V_0);
		float L_4 = L_3->get_x_1();
		L_3->set_x_1(((float)((float)L_4*(float)(-1.0f))));
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6 = V_0;
		NullCheck(L_5);
		Transform_set_localScale_m310756934(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
