﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Progresion2029956962.h"
#include "AssemblyU2DCSharp_Room2553083.h"
#include "AssemblyU2DCSharp_RoomGenerator3606720536.h"
#include "AssemblyU2DCSharp_SelfDestroy1472365294.h"
#include "AssemblyU2DCSharp_Shake79847142.h"
#include "AssemblyU2DCSharp_Shake_U3CshakeU3Ec__Iterator7570686417.h"
#include "AssemblyU2DCSharp_Thunder329757892.h"
#include "AssemblyU2DCSharp_Telefono2999111444.h"
#include "AssemblyU2DCSharp_Telefono_U3CdialingU3Ec__Iterato3806020820.h"
#include "AssemblyU2DCSharp_TimeLight2022754857.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1184786310.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3012254862.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2561026825.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2816782862.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CustomJoystick_FourW1827771971.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2600961761.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C181978984.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3033465374.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2501384728.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ThidPersonExampleCon1811656226.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Examples_Scenes_Touc1717774348.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Examples_Scenes_Touch178863463.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_CnInputMan540789462.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualAx2395187226.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualBu3335323691.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CommonOnScreenContro1743026727.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Dpad3878341687.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_DpadAxis235416056.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Sensitive1697973888.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_SimpleBut2998894860.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_ControlMo3285416379.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_SimpleJoy3821479766.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_Touchpad2035887356.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (Progresion_t2029956962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (Room_t2553083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[4] = 
{
	Room_t2553083::get_offset_of_x_2(),
	Room_t2553083::get_offset_of_y_3(),
	Room_t2553083::get_offset_of_z_4(),
	Room_t2553083::get_offset_of_index_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (RoomGenerator_t3606720536), -1, sizeof(RoomGenerator_t3606720536_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1702[35] = 
{
	RoomGenerator_t3606720536::get_offset_of_LadrilloObj_2(),
	RoomGenerator_t3606720536::get_offset_of_WallObj_3(),
	RoomGenerator_t3606720536::get_offset_of_Wall_4(),
	RoomGenerator_t3606720536::get_offset_of_Cristal_5(),
	RoomGenerator_t3606720536::get_offset_of_Pared_6(),
	RoomGenerator_t3606720536::get_offset_of_Shadow_7(),
	RoomGenerator_t3606720536::get_offset_of_CameraMan_8(),
	RoomGenerator_t3606720536::get_offset_of_Player_9(),
	RoomGenerator_t3606720536::get_offset_of_Ball_10(),
	RoomGenerator_t3606720536::get_offset_of_Controller_11(),
	RoomGenerator_t3606720536::get_offset_of_PlantaciUF3n_12(),
	RoomGenerator_t3606720536::get_offset_of_distanciaZCam_13(),
	RoomGenerator_t3606720536::get_offset_of_alturaCam_14(),
	RoomGenerator_t3606720536::get_offset_of_umbralRebotebounds_15(),
	RoomGenerator_t3606720536::get_offset_of_LockCam_16(),
	RoomGenerator_t3606720536::get_offset_of_LockPlayer_17(),
	RoomGenerator_t3606720536::get_offset_of_LockBall_18(),
	RoomGenerator_t3606720536::get_offset_of_Perspec_19(),
	RoomGenerator_t3606720536::get_offset_of_Ortho_20(),
	RoomGenerator_t3606720536::get_offset_of_Iso_21(),
	RoomGenerator_t3606720536::get_offset_of_Interface_22(),
	RoomGenerator_t3606720536::get_offset_of_Container_23(),
	RoomGenerator_t3606720536::get_offset_of_WallPos_24(),
	RoomGenerator_t3606720536_StaticFields::get_offset_of_fase_25(),
	RoomGenerator_t3606720536_StaticFields::get_offset_of_pasaFaseFactor_26(),
	RoomGenerator_t3606720536::get_offset_of_room_27(),
	RoomGenerator_t3606720536::get_offset_of_alto_28(),
	RoomGenerator_t3606720536::get_offset_of_ancho_29(),
	RoomGenerator_t3606720536::get_offset_of_fondo_30(),
	RoomGenerator_t3606720536::get_offset_of_minAlto_31(),
	RoomGenerator_t3606720536::get_offset_of_minAncho_32(),
	RoomGenerator_t3606720536::get_offset_of_minFondo_33(),
	RoomGenerator_t3606720536::get_offset_of_maxAlto_34(),
	RoomGenerator_t3606720536::get_offset_of_maxAncho_35(),
	RoomGenerator_t3606720536::get_offset_of_maxFondo_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (SelfDestroy_t1472365294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (Shake_t79847142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[15] = 
{
	Shake_t79847142::get_offset_of_shaking_2(),
	Shake_t79847142::get_offset_of_minMovementRange_3(),
	Shake_t79847142::get_offset_of_maxMovementRange_4(),
	Shake_t79847142::get_offset_of_positionRange_5(),
	Shake_t79847142::get_offset_of_maxShakeField_6(),
	Shake_t79847142::get_offset_of_initPosP_7(),
	Shake_t79847142::get_offset_of_initPosO_8(),
	Shake_t79847142::get_offset_of_initPosI_9(),
	Shake_t79847142::get_offset_of_shakeSizeP_10(),
	Shake_t79847142::get_offset_of_shakeSizeO_11(),
	Shake_t79847142::get_offset_of_shakeSizeI_12(),
	Shake_t79847142::get_offset_of_Perspec_13(),
	Shake_t79847142::get_offset_of_Ortho_14(),
	Shake_t79847142::get_offset_of_Iso_15(),
	Shake_t79847142::get_offset_of_Interface_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (U3CshakeU3Ec__Iterator7_t570686417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[8] = 
{
	U3CshakeU3Ec__Iterator7_t570686417::get_offset_of_t_0(),
	U3CshakeU3Ec__Iterator7_t570686417::get_offset_of_U3CpPU3E__0_1(),
	U3CshakeU3Ec__Iterator7_t570686417::get_offset_of_U3CpOU3E__1_2(),
	U3CshakeU3Ec__Iterator7_t570686417::get_offset_of_U3CpIU3E__2_3(),
	U3CshakeU3Ec__Iterator7_t570686417::get_offset_of_U24PC_4(),
	U3CshakeU3Ec__Iterator7_t570686417::get_offset_of_U24current_5(),
	U3CshakeU3Ec__Iterator7_t570686417::get_offset_of_U3CU24U3Et_6(),
	U3CshakeU3Ec__Iterator7_t570686417::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (Thunder_t329757892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[4] = 
{
	Thunder_t329757892::get_offset_of_minTime_2(),
	Thunder_t329757892::get_offset_of_threshold_3(),
	Thunder_t329757892::get_offset_of_light_4(),
	Thunder_t329757892::get_offset_of_lastTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (Telefono_t2999111444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[17] = 
{
	Telefono_t2999111444::get_offset_of_fotoDisplay_2(),
	Telefono_t2999111444::get_offset_of_button1_3(),
	Telefono_t2999111444::get_offset_of_button2_4(),
	Telefono_t2999111444::get_offset_of_button3_5(),
	Telefono_t2999111444::get_offset_of_button4_6(),
	Telefono_t2999111444::get_offset_of_button5_7(),
	Telefono_t2999111444::get_offset_of_button6_8(),
	Telefono_t2999111444::get_offset_of_button7_9(),
	Telefono_t2999111444::get_offset_of_button8_10(),
	Telefono_t2999111444::get_offset_of_button9_11(),
	Telefono_t2999111444::get_offset_of_button0_12(),
	Telefono_t2999111444::get_offset_of_buttonast_13(),
	Telefono_t2999111444::get_offset_of_buttonigual_14(),
	Telefono_t2999111444::get_offset_of_DialHUD_15(),
	Telefono_t2999111444::get_offset_of_currentNumber_16(),
	Telefono_t2999111444::get_offset_of_isDialing_17(),
	Telefono_t2999111444::get_offset_of_pictures_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (U3CdialingU3Ec__Iterator8_t3806020820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[4] = 
{
	U3CdialingU3Ec__Iterator8_t3806020820::get_offset_of_U3CtimeU3E__0_0(),
	U3CdialingU3Ec__Iterator8_t3806020820::get_offset_of_U24PC_1(),
	U3CdialingU3Ec__Iterator8_t3806020820::get_offset_of_U24current_2(),
	U3CdialingU3Ec__Iterator8_t3806020820::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (TimeLight_t2022754857), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (U3CModuleU3E_t86524797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (Camera2DFollow_t1184786310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[9] = 
{
	Camera2DFollow_t1184786310::get_offset_of_target_2(),
	Camera2DFollow_t1184786310::get_offset_of_damping_3(),
	Camera2DFollow_t1184786310::get_offset_of_lookAheadFactor_4(),
	Camera2DFollow_t1184786310::get_offset_of_lookAheadReturnSpeed_5(),
	Camera2DFollow_t1184786310::get_offset_of_lookAheadMoveThreshold_6(),
	Camera2DFollow_t1184786310::get_offset_of_m_OffsetZ_7(),
	Camera2DFollow_t1184786310::get_offset_of_m_LastTargetPosition_8(),
	Camera2DFollow_t1184786310::get_offset_of_m_CurrentVelocity_9(),
	Camera2DFollow_t1184786310::get_offset_of_m_LookAheadPos_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (Platformer2DUserControl_t3012254862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[2] = 
{
	Platformer2DUserControl_t3012254862::get_offset_of_m_Character_2(),
	Platformer2DUserControl_t3012254862::get_offset_of_m_Jump_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (PlatformerCharacter2D_t2561026825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[10] = 
{
	0,
	PlatformerCharacter2D_t2561026825::get_offset_of_m_MaxSpeed_3(),
	PlatformerCharacter2D_t2561026825::get_offset_of_m_JumpForce_4(),
	PlatformerCharacter2D_t2561026825::get_offset_of_m_AirControl_5(),
	PlatformerCharacter2D_t2561026825::get_offset_of_m_WhatIsGround_6(),
	PlatformerCharacter2D_t2561026825::get_offset_of_m_GroundCheck_7(),
	PlatformerCharacter2D_t2561026825::get_offset_of_m_Grounded_8(),
	PlatformerCharacter2D_t2561026825::get_offset_of_m_Anim_9(),
	PlatformerCharacter2D_t2561026825::get_offset_of_m_Rigidbody2D_10(),
	PlatformerCharacter2D_t2561026825::get_offset_of_m_FacingRight_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (Restarter_t2816782862), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (FourWayController_t1827771971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[2] = 
{
	FourWayController_t1827771971::get_offset_of_directionalVectors_2(),
	FourWayController_t1827771971::get_offset_of__mainCameraTransform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (AbstractTargetFollower_t2600961761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[4] = 
{
	AbstractTargetFollower_t2600961761::get_offset_of_m_Target_2(),
	AbstractTargetFollower_t2600961761::get_offset_of_m_AutoTargetPlayer_3(),
	AbstractTargetFollower_t2600961761::get_offset_of_m_UpdateType_4(),
	AbstractTargetFollower_t2600961761::get_offset_of_targetRigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (UpdateType_t181978984)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1717[4] = 
{
	UpdateType_t181978984::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (AutoCam_t3033465374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[12] = 
{
	AutoCam_t3033465374::get_offset_of_m_MoveSpeed_9(),
	AutoCam_t3033465374::get_offset_of_m_TurnSpeed_10(),
	AutoCam_t3033465374::get_offset_of_m_RollSpeed_11(),
	AutoCam_t3033465374::get_offset_of_m_FollowVelocity_12(),
	AutoCam_t3033465374::get_offset_of_m_FollowTilt_13(),
	AutoCam_t3033465374::get_offset_of_m_SpinTurnLimit_14(),
	AutoCam_t3033465374::get_offset_of_m_TargetVelocityLowerLimit_15(),
	AutoCam_t3033465374::get_offset_of_m_SmoothTurnTime_16(),
	AutoCam_t3033465374::get_offset_of_m_LastFlatAngle_17(),
	AutoCam_t3033465374::get_offset_of_m_CurrentTurnAmount_18(),
	AutoCam_t3033465374::get_offset_of_m_TurnSpeedVelocityChange_19(),
	AutoCam_t3033465374::get_offset_of_m_RollUp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (PivotBasedCameraRig_t2501384728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[3] = 
{
	PivotBasedCameraRig_t2501384728::get_offset_of_m_Cam_6(),
	PivotBasedCameraRig_t2501384728::get_offset_of_m_Pivot_7(),
	PivotBasedCameraRig_t2501384728::get_offset_of_m_LastTargetPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (ThidPersonExampleController_t1811656226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[4] = 
{
	ThidPersonExampleController_t1811656226::get_offset_of_MovementSpeed_2(),
	ThidPersonExampleController_t1811656226::get_offset_of__mainCameraTransform_3(),
	ThidPersonExampleController_t1811656226::get_offset_of__transform_4(),
	ThidPersonExampleController_t1811656226::get_offset_of__characterController_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (RotateCamera_t1717774348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[2] = 
{
	RotateCamera_t1717774348::get_offset_of_RotationSpeed_2(),
	RotateCamera_t1717774348::get_offset_of_OriginTransform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (RotationConstraint_t178863463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[7] = 
{
	RotationConstraint_t178863463::get_offset_of_Min_2(),
	RotationConstraint_t178863463::get_offset_of_Max_3(),
	RotationConstraint_t178863463::get_offset_of__transformCache_4(),
	RotationConstraint_t178863463::get_offset_of__minQuaternion_5(),
	RotationConstraint_t178863463::get_offset_of__maxQuaternion_6(),
	RotationConstraint_t178863463::get_offset_of__rotateAround_7(),
	RotationConstraint_t178863463::get_offset_of__range_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (CnInputManager_t540789462), -1, sizeof(CnInputManager_t540789462_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[3] = 
{
	CnInputManager_t540789462_StaticFields::get_offset_of__instance_0(),
	CnInputManager_t540789462::get_offset_of__virtualAxisDictionary_1(),
	CnInputManager_t540789462::get_offset_of__virtualButtonsDictionary_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (VirtualAxis_t2395187226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[2] = 
{
	VirtualAxis_t2395187226::get_offset_of_U3CNameU3Ek__BackingField_0(),
	VirtualAxis_t2395187226::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (VirtualButton_t3335323691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[4] = 
{
	VirtualButton_t3335323691::get_offset_of__lastPressedFrame_0(),
	VirtualButton_t3335323691::get_offset_of__lastReleasedFrame_1(),
	VirtualButton_t3335323691::get_offset_of_U3CNameU3Ek__BackingField_2(),
	VirtualButton_t3335323691::get_offset_of_U3CIsPressedU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (CommonOnScreenControl_t1743026727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (Dpad_t3878341687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[2] = 
{
	Dpad_t3878341687::get_offset_of_DpadAxis_2(),
	Dpad_t3878341687::get_offset_of_U3CCurrentEventCameraU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (DpadAxis_t235416056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[5] = 
{
	DpadAxis_t235416056::get_offset_of_AxisName_2(),
	DpadAxis_t235416056::get_offset_of_AxisMultiplier_3(),
	DpadAxis_t235416056::get_offset_of__virtualAxis_4(),
	DpadAxis_t235416056::get_offset_of_U3CRectTransformU3Ek__BackingField_5(),
	DpadAxis_t235416056::get_offset_of_U3CLastFingerIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (SensitiveJoystick_t1697973888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[1] = 
{
	SensitiveJoystick_t1697973888::get_offset_of_SensitivityCurve_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (SimpleButton_t2998894860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[2] = 
{
	SimpleButton_t2998894860::get_offset_of_ButtonName_2(),
	SimpleButton_t2998894860::get_offset_of__virtualButton_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (ControlMovementDirection_t3285416379)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1731[4] = 
{
	ControlMovementDirection_t3285416379::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (SimpleJoystick_t3821479766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[19] = 
{
	SimpleJoystick_t3821479766::get_offset_of_MovementRange_2(),
	SimpleJoystick_t3821479766::get_offset_of_HorizontalAxisName_3(),
	SimpleJoystick_t3821479766::get_offset_of_VerticalAxisName_4(),
	SimpleJoystick_t3821479766::get_offset_of_HideOnRelease_5(),
	SimpleJoystick_t3821479766::get_offset_of_MoveBase_6(),
	SimpleJoystick_t3821479766::get_offset_of_SnapsToFinger_7(),
	SimpleJoystick_t3821479766::get_offset_of_JoystickMoveAxis_8(),
	SimpleJoystick_t3821479766::get_offset_of_JoystickBase_9(),
	SimpleJoystick_t3821479766::get_offset_of_Stick_10(),
	SimpleJoystick_t3821479766::get_offset_of_TouchZone_11(),
	SimpleJoystick_t3821479766::get_offset_of__initialStickPosition_12(),
	SimpleJoystick_t3821479766::get_offset_of__intermediateStickPosition_13(),
	SimpleJoystick_t3821479766::get_offset_of__initialBasePosition_14(),
	SimpleJoystick_t3821479766::get_offset_of__baseTransform_15(),
	SimpleJoystick_t3821479766::get_offset_of__stickTransform_16(),
	SimpleJoystick_t3821479766::get_offset_of__oneOverMovementRange_17(),
	SimpleJoystick_t3821479766::get_offset_of_HorizintalAxis_18(),
	SimpleJoystick_t3821479766::get_offset_of_VerticalAxis_19(),
	SimpleJoystick_t3821479766::get_offset_of_U3CCurrentEventCameraU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (Touchpad_t2035887356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[10] = 
{
	Touchpad_t2035887356::get_offset_of_HorizontalAxisName_2(),
	Touchpad_t2035887356::get_offset_of_VerticalAxisName_3(),
	Touchpad_t2035887356::get_offset_of_PreserveInertia_4(),
	Touchpad_t2035887356::get_offset_of_Friction_5(),
	Touchpad_t2035887356::get_offset_of__horizintalAxis_6(),
	Touchpad_t2035887356::get_offset_of__verticalAxis_7(),
	Touchpad_t2035887356::get_offset_of__lastDragFrameNumber_8(),
	Touchpad_t2035887356::get_offset_of__isCurrentlyTweaking_9(),
	Touchpad_t2035887356::get_offset_of_ControlMoveAxis_10(),
	Touchpad_t2035887356::get_offset_of_U3CCurrentEventCameraU3Ek__BackingField_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
