﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Thunder
struct Thunder_t329757892;

#include "codegen/il2cpp-codegen.h"

// System.Void Thunder::.ctor()
extern "C"  void Thunder__ctor_m1982240551 (Thunder_t329757892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Thunder::Start()
extern "C"  void Thunder_Start_m929378343 (Thunder_t329757892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Thunder::Update()
extern "C"  void Thunder_Update_m3046777030 (Thunder_t329757892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
