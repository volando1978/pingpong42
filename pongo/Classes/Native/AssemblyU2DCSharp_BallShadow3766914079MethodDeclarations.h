﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BallShadow
struct BallShadow_t3766914079;

#include "codegen/il2cpp-codegen.h"

// System.Void BallShadow::.ctor()
extern "C"  void BallShadow__ctor_m2965847900 (BallShadow_t3766914079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallShadow::Update()
extern "C"  void BallShadow_Update_m3473833777 (BallShadow_t3766914079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallShadow::SetSizeAlpha(System.Single)
extern "C"  void BallShadow_SetSizeAlpha_m2837256776 (BallShadow_t3766914079 * __this, float ___dist0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
