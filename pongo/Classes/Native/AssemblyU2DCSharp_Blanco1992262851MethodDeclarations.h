﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Blanco
struct Blanco_t1992262851;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Blanco::.ctor()
extern "C"  void Blanco__ctor_m4004350776 (Blanco_t1992262851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blanco::Start()
extern "C"  void Blanco_Start_m2951488568 (Blanco_t1992262851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Blanco::Plof()
extern "C"  Il2CppObject * Blanco_Plof_m2022465383 (Blanco_t1992262851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
