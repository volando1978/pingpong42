﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shake
struct Shake_t79847142;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void Shake::.ctor()
extern "C"  void Shake__ctor_m3472411973 (Shake_t79847142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shake::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Shake_setCameras_m4002115067 (Shake_t79847142 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shake::Start()
extern "C"  void Shake_Start_m2419549765 (Shake_t79847142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shake::ShakeThatCamera(System.Single)
extern "C"  void Shake_ShakeThatCamera_m1815182614 (Shake_t79847142 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Shake::shake(System.Single)
extern "C"  Il2CppObject * Shake_shake_m2392108282 (Shake_t79847142 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
