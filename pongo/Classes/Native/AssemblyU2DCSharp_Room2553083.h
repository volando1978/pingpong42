﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Room
struct  Room_t2553083  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 Room::x
	int32_t ___x_2;
	// System.Int32 Room::y
	int32_t ___y_3;
	// System.Int32 Room::z
	int32_t ___z_4;
	// System.Int32 Room::index
	int32_t ___index_5;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Room_t2553083, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Room_t2553083, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Room_t2553083, ___z_4)); }
	inline int32_t get_z_4() const { return ___z_4; }
	inline int32_t* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(int32_t value)
	{
		___z_4 = value;
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(Room_t2553083, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
