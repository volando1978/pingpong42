﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelfDestroy
struct SelfDestroy_t1472365294;

#include "codegen/il2cpp-codegen.h"

// System.Void SelfDestroy::.ctor()
extern "C"  void SelfDestroy__ctor_m4251083837 (SelfDestroy_t1472365294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfDestroy::Start()
extern "C"  void SelfDestroy_Start_m3198221629 (SelfDestroy_t1472365294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfDestroy::Update()
extern "C"  void SelfDestroy_Update_m366474864 (SelfDestroy_t1472365294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
