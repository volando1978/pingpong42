﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// ColliderPlayer
struct ColliderPlayer_t3441697813;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColliderPlayer/<flan>c__Iterator3
struct  U3CflanU3Ec__Iterator3_t3689796459  : public Il2CppObject
{
public:
	// System.Single ColliderPlayer/<flan>c__Iterator3::<t>__0
	float ___U3CtU3E__0_0;
	// UnityEngine.Vector3 ColliderPlayer/<flan>c__Iterator3::<sc>__1
	Vector3_t4282066566  ___U3CscU3E__1_1;
	// UnityEngine.Vector3 ColliderPlayer/<flan>c__Iterator3::<targetScale>__2
	Vector3_t4282066566  ___U3CtargetScaleU3E__2_2;
	// System.Single ColliderPlayer/<flan>c__Iterator3::<scy>__3
	float ___U3CscyU3E__3_3;
	// UnityEngine.Vector3 ColliderPlayer/<flan>c__Iterator3::<lsc>__4
	Vector3_t4282066566  ___U3ClscU3E__4_4;
	// System.Int32 ColliderPlayer/<flan>c__Iterator3::$PC
	int32_t ___U24PC_5;
	// System.Object ColliderPlayer/<flan>c__Iterator3::$current
	Il2CppObject * ___U24current_6;
	// ColliderPlayer ColliderPlayer/<flan>c__Iterator3::<>f__this
	ColliderPlayer_t3441697813 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CflanU3Ec__Iterator3_t3689796459, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CscU3E__1_1() { return static_cast<int32_t>(offsetof(U3CflanU3Ec__Iterator3_t3689796459, ___U3CscU3E__1_1)); }
	inline Vector3_t4282066566  get_U3CscU3E__1_1() const { return ___U3CscU3E__1_1; }
	inline Vector3_t4282066566 * get_address_of_U3CscU3E__1_1() { return &___U3CscU3E__1_1; }
	inline void set_U3CscU3E__1_1(Vector3_t4282066566  value)
	{
		___U3CscU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtargetScaleU3E__2_2() { return static_cast<int32_t>(offsetof(U3CflanU3Ec__Iterator3_t3689796459, ___U3CtargetScaleU3E__2_2)); }
	inline Vector3_t4282066566  get_U3CtargetScaleU3E__2_2() const { return ___U3CtargetScaleU3E__2_2; }
	inline Vector3_t4282066566 * get_address_of_U3CtargetScaleU3E__2_2() { return &___U3CtargetScaleU3E__2_2; }
	inline void set_U3CtargetScaleU3E__2_2(Vector3_t4282066566  value)
	{
		___U3CtargetScaleU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CscyU3E__3_3() { return static_cast<int32_t>(offsetof(U3CflanU3Ec__Iterator3_t3689796459, ___U3CscyU3E__3_3)); }
	inline float get_U3CscyU3E__3_3() const { return ___U3CscyU3E__3_3; }
	inline float* get_address_of_U3CscyU3E__3_3() { return &___U3CscyU3E__3_3; }
	inline void set_U3CscyU3E__3_3(float value)
	{
		___U3CscyU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3ClscU3E__4_4() { return static_cast<int32_t>(offsetof(U3CflanU3Ec__Iterator3_t3689796459, ___U3ClscU3E__4_4)); }
	inline Vector3_t4282066566  get_U3ClscU3E__4_4() const { return ___U3ClscU3E__4_4; }
	inline Vector3_t4282066566 * get_address_of_U3ClscU3E__4_4() { return &___U3ClscU3E__4_4; }
	inline void set_U3ClscU3E__4_4(Vector3_t4282066566  value)
	{
		___U3ClscU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CflanU3Ec__Iterator3_t3689796459, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CflanU3Ec__Iterator3_t3689796459, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CflanU3Ec__Iterator3_t3689796459, ___U3CU3Ef__this_7)); }
	inline ColliderPlayer_t3441697813 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline ColliderPlayer_t3441697813 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(ColliderPlayer_t3441697813 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
