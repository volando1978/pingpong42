﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CommonOnScreenControl
struct CommonOnScreenControl_t1743026727;

#include "codegen/il2cpp-codegen.h"

// System.Void CommonOnScreenControl::.ctor()
extern "C"  void CommonOnScreenControl__ctor_m574268656 (CommonOnScreenControl_t1743026727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
