﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Blanco/<Plof>c__Iterator2
struct U3CPlofU3Ec__Iterator2_t1800312444;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Blanco/<Plof>c__Iterator2::.ctor()
extern "C"  void U3CPlofU3Ec__Iterator2__ctor_m3837868655 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Blanco/<Plof>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlofU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4221340803 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Blanco/<Plof>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlofU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2214983191 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Blanco/<Plof>c__Iterator2::MoveNext()
extern "C"  bool U3CPlofU3Ec__Iterator2_MoveNext_m1505796645 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blanco/<Plof>c__Iterator2::Dispose()
extern "C"  void U3CPlofU3Ec__Iterator2_Dispose_m3008120556 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blanco/<Plof>c__Iterator2::Reset()
extern "C"  void U3CPlofU3Ec__Iterator2_Reset_m1484301596 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
