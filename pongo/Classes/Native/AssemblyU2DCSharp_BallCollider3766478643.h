﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallCollider
struct  BallCollider_t3766478643  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject BallCollider::Controller
	GameObject_t3674682005 * ___Controller_2;
	// System.Int32 BallCollider::tocaSuelo
	int32_t ___tocaSuelo_3;
	// UnityEngine.GameObject BallCollider::nBotes
	GameObject_t3674682005 * ___nBotes_4;
	// UnityEngine.GameObject BallCollider::PlantaParticles
	GameObject_t3674682005 * ___PlantaParticles_5;
	// System.Single BallCollider::pitchMod
	float ___pitchMod_6;
	// System.Single BallCollider::clampBolaX
	float ___clampBolaX_7;
	// UnityEngine.Camera BallCollider::Perspec
	Camera_t2727095145 * ___Perspec_8;
	// UnityEngine.Camera BallCollider::Ortho
	Camera_t2727095145 * ___Ortho_9;
	// UnityEngine.Camera BallCollider::Iso
	Camera_t2727095145 * ___Iso_10;
	// UnityEngine.Camera BallCollider::Interface
	Camera_t2727095145 * ___Interface_11;
	// System.Boolean BallCollider::isSecondBounce
	bool ___isSecondBounce_12;
	// UnityEngine.GameObject BallCollider::audio
	GameObject_t3674682005 * ___audio_13;

public:
	inline static int32_t get_offset_of_Controller_2() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___Controller_2)); }
	inline GameObject_t3674682005 * get_Controller_2() const { return ___Controller_2; }
	inline GameObject_t3674682005 ** get_address_of_Controller_2() { return &___Controller_2; }
	inline void set_Controller_2(GameObject_t3674682005 * value)
	{
		___Controller_2 = value;
		Il2CppCodeGenWriteBarrier(&___Controller_2, value);
	}

	inline static int32_t get_offset_of_tocaSuelo_3() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___tocaSuelo_3)); }
	inline int32_t get_tocaSuelo_3() const { return ___tocaSuelo_3; }
	inline int32_t* get_address_of_tocaSuelo_3() { return &___tocaSuelo_3; }
	inline void set_tocaSuelo_3(int32_t value)
	{
		___tocaSuelo_3 = value;
	}

	inline static int32_t get_offset_of_nBotes_4() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___nBotes_4)); }
	inline GameObject_t3674682005 * get_nBotes_4() const { return ___nBotes_4; }
	inline GameObject_t3674682005 ** get_address_of_nBotes_4() { return &___nBotes_4; }
	inline void set_nBotes_4(GameObject_t3674682005 * value)
	{
		___nBotes_4 = value;
		Il2CppCodeGenWriteBarrier(&___nBotes_4, value);
	}

	inline static int32_t get_offset_of_PlantaParticles_5() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___PlantaParticles_5)); }
	inline GameObject_t3674682005 * get_PlantaParticles_5() const { return ___PlantaParticles_5; }
	inline GameObject_t3674682005 ** get_address_of_PlantaParticles_5() { return &___PlantaParticles_5; }
	inline void set_PlantaParticles_5(GameObject_t3674682005 * value)
	{
		___PlantaParticles_5 = value;
		Il2CppCodeGenWriteBarrier(&___PlantaParticles_5, value);
	}

	inline static int32_t get_offset_of_pitchMod_6() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___pitchMod_6)); }
	inline float get_pitchMod_6() const { return ___pitchMod_6; }
	inline float* get_address_of_pitchMod_6() { return &___pitchMod_6; }
	inline void set_pitchMod_6(float value)
	{
		___pitchMod_6 = value;
	}

	inline static int32_t get_offset_of_clampBolaX_7() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___clampBolaX_7)); }
	inline float get_clampBolaX_7() const { return ___clampBolaX_7; }
	inline float* get_address_of_clampBolaX_7() { return &___clampBolaX_7; }
	inline void set_clampBolaX_7(float value)
	{
		___clampBolaX_7 = value;
	}

	inline static int32_t get_offset_of_Perspec_8() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___Perspec_8)); }
	inline Camera_t2727095145 * get_Perspec_8() const { return ___Perspec_8; }
	inline Camera_t2727095145 ** get_address_of_Perspec_8() { return &___Perspec_8; }
	inline void set_Perspec_8(Camera_t2727095145 * value)
	{
		___Perspec_8 = value;
		Il2CppCodeGenWriteBarrier(&___Perspec_8, value);
	}

	inline static int32_t get_offset_of_Ortho_9() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___Ortho_9)); }
	inline Camera_t2727095145 * get_Ortho_9() const { return ___Ortho_9; }
	inline Camera_t2727095145 ** get_address_of_Ortho_9() { return &___Ortho_9; }
	inline void set_Ortho_9(Camera_t2727095145 * value)
	{
		___Ortho_9 = value;
		Il2CppCodeGenWriteBarrier(&___Ortho_9, value);
	}

	inline static int32_t get_offset_of_Iso_10() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___Iso_10)); }
	inline Camera_t2727095145 * get_Iso_10() const { return ___Iso_10; }
	inline Camera_t2727095145 ** get_address_of_Iso_10() { return &___Iso_10; }
	inline void set_Iso_10(Camera_t2727095145 * value)
	{
		___Iso_10 = value;
		Il2CppCodeGenWriteBarrier(&___Iso_10, value);
	}

	inline static int32_t get_offset_of_Interface_11() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___Interface_11)); }
	inline Camera_t2727095145 * get_Interface_11() const { return ___Interface_11; }
	inline Camera_t2727095145 ** get_address_of_Interface_11() { return &___Interface_11; }
	inline void set_Interface_11(Camera_t2727095145 * value)
	{
		___Interface_11 = value;
		Il2CppCodeGenWriteBarrier(&___Interface_11, value);
	}

	inline static int32_t get_offset_of_isSecondBounce_12() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___isSecondBounce_12)); }
	inline bool get_isSecondBounce_12() const { return ___isSecondBounce_12; }
	inline bool* get_address_of_isSecondBounce_12() { return &___isSecondBounce_12; }
	inline void set_isSecondBounce_12(bool value)
	{
		___isSecondBounce_12 = value;
	}

	inline static int32_t get_offset_of_audio_13() { return static_cast<int32_t>(offsetof(BallCollider_t3766478643, ___audio_13)); }
	inline GameObject_t3674682005 * get_audio_13() const { return ___audio_13; }
	inline GameObject_t3674682005 ** get_address_of_audio_13() { return &___audio_13; }
	inline void set_audio_13(GameObject_t3674682005 * value)
	{
		___audio_13 = value;
		Il2CppCodeGenWriteBarrier(&___audio_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
