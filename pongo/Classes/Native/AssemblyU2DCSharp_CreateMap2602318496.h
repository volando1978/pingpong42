﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateMap
struct  CreateMap_t2602318496  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] CreateMap::elementos
	GameObjectU5BU5D_t2662109048* ___elementos_2;

public:
	inline static int32_t get_offset_of_elementos_2() { return static_cast<int32_t>(offsetof(CreateMap_t2602318496, ___elementos_2)); }
	inline GameObjectU5BU5D_t2662109048* get_elementos_2() const { return ___elementos_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_elementos_2() { return &___elementos_2; }
	inline void set_elementos_2(GameObjectU5BU5D_t2662109048* value)
	{
		___elementos_2 = value;
		Il2CppCodeGenWriteBarrier(&___elementos_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
