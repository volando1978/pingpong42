﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BouncingAnim
struct BouncingAnim_t2247160086;

#include "codegen/il2cpp-codegen.h"

// System.Void BouncingAnim::.ctor()
extern "C"  void BouncingAnim__ctor_m3517307461 (BouncingAnim_t2247160086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BouncingAnim::Start()
extern "C"  void BouncingAnim_Start_m2464445253 (BouncingAnim_t2247160086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BouncingAnim::Update()
extern "C"  void BouncingAnim_Update_m3389210984 (BouncingAnim_t2247160086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BouncingAnim::SetBounce(System.Single)
extern "C"  void BouncingAnim_SetBounce_m1471097182 (BouncingAnim_t2247160086 * __this, float ___dist0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
