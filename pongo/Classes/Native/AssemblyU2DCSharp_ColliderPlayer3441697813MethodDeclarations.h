﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColliderPlayer
struct ColliderPlayer_t3441697813;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Collision
struct Collision_t2494107688;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ColliderPlayer::.ctor()
extern "C"  void ColliderPlayer__ctor_m2137026598 (ColliderPlayer_t3441697813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColliderPlayer::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void ColliderPlayer_setCameras_m1275307740 (ColliderPlayer_t3441697813 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColliderPlayer::Start()
extern "C"  void ColliderPlayer_Start_m1084164390 (ColliderPlayer_t3441697813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColliderPlayer::PlayAudio(System.Single)
extern "C"  void ColliderPlayer_PlayAudio_m1966464805 (ColliderPlayer_t3441697813 * __this, float ___pitch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColliderPlayer::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void ColliderPlayer_OnCollisionEnter_m3549370868 (ColliderPlayer_t3441697813 * __this, Collision_t2494107688 * ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ColliderPlayer::Lado(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ColliderPlayer_Lado_m2022745300 (ColliderPlayer_t3441697813 * __this, Vector3_t4282066566  ___cPoint0, Vector3_t4282066566  ___cScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ColliderPlayer::flan()
extern "C"  Il2CppObject * ColliderPlayer_flan_m1165343545 (ColliderPlayer_t3441697813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
