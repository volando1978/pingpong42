﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CnControls.DpadAxis
struct DpadAxis_t235416056;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void CnControls.DpadAxis::.ctor()
extern "C"  void DpadAxis__ctor_m3232219098 (DpadAxis_t235416056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform CnControls.DpadAxis::get_RectTransform()
extern "C"  RectTransform_t972643934 * DpadAxis_get_RectTransform_m108508377 (DpadAxis_t235416056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.DpadAxis::set_RectTransform(UnityEngine.RectTransform)
extern "C"  void DpadAxis_set_RectTransform_m1413112094 (DpadAxis_t235416056 * __this, RectTransform_t972643934 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CnControls.DpadAxis::get_LastFingerId()
extern "C"  int32_t DpadAxis_get_LastFingerId_m428322455 (DpadAxis_t235416056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.DpadAxis::set_LastFingerId(System.Int32)
extern "C"  void DpadAxis_set_LastFingerId_m1979767442 (DpadAxis_t235416056 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.DpadAxis::Awake()
extern "C"  void DpadAxis_Awake_m3469824317 (DpadAxis_t235416056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.DpadAxis::OnEnable()
extern "C"  void DpadAxis_OnEnable_m4238917996 (DpadAxis_t235416056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.DpadAxis::OnDisable()
extern "C"  void DpadAxis_OnDisable_m2998376257 (DpadAxis_t235416056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.DpadAxis::Press(UnityEngine.Vector2,UnityEngine.Camera,System.Int32)
extern "C"  void DpadAxis_Press_m531132620 (DpadAxis_t235416056 * __this, Vector2_t4282066565  ___screenPoint0, Camera_t2727095145 * ___eventCamera1, int32_t ___pointerId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.DpadAxis::TryRelease(System.Int32)
extern "C"  void DpadAxis_TryRelease_m3480600199 (DpadAxis_t235416056 * __this, int32_t ___pointerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
