﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move
struct  Move_t2404337  : public MonoBehaviour_t667441552
{
public:
	// System.Single Move::acceleration
	float ___acceleration_2;
	// System.Single Move::currentForzH
	float ___currentForzH_3;
	// System.Single Move::maxCurrentForzH
	float ___maxCurrentForzH_4;
	// System.Single Move::currentForzZ
	float ___currentForzZ_5;
	// System.Single Move::maxCurrentForzZ
	float ___maxCurrentForzZ_6;
	// System.Single Move::accelerationCube
	float ___accelerationCube_7;
	// System.Single Move::rapidez
	float ___rapidez_8;
	// System.Single Move::traslationH
	float ___traslationH_9;
	// UnityEngine.GameObject Move::TargetBall
	GameObject_t3674682005 * ___TargetBall_10;
	// UnityEngine.Vector3 Move::boundary
	Vector3_t4282066566  ___boundary_11;
	// UnityEngine.GameObject Move::GotoButton
	GameObject_t3674682005 * ___GotoButton_12;
	// System.Boolean Move::isColliding
	bool ___isColliding_13;
	// UnityEngine.Camera Move::Perspec
	Camera_t2727095145 * ___Perspec_14;
	// UnityEngine.Camera Move::Ortho
	Camera_t2727095145 * ___Ortho_15;
	// UnityEngine.Camera Move::Iso
	Camera_t2727095145 * ___Iso_16;
	// UnityEngine.Camera Move::Interface
	Camera_t2727095145 * ___Interface_17;
	// System.Int32 Move::ancho
	int32_t ___ancho_18;
	// System.Boolean Move::movingRight
	bool ___movingRight_19;
	// System.Boolean Move::movingLeft
	bool ___movingLeft_20;

public:
	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(Move_t2404337, ___acceleration_2)); }
	inline float get_acceleration_2() const { return ___acceleration_2; }
	inline float* get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(float value)
	{
		___acceleration_2 = value;
	}

	inline static int32_t get_offset_of_currentForzH_3() { return static_cast<int32_t>(offsetof(Move_t2404337, ___currentForzH_3)); }
	inline float get_currentForzH_3() const { return ___currentForzH_3; }
	inline float* get_address_of_currentForzH_3() { return &___currentForzH_3; }
	inline void set_currentForzH_3(float value)
	{
		___currentForzH_3 = value;
	}

	inline static int32_t get_offset_of_maxCurrentForzH_4() { return static_cast<int32_t>(offsetof(Move_t2404337, ___maxCurrentForzH_4)); }
	inline float get_maxCurrentForzH_4() const { return ___maxCurrentForzH_4; }
	inline float* get_address_of_maxCurrentForzH_4() { return &___maxCurrentForzH_4; }
	inline void set_maxCurrentForzH_4(float value)
	{
		___maxCurrentForzH_4 = value;
	}

	inline static int32_t get_offset_of_currentForzZ_5() { return static_cast<int32_t>(offsetof(Move_t2404337, ___currentForzZ_5)); }
	inline float get_currentForzZ_5() const { return ___currentForzZ_5; }
	inline float* get_address_of_currentForzZ_5() { return &___currentForzZ_5; }
	inline void set_currentForzZ_5(float value)
	{
		___currentForzZ_5 = value;
	}

	inline static int32_t get_offset_of_maxCurrentForzZ_6() { return static_cast<int32_t>(offsetof(Move_t2404337, ___maxCurrentForzZ_6)); }
	inline float get_maxCurrentForzZ_6() const { return ___maxCurrentForzZ_6; }
	inline float* get_address_of_maxCurrentForzZ_6() { return &___maxCurrentForzZ_6; }
	inline void set_maxCurrentForzZ_6(float value)
	{
		___maxCurrentForzZ_6 = value;
	}

	inline static int32_t get_offset_of_accelerationCube_7() { return static_cast<int32_t>(offsetof(Move_t2404337, ___accelerationCube_7)); }
	inline float get_accelerationCube_7() const { return ___accelerationCube_7; }
	inline float* get_address_of_accelerationCube_7() { return &___accelerationCube_7; }
	inline void set_accelerationCube_7(float value)
	{
		___accelerationCube_7 = value;
	}

	inline static int32_t get_offset_of_rapidez_8() { return static_cast<int32_t>(offsetof(Move_t2404337, ___rapidez_8)); }
	inline float get_rapidez_8() const { return ___rapidez_8; }
	inline float* get_address_of_rapidez_8() { return &___rapidez_8; }
	inline void set_rapidez_8(float value)
	{
		___rapidez_8 = value;
	}

	inline static int32_t get_offset_of_traslationH_9() { return static_cast<int32_t>(offsetof(Move_t2404337, ___traslationH_9)); }
	inline float get_traslationH_9() const { return ___traslationH_9; }
	inline float* get_address_of_traslationH_9() { return &___traslationH_9; }
	inline void set_traslationH_9(float value)
	{
		___traslationH_9 = value;
	}

	inline static int32_t get_offset_of_TargetBall_10() { return static_cast<int32_t>(offsetof(Move_t2404337, ___TargetBall_10)); }
	inline GameObject_t3674682005 * get_TargetBall_10() const { return ___TargetBall_10; }
	inline GameObject_t3674682005 ** get_address_of_TargetBall_10() { return &___TargetBall_10; }
	inline void set_TargetBall_10(GameObject_t3674682005 * value)
	{
		___TargetBall_10 = value;
		Il2CppCodeGenWriteBarrier(&___TargetBall_10, value);
	}

	inline static int32_t get_offset_of_boundary_11() { return static_cast<int32_t>(offsetof(Move_t2404337, ___boundary_11)); }
	inline Vector3_t4282066566  get_boundary_11() const { return ___boundary_11; }
	inline Vector3_t4282066566 * get_address_of_boundary_11() { return &___boundary_11; }
	inline void set_boundary_11(Vector3_t4282066566  value)
	{
		___boundary_11 = value;
	}

	inline static int32_t get_offset_of_GotoButton_12() { return static_cast<int32_t>(offsetof(Move_t2404337, ___GotoButton_12)); }
	inline GameObject_t3674682005 * get_GotoButton_12() const { return ___GotoButton_12; }
	inline GameObject_t3674682005 ** get_address_of_GotoButton_12() { return &___GotoButton_12; }
	inline void set_GotoButton_12(GameObject_t3674682005 * value)
	{
		___GotoButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___GotoButton_12, value);
	}

	inline static int32_t get_offset_of_isColliding_13() { return static_cast<int32_t>(offsetof(Move_t2404337, ___isColliding_13)); }
	inline bool get_isColliding_13() const { return ___isColliding_13; }
	inline bool* get_address_of_isColliding_13() { return &___isColliding_13; }
	inline void set_isColliding_13(bool value)
	{
		___isColliding_13 = value;
	}

	inline static int32_t get_offset_of_Perspec_14() { return static_cast<int32_t>(offsetof(Move_t2404337, ___Perspec_14)); }
	inline Camera_t2727095145 * get_Perspec_14() const { return ___Perspec_14; }
	inline Camera_t2727095145 ** get_address_of_Perspec_14() { return &___Perspec_14; }
	inline void set_Perspec_14(Camera_t2727095145 * value)
	{
		___Perspec_14 = value;
		Il2CppCodeGenWriteBarrier(&___Perspec_14, value);
	}

	inline static int32_t get_offset_of_Ortho_15() { return static_cast<int32_t>(offsetof(Move_t2404337, ___Ortho_15)); }
	inline Camera_t2727095145 * get_Ortho_15() const { return ___Ortho_15; }
	inline Camera_t2727095145 ** get_address_of_Ortho_15() { return &___Ortho_15; }
	inline void set_Ortho_15(Camera_t2727095145 * value)
	{
		___Ortho_15 = value;
		Il2CppCodeGenWriteBarrier(&___Ortho_15, value);
	}

	inline static int32_t get_offset_of_Iso_16() { return static_cast<int32_t>(offsetof(Move_t2404337, ___Iso_16)); }
	inline Camera_t2727095145 * get_Iso_16() const { return ___Iso_16; }
	inline Camera_t2727095145 ** get_address_of_Iso_16() { return &___Iso_16; }
	inline void set_Iso_16(Camera_t2727095145 * value)
	{
		___Iso_16 = value;
		Il2CppCodeGenWriteBarrier(&___Iso_16, value);
	}

	inline static int32_t get_offset_of_Interface_17() { return static_cast<int32_t>(offsetof(Move_t2404337, ___Interface_17)); }
	inline Camera_t2727095145 * get_Interface_17() const { return ___Interface_17; }
	inline Camera_t2727095145 ** get_address_of_Interface_17() { return &___Interface_17; }
	inline void set_Interface_17(Camera_t2727095145 * value)
	{
		___Interface_17 = value;
		Il2CppCodeGenWriteBarrier(&___Interface_17, value);
	}

	inline static int32_t get_offset_of_ancho_18() { return static_cast<int32_t>(offsetof(Move_t2404337, ___ancho_18)); }
	inline int32_t get_ancho_18() const { return ___ancho_18; }
	inline int32_t* get_address_of_ancho_18() { return &___ancho_18; }
	inline void set_ancho_18(int32_t value)
	{
		___ancho_18 = value;
	}

	inline static int32_t get_offset_of_movingRight_19() { return static_cast<int32_t>(offsetof(Move_t2404337, ___movingRight_19)); }
	inline bool get_movingRight_19() const { return ___movingRight_19; }
	inline bool* get_address_of_movingRight_19() { return &___movingRight_19; }
	inline void set_movingRight_19(bool value)
	{
		___movingRight_19 = value;
	}

	inline static int32_t get_offset_of_movingLeft_20() { return static_cast<int32_t>(offsetof(Move_t2404337, ___movingLeft_20)); }
	inline bool get_movingLeft_20() const { return ___movingLeft_20; }
	inline bool* get_address_of_movingLeft_20() { return &___movingLeft_20; }
	inline void set_movingLeft_20(bool value)
	{
		___movingLeft_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
