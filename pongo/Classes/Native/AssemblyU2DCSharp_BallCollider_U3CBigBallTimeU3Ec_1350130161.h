﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// BallCollider
struct BallCollider_t3766478643;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallCollider/<BigBallTime>c__Iterator0
struct  U3CBigBallTimeU3Ec__Iterator0_t1350130161  : public Il2CppObject
{
public:
	// System.Single BallCollider/<BigBallTime>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityEngine.Vector3 BallCollider/<BigBallTime>c__Iterator0::<originalScale>__1
	Vector3_t4282066566  ___U3CoriginalScaleU3E__1_1;
	// UnityEngine.Vector3 BallCollider/<BigBallTime>c__Iterator0::<bigball>__2
	Vector3_t4282066566  ___U3CbigballU3E__2_2;
	// System.Int32 BallCollider/<BigBallTime>c__Iterator0::$PC
	int32_t ___U24PC_3;
	// System.Object BallCollider/<BigBallTime>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// BallCollider BallCollider/<BigBallTime>c__Iterator0::<>f__this
	BallCollider_t3766478643 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBigBallTimeU3Ec__Iterator0_t1350130161, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CoriginalScaleU3E__1_1() { return static_cast<int32_t>(offsetof(U3CBigBallTimeU3Ec__Iterator0_t1350130161, ___U3CoriginalScaleU3E__1_1)); }
	inline Vector3_t4282066566  get_U3CoriginalScaleU3E__1_1() const { return ___U3CoriginalScaleU3E__1_1; }
	inline Vector3_t4282066566 * get_address_of_U3CoriginalScaleU3E__1_1() { return &___U3CoriginalScaleU3E__1_1; }
	inline void set_U3CoriginalScaleU3E__1_1(Vector3_t4282066566  value)
	{
		___U3CoriginalScaleU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbigballU3E__2_2() { return static_cast<int32_t>(offsetof(U3CBigBallTimeU3Ec__Iterator0_t1350130161, ___U3CbigballU3E__2_2)); }
	inline Vector3_t4282066566  get_U3CbigballU3E__2_2() const { return ___U3CbigballU3E__2_2; }
	inline Vector3_t4282066566 * get_address_of_U3CbigballU3E__2_2() { return &___U3CbigballU3E__2_2; }
	inline void set_U3CbigballU3E__2_2(Vector3_t4282066566  value)
	{
		___U3CbigballU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CBigBallTimeU3Ec__Iterator0_t1350130161, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CBigBallTimeU3Ec__Iterator0_t1350130161, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CBigBallTimeU3Ec__Iterator0_t1350130161, ___U3CU3Ef__this_5)); }
	inline BallCollider_t3766478643 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline BallCollider_t3766478643 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(BallCollider_t3766478643 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
