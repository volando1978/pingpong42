﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallShadow
struct  BallShadow_t3766914079  : public MonoBehaviour_t667441552
{
public:
	// System.Single BallShadow::sombraBola
	float ___sombraBola_2;
	// System.Single BallShadow::maxSombraBola
	float ___maxSombraBola_3;
	// System.Single BallShadow::alturaSombra
	float ___alturaSombra_4;

public:
	inline static int32_t get_offset_of_sombraBola_2() { return static_cast<int32_t>(offsetof(BallShadow_t3766914079, ___sombraBola_2)); }
	inline float get_sombraBola_2() const { return ___sombraBola_2; }
	inline float* get_address_of_sombraBola_2() { return &___sombraBola_2; }
	inline void set_sombraBola_2(float value)
	{
		___sombraBola_2 = value;
	}

	inline static int32_t get_offset_of_maxSombraBola_3() { return static_cast<int32_t>(offsetof(BallShadow_t3766914079, ___maxSombraBola_3)); }
	inline float get_maxSombraBola_3() const { return ___maxSombraBola_3; }
	inline float* get_address_of_maxSombraBola_3() { return &___maxSombraBola_3; }
	inline void set_maxSombraBola_3(float value)
	{
		___maxSombraBola_3 = value;
	}

	inline static int32_t get_offset_of_alturaSombra_4() { return static_cast<int32_t>(offsetof(BallShadow_t3766914079, ___alturaSombra_4)); }
	inline float get_alturaSombra_4() const { return ___alturaSombra_4; }
	inline float* get_address_of_alturaSombra_4() { return &___alturaSombra_4; }
	inline void set_alturaSombra_4(float value)
	{
		___alturaSombra_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
