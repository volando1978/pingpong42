﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FrontWall
struct FrontWall_t2131806067;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void FrontWall::.ctor()
extern "C"  void FrontWall__ctor_m3287809880 (FrontWall_t2131806067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontWall::CreateIt(System.Int32,System.Int32)
extern "C"  void FrontWall_CreateIt_m2121949075 (FrontWall_t2131806067 * __this, int32_t ___xWall0, int32_t ___yWall1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontWall::CreateBrickWall(System.Int32,System.Int32)
extern "C"  void FrontWall_CreateBrickWall_m1068921921 (FrontWall_t2131806067 * __this, int32_t ___xWall0, int32_t ___yWall1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrontWall::RemoveBrick(UnityEngine.GameObject)
extern "C"  void FrontWall_RemoveBrick_m666535627 (FrontWall_t2131806067 * __this, GameObject_t3674682005 * ___brick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
