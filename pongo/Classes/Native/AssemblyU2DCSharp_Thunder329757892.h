﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Light
struct Light_t4202674828;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Thunder
struct  Thunder_t329757892  : public MonoBehaviour_t667441552
{
public:
	// System.Single Thunder::minTime
	float ___minTime_2;
	// System.Single Thunder::threshold
	float ___threshold_3;
	// UnityEngine.Light Thunder::light
	Light_t4202674828 * ___light_4;
	// System.Single Thunder::lastTime
	float ___lastTime_5;

public:
	inline static int32_t get_offset_of_minTime_2() { return static_cast<int32_t>(offsetof(Thunder_t329757892, ___minTime_2)); }
	inline float get_minTime_2() const { return ___minTime_2; }
	inline float* get_address_of_minTime_2() { return &___minTime_2; }
	inline void set_minTime_2(float value)
	{
		___minTime_2 = value;
	}

	inline static int32_t get_offset_of_threshold_3() { return static_cast<int32_t>(offsetof(Thunder_t329757892, ___threshold_3)); }
	inline float get_threshold_3() const { return ___threshold_3; }
	inline float* get_address_of_threshold_3() { return &___threshold_3; }
	inline void set_threshold_3(float value)
	{
		___threshold_3 = value;
	}

	inline static int32_t get_offset_of_light_4() { return static_cast<int32_t>(offsetof(Thunder_t329757892, ___light_4)); }
	inline Light_t4202674828 * get_light_4() const { return ___light_4; }
	inline Light_t4202674828 ** get_address_of_light_4() { return &___light_4; }
	inline void set_light_4(Light_t4202674828 * value)
	{
		___light_4 = value;
		Il2CppCodeGenWriteBarrier(&___light_4, value);
	}

	inline static int32_t get_offset_of_lastTime_5() { return static_cast<int32_t>(offsetof(Thunder_t329757892, ___lastTime_5)); }
	inline float get_lastTime_5() const { return ___lastTime_5; }
	inline float* get_address_of_lastTime_5() { return &___lastTime_5; }
	inline void set_lastTime_5(float value)
	{
		___lastTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
