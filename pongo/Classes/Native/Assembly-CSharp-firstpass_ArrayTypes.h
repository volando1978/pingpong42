﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// CnControls.VirtualAxis
struct VirtualAxis_t2395187226;
// CnControls.VirtualButton
struct VirtualButton_t3335323691;
// CnControls.DpadAxis
struct DpadAxis_t235416056;

#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualAx2395187226.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualBu3335323691.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_DpadAxis235416056.h"

#pragma once
// CnControls.VirtualAxis[]
struct VirtualAxisU5BU5D_t4079305279  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VirtualAxis_t2395187226 * m_Items[1];

public:
	inline VirtualAxis_t2395187226 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualAxis_t2395187226 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualAxis_t2395187226 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CnControls.VirtualButton[]
struct VirtualButtonU5BU5D_t2843237066  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VirtualButton_t3335323691 * m_Items[1];

public:
	inline VirtualButton_t3335323691 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualButton_t3335323691 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualButton_t3335323691 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CnControls.DpadAxis[]
struct DpadAxisU5BU5D_t1203427113  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DpadAxis_t235416056 * m_Items[1];

public:
	inline DpadAxis_t235416056 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DpadAxis_t235416056 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DpadAxis_t235416056 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
