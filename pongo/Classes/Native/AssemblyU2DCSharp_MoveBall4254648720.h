﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveBall
struct  MoveBall_t4254648720  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 MoveBall::velocidad
	Vector3_t4282066566  ___velocidad_2;
	// UnityEngine.Vector3 MoveBall::boundary
	Vector3_t4282066566  ___boundary_3;
	// System.Boolean MoveBall::isReady
	bool ___isReady_4;

public:
	inline static int32_t get_offset_of_velocidad_2() { return static_cast<int32_t>(offsetof(MoveBall_t4254648720, ___velocidad_2)); }
	inline Vector3_t4282066566  get_velocidad_2() const { return ___velocidad_2; }
	inline Vector3_t4282066566 * get_address_of_velocidad_2() { return &___velocidad_2; }
	inline void set_velocidad_2(Vector3_t4282066566  value)
	{
		___velocidad_2 = value;
	}

	inline static int32_t get_offset_of_boundary_3() { return static_cast<int32_t>(offsetof(MoveBall_t4254648720, ___boundary_3)); }
	inline Vector3_t4282066566  get_boundary_3() const { return ___boundary_3; }
	inline Vector3_t4282066566 * get_address_of_boundary_3() { return &___boundary_3; }
	inline void set_boundary_3(Vector3_t4282066566  value)
	{
		___boundary_3 = value;
	}

	inline static int32_t get_offset_of_isReady_4() { return static_cast<int32_t>(offsetof(MoveBall_t4254648720, ___isReady_4)); }
	inline bool get_isReady_4() const { return ___isReady_4; }
	inline bool* get_address_of_isReady_4() { return &___isReady_4; }
	inline void set_isReady_4(bool value)
	{
		___isReady_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
