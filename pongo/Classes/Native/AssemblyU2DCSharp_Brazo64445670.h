﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Brazo
struct  Brazo_t64445670  : public MonoBehaviour_t667441552
{
public:
	// System.Single Brazo::acceleration
	float ___acceleration_2;
	// UnityEngine.Transform Brazo::brazo
	Transform_t1659122786 * ___brazo_3;

public:
	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(Brazo_t64445670, ___acceleration_2)); }
	inline float get_acceleration_2() const { return ___acceleration_2; }
	inline float* get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(float value)
	{
		___acceleration_2 = value;
	}

	inline static int32_t get_offset_of_brazo_3() { return static_cast<int32_t>(offsetof(Brazo_t64445670, ___brazo_3)); }
	inline Transform_t1659122786 * get_brazo_3() const { return ___brazo_3; }
	inline Transform_t1659122786 ** get_address_of_brazo_3() { return &___brazo_3; }
	inline void set_brazo_3(Transform_t1659122786 * value)
	{
		___brazo_3 = value;
		Il2CppCodeGenWriteBarrier(&___brazo_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
