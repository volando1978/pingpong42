﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CnControls.CnInputManager
struct CnInputManager_t540789462;
// System.String
struct String_t;
// CnControls.VirtualAxis
struct VirtualAxis_t2395187226;
// CnControls.VirtualButton
struct VirtualButton_t3335323691;
// System.Collections.Generic.List`1<CnControls.VirtualAxis>
struct List_1_t3763372778;
// System.Collections.Generic.List`1<CnControls.VirtualButton>
struct List_1_t408541947;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualAx2395187226.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CnControls_VirtualBu3335323691.h"

// System.Void CnControls.CnInputManager::.ctor()
extern "C"  void CnInputManager__ctor_m525640124 (CnInputManager_t540789462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CnControls.CnInputManager CnControls.CnInputManager::get_Instance()
extern "C"  CnInputManager_t540789462 * CnInputManager_get_Instance_m1958956814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CnControls.CnInputManager::get_TouchCount()
extern "C"  int32_t CnInputManager_get_TouchCount_m2484571883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch CnControls.CnInputManager::GetTouch(System.Int32)
extern "C"  Touch_t4210255029  CnInputManager_GetTouch_m1019003927 (Il2CppObject * __this /* static, unused */, int32_t ___touchIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CnControls.CnInputManager::GetAxis(System.String)
extern "C"  float CnInputManager_GetAxis_m2810903397 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CnControls.CnInputManager::GetAxisRaw(System.String)
extern "C"  float CnInputManager_GetAxisRaw_m692810837 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CnControls.CnInputManager::GetAxis(System.String,System.Boolean)
extern "C"  float CnInputManager_GetAxis_m562856312 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, bool ___isRaw1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.CnInputManager::GetButton(System.String)
extern "C"  bool CnInputManager_GetButton_m2809242788 (Il2CppObject * __this /* static, unused */, String_t* ___buttonName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.CnInputManager::GetButtonDown(System.String)
extern "C"  bool CnInputManager_GetButtonDown_m3486119458 (Il2CppObject * __this /* static, unused */, String_t* ___buttonName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.CnInputManager::GetButtonUp(System.String)
extern "C"  bool CnInputManager_GetButtonUp_m2544187337 (Il2CppObject * __this /* static, unused */, String_t* ___buttonName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.CnInputManager::AxisExists(System.String)
extern "C"  bool CnInputManager_AxisExists_m2616522713 (Il2CppObject * __this /* static, unused */, String_t* ___axisName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.CnInputManager::ButtonExists(System.String)
extern "C"  bool CnInputManager_ButtonExists_m2499826600 (Il2CppObject * __this /* static, unused */, String_t* ___buttonName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.CnInputManager::RegisterVirtualAxis(CnControls.VirtualAxis)
extern "C"  void CnInputManager_RegisterVirtualAxis_m2285669572 (Il2CppObject * __this /* static, unused */, VirtualAxis_t2395187226 * ___virtualAxis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.CnInputManager::UnregisterVirtualAxis(CnControls.VirtualAxis)
extern "C"  void CnInputManager_UnregisterVirtualAxis_m1023264331 (Il2CppObject * __this /* static, unused */, VirtualAxis_t2395187226 * ___virtualAxis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.CnInputManager::RegisterVirtualButton(CnControls.VirtualButton)
extern "C"  void CnInputManager_RegisterVirtualButton_m3391168612 (Il2CppObject * __this /* static, unused */, VirtualButton_t3335323691 * ___virtualButton0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.CnInputManager::UnregisterVirtualButton(CnControls.VirtualButton)
extern "C"  void CnInputManager_UnregisterVirtualButton_m1628192363 (Il2CppObject * __this /* static, unused */, VirtualButton_t3335323691 * ___virtualButton0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CnControls.CnInputManager::GetVirtualAxisValue(System.Collections.Generic.List`1<CnControls.VirtualAxis>,System.String,System.Boolean)
extern "C"  float CnInputManager_GetVirtualAxisValue_m4142473329 (Il2CppObject * __this /* static, unused */, List_1_t3763372778 * ___virtualAxisList0, String_t* ___axisName1, bool ___isRaw2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonDown(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern "C"  bool CnInputManager_GetAnyVirtualButtonDown_m706362819 (Il2CppObject * __this /* static, unused */, List_1_t408541947 * ___virtualButtons0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButtonUp(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern "C"  bool CnInputManager_GetAnyVirtualButtonUp_m3586789226 (Il2CppObject * __this /* static, unused */, List_1_t408541947 * ___virtualButtons0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CnControls.CnInputManager::GetAnyVirtualButton(System.Collections.Generic.List`1<CnControls.VirtualButton>)
extern "C"  bool CnInputManager_GetAnyVirtualButton_m3717308741 (Il2CppObject * __this /* static, unused */, List_1_t408541947 * ___virtualButtons0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
