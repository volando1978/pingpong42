﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CnControls.Touchpad
struct Touchpad_t2035887356;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void CnControls.Touchpad::.ctor()
extern "C"  void Touchpad__ctor_m770695510 (Touchpad_t2035887356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera CnControls.Touchpad::get_CurrentEventCamera()
extern "C"  Camera_t2727095145 * Touchpad_get_CurrentEventCamera_m63735220 (Touchpad_t2035887356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.Touchpad::set_CurrentEventCamera(UnityEngine.Camera)
extern "C"  void Touchpad_set_CurrentEventCamera_m3264480181 (Touchpad_t2035887356 * __this, Camera_t2727095145 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.Touchpad::OnEnable()
extern "C"  void Touchpad_OnEnable_m966352496 (Touchpad_t2035887356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.Touchpad::OnDisable()
extern "C"  void Touchpad_OnDisable_m333093565 (Touchpad_t2035887356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.Touchpad::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Touchpad_OnDrag_m1969753533 (Touchpad_t2035887356 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.Touchpad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Touchpad_OnPointerUp_m3176516497 (Touchpad_t2035887356 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.Touchpad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Touchpad_OnPointerDown_m2615129962 (Touchpad_t2035887356 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CnControls.Touchpad::Update()
extern "C"  void Touchpad_Update_m4143586423 (Touchpad_t2035887356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
