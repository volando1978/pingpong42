﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeLight
struct TimeLight_t2022754857;

#include "codegen/il2cpp-codegen.h"

// System.Void TimeLight::.ctor()
extern "C"  void TimeLight__ctor_m3113265506 (TimeLight_t2022754857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeLight::Start()
extern "C"  void TimeLight_Start_m2060403298 (TimeLight_t2022754857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
