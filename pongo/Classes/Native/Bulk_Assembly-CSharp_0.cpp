﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// BallCollider
struct BallCollider_t3766478643;
// Controller
struct Controller_t2630893500;
// System.Object
struct Il2CppObject;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Collider
struct Collider_t2939674232;
// MoveBall
struct MoveBall_t4254648720;
// Brick
struct Brick_t64452641;
// BallShadow
struct BallShadow_t3766914079;
// Shake
struct Shake_t79847142;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// BallCollider/<BigBallTime>c__Iterator0
struct U3CBigBallTimeU3Ec__Iterator0_t1350130161;
// BallCollider/<HitColor>c__Iterator1
struct U3CHitColorU3Ec__Iterator1_t1194571374;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// Blanco
struct Blanco_t1992262851;
// Blanco/<Plof>c__Iterator2
struct U3CPlofU3Ec__Iterator2_t1800312444;
// BouncingAnim
struct BouncingAnim_t2247160086;
// Brazo
struct Brazo_t64445670;
// ColliderPlayer
struct ColliderPlayer_t3441697813;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.BoxCollider
struct BoxCollider_t2538127765;
// ColliderPlayer/<flan>c__Iterator3
struct U3CflanU3Ec__Iterator3_t3689796459;
// RoomGenerator
struct RoomGenerator_t3606720536;
// Move
struct Move_t2404337;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// FrontWall
struct FrontWall_t2131806067;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// System.String
struct String_t;
// Controller/<CamAnim>c__Iterator4
struct U3CCamAnimU3Ec__Iterator4_t3416449610;
// Controller/<Cortina>c__Iterator5
struct U3CCortinaU3Ec__Iterator5_t3064932893;
// Controller/<runScore>c__Iterator6
struct U3CrunScoreU3Ec__Iterator6_t3971101203;
// CreateMap
struct CreateMap_t2602318496;
// Plants
struct Plants_t2393071496;
// Dimensions
struct Dimensions_t2407799789;
// MapController
struct MapController_t1754731000;
// Room
struct Room_t2553083;
// UnityEngine.Material
struct Material_t3870600107;
// Progresion
struct Progresion_t2029956962;
// UnityEngine.MeshRenderer
struct MeshRenderer_t2804666580;
// SelfDestroy
struct SelfDestroy_t1472365294;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// Shake/<shake>c__Iterator7
struct U3CshakeU3Ec__Iterator7_t570686417;
// Telefono
struct Telefono_t2999111444;
// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.UI.Image
struct Image_t538875265;
// Telefono/<dialing>c__Iterator8
struct U3CdialingU3Ec__Iterator8_t3806020820;
// Thunder
struct Thunder_t329757892;
// TimeLight
struct TimeLight_t2022754857;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_BallCollider3766478643.h"
#include "AssemblyU2DCSharp_BallCollider3766478643MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller2630893500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_Controller2630893500.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "AssemblyU2DCSharp_Brick64452641MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "AssemblyU2DCSharp_Shake79847142MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_MoveBall4254648720.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "AssemblyU2DCSharp_Brick64452641.h"
#include "AssemblyU2DCSharp_BallShadow3766914079.h"
#include "AssemblyU2DCSharp_Shake79847142.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_BallCollider_U3CBigBallTimeU3Ec_1350130161MethodDeclarations.h"
#include "AssemblyU2DCSharp_BallCollider_U3CBigBallTimeU3Ec_1350130161.h"
#include "AssemblyU2DCSharp_BallCollider_U3CHitColorU3Ec__It1194571374MethodDeclarations.h"
#include "AssemblyU2DCSharp_BallCollider_U3CHitColorU3Ec__It1194571374.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "AssemblyU2DCSharp_BallShadow3766914079MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32598853688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "AssemblyU2DCSharp_Blanco1992262851.h"
#include "AssemblyU2DCSharp_Blanco1992262851MethodDeclarations.h"
#include "AssemblyU2DCSharp_Blanco_U3CPlofU3Ec__Iterator21800312444MethodDeclarations.h"
#include "AssemblyU2DCSharp_Blanco_U3CPlofU3Ec__Iterator21800312444.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "AssemblyU2DCSharp_BouncingAnim2247160086.h"
#include "AssemblyU2DCSharp_BouncingAnim2247160086MethodDeclarations.h"
#include "AssemblyU2DCSharp_Brazo64445670.h"
#include "AssemblyU2DCSharp_Brazo64445670MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "AssemblyU2DCSharp_ColliderPlayer3441697813.h"
#include "AssemblyU2DCSharp_ColliderPlayer3441697813MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_Collision2494107688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2538127765.h"
#include "UnityEngine_UnityEngine_Bounds2711641849MethodDeclarations.h"
#include "AssemblyU2DCSharp_ColliderPlayer_U3CflanU3Ec__Iter3689796459MethodDeclarations.h"
#include "AssemblyU2DCSharp_ColliderPlayer_U3CflanU3Ec__Iter3689796459.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_State2540969054.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_RoomGenerator3606720536.h"
#include "AssemblyU2DCSharp_RoomGenerator3606720536MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_U3CCamAnimU3Ec__Itera3416449610MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_U3CCamAnimU3Ec__Itera3416449610.h"
#include "AssemblyU2DCSharp_Move2404337MethodDeclarations.h"
#include "AssemblyU2DCSharp_Move2404337.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_RectTransform972643934MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen747900261MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen747900261.h"
#include "AssemblyU2DCSharp_FrontWall2131806067MethodDeclarations.h"
#include "AssemblyU2DCSharp_FrontWall2131806067.h"
#include "AssemblyU2DCSharp_Controller_U3CCortinaU3Ec__Itera3064932893MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_U3CCortinaU3Ec__Itera3064932893.h"
#include "AssemblyU2DCSharp_Controller_U3CrunScoreU3Ec__Iter3971101203MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_U3CrunScoreU3Ec__Iter3971101203.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_State2540969054MethodDeclarations.h"
#include "AssemblyU2DCSharp_CreateMap2602318496.h"
#include "AssemblyU2DCSharp_CreateMap2602318496MethodDeclarations.h"
#include "AssemblyU2DCSharp_Plants2393071496MethodDeclarations.h"
#include "AssemblyU2DCSharp_Plants2393071496.h"
#include "AssemblyU2DCSharp_Dimensions2407799789.h"
#include "AssemblyU2DCSharp_Dimensions2407799789MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "AssemblyU2DCSharp_MapController1754731000.h"
#include "AssemblyU2DCSharp_MapController1754731000MethodDeclarations.h"
#include "AssemblyU2DCSharp_Room2553083.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "AssemblyU2DCSharp_MoveBall4254648720MethodDeclarations.h"
#include "AssemblyU2DCSharp_Plants_Tiempo1322696995.h"
#include "AssemblyU2DCSharp_Plants_Tiempo1322696995MethodDeclarations.h"
#include "AssemblyU2DCSharp_Progresion2029956962.h"
#include "AssemblyU2DCSharp_Progresion2029956962MethodDeclarations.h"
#include "AssemblyU2DCSharp_Room2553083MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2538127765MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580.h"
#include "AssemblyU2DCSharp_SelfDestroy1472365294.h"
#include "AssemblyU2DCSharp_SelfDestroy1472365294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177.h"
#include "AssemblyU2DCSharp_Shake_U3CshakeU3Ec__Iterator7570686417MethodDeclarations.h"
#include "AssemblyU2DCSharp_Shake_U3CshakeU3Ec__Iterator7570686417.h"
#include "AssemblyU2DCSharp_Telefono2999111444.h"
#include "AssemblyU2DCSharp_Telefono2999111444MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1885181538MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "AssemblyU2DCSharp_Telefono_U3CdialingU3Ec__Iterato3806020820MethodDeclarations.h"
#include "AssemblyU2DCSharp_Telefono_U3CdialingU3Ec__Iterato3806020820.h"
#include "AssemblyU2DCSharp_Thunder329757892.h"
#include "AssemblyU2DCSharp_Thunder329757892MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Light4202674828.h"
#include "AssemblyU2DCSharp_TimeLight2022754857.h"
#include "AssemblyU2DCSharp_TimeLight2022754857MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_TimeSpan413522987MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Controller>()
#define GameObject_GetComponent_TisController_t2630893500_m3221217313(__this, method) ((  Controller_t2630893500 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MoveBall>()
#define Component_GetComponent_TisMoveBall_t4254648720_m1623386869(__this, method) ((  MoveBall_t4254648720 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Brick>()
#define Component_GetComponent_TisBrick_t64452641_m132164344(__this, method) ((  Brick_t64452641 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<BallCollider>()
#define Component_GetComponentInChildren_TisBallCollider_t3766478643_m705816406(__this, method) ((  BallCollider_t3766478643 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<BallShadow>()
#define Component_GetComponentInChildren_TisBallShadow_t3766914079_m3662703978(__this, method) ((  BallShadow_t3766914079 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Shake>()
#define Component_GetComponent_TisShake_t79847142_m3483210259(__this, method) ((  Shake_t79847142 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(__this, method) ((  Renderer_t3076687687 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, method) ((  Renderer_t3076687687 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m782999868(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<BallCollider>()
#define GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182(__this, method) ((  BallCollider_t3766478643 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider>()
#define Component_GetComponent_TisBoxCollider_t2538127765_m1593759217(__this, method) ((  BoxCollider_t2538127765 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MoveBall>()
#define GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(__this, method) ((  MoveBall_t4254648720 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<RoomGenerator>()
#define GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481(__this, method) ((  RoomGenerator_t3606720536 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Move>()
#define GameObject_GetComponent_TisMove_t2404337_m307893836(__this, method) ((  Move_t2404337 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<ColliderPlayer>()
#define GameObject_GetComponentInChildren_TisColliderPlayer_t3441697813_m249032460(__this, method) ((  ColliderPlayer_t3441697813 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t972643934_m406276429(__this, method) ((  RectTransform_t972643934 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<BallCollider>()
#define Component_GetComponent_TisBallCollider_t3766478643_m174257010(__this, method) ((  BallCollider_t3766478643 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<FrontWall>()
#define GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678(__this, method) ((  FrontWall_t2131806067 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t9039225_m202917489(__this, method) ((  Text_t9039225 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<BallShadow>()
#define Component_GetComponent_TisBallShadow_t3766914079_m3429748870(__this, method) ((  BallShadow_t3766914079 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(__this, method) ((  AudioSource_t1740077639 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Plants>()
#define Component_GetComponent_TisPlants_t2393071496_m215378813(__this, method) ((  Plants_t2393071496 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Brick>()
#define GameObject_GetComponent_TisBrick_t64452641_m1253576608(__this, method) ((  Brick_t64452641 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Dimensions>()
#define Component_GetComponent_TisDimensions_t2407799789_m2712634872(__this, method) ((  Dimensions_t2407799789 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Room>()
#define GameObject_GetComponent_TisRoom_t2553083_m444218626(__this, method) ((  Room_t2553083 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t3674682005_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t3674682005 * (*) (Il2CppObject * /* static, unused */, GameObject_t3674682005 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider>()
#define GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(__this, method) ((  BoxCollider_t2538127765 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910(__this, method) ((  MeshRenderer_t2804666580 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<BallShadow>()
#define GameObject_GetComponent_TisBallShadow_t3766914079_m2067993822(__this, method) ((  BallShadow_t3766914079 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Plants>()
#define GameObject_GetComponent_TisPlants_t2393071496_m619420629(__this, method) ((  Plants_t2393071496 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872(__this, method) ((  ParticleSystem_t381473177 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t3896396478_m2812094092(__this, method) ((  Button_t3896396478 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t538875265_m2140199269(__this, method) ((  Image_t538875265 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BallCollider::.ctor()
extern "C"  void BallCollider__ctor_m3051189448 (BallCollider_t3766478643 * __this, const MethodInfo* method)
{
	{
		__this->set_isSecondBounce_12((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BallCollider::PlayAudio(System.Single)
extern const MethodInfo* GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var;
extern const uint32_t BallCollider_PlayAudio_m338850499_MetadataUsageId;
extern "C"  void BallCollider_PlayAudio_m338850499 (BallCollider_t3766478643 * __this, float ___pitch0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BallCollider_PlayAudio_m338850499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_Controller_2();
		NullCheck(L_0);
		Controller_t2630893500 * L_1 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_0, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		float L_2 = ___pitch0;
		NullCheck(L_1);
		Controller_PlayAudio_m1101257740(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BallCollider::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void BallCollider_setCameras_m1923159934 (BallCollider_t3766478643 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___Perspe0;
		__this->set_Perspec_8(L_0);
		Camera_t2727095145 * L_1 = ___Ortho1;
		__this->set_Ortho_9(L_1);
		Camera_t2727095145 * L_2 = ___Iso2;
		__this->set_Iso_10(L_2);
		Camera_t2727095145 * L_3 = ___Interface3;
		__this->set_Interface_11(L_3);
		return;
	}
}
// System.Void BallCollider::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisBallCollider_t3766478643_m705816406_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisBallShadow_t3766914079_m3662703978_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisShake_t79847142_m3483210259_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral64452641;
extern Il2CppCodeGenString* _stringLiteral1548947199;
extern Il2CppCodeGenString* _stringLiteral66203856;
extern Il2CppCodeGenString* _stringLiteral980556366;
extern Il2CppCodeGenString* _stringLiteral3461168748;
extern Il2CppCodeGenString* _stringLiteral80238310;
extern Il2CppCodeGenString* _stringLiteral3312290682;
extern Il2CppCodeGenString* _stringLiteral2409989;
extern Il2CppCodeGenString* _stringLiteral2062879;
extern const uint32_t BallCollider_OnTriggerEnter_m2106133424_MetadataUsageId;
extern "C"  void BallCollider_OnTriggerEnter_m2106133424 (BallCollider_t3766478643 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BallCollider_OnTriggerEnter_m2106133424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Collider_t2939674232 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m217485006(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, _stringLiteral64452641, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0282;
		}
	}
	{
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Transform_get_parent_m2236876972(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		MoveBall_t4254648720 * L_5 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_4, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		NullCheck(L_5);
		Vector3_t4282066566 * L_6 = L_5->get_address_of_velocidad_2();
		float L_7 = L_6->get_x_1();
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = Transform_get_parent_m2236876972(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		MoveBall_t4254648720 * L_10 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_9, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		NullCheck(L_10);
		Vector3_t4282066566 * L_11 = L_10->get_address_of_velocidad_2();
		float L_12 = L_11->get_y_2();
		Transform_t1659122786 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = Transform_get_parent_m2236876972(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		MoveBall_t4254648720 * L_15 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_14, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		NullCheck(L_15);
		Vector3_t4282066566 * L_16 = L_15->get_address_of_velocidad_2();
		float L_17 = L_16->get_z_3();
		Vector3__ctor_m2926210380((&V_0), L_7, L_12, L_17, /*hidden argument*/NULL);
		Collider_t2939674232 * L_18 = ___other0;
		NullCheck(L_18);
		Brick_t64452641 * L_19 = Component_GetComponent_TisBrick_t64452641_m132164344(L_18, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		NullCheck(L_19);
		String_t* L_20 = L_19->get_tipo_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_20, _stringLiteral1548947199, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b4;
		}
	}
	{
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t1659122786 * L_23 = Transform_get_parent_m2236876972(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		MoveBall_t4254648720 * L_24 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_23, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		float L_25 = (&V_0)->get_x_1();
		float L_26 = (&V_0)->get_y_2();
		float L_27 = (&V_0)->get_z_3();
		Vector3_t4282066566  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2926210380(&L_28, L_25, L_26, ((-L_27)), /*hidden argument*/NULL);
		NullCheck(L_24);
		L_24->set_velocidad_2(L_28);
	}

IL_00b4:
	{
		Transform_t1659122786 * L_29 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = Transform_get_parent_m2236876972(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		BallCollider_t3766478643 * L_31 = Component_GetComponentInChildren_TisBallCollider_t3766478643_m705816406(L_30, /*hidden argument*/Component_GetComponentInChildren_TisBallCollider_t3766478643_m705816406_MethodInfo_var);
		NullCheck(L_31);
		L_31->set_isSecondBounce_12((bool)1);
		Collider_t2939674232 * L_32 = ___other0;
		NullCheck(L_32);
		Brick_t64452641 * L_33 = Component_GetComponent_TisBrick_t64452641_m132164344(L_32, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		NullCheck(L_33);
		String_t* L_34 = L_33->get_tipo_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_35 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_34, _stringLiteral66203856, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0110;
		}
	}
	{
		Collider_t2939674232 * L_36 = ___other0;
		NullCheck(L_36);
		Brick_t64452641 * L_37 = Component_GetComponent_TisBrick_t64452641_m132164344(L_36, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		NullCheck(L_37);
		Brick_RestaToque_m2346085363(L_37, /*hidden argument*/NULL);
		Collider_t2939674232 * L_38 = ___other0;
		NullCheck(L_38);
		Brick_t64452641 * L_39 = Component_GetComponent_TisBrick_t64452641_m132164344(L_38, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		Collider_t2939674232 * L_40 = ___other0;
		NullCheck(L_40);
		Transform_t1659122786 * L_41 = Component_get_transform_m4257140443(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4282066566  L_42 = Transform_get_position_m2211398607(L_41, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_43 = __this->get_Controller_2();
		NullCheck(L_39);
		Brick_checkToques_m2912556642(L_39, L_42, L_43, /*hidden argument*/NULL);
		goto IL_0262;
	}

IL_0110:
	{
		Collider_t2939674232 * L_44 = ___other0;
		NullCheck(L_44);
		Brick_t64452641 * L_45 = Component_GetComponent_TisBrick_t64452641_m132164344(L_44, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		NullCheck(L_45);
		String_t* L_46 = L_45->get_tipo_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_47 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_46, _stringLiteral980556366, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01b5;
		}
	}
	{
		GameObject_t3674682005 * L_48 = __this->get_Controller_2();
		NullCheck(L_48);
		Controller_t2630893500 * L_49 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_48, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		Collider_t2939674232 * L_50 = ___other0;
		NullCheck(L_50);
		Transform_t1659122786 * L_51 = Component_get_transform_m4257140443(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t4282066566  L_52 = Transform_get_position_m2211398607(L_51, /*hidden argument*/NULL);
		Transform_t1659122786 * L_53 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_t1659122786 * L_54 = Transform_get_parent_m2236876972(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		MoveBall_t4254648720 * L_55 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_54, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		NullCheck(L_55);
		Vector3_t4282066566  L_56 = L_55->get_boundary_3();
		Transform_t1659122786 * L_57 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		Transform_t1659122786 * L_58 = Transform_get_parent_m2236876972(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		BallShadow_t3766914079 * L_59 = Component_GetComponentInChildren_TisBallShadow_t3766914079_m3662703978(L_58, /*hidden argument*/Component_GetComponentInChildren_TisBallShadow_t3766914079_m3662703978_MethodInfo_var);
		NullCheck(L_59);
		float L_60 = L_59->get_alturaSombra_4();
		NullCheck(L_49);
		Controller_CreateMultiBalls_m521653845(L_49, L_52, L_56, L_60, /*hidden argument*/NULL);
		Camera_t2727095145 * L_61 = __this->get_Iso_10();
		NullCheck(L_61);
		bool L_62 = Behaviour_get_isActiveAndEnabled_m210167461(L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_0194;
		}
	}
	{
		Camera_t2727095145 * L_63 = __this->get_Iso_10();
		NullCheck(L_63);
		Shake_t79847142 * L_64 = Component_GetComponent_TisShake_t79847142_m3483210259(L_63, /*hidden argument*/Component_GetComponent_TisShake_t79847142_m3483210259_MethodInfo_var);
		NullCheck(L_64);
		Shake_ShakeThatCamera_m1815182614(L_64, (0.05f), /*hidden argument*/NULL);
	}

IL_0194:
	{
		Collider_t2939674232 * L_65 = ___other0;
		NullCheck(L_65);
		Brick_t64452641 * L_66 = Component_GetComponent_TisBrick_t64452641_m132164344(L_65, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		Collider_t2939674232 * L_67 = ___other0;
		NullCheck(L_67);
		Transform_t1659122786 * L_68 = Component_get_transform_m4257140443(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		Vector3_t4282066566  L_69 = Transform_get_position_m2211398607(L_68, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_70 = __this->get_Controller_2();
		NullCheck(L_66);
		Brick_kill_m2419377885(L_66, L_69, L_70, /*hidden argument*/NULL);
		goto IL_0262;
	}

IL_01b5:
	{
		Collider_t2939674232 * L_71 = ___other0;
		NullCheck(L_71);
		Brick_t64452641 * L_72 = Component_GetComponent_TisBrick_t64452641_m132164344(L_71, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		NullCheck(L_72);
		String_t* L_73 = L_72->get_tipo_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_74 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_73, _stringLiteral1548947199, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0221;
		}
	}
	{
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral3461168748, /*hidden argument*/NULL);
		Camera_t2727095145 * L_75 = __this->get_Iso_10();
		NullCheck(L_75);
		bool L_76 = Behaviour_get_isActiveAndEnabled_m210167461(L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_0200;
		}
	}
	{
		Camera_t2727095145 * L_77 = __this->get_Iso_10();
		NullCheck(L_77);
		Shake_t79847142 * L_78 = Component_GetComponent_TisShake_t79847142_m3483210259(L_77, /*hidden argument*/Component_GetComponent_TisShake_t79847142_m3483210259_MethodInfo_var);
		NullCheck(L_78);
		Shake_ShakeThatCamera_m1815182614(L_78, (0.05f), /*hidden argument*/NULL);
	}

IL_0200:
	{
		Collider_t2939674232 * L_79 = ___other0;
		NullCheck(L_79);
		Brick_t64452641 * L_80 = Component_GetComponent_TisBrick_t64452641_m132164344(L_79, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		Collider_t2939674232 * L_81 = ___other0;
		NullCheck(L_81);
		Transform_t1659122786 * L_82 = Component_get_transform_m4257140443(L_81, /*hidden argument*/NULL);
		NullCheck(L_82);
		Vector3_t4282066566  L_83 = Transform_get_position_m2211398607(L_82, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_84 = __this->get_Controller_2();
		NullCheck(L_80);
		Brick_kill_m2419377885(L_80, L_83, L_84, /*hidden argument*/NULL);
		goto IL_0262;
	}

IL_0221:
	{
		Camera_t2727095145 * L_85 = __this->get_Iso_10();
		NullCheck(L_85);
		bool L_86 = Behaviour_get_isActiveAndEnabled_m210167461(L_85, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_0246;
		}
	}
	{
		Camera_t2727095145 * L_87 = __this->get_Iso_10();
		NullCheck(L_87);
		Shake_t79847142 * L_88 = Component_GetComponent_TisShake_t79847142_m3483210259(L_87, /*hidden argument*/Component_GetComponent_TisShake_t79847142_m3483210259_MethodInfo_var);
		NullCheck(L_88);
		Shake_ShakeThatCamera_m1815182614(L_88, (0.05f), /*hidden argument*/NULL);
	}

IL_0246:
	{
		Collider_t2939674232 * L_89 = ___other0;
		NullCheck(L_89);
		Brick_t64452641 * L_90 = Component_GetComponent_TisBrick_t64452641_m132164344(L_89, /*hidden argument*/Component_GetComponent_TisBrick_t64452641_m132164344_MethodInfo_var);
		Collider_t2939674232 * L_91 = ___other0;
		NullCheck(L_91);
		Transform_t1659122786 * L_92 = Component_get_transform_m4257140443(L_91, /*hidden argument*/NULL);
		NullCheck(L_92);
		Vector3_t4282066566  L_93 = Transform_get_position_m2211398607(L_92, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_94 = __this->get_Controller_2();
		NullCheck(L_90);
		Brick_kill_m2419377885(L_90, L_93, L_94, /*hidden argument*/NULL);
	}

IL_0262:
	{
		Transform_t1659122786 * L_95 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_95);
		Vector3_t4282066566  L_96 = Transform_get_position_m2211398607(L_95, /*hidden argument*/NULL);
		V_2 = L_96;
		float L_97 = (&V_2)->get_z_3();
		float L_98 = __this->get_pitchMod_6();
		BallCollider_PlayAudio_m338850499(__this, ((float)((float)L_97/(float)L_98)), /*hidden argument*/NULL);
	}

IL_0282:
	{
		Collider_t2939674232 * L_99 = ___other0;
		NullCheck(L_99);
		String_t* L_100 = Component_get_tag_m217485006(L_99, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_101 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_100, _stringLiteral80238310, /*hidden argument*/NULL);
		if (!L_101)
		{
			goto IL_02a5;
		}
	}
	{
		int32_t L_102 = __this->get_tocaSuelo_3();
		__this->set_tocaSuelo_3(((int32_t)((int32_t)L_102+(int32_t)1)));
	}

IL_02a5:
	{
		Collider_t2939674232 * L_103 = ___other0;
		NullCheck(L_103);
		String_t* L_104 = Component_get_tag_m217485006(L_103, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_105 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_104, _stringLiteral3312290682, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_030d;
		}
	}
	{
		int32_t L_106 = __this->get_tocaSuelo_3();
		__this->set_tocaSuelo_3(((int32_t)((int32_t)L_106+(int32_t)1)));
		GameObject_t3674682005 * L_107 = __this->get_Controller_2();
		NullCheck(L_107);
		Controller_t2630893500 * L_108 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_107, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		GameObject_t3674682005 * L_109 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_109);
		Transform_t1659122786 * L_110 = GameObject_get_transform_m1278640159(L_109, /*hidden argument*/NULL);
		NullCheck(L_110);
		Transform_t1659122786 * L_111 = Transform_get_parent_m2236876972(L_110, /*hidden argument*/NULL);
		NullCheck(L_111);
		GameObject_t3674682005 * L_112 = Component_get_gameObject_m1170635899(L_111, /*hidden argument*/NULL);
		NullCheck(L_108);
		Controller_CheckEndGame_m111272194(L_108, L_112, /*hidden argument*/NULL);
		Transform_t1659122786 * L_113 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_113);
		Vector3_t4282066566  L_114 = Transform_get_position_m2211398607(L_113, /*hidden argument*/NULL);
		V_3 = L_114;
		float L_115 = (&V_3)->get_z_3();
		float L_116 = __this->get_pitchMod_6();
		BallCollider_PlayAudio_m338850499(__this, ((float)((float)L_115/(float)L_116)), /*hidden argument*/NULL);
	}

IL_030d:
	{
		Collider_t2939674232 * L_117 = ___other0;
		NullCheck(L_117);
		String_t* L_118 = Component_get_tag_m217485006(L_117, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_119 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_118, _stringLiteral2409989, /*hidden argument*/NULL);
		if (!L_119)
		{
			goto IL_035a;
		}
	}
	{
		__this->set_isSecondBounce_12((bool)1);
		Camera_t2727095145 * L_120 = __this->get_Iso_10();
		NullCheck(L_120);
		bool L_121 = Behaviour_get_isActiveAndEnabled_m210167461(L_120, /*hidden argument*/NULL);
		if (!L_121)
		{
			goto IL_035a;
		}
	}
	{
		Transform_t1659122786 * L_122 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_122);
		Vector3_t4282066566  L_123 = Transform_get_position_m2211398607(L_122, /*hidden argument*/NULL);
		V_4 = L_123;
		float L_124 = (&V_4)->get_z_3();
		float L_125 = __this->get_pitchMod_6();
		BallCollider_PlayAudio_m338850499(__this, ((float)((float)L_124/(float)L_125)), /*hidden argument*/NULL);
	}

IL_035a:
	{
		Collider_t2939674232 * L_126 = ___other0;
		NullCheck(L_126);
		String_t* L_127 = Component_get_tag_m217485006(L_126, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_128 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_127, _stringLiteral2062879, /*hidden argument*/NULL);
		if (!L_128)
		{
			goto IL_042b;
		}
	}
	{
		Transform_t1659122786 * L_129 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_129);
		Transform_t1659122786 * L_130 = Transform_get_parent_m2236876972(L_129, /*hidden argument*/NULL);
		NullCheck(L_130);
		MoveBall_t4254648720 * L_131 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_130, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		NullCheck(L_131);
		Vector3_t4282066566 * L_132 = L_131->get_address_of_velocidad_2();
		float L_133 = L_132->get_x_1();
		Transform_t1659122786 * L_134 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_134);
		Transform_t1659122786 * L_135 = Transform_get_parent_m2236876972(L_134, /*hidden argument*/NULL);
		NullCheck(L_135);
		MoveBall_t4254648720 * L_136 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_135, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		NullCheck(L_136);
		Vector3_t4282066566 * L_137 = L_136->get_address_of_velocidad_2();
		float L_138 = L_137->get_y_2();
		Transform_t1659122786 * L_139 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_139);
		Transform_t1659122786 * L_140 = Transform_get_parent_m2236876972(L_139, /*hidden argument*/NULL);
		NullCheck(L_140);
		MoveBall_t4254648720 * L_141 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_140, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		NullCheck(L_141);
		Vector3_t4282066566 * L_142 = L_141->get_address_of_velocidad_2();
		float L_143 = L_142->get_z_3();
		Vector3__ctor_m2926210380((&V_1), L_133, L_138, L_143, /*hidden argument*/NULL);
		Transform_t1659122786 * L_144 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_144);
		Transform_t1659122786 * L_145 = Transform_get_parent_m2236876972(L_144, /*hidden argument*/NULL);
		NullCheck(L_145);
		MoveBall_t4254648720 * L_146 = Component_GetComponent_TisMoveBall_t4254648720_m1623386869(L_145, /*hidden argument*/Component_GetComponent_TisMoveBall_t4254648720_m1623386869_MethodInfo_var);
		float L_147 = (&V_1)->get_x_1();
		float L_148 = (&V_1)->get_y_2();
		float L_149 = (&V_1)->get_z_3();
		Vector3_t4282066566  L_150;
		memset(&L_150, 0, sizeof(L_150));
		Vector3__ctor_m2926210380(&L_150, L_147, L_148, ((-L_149)), /*hidden argument*/NULL);
		NullCheck(L_146);
		L_146->set_velocidad_2(L_150);
		Transform_t1659122786 * L_151 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_151);
		Transform_t1659122786 * L_152 = Transform_get_parent_m2236876972(L_151, /*hidden argument*/NULL);
		NullCheck(L_152);
		BallCollider_t3766478643 * L_153 = Component_GetComponentInChildren_TisBallCollider_t3766478643_m705816406(L_152, /*hidden argument*/Component_GetComponentInChildren_TisBallCollider_t3766478643_m705816406_MethodInfo_var);
		NullCheck(L_153);
		L_153->set_isSecondBounce_12((bool)1);
		Transform_t1659122786 * L_154 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_154);
		Vector3_t4282066566  L_155 = Transform_get_position_m2211398607(L_154, /*hidden argument*/NULL);
		V_5 = L_155;
		float L_156 = (&V_5)->get_z_3();
		float L_157 = __this->get_pitchMod_6();
		BallCollider_PlayAudio_m338850499(__this, ((float)((float)L_156/(float)L_157)), /*hidden argument*/NULL);
	}

IL_042b:
	{
		return;
	}
}
// System.Collections.IEnumerator BallCollider::BigBallTime()
extern Il2CppClass* U3CBigBallTimeU3Ec__Iterator0_t1350130161_il2cpp_TypeInfo_var;
extern const uint32_t BallCollider_BigBallTime_m644600426_MetadataUsageId;
extern "C"  Il2CppObject * BallCollider_BigBallTime_m644600426 (BallCollider_t3766478643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BallCollider_BigBallTime_m644600426_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBigBallTimeU3Ec__Iterator0_t1350130161 * V_0 = NULL;
	{
		U3CBigBallTimeU3Ec__Iterator0_t1350130161 * L_0 = (U3CBigBallTimeU3Ec__Iterator0_t1350130161 *)il2cpp_codegen_object_new(U3CBigBallTimeU3Ec__Iterator0_t1350130161_il2cpp_TypeInfo_var);
		U3CBigBallTimeU3Ec__Iterator0__ctor_m3073329610(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBigBallTimeU3Ec__Iterator0_t1350130161 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CBigBallTimeU3Ec__Iterator0_t1350130161 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator BallCollider::HitColor(UnityEngine.GameObject)
extern Il2CppClass* U3CHitColorU3Ec__Iterator1_t1194571374_il2cpp_TypeInfo_var;
extern const uint32_t BallCollider_HitColor_m1095455820_MetadataUsageId;
extern "C"  Il2CppObject * BallCollider_HitColor_m1095455820 (BallCollider_t3766478643 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BallCollider_HitColor_m1095455820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CHitColorU3Ec__Iterator1_t1194571374 * V_0 = NULL;
	{
		U3CHitColorU3Ec__Iterator1_t1194571374 * L_0 = (U3CHitColorU3Ec__Iterator1_t1194571374 *)il2cpp_codegen_object_new(U3CHitColorU3Ec__Iterator1_t1194571374_il2cpp_TypeInfo_var);
		U3CHitColorU3Ec__Iterator1__ctor_m3621972157(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CHitColorU3Ec__Iterator1_t1194571374 * L_1 = V_0;
		GameObject_t3674682005 * L_2 = ___other0;
		NullCheck(L_1);
		L_1->set_other_1(L_2);
		U3CHitColorU3Ec__Iterator1_t1194571374 * L_3 = V_0;
		GameObject_t3674682005 * L_4 = ___other0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Eother_5(L_4);
		U3CHitColorU3Ec__Iterator1_t1194571374 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_6(__this);
		U3CHitColorU3Ec__Iterator1_t1194571374 * L_6 = V_0;
		return L_6;
	}
}
// System.Void BallCollider/<BigBallTime>c__Iterator0::.ctor()
extern "C"  void U3CBigBallTimeU3Ec__Iterator0__ctor_m3073329610 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object BallCollider/<BigBallTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBigBallTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m525618834 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object BallCollider/<BigBallTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBigBallTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1833547302 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean BallCollider/<BigBallTime>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const uint32_t U3CBigBallTimeU3Ec__Iterator0_MoveNext_m2782394578_MetadataUsageId;
extern "C"  bool U3CBigBallTimeU3Ec__Iterator0_MoveNext_m2782394578 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBigBallTimeU3Ec__Iterator0_MoveNext_m2782394578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00c9;
		}
	}
	{
		goto IL_0105;
	}

IL_0021:
	{
		__this->set_U3CtU3E__0_0((8.0f));
		BallCollider_t3766478643 * L_2 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Transform_get_parent_m2236876972(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_localScale_m3886572677(L_6, /*hidden argument*/NULL);
		__this->set_U3CoriginalScaleU3E__1_1(L_7);
		Vector3_t4282066566  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m2926210380(&L_8, (4.0f), (4.0f), (4.0f), /*hidden argument*/NULL);
		__this->set_U3CbigballU3E__2_2(L_8);
		goto IL_00c9;
	}

IL_0070:
	{
		BallCollider_t3766478643 * L_9 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = Transform_get_parent_m2236876972(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = Component_get_transform_m4257140443(L_12, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = __this->get_U3CbigballU3E__2_2();
		NullCheck(L_13);
		Transform_set_localScale_m310756934(L_13, L_14, /*hidden argument*/NULL);
		float L_15 = __this->get_U3CtU3E__0_0();
		float L_16 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtU3E__0_0(((float)((float)L_15-(float)((float)((float)(1.0f)*(float)L_16)))));
		WaitForSeconds_t1615819279 * L_17 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_17, (0.0f), /*hidden argument*/NULL);
		__this->set_U24current_4(L_17);
		__this->set_U24PC_3(1);
		goto IL_0107;
	}

IL_00c9:
	{
		float L_18 = __this->get_U3CtU3E__0_0();
		if ((((float)L_18) > ((float)(0.0f))))
		{
			goto IL_0070;
		}
	}
	{
		BallCollider_t3766478643 * L_19 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = Component_get_gameObject_m1170635899(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = Transform_get_parent_m2236876972(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t1659122786 * L_23 = Component_get_transform_m4257140443(L_22, /*hidden argument*/NULL);
		Vector3_t4282066566  L_24 = __this->get_U3CoriginalScaleU3E__1_1();
		NullCheck(L_23);
		Transform_set_localScale_m310756934(L_23, L_24, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0105:
	{
		return (bool)0;
	}

IL_0107:
	{
		return (bool)1;
	}
	// Dead block : IL_0109: ldloc.1
}
// System.Void BallCollider/<BigBallTime>c__Iterator0::Dispose()
extern "C"  void U3CBigBallTimeU3Ec__Iterator0_Dispose_m2725505927 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void BallCollider/<BigBallTime>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBigBallTimeU3Ec__Iterator0_Reset_m719762551_MetadataUsageId;
extern "C"  void U3CBigBallTimeU3Ec__Iterator0_Reset_m719762551 (U3CBigBallTimeU3Ec__Iterator0_t1350130161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBigBallTimeU3Ec__Iterator0_Reset_m719762551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BallCollider/<HitColor>c__Iterator1::.ctor()
extern "C"  void U3CHitColorU3Ec__Iterator1__ctor_m3621972157 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object BallCollider/<HitColor>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CHitColorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m98097461 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object BallCollider/<HitColor>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CHitColorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m369485513 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean BallCollider/<HitColor>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern const uint32_t U3CHitColorU3Ec__Iterator1_MoveNext_m12638743_MetadataUsageId;
extern "C"  bool U3CHitColorU3Ec__Iterator1_MoveNext_m12638743 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CHitColorU3Ec__Iterator1_MoveNext_m12638743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_00a5;
		}
		if (L_1 == 2)
		{
			goto IL_00fb;
		}
	}
	{
		goto IL_0102;
	}

IL_0025:
	{
		__this->set_U3CtU3E__0_0(1);
		GameObject_t3674682005 * L_2 = __this->get_other_1();
		__this->set_U3CgU3E__1_2(L_2);
		goto IL_00a5;
	}

IL_003d:
	{
		int32_t L_3 = __this->get_U3CtU3E__0_0();
		__this->set_U3CtU3E__0_0(((int32_t)((int32_t)L_3-(int32_t)1)));
		GameObject_t3674682005 * L_4 = __this->get_U3CgU3E__1_2();
		NullCheck(L_4);
		Renderer_t3076687687 * L_5 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_5);
		Material_t3870600107 * L_6 = Renderer_get_material_m2720864603(L_5, /*hidden argument*/NULL);
		BallCollider_t3766478643 * L_7 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = Transform_get_parent_m2236876972(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Renderer_t3076687687 * L_12 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_11, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_12);
		Material_t3870600107 * L_13 = Renderer_get_material_m2720864603(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Color_t4194546905  L_14 = Material_get_color_m2268945527(L_13, /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_set_color_m3296857020(L_6, L_14, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_15 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_15, (0.2f), /*hidden argument*/NULL);
		__this->set_U24current_4(L_15);
		__this->set_U24PC_3(1);
		goto IL_0104;
	}

IL_00a5:
	{
		int32_t L_16 = __this->get_U3CtU3E__0_0();
		if ((((int32_t)L_16) > ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t3674682005 * L_17 = __this->get_U3CgU3E__1_2();
		NullCheck(L_17);
		Renderer_t3076687687 * L_18 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_17, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_18);
		Material_t3870600107 * L_19 = Renderer_get_material_m2720864603(L_18, /*hidden argument*/NULL);
		Color_t4194546905  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Color__ctor_m2252924356(&L_20, (0.359f), (0.109f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Material_set_color_m3296857020(L_19, L_20, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_21 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_21, (0.2f), /*hidden argument*/NULL);
		__this->set_U24current_4(L_21);
		__this->set_U24PC_3(2);
		goto IL_0104;
	}

IL_00fb:
	{
		__this->set_U24PC_3((-1));
	}

IL_0102:
	{
		return (bool)0;
	}

IL_0104:
	{
		return (bool)1;
	}
	// Dead block : IL_0106: ldloc.1
}
// System.Void BallCollider/<HitColor>c__Iterator1::Dispose()
extern "C"  void U3CHitColorU3Ec__Iterator1_Dispose_m1690016186 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void BallCollider/<HitColor>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CHitColorU3Ec__Iterator1_Reset_m1268405098_MetadataUsageId;
extern "C"  void U3CHitColorU3Ec__Iterator1_Reset_m1268405098 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CHitColorU3Ec__Iterator1_Reset_m1268405098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BallShadow::.ctor()
extern "C"  void BallShadow__ctor_m2965847900 (BallShadow_t3766914079 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BallShadow::Update()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BallShadow_Update_m3473833777_MetadataUsageId;
extern "C"  void BallShadow_Update_m3473833777 (BallShadow_t3766914079 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BallShadow_Update_m3473833777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Transform_get_parent_m2236876972(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		float L_4 = (&V_3)->get_x_1();
		float L_5 = __this->get_alturaSombra_4();
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Transform_get_parent_m2236876972(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		float L_10 = (&V_4)->get_z_3();
		Vector3__ctor_m2926210380((&V_0), L_4, L_5, L_10, /*hidden argument*/NULL);
		Transform_t1659122786 * L_11 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_12 = V_0;
		NullCheck(L_11);
		Transform_set_position_m3111394108(L_11, L_12, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_1), (90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Transform_t1659122786 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = V_1;
		Quaternion_t1553702882  L_15 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_rotation_m1525803229(L_13, L_15, /*hidden argument*/NULL);
		Transform_t1659122786 * L_16 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = Transform_get_position_m2211398607(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		float L_18 = (&V_5)->get_y_2();
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = Transform_get_parent_m2236876972(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = Component_get_transform_m4257140443(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_position_m2211398607(L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		float L_23 = (&V_6)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = powf(((float)((float)L_18-(float)L_23)), (2.0f));
		V_2 = ((float)((float)L_24/(float)(2.0f)));
		float L_25 = V_2;
		BallShadow_SetSizeAlpha_m2837256776(__this, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BallShadow::SetSizeAlpha(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var;
extern const uint32_t BallShadow_SetSizeAlpha_m2837256776_MetadataUsageId;
extern "C"  void BallShadow_SetSizeAlpha_m2837256776 (BallShadow_t3766914079 * __this, float ___dist0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BallShadow_SetSizeAlpha_m2837256776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Color32_t598853688  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		float L_0 = ___dist0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		float L_2 = __this->get_sombraBola_2();
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)L_1*(float)L_2))));
		float L_3 = V_0;
		float L_4 = __this->get_maxSombraBola_3();
		if ((!(((float)L_3) < ((float)L_4))))
		{
			goto IL_0027;
		}
	}
	{
		float L_5 = __this->get_maxSombraBola_3();
		V_0 = L_5;
	}

IL_0027:
	{
		Renderer_t3076687687 * L_6 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_6);
		Material_t3870600107 * L_7 = Renderer_get_material_m2720864603(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Color_t4194546905  L_8 = Material_get_color_m2268945527(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_2)->get_r_0();
		Renderer_t3076687687 * L_10 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_10);
		Material_t3870600107 * L_11 = Renderer_get_material_m2720864603(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Color_t4194546905  L_12 = Material_get_color_m2268945527(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = (&V_3)->get_g_1();
		Renderer_t3076687687 * L_14 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_14);
		Material_t3870600107 * L_15 = Renderer_get_material_m2720864603(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Color_t4194546905  L_16 = Material_get_color_m2268945527(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		float L_17 = (&V_4)->get_b_2();
		float L_18 = V_0;
		Color_t4194546905  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Color__ctor_m2252924356(&L_19, L_9, L_13, L_17, L_18, /*hidden argument*/NULL);
		Color32_t598853688  L_20 = Color32_op_Implicit_m3684884838(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		Renderer_t3076687687 * L_21 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_21);
		Material_t3870600107 * L_22 = Renderer_get_material_m2720864603(L_21, /*hidden argument*/NULL);
		Color32_t598853688  L_23 = V_1;
		Color_t4194546905  L_24 = Color32_op_Implicit_m358459656(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Material_set_color_m3296857020(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Blanco::.ctor()
extern "C"  void Blanco__ctor_m4004350776 (Blanco_t1992262851 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Blanco::Start()
extern Il2CppCodeGenString* _stringLiteral2490611;
extern const uint32_t Blanco_Start_m2951488568_MetadataUsageId;
extern "C"  void Blanco_Start_m2951488568 (Blanco_t1992262851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Blanco_Start_m2951488568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_diametroInicial_4();
		float L_1 = __this->get_diametroInicial_4();
		float L_2 = __this->get_diametroInicial_4();
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_sc_3(L_3);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_5 = __this->get_sc_3();
		NullCheck(L_4);
		Transform_set_localScale_m310756934(L_4, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral2490611, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Blanco::Plof()
extern Il2CppClass* U3CPlofU3Ec__Iterator2_t1800312444_il2cpp_TypeInfo_var;
extern const uint32_t Blanco_Plof_m2022465383_MetadataUsageId;
extern "C"  Il2CppObject * Blanco_Plof_m2022465383 (Blanco_t1992262851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Blanco_Plof_m2022465383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPlofU3Ec__Iterator2_t1800312444 * V_0 = NULL;
	{
		U3CPlofU3Ec__Iterator2_t1800312444 * L_0 = (U3CPlofU3Ec__Iterator2_t1800312444 *)il2cpp_codegen_object_new(U3CPlofU3Ec__Iterator2_t1800312444_il2cpp_TypeInfo_var);
		U3CPlofU3Ec__Iterator2__ctor_m3837868655(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlofU3Ec__Iterator2_t1800312444 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CPlofU3Ec__Iterator2_t1800312444 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Blanco/<Plof>c__Iterator2::.ctor()
extern "C"  void U3CPlofU3Ec__Iterator2__ctor_m3837868655 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Blanco/<Plof>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPlofU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4221340803 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Blanco/<Plof>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPlofU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2214983191 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Blanco/<Plof>c__Iterator2::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlofU3Ec__Iterator2_MoveNext_m1505796645_MetadataUsageId;
extern "C"  bool U3CPlofU3Ec__Iterator2_MoveNext_m1505796645 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPlofU3Ec__Iterator2_MoveNext_m1505796645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_00e1;
		}
		if (L_1 == 2)
		{
			goto IL_0112;
		}
	}
	{
		goto IL_0129;
	}

IL_0025:
	{
		Blanco_t1992262851 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		L_2->set_t_2((1.0f));
		goto IL_00e1;
	}

IL_003a:
	{
		Blanco_t1992262851 * L_3 = __this->get_U3CU3Ef__this_2();
		Blanco_t1992262851 * L_4 = L_3;
		NullCheck(L_4);
		float L_5 = L_4->get_t_2();
		float L_6 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_t_2(((float)((float)L_5-(float)((float)((float)(8.0f)*(float)L_6)))));
		Blanco_t1992262851 * L_7 = __this->get_U3CU3Ef__this_2();
		Blanco_t1992262851 * L_8 = L_7;
		NullCheck(L_8);
		float L_9 = L_8->get_diametroInicial_4();
		NullCheck(L_8);
		L_8->set_diametroInicial_4(((float)((float)L_9-(float)(0.1f))));
		Blanco_t1992262851 * L_10 = __this->get_U3CU3Ef__this_2();
		Blanco_t1992262851 * L_11 = L_10;
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = L_11->get_sc_3();
		Blanco_t1992262851 * L_13 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_13);
		float L_14 = L_13->get_diametroInicial_4();
		Blanco_t1992262851 * L_15 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_15);
		float L_16 = L_15->get_diametroInicial_4();
		Blanco_t1992262851 * L_17 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_17);
		float L_18 = L_17->get_diametroInicial_4();
		Vector3_t4282066566  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m2926210380(&L_19, L_14, L_16, L_18, /*hidden argument*/NULL);
		Vector3_t4282066566  L_20 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_12, L_19, /*hidden argument*/NULL);
		NullCheck(L_11);
		L_11->set_sc_3(L_20);
		Blanco_t1992262851 * L_21 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(L_21, /*hidden argument*/NULL);
		Blanco_t1992262851 * L_23 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = L_23->get_sc_3();
		NullCheck(L_22);
		Transform_set_localScale_m310756934(L_22, L_24, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_25 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_25, (0.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_25);
		__this->set_U24PC_0(1);
		goto IL_012b;
	}

IL_00e1:
	{
		Blanco_t1992262851 * L_26 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_26);
		float L_27 = L_26->get_t_2();
		if ((((float)L_27) > ((float)(0.0f))))
		{
			goto IL_003a;
		}
	}
	{
		WaitForSeconds_t1615819279 * L_28 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_28, (0.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_28);
		__this->set_U24PC_0(2);
		goto IL_012b;
	}

IL_0112:
	{
		Blanco_t1992262851 * L_29 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = Component_get_gameObject_m1170635899(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0129:
	{
		return (bool)0;
	}

IL_012b:
	{
		return (bool)1;
	}
	// Dead block : IL_012d: ldloc.1
}
// System.Void Blanco/<Plof>c__Iterator2::Dispose()
extern "C"  void U3CPlofU3Ec__Iterator2_Dispose_m3008120556 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Blanco/<Plof>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPlofU3Ec__Iterator2_Reset_m1484301596_MetadataUsageId;
extern "C"  void U3CPlofU3Ec__Iterator2_Reset_m1484301596 (U3CPlofU3Ec__Iterator2_t1800312444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPlofU3Ec__Iterator2_Reset_m1484301596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BouncingAnim::.ctor()
extern "C"  void BouncingAnim__ctor_m3517307461 (BouncingAnim_t2247160086 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BouncingAnim::Start()
extern "C"  void BouncingAnim_Start_m2464445253 (BouncingAnim_t2247160086 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BouncingAnim::Update()
extern "C"  void BouncingAnim_Update_m3389210984 (BouncingAnim_t2247160086 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_alturaSueloAnim_4();
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_y_2();
		BouncingAnim_SetBounce_m1471097182(__this, ((float)((float)L_0-(float)L_3)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void BouncingAnim::SetBounce(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t BouncingAnim_SetBounce_m1471097182_MetadataUsageId;
extern "C"  void BouncingAnim_SetBounce_m1471097182 (BouncingAnim_t2247160086 * __this, float ___dist0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BouncingAnim_SetBounce_m1471097182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = ___dist0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		float L_2 = __this->get_stretch_2();
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)L_1*(float)L_2))));
		float L_3 = V_0;
		float L_4 = __this->get_maxStretch_3();
		if ((!(((float)L_3) < ((float)L_4))))
		{
			goto IL_0027;
		}
	}
	{
		float L_5 = __this->get_maxStretch_3();
		V_0 = L_5;
	}

IL_0027:
	{
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_localScale_m3886572677(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_x_1();
		float L_9 = V_0;
		Transform_t1659122786 * L_10 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_localScale_m3886572677(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_z_3();
		Vector3__ctor_m2926210380((&V_1), L_8, ((float)((float)L_9/(float)(2.0f))), L_12, /*hidden argument*/NULL);
		Transform_t1659122786 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = V_1;
		NullCheck(L_13);
		Transform_set_localScale_m310756934(L_13, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Brazo::.ctor()
extern "C"  void Brazo__ctor_m1688309573 (Brazo_t64445670 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Brazo::Start()
extern "C"  void Brazo_Start_m635447365 (Brazo_t64445670 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Brazo::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t Brazo_Update_m2524851304_MetadataUsageId;
extern "C"  void Brazo_Update_m2524851304 (Brazo_t64445670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Brazo_Update_m2524851304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t4282066566  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t4282066566  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t4282066566  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_008f;
		}
	}
	{
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		V_4 = L_2;
		float L_3 = (&V_4)->get_x_1();
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_5 = L_5;
		float L_6 = (&V_5)->get_y_2();
		Transform_t1659122786 * L_7 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_6 = L_8;
		float L_9 = (&V_6)->get_z_3();
		Vector3__ctor_m2926210380((&V_0), L_3, L_6, L_9, /*hidden argument*/NULL);
		Vector3_t4282066566 * L_10 = (&V_0);
		float L_11 = L_10->get_x_1();
		float L_12 = __this->get_acceleration_2();
		float L_13 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_10->set_x_1(((float)((float)L_11-(float)((float)((float)L_12*(float)L_13)))));
		Transform_t1659122786 * L_14 = __this->get_brazo_3();
		NullCheck(L_14);
		Transform_t1659122786 * L_15 = Component_get_transform_m4257140443(L_14, /*hidden argument*/NULL);
		float L_16 = (&V_0)->get_x_1();
		Vector3_t4282066566  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2926210380(&L_17, L_16, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_Translate_m2849099360(L_15, L_17, /*hidden argument*/NULL);
	}

IL_008f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_18 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_011e;
		}
	}
	{
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		float L_21 = (&V_7)->get_x_1();
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = Transform_get_position_m2211398607(L_22, /*hidden argument*/NULL);
		V_8 = L_23;
		float L_24 = (&V_8)->get_y_2();
		Transform_t1659122786 * L_25 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t4282066566  L_26 = Transform_get_position_m2211398607(L_25, /*hidden argument*/NULL);
		V_9 = L_26;
		float L_27 = (&V_9)->get_z_3();
		Vector3__ctor_m2926210380((&V_1), L_21, L_24, L_27, /*hidden argument*/NULL);
		Vector3_t4282066566 * L_28 = (&V_1);
		float L_29 = L_28->get_x_1();
		float L_30 = __this->get_acceleration_2();
		float L_31 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_28->set_x_1(((float)((float)L_29+(float)((float)((float)L_30*(float)L_31)))));
		Transform_t1659122786 * L_32 = __this->get_brazo_3();
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = Component_get_transform_m4257140443(L_32, /*hidden argument*/NULL);
		float L_34 = (&V_1)->get_x_1();
		Vector3_t4282066566  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m2926210380(&L_35, L_34, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_Translate_m2849099360(L_33, L_35, /*hidden argument*/NULL);
	}

IL_011e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_36 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)119), /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01ae;
		}
	}
	{
		Transform_t1659122786 * L_37 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t4282066566  L_38 = Transform_get_position_m2211398607(L_37, /*hidden argument*/NULL);
		V_10 = L_38;
		float L_39 = (&V_10)->get_x_1();
		Transform_t1659122786 * L_40 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t4282066566  L_41 = Transform_get_position_m2211398607(L_40, /*hidden argument*/NULL);
		V_11 = L_41;
		float L_42 = (&V_11)->get_y_2();
		Transform_t1659122786 * L_43 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_t4282066566  L_44 = Transform_get_position_m2211398607(L_43, /*hidden argument*/NULL);
		V_12 = L_44;
		float L_45 = (&V_12)->get_z_3();
		Vector3__ctor_m2926210380((&V_2), L_39, L_42, L_45, /*hidden argument*/NULL);
		Vector3_t4282066566 * L_46 = (&V_2);
		float L_47 = L_46->get_z_3();
		float L_48 = __this->get_acceleration_2();
		float L_49 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_46->set_z_3(((float)((float)L_47+(float)((float)((float)L_48*(float)L_49)))));
		Transform_t1659122786 * L_50 = __this->get_brazo_3();
		NullCheck(L_50);
		Transform_t1659122786 * L_51 = Component_get_transform_m4257140443(L_50, /*hidden argument*/NULL);
		float L_52 = (&V_2)->get_z_3();
		Vector3_t4282066566  L_53;
		memset(&L_53, 0, sizeof(L_53));
		Vector3__ctor_m2926210380(&L_53, (0.0f), ((-L_52)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_Translate_m2849099360(L_51, L_53, /*hidden argument*/NULL);
	}

IL_01ae:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_54 = Input_GetKey_m1349175653(NULL /*static, unused*/, ((int32_t)115), /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_023d;
		}
	}
	{
		Transform_t1659122786 * L_55 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Vector3_t4282066566  L_56 = Transform_get_position_m2211398607(L_55, /*hidden argument*/NULL);
		V_13 = L_56;
		float L_57 = (&V_13)->get_x_1();
		Transform_t1659122786 * L_58 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		Vector3_t4282066566  L_59 = Transform_get_position_m2211398607(L_58, /*hidden argument*/NULL);
		V_14 = L_59;
		float L_60 = (&V_14)->get_y_2();
		Transform_t1659122786 * L_61 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_61);
		Vector3_t4282066566  L_62 = Transform_get_position_m2211398607(L_61, /*hidden argument*/NULL);
		V_15 = L_62;
		float L_63 = (&V_15)->get_z_3();
		Vector3__ctor_m2926210380((&V_3), L_57, L_60, L_63, /*hidden argument*/NULL);
		Vector3_t4282066566 * L_64 = (&V_3);
		float L_65 = L_64->get_z_3();
		float L_66 = __this->get_acceleration_2();
		float L_67 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_64->set_z_3(((float)((float)L_65+(float)((float)((float)L_66*(float)L_67)))));
		Transform_t1659122786 * L_68 = __this->get_brazo_3();
		NullCheck(L_68);
		Transform_t1659122786 * L_69 = Component_get_transform_m4257140443(L_68, /*hidden argument*/NULL);
		float L_70 = (&V_3)->get_z_3();
		Vector3_t4282066566  L_71;
		memset(&L_71, 0, sizeof(L_71));
		Vector3__ctor_m2926210380(&L_71, (0.0f), L_70, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_69);
		Transform_Translate_m2849099360(L_69, L_71, /*hidden argument*/NULL);
	}

IL_023d:
	{
		return;
	}
}
// System.Void Brick::.ctor()
extern "C"  void Brick__ctor_m1887233642 (Brick_t64452641 * __this, const MethodInfo* method)
{
	{
		__this->set_nToques_3(1);
		__this->set_isReadyToDie_4((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Brick::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral66203856;
extern Il2CppCodeGenString* _stringLiteral980556366;
extern Il2CppCodeGenString* _stringLiteral1548947199;
extern const uint32_t Brick_Start_m834371434_MetadataUsageId;
extern "C"  void Brick_Start_m834371434 (Brick_t64452641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Brick_Start_m834371434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_tipo_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, _stringLiteral66203856, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		Renderer_t3076687687 * L_2 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		Material_t3870600107 * L_3 = __this->get_dobleMaterial_5();
		NullCheck(L_2);
		Renderer_set_material_m1012580896(L_2, L_3, /*hidden argument*/NULL);
		__this->set_nToques_3(2);
		goto IL_00ad;
	}

IL_0032:
	{
		String_t* L_4 = __this->get_tipo_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, _stringLiteral980556366, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0072;
		}
	}
	{
		Renderer_t3076687687 * L_6 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		Material_t3870600107 * L_7 = __this->get_dobleMaterial_5();
		NullCheck(L_6);
		Renderer_set_material_m1012580896(L_6, L_7, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_8 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_8);
		Material_t3870600107 * L_9 = Renderer_get_material_m2720864603(L_8, /*hidden argument*/NULL);
		Color_t4194546905  L_10 = Color_get_cyan_m3697299923(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_set_color_m3296857020(L_9, L_10, /*hidden argument*/NULL);
		goto IL_00ad;
	}

IL_0072:
	{
		String_t* L_11 = __this->get_tipo_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_11, _stringLiteral1548947199, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00ad;
		}
	}
	{
		Renderer_t3076687687 * L_13 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		Material_t3870600107 * L_14 = __this->get_dobleMaterial_5();
		NullCheck(L_13);
		Renderer_set_material_m1012580896(L_13, L_14, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_15 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_15);
		Material_t3870600107 * L_16 = Renderer_get_material_m2720864603(L_15, /*hidden argument*/NULL);
		Color_t4194546905  L_17 = Color_get_green_m2005284533(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Material_set_color_m3296857020(L_16, L_17, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		return;
	}
}
// System.Void Brick::kill(UnityEngine.Vector3,UnityEngine.GameObject)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var;
extern const uint32_t Brick_kill_m2419377885_MetadataUsageId;
extern "C"  void Brick_kill_m2419377885 (Brick_t64452641 * __this, Vector3_t4282066566  ___pos0, GameObject_t3674682005 * ___Controller1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Brick_kill_m2419377885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_Blanco_7();
		Vector3_t4282066566  L_1 = ___pos0;
		Quaternion_t1553702882  L_2 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Instantiate_m2255090103(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = ___Controller1;
		NullCheck(L_3);
		Controller_t2630893500 * L_4 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_3, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Controller_RemoveBrick_m1458410322(L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_tocaBrick_9();
		__this->set_tocaBrick_9(((int32_t)((int32_t)L_6+(int32_t)1)));
		GameObject_t3674682005 * L_7 = ___Controller1;
		NullCheck(L_7);
		Controller_t2630893500 * L_8 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_7, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		Controller_t2630893500 * L_9 = L_8;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_NBricks_39();
		NullCheck(L_9);
		L_9->set_NBricks_39(((int32_t)((int32_t)L_10+(int32_t)1)));
		GameObject_t3674682005 * L_11 = ___Controller1;
		NullCheck(L_11);
		Controller_t2630893500 * L_12 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_11, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		NullCheck(L_12);
		Controller_UpdateHUD_m1160851307(L_12, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_13 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Brick::RestaToque()
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var;
extern const uint32_t Brick_RestaToque_m2346085363_MetadataUsageId;
extern "C"  void Brick_RestaToque_m2346085363 (Brick_t64452641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Brick_RestaToque_m2346085363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_nToques_3();
		__this->set_nToques_3(((int32_t)((int32_t)L_0-(int32_t)1)));
		Renderer_t3076687687 * L_1 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		Material_t3870600107 * L_2 = __this->get_dobleMaterialTocado_6();
		NullCheck(L_1);
		Renderer_set_material_m1012580896(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Brick::checkToques(UnityEngine.Vector3,UnityEngine.GameObject)
extern "C"  void Brick_checkToques_m2912556642 (Brick_t64452641 * __this, Vector3_t4282066566  ___pos0, GameObject_t3674682005 * ___Controller1, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_nToques_3();
		if ((((int32_t)L_0) >= ((int32_t)2)))
		{
			goto IL_0013;
		}
	}
	{
		__this->set_isReadyToDie_4((bool)1);
	}

IL_0013:
	{
		int32_t L_1 = __this->get_nToques_3();
		if ((((int32_t)L_1) >= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		Vector3_t4282066566  L_2 = ___pos0;
		GameObject_t3674682005 * L_3 = ___Controller1;
		Brick_kill_m2419377885(__this, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void ColliderPlayer::.ctor()
extern "C"  void ColliderPlayer__ctor_m2137026598 (ColliderPlayer_t3441697813 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColliderPlayer::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void ColliderPlayer_setCameras_m1275307740 (ColliderPlayer_t3441697813 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___Perspe0;
		__this->set_Perspec_8(L_0);
		Camera_t2727095145 * L_1 = ___Ortho1;
		__this->set_Ortho_9(L_1);
		Camera_t2727095145 * L_2 = ___Iso2;
		__this->set_Iso_10(L_2);
		Camera_t2727095145 * L_3 = ___Interface3;
		__this->set_Interface_11(L_3);
		return;
	}
}
// System.Void ColliderPlayer::Start()
extern "C"  void ColliderPlayer_Start_m1084164390 (ColliderPlayer_t3441697813 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Transform_get_parent_m2236876972(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_localScale_m3886572677(L_2, /*hidden argument*/NULL);
		__this->set_sc_7(L_3);
		return;
	}
}
// System.Void ColliderPlayer::PlayAudio(System.Single)
extern const MethodInfo* GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var;
extern const uint32_t ColliderPlayer_PlayAudio_m1966464805_MetadataUsageId;
extern "C"  void ColliderPlayer_PlayAudio_m1966464805 (ColliderPlayer_t3441697813 * __this, float ___pitch0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColliderPlayer_PlayAudio_m1966464805_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_Controller_12();
		NullCheck(L_0);
		Controller_t2630893500 * L_1 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_0, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		float L_2 = ___pitch0;
		NullCheck(L_1);
		Controller_PlayAudio_m1101257740(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ColliderPlayer::OnCollisionEnter(UnityEngine.Collision)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoxCollider_t2538127765_m1593759217_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisShake_t79847142_m3483210259_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2062879;
extern const uint32_t ColliderPlayer_OnCollisionEnter_m3549370868_MetadataUsageId;
extern "C"  void ColliderPlayer_OnCollisionEnter_m3549370868 (ColliderPlayer_t3441697813 * __this, Collision_t2494107688 * ___col0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColliderPlayer_OnCollisionEnter_m3549370868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ContactPoint_t243083348  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ContactPointU5BU5D_t715040733* V_2 = NULL;
	int32_t V_3 = 0;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	GameObject_t3674682005 * V_5 = NULL;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Bounds_t2711641849  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		Collision_t2494107688 * L_0 = ___col0;
		NullCheck(L_0);
		Collider_t2939674232 * L_1 = Collision_get_collider_m1325344374(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Component_get_tag_m217485006(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, _stringLiteral2062879, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0162;
		}
	}
	{
		Collision_t2494107688 * L_4 = ___col0;
		NullCheck(L_4);
		Collider_t2939674232 * L_5 = Collision_get_collider_m1325344374(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		BallCollider_t3766478643 * L_7 = GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182(L_6, /*hidden argument*/GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182_MethodInfo_var);
		NullCheck(L_7);
		bool L_8 = L_7->get_isSecondBounce_12();
		if (!L_8)
		{
			goto IL_0162;
		}
	}
	{
		Vector3_t4282066566  L_9 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_9;
		Collision_t2494107688 * L_10 = ___col0;
		NullCheck(L_10);
		ContactPointU5BU5D_t715040733* L_11 = Collision_get_contacts_m658316947(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		V_3 = 0;
		goto IL_0159;
	}

IL_0048:
	{
		ContactPointU5BU5D_t715040733* L_12 = V_2;
		int32_t L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		V_1 = (*(ContactPoint_t243083348 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13))));
		Vector3_t4282066566  L_14 = ContactPoint_get_point_m1387782344((&V_1), /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = ContactPoint_get_normal_m1137164497((&V_1), /*hidden argument*/NULL);
		Color_t4194546905  L_16 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_DrawRay_m123146114(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		Vector3_t4282066566  L_17 = ContactPoint_get_point_m1387782344((&V_1), /*hidden argument*/NULL);
		V_0 = L_17;
		Transform_t1659122786 * L_18 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4282066566  L_19 = Transform_get_localScale_m3886572677(L_18, /*hidden argument*/NULL);
		BoxCollider_t2538127765 * L_20 = Component_GetComponent_TisBoxCollider_t2538127765_m1593759217(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider_t2538127765_m1593759217_MethodInfo_var);
		NullCheck(L_20);
		Bounds_t2711641849  L_21 = Collider_get_bounds_m1050008332(L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		Vector3_t4282066566  L_22 = Bounds_get_size_m3666348432((&V_7), /*hidden argument*/NULL);
		Vector3_t4282066566  L_23 = Vector3_Scale_m3746402337(NULL /*static, unused*/, L_19, L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Collision_t2494107688 * L_24 = ___col0;
		NullCheck(L_24);
		Collider_t2939674232 * L_25 = Collision_get_collider_m1325344374(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		GameObject_t3674682005 * L_26 = Component_get_gameObject_m1170635899(L_25, /*hidden argument*/NULL);
		V_5 = L_26;
		GameObject_t3674682005 * L_27 = V_5;
		NullCheck(L_27);
		MoveBall_t4254648720 * L_28 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_27, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		NullCheck(L_28);
		Vector3_t4282066566 * L_29 = L_28->get_address_of_velocidad_2();
		float L_30 = L_29->get_x_1();
		GameObject_t3674682005 * L_31 = V_5;
		NullCheck(L_31);
		MoveBall_t4254648720 * L_32 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_31, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		NullCheck(L_32);
		Vector3_t4282066566 * L_33 = L_32->get_address_of_velocidad_2();
		float L_34 = L_33->get_y_2();
		GameObject_t3674682005 * L_35 = V_5;
		NullCheck(L_35);
		MoveBall_t4254648720 * L_36 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_35, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		NullCheck(L_36);
		Vector3_t4282066566 * L_37 = L_36->get_address_of_velocidad_2();
		float L_38 = L_37->get_z_3();
		Vector3__ctor_m2926210380((&V_6), L_30, L_34, L_38, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_39 = V_5;
		NullCheck(L_39);
		MoveBall_t4254648720 * L_40 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_39, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		Vector3_t4282066566  L_41 = V_0;
		Vector3_t4282066566  L_42 = V_4;
		Vector3_t4282066566  L_43 = ColliderPlayer_Lado_m2022745300(__this, L_41, L_42, /*hidden argument*/NULL);
		V_8 = L_43;
		float L_44 = (&V_8)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_45 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_44, (-20.0f), (20.0f), /*hidden argument*/NULL);
		float L_46 = (&V_6)->get_y_2();
		float L_47 = (&V_6)->get_z_3();
		Vector3_t4282066566  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Vector3__ctor_m2926210380(&L_48, L_45, L_46, ((-L_47)), /*hidden argument*/NULL);
		NullCheck(L_40);
		L_40->set_velocidad_2(L_48);
		GameObject_t3674682005 * L_49 = V_5;
		NullCheck(L_49);
		BallCollider_t3766478643 * L_50 = GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182(L_49, /*hidden argument*/GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182_MethodInfo_var);
		NullCheck(L_50);
		L_50->set_isSecondBounce_12((bool)0);
		Camera_t2727095145 * L_51 = __this->get_Iso_10();
		NullCheck(L_51);
		bool L_52 = Behaviour_get_isActiveAndEnabled_m210167461(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0155;
		}
	}
	{
		Camera_t2727095145 * L_53 = __this->get_Iso_10();
		NullCheck(L_53);
		Shake_t79847142 * L_54 = Component_GetComponent_TisShake_t79847142_m3483210259(L_53, /*hidden argument*/Component_GetComponent_TisShake_t79847142_m3483210259_MethodInfo_var);
		NullCheck(L_54);
		Shake_ShakeThatCamera_m1815182614(L_54, (0.05f), /*hidden argument*/NULL);
	}

IL_0155:
	{
		int32_t L_55 = V_3;
		V_3 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_0159:
	{
		int32_t L_56 = V_3;
		ContactPointU5BU5D_t715040733* L_57 = V_2;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_0048;
		}
	}

IL_0162:
	{
		return;
	}
}
// UnityEngine.Vector3 ColliderPlayer::Lado(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  ColliderPlayer_Lado_m2022745300 (ColliderPlayer_t3441697813 * __this, Vector3_t4282066566  ___cPoint0, Vector3_t4282066566  ___cScale1, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = ___cPoint0;
		NullCheck(L_0);
		Vector3_t4282066566  L_2 = Transform_InverseTransformPoint_m1626812000(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.IEnumerator ColliderPlayer::flan()
extern Il2CppClass* U3CflanU3Ec__Iterator3_t3689796459_il2cpp_TypeInfo_var;
extern const uint32_t ColliderPlayer_flan_m1165343545_MetadataUsageId;
extern "C"  Il2CppObject * ColliderPlayer_flan_m1165343545 (ColliderPlayer_t3441697813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColliderPlayer_flan_m1165343545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CflanU3Ec__Iterator3_t3689796459 * V_0 = NULL;
	{
		U3CflanU3Ec__Iterator3_t3689796459 * L_0 = (U3CflanU3Ec__Iterator3_t3689796459 *)il2cpp_codegen_object_new(U3CflanU3Ec__Iterator3_t3689796459_il2cpp_TypeInfo_var);
		U3CflanU3Ec__Iterator3__ctor_m2224992864(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CflanU3Ec__Iterator3_t3689796459 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_7(__this);
		U3CflanU3Ec__Iterator3_t3689796459 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ColliderPlayer/<flan>c__Iterator3::.ctor()
extern "C"  void U3CflanU3Ec__Iterator3__ctor_m2224992864 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ColliderPlayer/<flan>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CflanU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1259654706 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object ColliderPlayer/<flan>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CflanU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3091035590 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean ColliderPlayer/<flan>c__Iterator3::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const uint32_t U3CflanU3Ec__Iterator3_MoveNext_m4199599252_MetadataUsageId;
extern "C"  bool U3CflanU3Ec__Iterator3_MoveNext_m4199599252 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CflanU3Ec__Iterator3_MoveNext_m4199599252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0131;
		}
	}
	{
		goto IL_0168;
	}

IL_0021:
	{
		__this->set_U3CtU3E__0_0((0.1f));
		ColliderPlayer_t3441697813 * L_2 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Transform_get_parent_m2236876972(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_localScale_m3886572677(L_5, /*hidden argument*/NULL);
		__this->set_U3CscU3E__1_1(L_6);
		goto IL_0131;
	}

IL_0051:
	{
		float L_7 = __this->get_U3CtU3E__0_0();
		float L_8 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtU3E__0_0(((float)((float)L_7-(float)((float)((float)(0.1f)*(float)L_8)))));
		ColliderPlayer_t3441697813 * L_9 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = Component_get_transform_m4257140443(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = Transform_get_parent_m2236876972(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = Component_get_transform_m4257140443(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_localScale_m3886572677(L_12, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, L_13, (2.0f), /*hidden argument*/NULL);
		__this->set_U3CtargetScaleU3E__2_2(L_14);
		Vector3_t4282066566 * L_15 = __this->get_address_of_U3CscU3E__1_1();
		float L_16 = L_15->get_y_2();
		__this->set_U3CscyU3E__3_3(L_16);
		float L_17 = __this->get_U3CscyU3E__3_3();
		Vector3_t4282066566 * L_18 = __this->get_address_of_U3CtargetScaleU3E__2_2();
		float L_19 = L_18->get_y_2();
		float L_20 = __this->get_U3CscyU3E__3_3();
		__this->set_U3CscyU3E__3_3(((float)((float)L_17+(float)((float)((float)((float)((float)L_19-(float)L_20))*(float)(0.1f))))));
		Vector3_t4282066566 * L_21 = __this->get_address_of_U3CscU3E__1_1();
		float L_22 = L_21->get_x_1();
		Vector3_t4282066566 * L_23 = __this->get_address_of_U3CtargetScaleU3E__2_2();
		float L_24 = L_23->get_y_2();
		Vector3_t4282066566 * L_25 = __this->get_address_of_U3CscU3E__1_1();
		float L_26 = L_25->get_z_3();
		Vector3_t4282066566  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m2926210380(&L_27, L_22, L_24, L_26, /*hidden argument*/NULL);
		__this->set_U3ClscU3E__4_4(L_27);
		ColliderPlayer_t3441697813 * L_28 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_28);
		Transform_t1659122786 * L_29 = Component_get_transform_m4257140443(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = Transform_get_parent_m2236876972(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = Component_get_transform_m4257140443(L_30, /*hidden argument*/NULL);
		Vector3_t4282066566  L_32 = __this->get_U3ClscU3E__4_4();
		NullCheck(L_31);
		Transform_set_localScale_m310756934(L_31, L_32, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_33 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_33, (0.0f), /*hidden argument*/NULL);
		__this->set_U24current_6(L_33);
		__this->set_U24PC_5(1);
		goto IL_016a;
	}

IL_0131:
	{
		float L_34 = __this->get_U3CtU3E__0_0();
		if ((((float)L_34) >= ((float)(0.0f))))
		{
			goto IL_0051;
		}
	}
	{
		ColliderPlayer_t3441697813 * L_35 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = Component_get_transform_m4257140443(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t1659122786 * L_37 = Transform_get_parent_m2236876972(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t1659122786 * L_38 = Component_get_transform_m4257140443(L_37, /*hidden argument*/NULL);
		Vector3_t4282066566  L_39 = __this->get_U3CscU3E__1_1();
		NullCheck(L_38);
		Transform_set_localScale_m310756934(L_38, L_39, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_0168:
	{
		return (bool)0;
	}

IL_016a:
	{
		return (bool)1;
	}
	// Dead block : IL_016c: ldloc.1
}
// System.Void ColliderPlayer/<flan>c__Iterator3::Dispose()
extern "C"  void U3CflanU3Ec__Iterator3_Dispose_m3517679261 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void ColliderPlayer/<flan>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CflanU3Ec__Iterator3_Reset_m4166393101_MetadataUsageId;
extern "C"  void U3CflanU3Ec__Iterator3_Reset_m4166393101 (U3CflanU3Ec__Iterator3_t3689796459 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CflanU3Ec__Iterator3_Reset_m4166393101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m3787541599 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	{
		__this->set_isEnterButtonReady_37((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::isTrampa()
extern Il2CppCodeGenString* _stringLiteral2390693;
extern const uint32_t Controller_isTrampa_m2013472266_MetadataUsageId;
extern "C"  void Controller_isTrampa_m2013472266 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_isTrampa_m2013472266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral2390693, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::Start()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral804549319;
extern const uint32_t Controller_Start_m2734679391_MetadataUsageId;
extern "C"  void Controller_Start_m2734679391 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_Start_m2734679391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_gameState_47(0);
		Controller_HideUIPanelGameOver_m106393041(__this, /*hidden argument*/NULL);
		Controller_HideUIPanelJuego_m599578937(__this, /*hidden argument*/NULL);
		Controller_ShowUIPanelMenu_m2881324079(__this, /*hidden argument*/NULL);
		Controller_HideUIPanelTransicion_m3230741105(__this, /*hidden argument*/NULL);
		Controller_getData_m4285674173(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral804549319, /*hidden argument*/NULL);
		__this->set_NRoom_38(0);
		__this->set_NBricks_39(0);
		Controller_UpdateHUD_m1160851307(__this, /*hidden argument*/NULL);
		Camera_t2727095145 * L_0 = __this->get_Perspec_30();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		Camera_t2727095145 * L_2 = __this->get_Ortho_31();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		Camera_t2727095145 * L_4 = __this->get_Iso_32();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::StartGame()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* RoomGenerator_t3606720536_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2212941408;
extern Il2CppCodeGenString* _stringLiteral3421258729;
extern Il2CppCodeGenString* _stringLiteral1024831109;
extern const uint32_t Controller_StartGame_m3824060913_MetadataUsageId;
extern "C"  void Controller_StartGame_m3824060913 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_StartGame_m3824060913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_gameState_47(1);
		Controller_RemoveAll_m1435005626(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral2212941408, /*hidden argument*/NULL);
		Controller_CreatePlayer_m799294658(__this, /*hidden argument*/NULL);
		Controller_CreateBall_m3943245920(__this, /*hidden argument*/NULL);
		Controller_CreateRoom_m119364668(__this, /*hidden argument*/NULL);
		Controller_HideUIPanelMenu_m1476205066(__this, /*hidden argument*/NULL);
		Controller_HideUIPanelTransicion_m3230741105(__this, /*hidden argument*/NULL);
		Controller_ShowUIPanelJuego_m1208595380(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3421258729);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3421258729);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(RoomGenerator_t3606720536_il2cpp_TypeInfo_var);
		int32_t L_2 = ((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->get_fase_25();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1024831109);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1024831109);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		int32_t L_7 = __this->get_NRoom_38();
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3016520001(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Controller::CamAnim()
extern Il2CppClass* U3CCamAnimU3Ec__Iterator4_t3416449610_il2cpp_TypeInfo_var;
extern const uint32_t Controller_CamAnim_m3900757941_MetadataUsageId;
extern "C"  Il2CppObject * Controller_CamAnim_m3900757941 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_CamAnim_m3900757941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCamAnimU3Ec__Iterator4_t3416449610 * V_0 = NULL;
	{
		U3CCamAnimU3Ec__Iterator4_t3416449610 * L_0 = (U3CCamAnimU3Ec__Iterator4_t3416449610 *)il2cpp_codegen_object_new(U3CCamAnimU3Ec__Iterator4_t3416449610_il2cpp_TypeInfo_var);
		U3CCamAnimU3Ec__Iterator4__ctor_m3640395665(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCamAnimU3Ec__Iterator4_t3416449610 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CCamAnimU3Ec__Iterator4_t3416449610 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Controller::RemoveAll()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Controller_RemoveAll_m1435005626_MetadataUsageId;
extern "C"  void Controller_RemoveAll_m1435005626 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_RemoveAll_m1435005626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_Room_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_player_28();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_ball_27();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_FrontWall_6();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::CreateRoom()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var;
extern const uint32_t Controller_CreateRoom_m119364668_MetadataUsageId;
extern "C"  void Controller_CreateRoom_m119364668 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_CreateRoom_m119364668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalRooms_41();
		int32_t L_1 = __this->get_NRoom_38();
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_NTotalRooms_41(((int32_t)((int32_t)L_0+(int32_t)L_1)));
		GameObject_t3674682005 * L_2 = __this->get_RoomGen_5();
		NullCheck(L_2);
		RoomGenerator_t3606720536 * L_3 = GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481(L_2, /*hidden argument*/GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var);
		Camera_t2727095145 * L_4 = __this->get_Perspec_30();
		Camera_t2727095145 * L_5 = __this->get_Ortho_31();
		Camera_t2727095145 * L_6 = __this->get_Iso_32();
		Camera_t2727095145 * L_7 = __this->get_Interface_33();
		NullCheck(L_3);
		RoomGenerator_setCameras_m2717393161(L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_8 = __this->get_RoomGen_5();
		NullCheck(L_8);
		RoomGenerator_t3606720536 * L_9 = GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481(L_8, /*hidden argument*/GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var);
		NullCheck(L_9);
		RoomGenerator_GenerateRoom_m2374567553(L_9, /*hidden argument*/NULL);
		Controller_setData_m4071057097(NULL /*static, unused*/, /*hidden argument*/NULL);
		Controller_UpdateHUD_m1160851307(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::CreatePlayer()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisColliderPlayer_t3441697813_m249032460_MethodInfo_var;
extern const uint32_t Controller_CreatePlayer_m799294658_MetadataUsageId;
extern "C"  void Controller_CreatePlayer_m799294658 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_CreatePlayer_m799294658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_PlayerObj_3();
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_4 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		__this->set_player_28(((GameObject_t3674682005 *)IsInstSealed(L_4, GameObject_t3674682005_il2cpp_TypeInfo_var)));
		GameObject_t3674682005 * L_5 = __this->get_RoomGen_5();
		NullCheck(L_5);
		RoomGenerator_t3606720536 * L_6 = GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481(L_5, /*hidden argument*/GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var);
		GameObject_t3674682005 * L_7 = __this->get_player_28();
		NullCheck(L_6);
		L_6->set_Player_9(L_7);
		GameObject_t3674682005 * L_8 = __this->get_player_28();
		NullCheck(L_8);
		Move_t2404337 * L_9 = GameObject_GetComponent_TisMove_t2404337_m307893836(L_8, /*hidden argument*/GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var);
		Camera_t2727095145 * L_10 = __this->get_Perspec_30();
		Camera_t2727095145 * L_11 = __this->get_Ortho_31();
		Camera_t2727095145 * L_12 = __this->get_Iso_32();
		Camera_t2727095145 * L_13 = __this->get_Interface_33();
		NullCheck(L_9);
		Move_setCameras_m1043826816(L_9, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_14 = __this->get_player_28();
		NullCheck(L_14);
		ColliderPlayer_t3441697813 * L_15 = GameObject_GetComponentInChildren_TisColliderPlayer_t3441697813_m249032460(L_14, /*hidden argument*/GameObject_GetComponentInChildren_TisColliderPlayer_t3441697813_m249032460_MethodInfo_var);
		Camera_t2727095145 * L_16 = __this->get_Perspec_30();
		Camera_t2727095145 * L_17 = __this->get_Ortho_31();
		Camera_t2727095145 * L_18 = __this->get_Iso_32();
		Camera_t2727095145 * L_19 = __this->get_Interface_33();
		NullCheck(L_15);
		ColliderPlayer_setCameras_m1275307740(L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_20 = __this->get_player_28();
		NullCheck(L_20);
		ColliderPlayer_t3441697813 * L_21 = GameObject_GetComponentInChildren_TisColliderPlayer_t3441697813_m249032460(L_20, /*hidden argument*/GameObject_GetComponentInChildren_TisColliderPlayer_t3441697813_m249032460_MethodInfo_var);
		GameObject_t3674682005 * L_22 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->set_Controller_12(L_22);
		return;
	}
}
// System.Void Controller::HideUIPanelJuego()
extern "C"  void Controller_HideUIPanelJuego_m599578937 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m1517109030((&V_0), (((float)((float)L_0))), (((float)((float)((int32_t)((int32_t)L_1+(int32_t)((int32_t)8000)))))), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_UIPanelJuego_10();
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = V_0;
		Vector3_t4282066566  L_5 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m3111394108(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::ShowUIPanelJuego()
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const uint32_t Controller_ShowUIPanelJuego_m1208595380_MetadataUsageId;
extern "C"  void Controller_ShowUIPanelJuego_m1208595380 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_ShowUIPanelJuego_m1208595380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2__ctor_m1517109030((&V_0), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = __this->get_UIPanelJuego_10();
		NullCheck(L_0);
		RectTransform_t972643934 * L_1 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_2 = V_0;
		NullCheck(L_1);
		RectTransform_set_anchoredPosition_m1498949997(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::HideUIPanelGameOver()
extern "C"  void Controller_HideUIPanelGameOver_m106393041 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m1517109030((&V_0), (((float)((float)L_0))), (((float)((float)((int32_t)((int32_t)L_1+(int32_t)((int32_t)2000)))))), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_UIPanelGOver_9();
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = V_0;
		Vector3_t4282066566  L_5 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m3111394108(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::ShowUIPanelGameOver()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral107540;
extern const uint32_t Controller_ShowUIPanelGameOver_m1373388150_MetadataUsageId;
extern "C"  void Controller_ShowUIPanelGameOver_m1373388150 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_ShowUIPanelGameOver_m1373388150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Vector2__ctor_m1517109030((&V_0), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = __this->get_UIPanelGOver_9();
		NullCheck(L_0);
		RectTransform_t972643934 * L_1 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_2 = V_0;
		NullCheck(L_1);
		RectTransform_set_anchoredPosition_m1498949997(L_1, L_2, /*hidden argument*/NULL);
		int32_t L_3 = Random_Range_m75452833(NULL /*static, unused*/, 1, 3, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral107540, L_6, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_NBricks_39();
		int32_t L_9 = V_1;
		if (((int32_t)((int32_t)L_8%(int32_t)L_9)))
		{
			goto IL_0052;
		}
	}
	{
		Controller_ShowMinigameButton_m1217470909(__this, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void Controller::ShowMinigameButton()
extern "C"  void Controller_ShowMinigameButton_m1217470909 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_MinigameButton_25();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::HideUIPanelMenu()
extern "C"  void Controller_HideUIPanelMenu_m1476205066 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m1517109030((&V_0), (((float)((float)L_0))), (((float)((float)((int32_t)((int32_t)L_1+(int32_t)((int32_t)2000)))))), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_UIPanelMenu_8();
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = V_0;
		Vector3_t4282066566  L_5 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m3111394108(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::ShowUIPanelMenu()
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const uint32_t Controller_ShowUIPanelMenu_m2881324079_MetadataUsageId;
extern "C"  void Controller_ShowUIPanelMenu_m2881324079 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_ShowUIPanelMenu_m2881324079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2__ctor_m1517109030((&V_0), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = __this->get_UIPanelMenu_8();
		NullCheck(L_0);
		RectTransform_t972643934 * L_1 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_2 = V_0;
		NullCheck(L_1);
		RectTransform_set_anchoredPosition_m1498949997(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::HideUIPanelTransicion()
extern "C"  void Controller_HideUIPanelTransicion_m3230741105 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m1517109030((&V_0), (((float)((float)L_0))), (((float)((float)((int32_t)((int32_t)L_1+(int32_t)((int32_t)2000)))))), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_UIPanelTransicion_11();
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = V_0;
		Vector3_t4282066566  L_5 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m3111394108(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::ShowUIPanelTransicion()
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern const uint32_t Controller_ShowUIPanelTransicion_m1042328790_MetadataUsageId;
extern "C"  void Controller_ShowUIPanelTransicion_m1042328790 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_ShowUIPanelTransicion_m1042328790_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2__ctor_m1517109030((&V_0), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = __this->get_UIPanelTransicion_11();
		NullCheck(L_0);
		RectTransform_t972643934 * L_1 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		Vector2_t4282066565  L_2 = V_0;
		NullCheck(L_1);
		RectTransform_set_anchoredPosition_m1498949997(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::CreateBall()
extern Il2CppClass* List_1_t747900261_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2397334390_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBallCollider_t3766478643_m174257010_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2091695206_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2062879;
extern const uint32_t Controller_CreateBall_m3943245920_MetadataUsageId;
extern "C"  void Controller_CreateBall_m3943245920 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_CreateBall_m3943245920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t747900261 * L_0 = (List_1_t747900261 *)il2cpp_codegen_object_new(List_1_t747900261_il2cpp_TypeInfo_var);
		List_1__ctor_m2397334390(L_0, /*hidden argument*/List_1__ctor_m2397334390_MethodInfo_var);
		__this->set_Multibolas_45(L_0);
		GameObject_t3674682005 * L_1 = __this->get_BallObj_4();
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_4 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_5 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		__this->set_ball_27(((GameObject_t3674682005 *)IsInstSealed(L_5, GameObject_t3674682005_il2cpp_TypeInfo_var)));
		GameObject_t3674682005 * L_6 = __this->get_ball_27();
		NullCheck(L_6);
		Object_set_name_m1123518500(L_6, _stringLiteral2062879, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_7 = __this->get_ball_27();
		NullCheck(L_7);
		GameObject_set_tag_m859036203(L_7, _stringLiteral2062879, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_8 = __this->get_RoomGen_5();
		NullCheck(L_8);
		RoomGenerator_t3606720536 * L_9 = GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481(L_8, /*hidden argument*/GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var);
		GameObject_t3674682005 * L_10 = __this->get_ball_27();
		NullCheck(L_9);
		L_9->set_Ball_10(L_10);
		GameObject_t3674682005 * L_11 = __this->get_ball_27();
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = Transform_GetChild_m4040462992(L_12, 1, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = Component_get_transform_m4257140443(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		BallCollider_t3766478643 * L_15 = Component_GetComponent_TisBallCollider_t3766478643_m174257010(L_14, /*hidden argument*/Component_GetComponent_TisBallCollider_t3766478643_m174257010_MethodInfo_var);
		GameObject_t3674682005 * L_16 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_Controller_2(L_16);
		GameObject_t3674682005 * L_17 = __this->get_ball_27();
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = GameObject_get_transform_m1278640159(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = Transform_GetChild_m4040462992(L_18, 1, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = Component_get_transform_m4257140443(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		BallCollider_t3766478643 * L_21 = Component_GetComponent_TisBallCollider_t3766478643_m174257010(L_20, /*hidden argument*/Component_GetComponent_TisBallCollider_t3766478643_m174257010_MethodInfo_var);
		GameObject_t3674682005 * L_22 = __this->get_BotesText_12();
		NullCheck(L_21);
		L_21->set_nBotes_4(L_22);
		GameObject_t3674682005 * L_23 = __this->get_ball_27();
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = Transform_GetChild_m4040462992(L_24, 1, /*hidden argument*/NULL);
		NullCheck(L_25);
		BallCollider_t3766478643 * L_26 = Component_GetComponent_TisBallCollider_t3766478643_m174257010(L_25, /*hidden argument*/Component_GetComponent_TisBallCollider_t3766478643_m174257010_MethodInfo_var);
		Camera_t2727095145 * L_27 = __this->get_Perspec_30();
		Camera_t2727095145 * L_28 = __this->get_Ortho_31();
		Camera_t2727095145 * L_29 = __this->get_Iso_32();
		Camera_t2727095145 * L_30 = __this->get_Interface_33();
		NullCheck(L_26);
		BallCollider_setCameras_m1923159934(L_26, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
		List_1_t747900261 * L_31 = __this->get_Multibolas_45();
		GameObject_t3674682005 * L_32 = __this->get_ball_27();
		NullCheck(L_31);
		List_1_Add_m2091695206(L_31, L_32, /*hidden argument*/List_1_Add_m2091695206_MethodInfo_var);
		return;
	}
}
// System.Void Controller::RestartGame()
extern "C"  void Controller_RestartGame_m3205758686 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	{
		Controller_StartGame_m3824060913(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::Update()
extern "C"  void Controller_Update_m3176534670 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Controller::RemoveBrick(UnityEngine.GameObject)
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1580823225;
extern const uint32_t Controller_RemoveBrick_m1458410322_MetadataUsageId;
extern "C"  void Controller_RemoveBrick_m1458410322 (Controller_t2630893500 * __this, GameObject_t3674682005 * ___brick0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_RemoveBrick_m1458410322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_FrontWall_6();
		NullCheck(L_0);
		FrontWall_t2131806067 * L_1 = GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678(L_0, /*hidden argument*/GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678_MethodInfo_var);
		GameObject_t3674682005 * L_2 = ___brick0;
		NullCheck(L_1);
		FrontWall_RemoveBrick_m666535627(L_1, L_2, /*hidden argument*/NULL);
		bool L_3 = Controller_isWallEmpty_m1320274370(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_gameState_47();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_003d;
		}
	}
	{
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_gameState_47(4);
		Controller_PasaFase_m306660593(__this, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral1580823225, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Controller::PasaFase()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* RoomGenerator_t3606720536_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2619881806;
extern Il2CppCodeGenString* _stringLiteral19476615;
extern Il2CppCodeGenString* _stringLiteral1024831109;
extern const uint32_t Controller_PasaFase_m306660593_MetadataUsageId;
extern "C"  void Controller_PasaFase_m306660593 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_PasaFase_m306660593_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Controller_UpdateNumberRoom_m1872280882(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = __this->get_RoomGen_5();
		NullCheck(L_0);
		RoomGenerator_t3606720536 * L_1 = GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481(L_0, /*hidden argument*/GameObject_GetComponent_TisRoomGenerator_t3606720536_m2811985481_MethodInfo_var);
		int32_t L_2 = __this->get_NRoom_38();
		NullCheck(L_1);
		RoomGenerator_increaseFase_m279346609(L_1, L_2, /*hidden argument*/NULL);
		Controller_DestroyAllBalls_m2867674762(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_player_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Controller_ShowUIPanelTransicion_m1042328790(__this, /*hidden argument*/NULL);
		Controller_HideUIPanelJuego_m599578937(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral2619881806, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral19476615);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral19476615);
		ObjectU5BU5D_t1108656482* L_5 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(RoomGenerator_t3606720536_il2cpp_TypeInfo_var);
		int32_t L_6 = ((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->get_fase_25();
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_8);
		ObjectU5BU5D_t1108656482* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		ArrayElementTypeCheck (L_9, _stringLiteral1024831109);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1024831109);
		ObjectU5BU5D_t1108656482* L_10 = L_9;
		int32_t L_11 = __this->get_NRoom_38();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::UpdateNumberRoom()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral904014880;
extern const uint32_t Controller_UpdateNumberRoom_m1872280882_MetadataUsageId;
extern "C"  void Controller_UpdateNumberRoom_m1872280882 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_UpdateNumberRoom_m1872280882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_NRoom_38();
		__this->set_NRoom_38(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_NRoom_38();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral904014880, L_3, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Controller::Cortina()
extern Il2CppClass* U3CCortinaU3Ec__Iterator5_t3064932893_il2cpp_TypeInfo_var;
extern const uint32_t Controller_Cortina_m4128456483_MetadataUsageId;
extern "C"  Il2CppObject * Controller_Cortina_m4128456483 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_Cortina_m4128456483_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCortinaU3Ec__Iterator5_t3064932893 * V_0 = NULL;
	{
		U3CCortinaU3Ec__Iterator5_t3064932893 * L_0 = (U3CCortinaU3Ec__Iterator5_t3064932893 *)il2cpp_codegen_object_new(U3CCortinaU3Ec__Iterator5_t3064932893_il2cpp_TypeInfo_var);
		U3CCortinaU3Ec__Iterator5__ctor_m1381489950(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCortinaU3Ec__Iterator5_t3064932893 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CCortinaU3Ec__Iterator5_t3064932893 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Controller::EnableEnterButton()
extern "C"  void Controller_EnableEnterButton_m3902825572 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	{
		__this->set_isEnterButtonReady_37((bool)1);
		GameObject_t3674682005 * L_0 = __this->get_EnterButton_26();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::DisableEnterButton()
extern "C"  void Controller_DisableEnterButton_m3804347815 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	{
		__this->set_isEnterButtonReady_37((bool)0);
		GameObject_t3674682005 * L_0 = __this->get_EnterButton_26();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Controller::isWallEmpty()
extern const MethodInfo* GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m487786338_MethodInfo_var;
extern const uint32_t Controller_isWallEmpty_m1320274370_MetadataUsageId;
extern "C"  bool Controller_isWallEmpty_m1320274370 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_isWallEmpty_m1320274370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_FrontWall_6();
		NullCheck(L_0);
		FrontWall_t2131806067 * L_1 = GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678(L_0, /*hidden argument*/GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678_MethodInfo_var);
		NullCheck(L_1);
		List_1_t747900261 * L_2 = L_1->get_Bricks_3();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m487786338(L_2, /*hidden argument*/List_1_get_Count_m487786338_MethodInfo_var);
		if (L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)1;
	}

IL_001c:
	{
		return (bool)0;
	}
}
// System.Void Controller::GameOver()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern const uint32_t Controller_GameOver_m4035021163_MetadataUsageId;
extern "C"  void Controller_GameOver_m4035021163 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_GameOver_m4035021163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Controller_DestroyAllBalls_m2867674762(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = __this->get_player_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_gameState_47(3);
		Controller_ShowUIPanelGameOver_m1373388150(__this, /*hidden argument*/NULL);
		Controller_HideUIPanelMenu_m1476205066(__this, /*hidden argument*/NULL);
		Controller_HideUIPanelJuego_m599578937(__this, /*hidden argument*/NULL);
		Controller_UpdateHUD_m1160851307(__this, /*hidden argument*/NULL);
		Controller_GameOverStats_m517432214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::GameOverStats()
extern "C"  void Controller_GameOverStats_m517432214 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	{
		Controller_UpdateHUD_m1160851307(__this, /*hidden argument*/NULL);
		Controller_setData_m4071057097(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Controller::runScore()
extern Il2CppClass* U3CrunScoreU3Ec__Iterator6_t3971101203_il2cpp_TypeInfo_var;
extern const uint32_t Controller_runScore_m2752663604_MetadataUsageId;
extern "C"  Il2CppObject * Controller_runScore_m2752663604 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_runScore_m2752663604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CrunScoreU3Ec__Iterator6_t3971101203 * V_0 = NULL;
	{
		U3CrunScoreU3Ec__Iterator6_t3971101203 * L_0 = (U3CrunScoreU3Ec__Iterator6_t3971101203 *)il2cpp_codegen_object_new(U3CrunScoreU3Ec__Iterator6_t3971101203_il2cpp_TypeInfo_var);
		U3CrunScoreU3Ec__Iterator6__ctor_m1477857464(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CrunScoreU3Ec__Iterator6_t3971101203 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CrunScoreU3Ec__Iterator6_t3971101203 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Controller::DestroyAllBalls()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2062879;
extern const uint32_t Controller_DestroyAllBalls_m2867674762_MetadataUsageId;
extern "C"  void Controller_DestroyAllBalls_m2867674762 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_DestroyAllBalls_m2867674762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObjectU5BU5D_t2662109048* V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	GameObjectU5BU5D_t2662109048* V_2 = NULL;
	int32_t V_3 = 0;
	{
		GameObjectU5BU5D_t2662109048* L_0 = GameObject_FindGameObjectsWithTag_m3058873418(NULL /*static, unused*/, _stringLiteral2062879, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObjectU5BU5D_t2662109048* L_1 = V_0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_0022;
	}

IL_0014:
	{
		GameObjectU5BU5D_t2662109048* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		GameObject_t3674682005 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		GameObject_t3674682005 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_8 = V_3;
		GameObjectU5BU5D_t2662109048* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void Controller::UpdateHUD()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral78160549;
extern Il2CppCodeGenString* _stringLiteral862778894;
extern Il2CppCodeGenString* _stringLiteral2422978632;
extern const uint32_t Controller_UpdateHUD_m1160851307_MetadataUsageId;
extern "C"  void Controller_UpdateHUD_m1160851307 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_UpdateHUD_m1160851307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_RoomText_13();
		NullCheck(L_0);
		Text_t9039225 * L_1 = GameObject_GetComponent_TisText_t9039225_m202917489(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		int32_t L_2 = __this->get_NRoom_38();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral78160549, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_5);
		GameObject_t3674682005 * L_6 = __this->get_BricksText_14();
		NullCheck(L_6);
		Text_t9039225 * L_7 = GameObject_GetComponent_TisText_t9039225_m202917489(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		int32_t L_8 = __this->get_NBricks_39();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_9);
		String_t* L_11 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral862778894, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_11);
		GameObject_t3674682005 * L_12 = __this->get_BricksTextTransicion_20();
		NullCheck(L_12);
		Text_t9039225 * L_13 = GameObject_GetComponent_TisText_t9039225_m202917489(L_12, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		int32_t L_14 = __this->get_NBricks_39();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		String_t* L_17 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral862778894, L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, L_17);
		GameObject_t3674682005 * L_18 = __this->get_RoomTextTransicion_21();
		NullCheck(L_18);
		Text_t9039225 * L_19 = GameObject_GetComponent_TisText_t9039225_m202917489(L_18, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		int32_t L_20 = __this->get_NRoom_38();
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_21);
		String_t* L_23 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral78160549, L_22, /*hidden argument*/NULL);
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, L_23);
		GameObject_t3674682005 * L_24 = __this->get_RoomTextGameOver_16();
		NullCheck(L_24);
		Text_t9039225 * L_25 = GameObject_GetComponent_TisText_t9039225_m202917489(L_24, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		int32_t L_26 = __this->get_NRoom_38();
		int32_t L_27 = L_26;
		Il2CppObject * L_28 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_27);
		String_t* L_29 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral78160549, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_29);
		GameObject_t3674682005 * L_30 = __this->get_BricksTextGameOver_17();
		NullCheck(L_30);
		Text_t9039225 * L_31 = GameObject_GetComponent_TisText_t9039225_m202917489(L_30, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		int32_t L_32 = __this->get_NBricks_39();
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_33);
		String_t* L_35 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral862778894, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_31, L_35);
		GameObject_t3674682005 * L_36 = __this->get_TotalRoomMenuText_22();
		NullCheck(L_36);
		Text_t9039225 * L_37 = GameObject_GetComponent_TisText_t9039225_m202917489(L_36, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		int32_t L_38 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalRooms_41();
		int32_t L_39 = L_38;
		Il2CppObject * L_40 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_39);
		String_t* L_41 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2422978632, L_40, /*hidden argument*/NULL);
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, L_41);
		GameObject_t3674682005 * L_42 = __this->get_TotalBricksMenuText_23();
		NullCheck(L_42);
		Text_t9039225 * L_43 = GameObject_GetComponent_TisText_t9039225_m202917489(L_42, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		int32_t L_44 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalBricks_40();
		int32_t L_45 = L_44;
		Il2CppObject * L_46 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_45);
		String_t* L_47 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral862778894, L_46, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_43, L_47);
		return;
	}
}
// System.Void Controller::MoveLeft()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var;
extern const uint32_t Controller_MoveLeft_m1725249661_MetadataUsageId;
extern "C"  void Controller_MoveLeft_m1725249661 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_MoveLeft_m1725249661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_ball_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_ball_27();
		NullCheck(L_2);
		MoveBall_t4254648720 * L_3 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_2, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		NullCheck(L_3);
		bool L_4 = L_3->get_isReady_4();
		if (L_4)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t3674682005 * L_5 = __this->get_ball_27();
		NullCheck(L_5);
		MoveBall_t4254648720 * L_6 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_5, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_isReady_4((bool)1);
		GameObject_t3674682005 * L_7 = __this->get_ball_27();
		NullCheck(L_7);
		MoveBall_t4254648720 * L_8 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_7, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		MoveBall_t4254648720 * L_9 = L_8;
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = L_9->get_velocidad_2();
		Vector3_t4282066566  L_11 = Vector3_get_left_m1616598929(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_12 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_velocidad_2(L_12);
	}

IL_0056:
	{
		GameObject_t3674682005 * L_13 = __this->get_player_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0076;
		}
	}
	{
		GameObject_t3674682005 * L_15 = __this->get_player_28();
		NullCheck(L_15);
		Move_t2404337 * L_16 = GameObject_GetComponent_TisMove_t2404337_m307893836(L_15, /*hidden argument*/GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var);
		NullCheck(L_16);
		Move_Left_m2948975745(L_16, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void Controller::MoveRight()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var;
extern const uint32_t Controller_MoveRight_m3088342408_MetadataUsageId;
extern "C"  void Controller_MoveRight_m3088342408 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_MoveRight_m3088342408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_ball_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_ball_27();
		NullCheck(L_2);
		MoveBall_t4254648720 * L_3 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_2, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		NullCheck(L_3);
		bool L_4 = L_3->get_isReady_4();
		if (L_4)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t3674682005 * L_5 = __this->get_ball_27();
		NullCheck(L_5);
		MoveBall_t4254648720 * L_6 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_5, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_isReady_4((bool)1);
		GameObject_t3674682005 * L_7 = __this->get_ball_27();
		NullCheck(L_7);
		MoveBall_t4254648720 * L_8 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_7, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		MoveBall_t4254648720 * L_9 = L_8;
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = L_9->get_velocidad_2();
		Vector3_t4282066566  L_11 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_12 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_velocidad_2(L_12);
	}

IL_0056:
	{
		GameObject_t3674682005 * L_13 = __this->get_player_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0076;
		}
	}
	{
		GameObject_t3674682005 * L_15 = __this->get_player_28();
		NullCheck(L_15);
		Move_t2404337 * L_16 = GameObject_GetComponent_TisMove_t2404337_m307893836(L_15, /*hidden argument*/GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var);
		NullCheck(L_16);
		Move_Right_m2369145348(L_16, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void Controller::resetMovimiento()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var;
extern const uint32_t Controller_resetMovimiento_m1212277985_MetadataUsageId;
extern "C"  void Controller_resetMovimiento_m1212277985 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_resetMovimiento_m1212277985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_player_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_player_28();
		NullCheck(L_2);
		Move_t2404337 * L_3 = GameObject_GetComponent_TisMove_t2404337_m307893836(L_2, /*hidden argument*/GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var);
		NullCheck(L_3);
		Move_resetMove_m3748710728(L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Controller::CreateMultiBalls(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBallCollider_t3766478643_m174257010_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBallShadow_t3766914079_m3429748870_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2091695206_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2443671092;
extern Il2CppCodeGenString* _stringLiteral2062879;
extern const uint32_t Controller_CreateMultiBalls_m521653845_MetadataUsageId;
extern "C"  void Controller_CreateMultiBalls_m521653845 (Controller_t2630893500 * __this, Vector3_t4282066566  ___pos0, Vector3_t4282066566  ___boundaries1, float ___alturaSombra2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_CreateMultiBalls_m521653845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t3674682005 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_00e5;
	}

IL_0007:
	{
		GameObject_t3674682005 * L_0 = __this->get_BallObj_4();
		Vector3_t4282066566  L_1 = ___pos0;
		Quaternion_t1553702882  L_2 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_3 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = ((GameObject_t3674682005 *)IsInstSealed(L_3, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_4 = V_1;
		NullCheck(L_4);
		Object_set_name_m1123518500(L_4, _stringLiteral2443671092, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_5 = V_1;
		NullCheck(L_5);
		GameObject_set_tag_m859036203(L_5, _stringLiteral2062879, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_6 = V_1;
		NullCheck(L_6);
		MoveBall_t4254648720 * L_7 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_6, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		Vector3_t4282066566  L_8 = ___boundaries1;
		NullCheck(L_7);
		L_7->set_boundary_3(L_8);
		GameObject_t3674682005 * L_9 = V_1;
		NullCheck(L_9);
		MoveBall_t4254648720 * L_10 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_9, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		int32_t L_11 = Random_Range_m75452833(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		int32_t L_12 = Random_Range_m75452833(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2926210380(&L_13, (((float)((float)L_11))), (0.0f), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_12*(int32_t)2))-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_velocidad_2(L_13);
		GameObject_t3674682005 * L_14 = V_1;
		NullCheck(L_14);
		BallCollider_t3766478643 * L_15 = GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182(L_14, /*hidden argument*/GameObject_GetComponentInChildren_TisBallCollider_t3766478643_m204171182_MethodInfo_var);
		Camera_t2727095145 * L_16 = __this->get_Perspec_30();
		Camera_t2727095145 * L_17 = __this->get_Ortho_31();
		Camera_t2727095145 * L_18 = __this->get_Iso_32();
		Camera_t2727095145 * L_19 = __this->get_Interface_33();
		NullCheck(L_15);
		BallCollider_setCameras_m1923159934(L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_20 = V_1;
		NullCheck(L_20);
		MoveBall_t4254648720 * L_21 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_20, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		NullCheck(L_21);
		L_21->set_isReady_4((bool)1);
		GameObject_t3674682005 * L_22 = V_1;
		NullCheck(L_22);
		Transform_t1659122786 * L_23 = GameObject_get_transform_m1278640159(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = Transform_GetChild_m4040462992(L_23, 1, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = Component_get_transform_m4257140443(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		BallCollider_t3766478643 * L_26 = Component_GetComponent_TisBallCollider_t3766478643_m174257010(L_25, /*hidden argument*/Component_GetComponent_TisBallCollider_t3766478643_m174257010_MethodInfo_var);
		GameObject_t3674682005 * L_27 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		L_26->set_Controller_2(L_27);
		GameObject_t3674682005 * L_28 = V_1;
		NullCheck(L_28);
		Transform_t1659122786 * L_29 = GameObject_get_transform_m1278640159(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = Transform_GetChild_m4040462992(L_29, 0, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = Component_get_transform_m4257140443(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		BallShadow_t3766914079 * L_32 = Component_GetComponent_TisBallShadow_t3766914079_m3429748870(L_31, /*hidden argument*/Component_GetComponent_TisBallShadow_t3766914079_m3429748870_MethodInfo_var);
		float L_33 = ___alturaSombra2;
		NullCheck(L_32);
		L_32->set_alturaSombra_4(L_33);
		List_1_t747900261 * L_34 = __this->get_Multibolas_45();
		GameObject_t3674682005 * L_35 = V_1;
		NullCheck(L_34);
		List_1_Add_m2091695206(L_34, L_35, /*hidden argument*/List_1_Add_m2091695206_MethodInfo_var);
		int32_t L_36 = V_0;
		V_0 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00e5:
	{
		int32_t L_37 = V_0;
		int32_t L_38 = __this->get_numMultiplyBalls_44();
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Controller::CheckEndGame(UnityEngine.GameObject)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m1159674165_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m487786338_MethodInfo_var;
extern const uint32_t Controller_CheckEndGame_m111272194_MetadataUsageId;
extern "C"  void Controller_CheckEndGame_m111272194 (Controller_t2630893500 * __this, GameObject_t3674682005 * ___ball0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_CheckEndGame_m111272194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t747900261 * L_0 = __this->get_Multibolas_45();
		GameObject_t3674682005 * L_1 = ___ball0;
		NullCheck(L_0);
		List_1_Remove_m1159674165(L_0, L_1, /*hidden argument*/List_1_Remove_m1159674165_MethodInfo_var);
		GameObject_t3674682005 * L_2 = ___ball0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		List_1_t747900261 * L_3 = __this->get_Multibolas_45();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m487786338(L_3, /*hidden argument*/List_1_get_Count_m487786338_MethodInfo_var);
		if ((((int32_t)L_4) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		Controller_GameOver_m4035021163(__this, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void Controller::PlayAudio(System.Single)
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var;
extern const uint32_t Controller_PlayAudio_m1101257740_MetadataUsageId;
extern "C"  void Controller_PlayAudio_m1101257740 (Controller_t2630893500 * __this, float ___pitch0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_PlayAudio_m1101257740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_audio_46();
		NullCheck(L_0);
		AudioSource_t1740077639 * L_1 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		float L_2 = ___pitch0;
		NullCheck(L_1);
		AudioSource_set_pitch_m1518407234(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_audio_46();
		NullCheck(L_3);
		AudioSource_t1740077639 * L_4 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_3, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		NullCheck(L_4);
		bool L_5 = AudioSource_get_isPlaying_m4213444423(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t3674682005 * L_6 = __this->get_audio_46();
		NullCheck(L_6);
		AudioSource_t1740077639 * L_7 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_6, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		NullCheck(L_7);
		AudioSource_Play_m1360558992(L_7, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void Controller::setData()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3070616758;
extern Il2CppCodeGenString* _stringLiteral3577428500;
extern Il2CppCodeGenString* _stringLiteral1576697504;
extern const uint32_t Controller_setData_m4071057097_MetadataUsageId;
extern "C"  void Controller_setData_m4071057097 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_setData_m4071057097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalBricks_40();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral3070616758, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalRooms_41();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral3577428500, L_1, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral1576697504, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::getData()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1256020774;
extern Il2CppCodeGenString* _stringLiteral3070616758;
extern Il2CppCodeGenString* _stringLiteral1289740910;
extern Il2CppCodeGenString* _stringLiteral3577428500;
extern Il2CppCodeGenString* _stringLiteral2939287016;
extern const uint32_t Controller_getData_m4285674173_MetadataUsageId;
extern "C"  void Controller_getData_m4285674173 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_getData_m4285674173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral1256020774, /*hidden argument*/NULL);
		bool L_0 = PlayerPrefs_HasKey_m2032560073(NULL /*static, unused*/, _stringLiteral3070616758, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_1 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral3070616758, /*hidden argument*/NULL);
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_NTotalBricks_40(L_1);
		int32_t L_2 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalBricks_40();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1289740910, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0041:
	{
		bool L_6 = PlayerPrefs_HasKey_m2032560073(NULL /*static, unused*/, _stringLiteral3577428500, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_7 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral3577428500, /*hidden argument*/NULL);
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_NTotalRooms_41(L_7);
		int32_t L_8 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalRooms_41();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2939287016, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void Controller::deleteData()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern const uint32_t Controller_deleteData_m1540580506_MetadataUsageId;
extern "C"  void Controller_deleteData_m1540580506 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_deleteData_m1540580506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayerPrefs_DeleteAll_m2619453438(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_NTotalBricks_40(0);
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_NTotalRooms_41(0);
		PlayerPrefs_Save_m3891538519(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::OnApplicationQuit()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Controller_OnApplicationQuit_m2648122205_MetadataUsageId;
extern "C"  void Controller_OnApplicationQuit_m2648122205 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_OnApplicationQuit_m2648122205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::SetBool(System.String,System.Boolean)
extern "C"  void Controller_SetBool_m1497303716 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___value1, const MethodInfo* method)
{
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___value1;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Controller::GetBool(System.String)
extern "C"  bool Controller_GetBool_m3058516473 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean Controller::GetBool(System.String,System.Boolean)
extern "C"  bool Controller_GetBool_m3247030372 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		bool L_1 = PlayerPrefs_HasKey_m2032560073(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___name0;
		bool L_3 = Controller_GetBool_m3058516473(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		bool L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Void Controller::StartMinigame()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern const uint32_t Controller_StartMinigame_m3672498696_MetadataUsageId;
extern "C"  void Controller_StartMinigame_m3672498696 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_StartMinigame_m3672498696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_gameState_47(6);
		GameObject_t3674682005 * L_0 = __this->get_Telefono_24();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::EndMinigame()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern const uint32_t Controller_EndMinigame_m3932820033_MetadataUsageId;
extern "C"  void Controller_EndMinigame_m3932820033 (Controller_t2630893500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Controller_EndMinigame_m3932820033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_gameState_47(0);
		GameObject_t3674682005 * L_0 = __this->get_Telefono_24();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller/<CamAnim>c__Iterator4::.ctor()
extern "C"  void U3CCamAnimU3Ec__Iterator4__ctor_m3640395665 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Controller/<CamAnim>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCamAnimU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3244763883 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Controller/<CamAnim>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCamAnimU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3479856255 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Controller/<CamAnim>c__Iterator4::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const uint32_t U3CCamAnimU3Ec__Iterator4_MoveNext_m2045962411_MetadataUsageId;
extern "C"  bool U3CCamAnimU3Ec__Iterator4_MoveNext_m2045962411 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCamAnimU3Ec__Iterator4_MoveNext_m2045962411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_00c0;
		}
		if (L_1 == 2)
		{
			goto IL_0132;
		}
		if (L_1 == 3)
		{
			goto IL_01a4;
		}
	}
	{
		goto IL_01c5;
	}

IL_0029:
	{
		Controller_t2630893500 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Camera_t2727095145 * L_3 = L_2->get_Perspec_30();
		NullCheck(L_3);
		Camera_set_nearClipPlane_m534185950(L_3, (20.0f), /*hidden argument*/NULL);
		Controller_t2630893500 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		Camera_t2727095145 * L_5 = L_4->get_Ortho_31();
		NullCheck(L_5);
		Camera_set_nearClipPlane_m534185950(L_5, (20.0f), /*hidden argument*/NULL);
		Controller_t2630893500 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		Camera_t2727095145 * L_7 = L_6->get_Iso_32();
		NullCheck(L_7);
		Camera_set_nearClipPlane_m534185950(L_7, (20.0f), /*hidden argument*/NULL);
		Controller_t2630893500 * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		Camera_t2727095145 * L_9 = L_8->get_Perspec_30();
		NullCheck(L_9);
		bool L_10 = Behaviour_get_isActiveAndEnabled_m210167461(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00da;
		}
	}
	{
		goto IL_00c0;
	}

IL_0082:
	{
		Controller_t2630893500 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		Camera_t2727095145 * L_12 = L_11->get_Perspec_30();
		Camera_t2727095145 * L_13 = L_12;
		NullCheck(L_13);
		float L_14 = Camera_get_nearClipPlane_m4074655061(L_13, /*hidden argument*/NULL);
		NullCheck(L_13);
		Camera_set_nearClipPlane_m534185950(L_13, ((float)((float)L_14-(float)(1.0f))), /*hidden argument*/NULL);
		Controller_t2630893500 * L_15 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_15);
		float L_16 = L_15->get_speedCamAnim_35();
		WaitForSeconds_t1615819279 * L_17 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_17, L_16, /*hidden argument*/NULL);
		__this->set_U24current_1(L_17);
		__this->set_U24PC_0(1);
		goto IL_01c7;
	}

IL_00c0:
	{
		Controller_t2630893500 * L_18 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_18);
		Camera_t2727095145 * L_19 = L_18->get_Perspec_30();
		NullCheck(L_19);
		float L_20 = Camera_get_nearClipPlane_m4074655061(L_19, /*hidden argument*/NULL);
		if ((((float)L_20) > ((float)(1.0f))))
		{
			goto IL_0082;
		}
	}

IL_00da:
	{
		Controller_t2630893500 * L_21 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_21);
		Camera_t2727095145 * L_22 = L_21->get_Ortho_31();
		NullCheck(L_22);
		bool L_23 = Behaviour_get_isActiveAndEnabled_m210167461(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_014c;
		}
	}
	{
		goto IL_0132;
	}

IL_00f4:
	{
		Controller_t2630893500 * L_24 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_24);
		Camera_t2727095145 * L_25 = L_24->get_Ortho_31();
		Camera_t2727095145 * L_26 = L_25;
		NullCheck(L_26);
		float L_27 = Camera_get_nearClipPlane_m4074655061(L_26, /*hidden argument*/NULL);
		NullCheck(L_26);
		Camera_set_nearClipPlane_m534185950(L_26, ((float)((float)L_27-(float)(1.0f))), /*hidden argument*/NULL);
		Controller_t2630893500 * L_28 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_28);
		float L_29 = L_28->get_speedCamAnim_35();
		WaitForSeconds_t1615819279 * L_30 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_30, L_29, /*hidden argument*/NULL);
		__this->set_U24current_1(L_30);
		__this->set_U24PC_0(2);
		goto IL_01c7;
	}

IL_0132:
	{
		Controller_t2630893500 * L_31 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_31);
		Camera_t2727095145 * L_32 = L_31->get_Ortho_31();
		NullCheck(L_32);
		float L_33 = Camera_get_nearClipPlane_m4074655061(L_32, /*hidden argument*/NULL);
		if ((((float)L_33) > ((float)(-15.0f))))
		{
			goto IL_00f4;
		}
	}

IL_014c:
	{
		Controller_t2630893500 * L_34 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_34);
		Camera_t2727095145 * L_35 = L_34->get_Iso_32();
		NullCheck(L_35);
		bool L_36 = Behaviour_get_isActiveAndEnabled_m210167461(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_01be;
		}
	}
	{
		goto IL_01a4;
	}

IL_0166:
	{
		Controller_t2630893500 * L_37 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_37);
		Camera_t2727095145 * L_38 = L_37->get_Iso_32();
		Camera_t2727095145 * L_39 = L_38;
		NullCheck(L_39);
		float L_40 = Camera_get_nearClipPlane_m4074655061(L_39, /*hidden argument*/NULL);
		NullCheck(L_39);
		Camera_set_nearClipPlane_m534185950(L_39, ((float)((float)L_40-(float)(1.0f))), /*hidden argument*/NULL);
		Controller_t2630893500 * L_41 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_41);
		float L_42 = L_41->get_speedCamAnim_35();
		WaitForSeconds_t1615819279 * L_43 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_43, L_42, /*hidden argument*/NULL);
		__this->set_U24current_1(L_43);
		__this->set_U24PC_0(3);
		goto IL_01c7;
	}

IL_01a4:
	{
		Controller_t2630893500 * L_44 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_44);
		Camera_t2727095145 * L_45 = L_44->get_Iso_32();
		NullCheck(L_45);
		float L_46 = Camera_get_nearClipPlane_m4074655061(L_45, /*hidden argument*/NULL);
		if ((((float)L_46) > ((float)(-15.0f))))
		{
			goto IL_0166;
		}
	}

IL_01be:
	{
		__this->set_U24PC_0((-1));
	}

IL_01c5:
	{
		return (bool)0;
	}

IL_01c7:
	{
		return (bool)1;
	}
	// Dead block : IL_01c9: ldloc.1
}
// System.Void Controller/<CamAnim>c__Iterator4::Dispose()
extern "C"  void U3CCamAnimU3Ec__Iterator4_Dispose_m2215138190 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Controller/<CamAnim>c__Iterator4::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCamAnimU3Ec__Iterator4_Reset_m1286828606_MetadataUsageId;
extern "C"  void U3CCamAnimU3Ec__Iterator4_Reset_m1286828606 (U3CCamAnimU3Ec__Iterator4_t3416449610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCamAnimU3Ec__Iterator4_Reset_m1286828606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Controller/<Cortina>c__Iterator5::.ctor()
extern "C"  void U3CCortinaU3Ec__Iterator5__ctor_m1381489950 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Controller/<Cortina>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCortinaU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m745223038 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Controller/<Cortina>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCortinaU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m4190106386 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Controller/<Cortina>c__Iterator5::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const uint32_t U3CCortinaU3Ec__Iterator5_MoveNext_m533400574_MetadataUsageId;
extern "C"  bool U3CCortinaU3Ec__Iterator5_MoveNext_m533400574 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCortinaU3Ec__Iterator5_MoveNext_m533400574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0080;
		}
	}
	{
		goto IL_00a7;
	}

IL_0021:
	{
		Controller_t2630893500 * L_2 = __this->get_U3CU3Ef__this_2();
		Controller_t2630893500 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		float L_4 = L_3->get_tiempoPausa_43();
		NullCheck(L_2);
		L_2->set_tiempo_42(L_4);
		Controller_t2630893500 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		Controller_DisableEnterButton_m3804347815(L_5, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0047:
	{
		Controller_t2630893500 * L_6 = __this->get_U3CU3Ef__this_2();
		Controller_t2630893500 * L_7 = L_6;
		NullCheck(L_7);
		float L_8 = L_7->get_tiempo_42();
		float L_9 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_tiempo_42(((float)((float)L_8-(float)((float)((float)(1.0f)*(float)L_9)))));
		WaitForSeconds_t1615819279 * L_10 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_10, (0.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_10);
		__this->set_U24PC_0(1);
		goto IL_00a9;
	}

IL_0080:
	{
		Controller_t2630893500 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		float L_12 = L_11->get_tiempo_42();
		if ((((float)L_12) > ((float)(0.0f))))
		{
			goto IL_0047;
		}
	}
	{
		Controller_t2630893500 * L_13 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_13);
		Controller_EnableEnterButton_m3902825572(L_13, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_00a7:
	{
		return (bool)0;
	}

IL_00a9:
	{
		return (bool)1;
	}
	// Dead block : IL_00ab: ldloc.1
}
// System.Void Controller/<Cortina>c__Iterator5::Dispose()
extern "C"  void U3CCortinaU3Ec__Iterator5_Dispose_m365230555 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Controller/<Cortina>c__Iterator5::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCortinaU3Ec__Iterator5_Reset_m3322890187_MetadataUsageId;
extern "C"  void U3CCortinaU3Ec__Iterator5_Reset_m3322890187 (U3CCortinaU3Ec__Iterator5_t3064932893 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCortinaU3Ec__Iterator5_Reset_m3322890187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Controller/<runScore>c__Iterator6::.ctor()
extern "C"  void U3CrunScoreU3Ec__Iterator6__ctor_m1477857464 (U3CrunScoreU3Ec__Iterator6_t3971101203 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Controller/<runScore>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CrunScoreU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m630683354 (U3CrunScoreU3Ec__Iterator6_t3971101203 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Controller/<runScore>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CrunScoreU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m70676078 (U3CrunScoreU3Ec__Iterator6_t3971101203 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean Controller/<runScore>c__Iterator6::MoveNext()
extern Il2CppClass* Controller_t2630893500_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4107351757;
extern Il2CppCodeGenString* _stringLiteral1026682660;
extern Il2CppCodeGenString* _stringLiteral3504;
extern Il2CppCodeGenString* _stringLiteral280331072;
extern const uint32_t U3CrunScoreU3Ec__Iterator6_MoveNext_m2809425724_MetadataUsageId;
extern "C"  bool U3CrunScoreU3Ec__Iterator6_MoveNext_m2809425724 (U3CrunScoreU3Ec__Iterator6_t3971101203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CrunScoreU3Ec__Iterator6_MoveNext_m2809425724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0106;
		}
	}
	{
		goto IL_0122;
	}

IL_0021:
	{
		Controller_t2630893500 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_NBricks_39();
		int32_t L_4 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalBricks_40();
		__this->set_U3CpU3E__0_0(((int32_t)((int32_t)L_3+(int32_t)L_4)));
		ObjectU5BU5D_t1108656482* L_5 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, _stringLiteral4107351757);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4107351757);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		Controller_t2630893500 * L_7 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_NBricks_39();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_6;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, _stringLiteral1026682660);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1026682660);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		int32_t L_13 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalBricks_40();
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, _stringLiteral3504);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3504);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		int32_t L_18 = __this->get_U3CpU3E__0_0();
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 5);
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		goto IL_0106;
	}

IL_0093:
	{
		int32_t L_22 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalBricks_40();
		((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->set_NTotalBricks_40(((int32_t)((int32_t)L_22+(int32_t)1)));
		Controller_t2630893500 * L_23 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_23);
		Controller_UpdateHUD_m1160851307(L_23, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_24 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, _stringLiteral280331072);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral280331072);
		ObjectU5BU5D_t1108656482* L_25 = L_24;
		Controller_t2630893500 * L_26 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_NBricks_39();
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 1);
		ArrayElementTypeCheck (L_25, L_29);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_29);
		ObjectU5BU5D_t1108656482* L_30 = L_25;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 2);
		ArrayElementTypeCheck (L_30, _stringLiteral1026682660);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1026682660);
		ObjectU5BU5D_t1108656482* L_31 = L_30;
		int32_t L_32 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalBricks_40();
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 3);
		ArrayElementTypeCheck (L_31, L_34);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m3016520001(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_36 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_36, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_36);
		__this->set_U24PC_1(1);
		goto IL_0124;
	}

IL_0106:
	{
		int32_t L_37 = __this->get_U3CpU3E__0_0();
		int32_t L_38 = ((Controller_t2630893500_StaticFields*)Controller_t2630893500_il2cpp_TypeInfo_var->static_fields)->get_NTotalBricks_40();
		if ((((int32_t)L_37) > ((int32_t)L_38)))
		{
			goto IL_0093;
		}
	}
	{
		Controller_setData_m4071057097(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_0122:
	{
		return (bool)0;
	}

IL_0124:
	{
		return (bool)1;
	}
	// Dead block : IL_0126: ldloc.1
}
// System.Void Controller/<runScore>c__Iterator6::Dispose()
extern "C"  void U3CrunScoreU3Ec__Iterator6_Dispose_m2780098293 (U3CrunScoreU3Ec__Iterator6_t3971101203 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void Controller/<runScore>c__Iterator6::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CrunScoreU3Ec__Iterator6_Reset_m3419257701_MetadataUsageId;
extern "C"  void U3CrunScoreU3Ec__Iterator6_Reset_m3419257701 (U3CrunScoreU3Ec__Iterator6_t3971101203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CrunScoreU3Ec__Iterator6_Reset_m3419257701_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void CreateMap::.ctor()
extern "C"  void CreateMap__ctor_m4077309643 (CreateMap_t2602318496 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateMap::Start()
extern const MethodInfo* Component_GetComponent_TisPlants_t2393071496_m215378813_MethodInfo_var;
extern const uint32_t CreateMap_Start_m3024447435_MetadataUsageId;
extern "C"  void CreateMap_Start_m3024447435 (CreateMap_t2602318496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CreateMap_Start_m3024447435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Plants_t2393071496 * L_0 = Component_GetComponent_TisPlants_t2393071496_m215378813(__this, /*hidden argument*/Component_GetComponent_TisPlants_t2393071496_m215378813_MethodInfo_var);
		NullCheck(L_0);
		Plants_CreateMap_m3058892373(L_0, 5, 5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Dimensions::.ctor()
extern "C"  void Dimensions__ctor_m666081102 (Dimensions_t2407799789 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Dimensions::Awake()
extern "C"  void Dimensions_Awake_m903686321 (Dimensions_t2407799789 * __this, const MethodInfo* method)
{
	{
		Dimensions_GenerateDimensions_m1429542424(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Dimensions::GenerateDimensions()
extern "C"  void Dimensions_GenerateDimensions_m1429542424 (Dimensions_t2407799789 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_minAlto_6();
		int32_t L_1 = __this->get_maxAlto_9();
		int32_t L_2 = Random_Range_m75452833(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_alto_2(L_2);
		int32_t L_3 = __this->get_minAncho_7();
		int32_t L_4 = __this->get_maxAncho_10();
		int32_t L_5 = Random_Range_m75452833(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_ancho_3(L_5);
		int32_t L_6 = __this->get_minFondo_8();
		int32_t L_7 = __this->get_maxFondo_11();
		int32_t L_8 = Random_Range_m75452833(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		__this->set_fondo_4(L_8);
		return;
	}
}
// System.Void FrontWall::.ctor()
extern "C"  void FrontWall__ctor_m3287809880 (FrontWall_t2131806067 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrontWall::CreateIt(System.Int32,System.Int32)
extern Il2CppClass* List_1_t747900261_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2397334390_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBrick_t64452641_m1253576608_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2091695206_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral927034807;
extern Il2CppCodeGenString* _stringLiteral2914164735;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral980556366;
extern Il2CppCodeGenString* _stringLiteral1548947199;
extern Il2CppCodeGenString* _stringLiteral2339088647;
extern const uint32_t FrontWall_CreateIt_m2121949075_MetadataUsageId;
extern "C"  void FrontWall_CreateIt_m2121949075 (FrontWall_t2131806067 * __this, int32_t ___xWall0, int32_t ___yWall1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FrontWall_CreateIt_m2121949075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t3674682005 * V_3 = NULL;
	Color32_t598853688  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		List_1_t747900261 * L_0 = (List_1_t747900261 *)il2cpp_codegen_object_new(List_1_t747900261_il2cpp_TypeInfo_var);
		List_1__ctor_m2397334390(L_0, /*hidden argument*/List_1__ctor_m2397334390_MethodInfo_var);
		__this->set_Bricks_3(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_set_name_m1123518500(L_1, _stringLiteral927034807, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0174;
	}

IL_0022:
	{
		V_1 = 0;
		goto IL_0169;
	}

IL_0029:
	{
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		V_7 = L_3;
		float L_4 = (&V_7)->get_x_1();
		int32_t L_5 = V_0;
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		V_8 = L_7;
		float L_8 = (&V_8)->get_y_2();
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		V_9 = L_10;
		float L_11 = (&V_9)->get_z_3();
		int32_t L_12 = V_1;
		Vector3__ctor_m2926210380((&V_2), ((float)((float)L_4+(float)(((float)((float)L_5))))), L_8, ((float)((float)L_11-(float)(((float)((float)L_12))))), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_13 = __this->get_BrickObj_2();
		Vector3_t4282066566  L_14 = V_2;
		Quaternion_t1553702882  L_15 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_16 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		V_3 = ((GameObject_t3674682005 *)IsInstSealed(L_16, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_17 = V_3;
		String_t* L_18 = Int32_ToString_m1286526384((&V_0), /*hidden argument*/NULL);
		String_t* L_19 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral2914164735, L_18, _stringLiteral32, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		Object_set_name_m1123518500(L_17, L_20, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_21 = V_3;
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_23 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_SetParent_m3449663462(L_22, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_0;
		int32_t L_26 = V_0;
		int32_t L_27 = V_1;
		Color_t4194546905  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Color__ctor_m103496991(&L_28, ((float)((float)(((float)((float)L_25)))*(float)(0.1f))), ((float)((float)(((float)((float)L_26)))*(float)(0.1f))), ((float)((float)(((float)((float)L_27)))*(float)(0.1f))), /*hidden argument*/NULL);
		Color32_t598853688  L_29 = Color32_op_Implicit_m3684884838(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		GameObject_t3674682005 * L_30 = V_3;
		NullCheck(L_30);
		Renderer_t3076687687 * L_31 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_30, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_31);
		Material_t3870600107 * L_32 = Renderer_get_material_m2720864603(L_31, /*hidden argument*/NULL);
		Color32_t598853688  L_33 = V_4;
		Color_t4194546905  L_34 = Color32_op_Implicit_m358459656(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		Material_set_color_m3296857020(L_32, L_34, /*hidden argument*/NULL);
		int32_t L_35 = ___xWall0;
		int32_t L_36 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_35, /*hidden argument*/NULL);
		V_5 = L_36;
		int32_t L_37 = ___yWall1;
		int32_t L_38 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_37, /*hidden argument*/NULL);
		V_6 = L_38;
		int32_t L_39 = V_1;
		int32_t L_40 = V_5;
		if ((!(((uint32_t)L_39) == ((uint32_t)L_40))))
		{
			goto IL_012c;
		}
	}
	{
		GameObject_t3674682005 * L_41 = V_3;
		NullCheck(L_41);
		Brick_t64452641 * L_42 = GameObject_GetComponent_TisBrick_t64452641_m1253576608(L_41, /*hidden argument*/GameObject_GetComponent_TisBrick_t64452641_m1253576608_MethodInfo_var);
		NullCheck(L_42);
		L_42->set_tipo_2(_stringLiteral980556366);
		goto IL_0159;
	}

IL_012c:
	{
		int32_t L_43 = V_0;
		int32_t L_44 = V_6;
		if ((!(((uint32_t)L_43) == ((uint32_t)L_44))))
		{
			goto IL_0149;
		}
	}
	{
		GameObject_t3674682005 * L_45 = V_3;
		NullCheck(L_45);
		Brick_t64452641 * L_46 = GameObject_GetComponent_TisBrick_t64452641_m1253576608(L_45, /*hidden argument*/GameObject_GetComponent_TisBrick_t64452641_m1253576608_MethodInfo_var);
		NullCheck(L_46);
		L_46->set_tipo_2(_stringLiteral1548947199);
		goto IL_0159;
	}

IL_0149:
	{
		GameObject_t3674682005 * L_47 = V_3;
		NullCheck(L_47);
		Brick_t64452641 * L_48 = GameObject_GetComponent_TisBrick_t64452641_m1253576608(L_47, /*hidden argument*/GameObject_GetComponent_TisBrick_t64452641_m1253576608_MethodInfo_var);
		NullCheck(L_48);
		L_48->set_tipo_2(_stringLiteral2339088647);
	}

IL_0159:
	{
		List_1_t747900261 * L_49 = __this->get_Bricks_3();
		GameObject_t3674682005 * L_50 = V_3;
		NullCheck(L_49);
		List_1_Add_m2091695206(L_49, L_50, /*hidden argument*/List_1_Add_m2091695206_MethodInfo_var);
		int32_t L_51 = V_1;
		V_1 = ((int32_t)((int32_t)L_51+(int32_t)2));
	}

IL_0169:
	{
		int32_t L_52 = V_1;
		int32_t L_53 = ___yWall1;
		if ((((int32_t)L_52) < ((int32_t)L_53)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_54 = V_0;
		V_0 = ((int32_t)((int32_t)L_54+(int32_t)2));
	}

IL_0174:
	{
		int32_t L_55 = V_0;
		int32_t L_56 = ___xWall0;
		if ((((int32_t)L_55) < ((int32_t)L_56)))
		{
			goto IL_0022;
		}
	}
	{
		return;
	}
}
// System.Void FrontWall::CreateBrickWall(System.Int32,System.Int32)
extern "C"  void FrontWall_CreateBrickWall_m1068921921 (FrontWall_t2131806067 * __this, int32_t ___xWall0, int32_t ___yWall1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___xWall0;
		int32_t L_1 = ___yWall1;
		FrontWall_CreateIt_m2121949075(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrontWall::RemoveBrick(UnityEngine.GameObject)
extern const MethodInfo* List_1_Remove_m1159674165_MethodInfo_var;
extern const uint32_t FrontWall_RemoveBrick_m666535627_MetadataUsageId;
extern "C"  void FrontWall_RemoveBrick_m666535627 (FrontWall_t2131806067 * __this, GameObject_t3674682005 * ___brick0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FrontWall_RemoveBrick_m666535627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t747900261 * L_0 = __this->get_Bricks_3();
		GameObject_t3674682005 * L_1 = ___brick0;
		NullCheck(L_0);
		List_1_Remove_m1159674165(L_0, L_1, /*hidden argument*/List_1_Remove_m1159674165_MethodInfo_var);
		return;
	}
}
// System.Void MapController::.ctor()
extern "C"  void MapController__ctor_m3215722099 (MapController_t1754731000 * __this, const MethodInfo* method)
{
	{
		__this->set_dragSpeed_9((2.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapController::Start()
extern const MethodInfo* Component_GetComponent_TisDimensions_t2407799789_m2712634872_MethodInfo_var;
extern const uint32_t MapController_Start_m2162859891_MetadataUsageId;
extern "C"  void MapController_Start_m2162859891 (MapController_t1754731000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MapController_Start_m2162859891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dimensions_t2407799789 * L_0 = Component_GetComponent_TisDimensions_t2407799789_m2712634872(__this, /*hidden argument*/Component_GetComponent_TisDimensions_t2407799789_m2712634872_MethodInfo_var);
		__this->set_dim_7(L_0);
		int32_t L_1 = __this->get_x_2();
		int32_t L_2 = __this->get_y_3();
		int32_t L_3 = __this->get_z_4();
		MapController_CreateMapRoom_m112154365(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapController::CreateMapRoom(System.Int32,System.Int32,System.Int32)
extern Il2CppClass* List_1_t747900261_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2397334390_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRoom_t2553083_m444218626_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2091695206_MethodInfo_var;
extern const MethodInfo* List_1_LastIndexOf_m1716248611_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral231564060;
extern Il2CppCodeGenString* _stringLiteral79145605;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t MapController_CreateMapRoom_m112154365_MetadataUsageId;
extern "C"  void MapController_CreateMapRoom_m112154365 (MapController_t1754731000 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MapController_CreateMapRoom_m112154365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	GameObject_t3674682005 * V_8 = NULL;
	Color32_t598853688  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		List_1_t747900261 * L_0 = (List_1_t747900261 *)il2cpp_codegen_object_new(List_1_t747900261_il2cpp_TypeInfo_var);
		List_1__ctor_m2397334390(L_0, /*hidden argument*/List_1__ctor_m2397334390_MethodInfo_var);
		__this->set_Rooms_6(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_set_name_m1123518500(L_1, _stringLiteral231564060, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0201;
	}

IL_0022:
	{
		V_1 = 0;
		goto IL_01f6;
	}

IL_0029:
	{
		V_2 = 0;
		goto IL_01eb;
	}

IL_0030:
	{
		Dimensions_t2407799789 * L_2 = __this->get_dim_7();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_alto_2();
		V_3 = L_3;
		Dimensions_t2407799789 * L_4 = __this->get_dim_7();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_ancho_3();
		V_4 = L_5;
		Dimensions_t2407799789 * L_6 = __this->get_dim_7();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_fondo_4();
		V_5 = L_7;
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		V_10 = L_9;
		float L_10 = (&V_10)->get_x_1();
		int32_t L_11 = V_0;
		int32_t L_12 = Random_Range_m75452833(NULL /*static, unused*/, 1, 5, /*hidden argument*/NULL);
		Transform_t1659122786 * L_13 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t4282066566  L_14 = Transform_get_position_m2211398607(L_13, /*hidden argument*/NULL);
		V_11 = L_14;
		float L_15 = (&V_11)->get_y_2();
		int32_t L_16 = V_1;
		int32_t L_17 = Random_Range_m75452833(NULL /*static, unused*/, 1, 5, /*hidden argument*/NULL);
		Transform_t1659122786 * L_18 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t4282066566  L_19 = Transform_get_position_m2211398607(L_18, /*hidden argument*/NULL);
		V_12 = L_19;
		float L_20 = (&V_12)->get_z_3();
		int32_t L_21 = V_2;
		int32_t L_22 = Random_Range_m75452833(NULL /*static, unused*/, 1, 5, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_6), ((float)((float)((float)((float)L_10+(float)(((float)((float)L_11)))))+(float)(((float)((float)L_12))))), ((float)((float)((float)((float)L_15+(float)(((float)((float)L_16)))))+(float)(((float)((float)L_17))))), ((float)((float)((float)((float)L_20-(float)(((float)((float)L_21)))))+(float)(((float)((float)L_22))))), /*hidden argument*/NULL);
		int32_t L_23 = V_4;
		int32_t L_24 = V_3;
		int32_t L_25 = V_5;
		Vector3__ctor_m2926210380((&V_7), ((float)((float)(((float)((float)L_23)))*(float)(0.1f))), ((float)((float)(((float)((float)L_24)))*(float)(0.1f))), ((float)((float)(((float)((float)L_25)))*(float)(0.1f))), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_26 = __this->get_RoomObj_5();
		Vector3_t4282066566  L_27 = V_6;
		Quaternion_t1553702882  L_28 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_29 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_8 = ((GameObject_t3674682005 *)IsInstSealed(L_29, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_30 = V_8;
		NullCheck(L_30);
		Room_t2553083 * L_31 = GameObject_GetComponent_TisRoom_t2553083_m444218626(L_30, /*hidden argument*/GameObject_GetComponent_TisRoom_t2553083_m444218626_MethodInfo_var);
		int32_t L_32 = V_4;
		NullCheck(L_31);
		L_31->set_x_2(L_32);
		GameObject_t3674682005 * L_33 = V_8;
		NullCheck(L_33);
		Room_t2553083 * L_34 = GameObject_GetComponent_TisRoom_t2553083_m444218626(L_33, /*hidden argument*/GameObject_GetComponent_TisRoom_t2553083_m444218626_MethodInfo_var);
		int32_t L_35 = V_3;
		NullCheck(L_34);
		L_34->set_y_3(L_35);
		GameObject_t3674682005 * L_36 = V_8;
		NullCheck(L_36);
		Room_t2553083 * L_37 = GameObject_GetComponent_TisRoom_t2553083_m444218626(L_36, /*hidden argument*/GameObject_GetComponent_TisRoom_t2553083_m444218626_MethodInfo_var);
		int32_t L_38 = V_5;
		NullCheck(L_37);
		L_37->set_z_4(L_38);
		GameObject_t3674682005 * L_39 = V_8;
		NullCheck(L_39);
		Transform_t1659122786 * L_40 = GameObject_get_transform_m1278640159(L_39, /*hidden argument*/NULL);
		Vector3_t4282066566  L_41 = V_6;
		NullCheck(L_40);
		Transform_set_position_m3111394108(L_40, L_41, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_42 = V_8;
		NullCheck(L_42);
		Transform_t1659122786 * L_43 = GameObject_get_transform_m1278640159(L_42, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_44 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_rotation_m1525803229(L_43, L_44, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_45 = V_8;
		NullCheck(L_45);
		Transform_t1659122786 * L_46 = GameObject_get_transform_m1278640159(L_45, /*hidden argument*/NULL);
		Vector3_t4282066566  L_47 = V_7;
		NullCheck(L_46);
		Transform_set_localScale_m310756934(L_46, L_47, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_48 = V_8;
		String_t* L_49 = Int32_ToString_m1286526384((&V_0), /*hidden argument*/NULL);
		String_t* L_50 = Int32_ToString_m1286526384((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral79145605, L_49, _stringLiteral32, L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		Object_set_name_m1123518500(L_48, L_51, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_52 = V_8;
		NullCheck(L_52);
		Transform_t1659122786 * L_53 = GameObject_get_transform_m1278640159(L_52, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_54 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_t1659122786 * L_55 = GameObject_get_transform_m1278640159(L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_SetParent_m3449663462(L_53, L_55, /*hidden argument*/NULL);
		int32_t L_56 = V_1;
		int32_t L_57 = V_2;
		Color_t4194546905  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Color__ctor_m103496991(&L_58, (1.0f), ((float)((float)(((float)((float)L_56)))*(float)(0.1f))), ((float)((float)(((float)((float)L_57)))*(float)(0.1f))), /*hidden argument*/NULL);
		Color32_t598853688  L_59 = Color32_op_Implicit_m3684884838(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		V_9 = L_59;
		GameObject_t3674682005 * L_60 = V_8;
		NullCheck(L_60);
		Renderer_t3076687687 * L_61 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_60, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_61);
		Material_t3870600107 * L_62 = Renderer_get_material_m2720864603(L_61, /*hidden argument*/NULL);
		Color32_t598853688  L_63 = V_9;
		Color_t4194546905  L_64 = Color32_op_Implicit_m358459656(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		Material_set_color_m3296857020(L_62, L_64, /*hidden argument*/NULL);
		List_1_t747900261 * L_65 = __this->get_Rooms_6();
		GameObject_t3674682005 * L_66 = V_8;
		NullCheck(L_65);
		List_1_Add_m2091695206(L_65, L_66, /*hidden argument*/List_1_Add_m2091695206_MethodInfo_var);
		GameObject_t3674682005 * L_67 = V_8;
		NullCheck(L_67);
		Room_t2553083 * L_68 = GameObject_GetComponent_TisRoom_t2553083_m444218626(L_67, /*hidden argument*/GameObject_GetComponent_TisRoom_t2553083_m444218626_MethodInfo_var);
		List_1_t747900261 * L_69 = __this->get_Rooms_6();
		GameObject_t3674682005 * L_70 = V_8;
		NullCheck(L_69);
		int32_t L_71 = List_1_LastIndexOf_m1716248611(L_69, L_70, /*hidden argument*/List_1_LastIndexOf_m1716248611_MethodInfo_var);
		NullCheck(L_68);
		L_68->set_index_5(L_71);
		int32_t L_72 = V_2;
		V_2 = ((int32_t)((int32_t)L_72+(int32_t)2));
	}

IL_01eb:
	{
		int32_t L_73 = V_2;
		int32_t L_74 = ___z2;
		if ((((int32_t)L_73) < ((int32_t)L_74)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_75 = V_1;
		V_1 = ((int32_t)((int32_t)L_75+(int32_t)2));
	}

IL_01f6:
	{
		int32_t L_76 = V_1;
		int32_t L_77 = ___y1;
		if ((((int32_t)L_76) < ((int32_t)L_77)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_78 = V_0;
		V_0 = ((int32_t)((int32_t)L_78+(int32_t)2));
	}

IL_0201:
	{
		int32_t L_79 = V_0;
		int32_t L_80 = ___x0;
		if ((((int32_t)L_79) < ((int32_t)L_80)))
		{
			goto IL_0022;
		}
	}
	{
		return;
	}
}
// System.Void MapController::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRoom_t2553083_m444218626_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1446074798;
extern const uint32_t MapController_Update_m2629999354_MetadataUsageId;
extern "C"  void MapController_Update_m2629999354 (MapController_t1754731000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MapController_Update_m2629999354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t3134616544  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GameObject_t3674682005 * V_1 = NULL;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Camera_t2727095145 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_1 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ray_t3134616544  L_2 = Camera_ScreenPointToRay_m1733083890(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Ray_t3134616544  L_3 = V_0;
		RaycastHit_t4003175726 * L_4 = __this->get_address_of_hit_8();
		bool L_5 = Physics_Raycast_m1343340263(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00d6;
		}
	}
	{
		RaycastHit_t4003175726 * L_7 = __this->get_address_of_hit_8();
		Transform_t1659122786 * L_8 = RaycastHit_get_transform_m905149094(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m3709440845(L_8, /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_10 = __this->get_address_of_hit_8();
		Transform_t1659122786 * L_11 = RaycastHit_get_transform_m905149094(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_t3674682005 * L_12 = Component_get_gameObject_m1170635899(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Room_t2553083 * L_13 = GameObject_GetComponent_TisRoom_t2553083_m444218626(L_12, /*hidden argument*/GameObject_GetComponent_TisRoom_t2553083_m444218626_MethodInfo_var);
		NullCheck(L_13);
		int32_t L_14 = L_13->get_index_5();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2809334143(NULL /*static, unused*/, L_9, _stringLiteral1446074798, L_16, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_18 = __this->get_Panel_12();
		RaycastHit_t4003175726 * L_19 = __this->get_address_of_hit_8();
		Transform_t1659122786 * L_20 = RaycastHit_get_transform_m905149094(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t4282066566  L_21 = Transform_get_position_m2211398607(L_20, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_22 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_23 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_18, L_21, L_22, /*hidden argument*/NULL);
		V_1 = ((GameObject_t3674682005 *)IsInstSealed(L_23, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_24 = V_1;
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = GameObject_get_transform_m1278640159(L_24, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_26 = __this->get_Canvas_11();
		NullCheck(L_26);
		Transform_t1659122786 * L_27 = GameObject_get_transform_m1278640159(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_SetParent_m3449663462(L_25, L_27, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_28 = V_1;
		NullCheck(L_28);
		RectTransform_t972643934 * L_29 = GameObject_GetComponent_TisRectTransform_t972643934_m406276429(L_28, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t972643934_m406276429_MethodInfo_var);
		RaycastHit_t4003175726 * L_30 = __this->get_address_of_hit_8();
		Transform_t1659122786 * L_31 = RaycastHit_get_transform_m905149094(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		NullCheck(L_29);
		RectTransform_set_anchoredPosition3D_m3457056443(L_29, L_32, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_33 = V_1;
		NullCheck(L_33);
		Transform_t1659122786 * L_34 = GameObject_get_transform_m1278640159(L_33, /*hidden argument*/NULL);
		Camera_t2727095145 * L_35 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = Component_get_transform_m4257140443(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_LookAt_m2663225588(L_34, L_36, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_37 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00ed;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_38 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_dragOrigin_10(L_38);
		return;
	}

IL_00ed:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_39 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_39)
		{
			goto IL_00f9;
		}
	}
	{
		return;
	}

IL_00f9:
	{
		Camera_t2727095145 * L_40 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_41 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_42 = __this->get_dragOrigin_10();
		Vector3_t4282066566  L_43 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t4282066566  L_44 = Camera_ScreenToViewportPoint_m3727203754(L_40, L_43, /*hidden argument*/NULL);
		V_2 = L_44;
		float L_45 = (&V_2)->get_x_1();
		float L_46 = __this->get_dragSpeed_9();
		float L_47 = (&V_2)->get_y_2();
		float L_48 = __this->get_dragSpeed_9();
		Vector3__ctor_m2926210380((&V_3), ((float)((float)L_45*(float)L_46)), (0.0f), ((float)((float)L_47*(float)L_48)), /*hidden argument*/NULL);
		Transform_t1659122786 * L_49 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_50 = V_3;
		NullCheck(L_49);
		Transform_Translate_m1056984957(L_49, L_50, 0, /*hidden argument*/NULL);
		Transform_t1659122786 * L_51 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_52 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		Vector3_t4282066566  L_53 = Transform_get_position_m2211398607(L_52, /*hidden argument*/NULL);
		Vector3_t4282066566  L_54 = Vector3_ClampMagnitude_m4004286216(NULL /*static, unused*/, L_53, (10.0f), /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_set_position_m3111394108(L_51, L_54, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Move::.ctor()
extern "C"  void Move__ctor_m324221386 (Move_t2404337 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Move::Start()
extern "C"  void Move_Start_m3566326474 (Move_t2404337 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_acceleration_2();
		float L_1 = __this->get_rapidez_8();
		__this->set_accelerationCube_7(((float)((float)L_0*(float)L_1)));
		return;
	}
}
// System.Void Move::setAncho(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3591038228;
extern Il2CppCodeGenString* _stringLiteral1217014403;
extern const uint32_t Move_setAncho_m3968134854_MetadataUsageId;
extern "C"  void Move_setAncho_m3968134854 (Move_t2404337 * __this, int32_t ___ancho0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Move_setAncho_m3968134854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___ancho0;
		__this->set_ancho_18(L_0);
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral3591038228);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3591038228);
		ObjectU5BU5D_t1108656482* L_2 = L_1;
		int32_t L_3 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1217014403);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1217014403);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		int32_t L_8 = __this->get_ancho_18();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3016520001(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Move::SetBoundary(UnityEngine.Vector3)
extern "C"  void Move_SetBoundary_m976619013 (Move_t2404337 * __this, Vector3_t4282066566  ___boundary0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___boundary0;
		__this->set_boundary_11(L_0);
		return;
	}
}
// System.Void Move::move()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t Move_move_m3903435179_MetadataUsageId;
extern "C"  void Move_move_m3903435179 (Move_t2404337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Move_move_m3903435179_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t4003175726  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t4282066566  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t4282066566  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t4282066566  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t4282066566  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector3_t4282066566  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t4282066566  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t4282066566  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_t4282066566  V_20;
	memset(&V_20, 0, sizeof(V_20));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_008a;
		}
	}
	{
		Camera_t2727095145 * L_1 = __this->get_Interface_17();
		int32_t L_2 = __this->get_ancho_18();
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, (((float)((float)L_2))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_4 = Camera_ScreenToWorldPoint_m1572306334(L_1, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Camera_t2727095145 * L_5 = __this->get_Interface_17();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_6 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Ray_t3134616544  L_7 = Camera_ScreenPointToRay_m1733083890(L_5, L_6, /*hidden argument*/NULL);
		bool L_8 = Physics_Raycast_m1235528076(NULL /*static, unused*/, L_7, (&V_0), (100.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_008a;
		}
	}
	{
		Camera_t2727095145 * L_9 = __this->get_Interface_17();
		NullCheck(L_9);
		float L_10 = Camera_get_orthographicSize_m3215515490(L_9, /*hidden argument*/NULL);
		V_2 = ((float)((float)(2.0f)*(float)L_10));
		float L_11 = V_2;
		Camera_t2727095145 * L_12 = __this->get_Interface_17();
		NullCheck(L_12);
		float L_13 = Camera_get_aspect_m4145685929(L_12, /*hidden argument*/NULL);
		V_3 = ((float)((float)L_11*(float)L_13));
		Vector3_t4282066566  L_14 = RaycastHit_get_point_m4165497838((&V_0), /*hidden argument*/NULL);
		V_5 = L_14;
		float L_15 = (&V_5)->get_x_1();
		int32_t L_16 = __this->get_ancho_18();
		float L_17 = V_3;
		V_4 = ((float)((float)((float)((float)L_15*(float)(((float)((float)L_16)))))/(float)L_17));
	}

IL_008a:
	{
		bool L_18 = __this->get_movingLeft_20();
		if (!L_18)
		{
			goto IL_00a8;
		}
	}
	{
		float L_19 = __this->get_traslationH_9();
		float L_20 = __this->get_acceleration_2();
		__this->set_traslationH_9(((float)((float)L_19-(float)L_20)));
	}

IL_00a8:
	{
		bool L_21 = __this->get_movingRight_19();
		if (!L_21)
		{
			goto IL_00c6;
		}
	}
	{
		float L_22 = __this->get_traslationH_9();
		float L_23 = __this->get_acceleration_2();
		__this->set_traslationH_9(((float)((float)L_22+(float)L_23)));
	}

IL_00c6:
	{
		float L_24 = __this->get_traslationH_9();
		float L_25 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_traslationH_9(((float)((float)L_24*(float)L_25)));
		Transform_t1659122786 * L_26 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_27 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t4282066566  L_28 = Transform_get_position_m2211398607(L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		float L_29 = (&V_6)->get_x_1();
		float L_30 = __this->get_traslationH_9();
		Transform_t1659122786 * L_31 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		V_7 = L_32;
		float L_33 = (&V_7)->get_y_2();
		Transform_t1659122786 * L_34 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t4282066566  L_35 = Transform_get_position_m2211398607(L_34, /*hidden argument*/NULL);
		V_8 = L_35;
		float L_36 = (&V_8)->get_z_3();
		Vector3_t4282066566  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Vector3__ctor_m2926210380(&L_37, ((float)((float)L_29+(float)L_30)), L_33, L_36, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m3111394108(L_26, L_37, /*hidden argument*/NULL);
		Transform_t1659122786 * L_38 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t4282066566  L_39 = Transform_get_position_m2211398607(L_38, /*hidden argument*/NULL);
		V_9 = L_39;
		float L_40 = (&V_9)->get_x_1();
		Vector3_t4282066566 * L_41 = __this->get_address_of_boundary_11();
		float L_42 = L_41->get_x_1();
		if ((!(((float)L_40) < ((float)((-L_42))))))
		{
			goto IL_0194;
		}
	}
	{
		Transform_t1659122786 * L_43 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566 * L_44 = __this->get_address_of_boundary_11();
		float L_45 = L_44->get_x_1();
		Transform_t1659122786 * L_46 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t4282066566  L_47 = Transform_get_position_m2211398607(L_46, /*hidden argument*/NULL);
		V_10 = L_47;
		float L_48 = (&V_10)->get_y_2();
		Transform_t1659122786 * L_49 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		Vector3_t4282066566  L_50 = Transform_get_position_m2211398607(L_49, /*hidden argument*/NULL);
		V_11 = L_50;
		float L_51 = (&V_11)->get_z_3();
		Vector3_t4282066566  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector3__ctor_m2926210380(&L_52, ((-L_45)), L_48, L_51, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_position_m3111394108(L_43, L_52, /*hidden argument*/NULL);
	}

IL_0194:
	{
		Transform_t1659122786 * L_53 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_53);
		Vector3_t4282066566  L_54 = Transform_get_position_m2211398607(L_53, /*hidden argument*/NULL);
		V_12 = L_54;
		float L_55 = (&V_12)->get_x_1();
		Vector3_t4282066566 * L_56 = __this->get_address_of_boundary_11();
		float L_57 = L_56->get_x_1();
		if ((!(((float)L_55) > ((float)L_57))))
		{
			goto IL_01fb;
		}
	}
	{
		Transform_t1659122786 * L_58 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566 * L_59 = __this->get_address_of_boundary_11();
		float L_60 = L_59->get_x_1();
		Transform_t1659122786 * L_61 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_61);
		Vector3_t4282066566  L_62 = Transform_get_position_m2211398607(L_61, /*hidden argument*/NULL);
		V_13 = L_62;
		float L_63 = (&V_13)->get_y_2();
		Transform_t1659122786 * L_64 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		Vector3_t4282066566  L_65 = Transform_get_position_m2211398607(L_64, /*hidden argument*/NULL);
		V_14 = L_65;
		float L_66 = (&V_14)->get_z_3();
		Vector3_t4282066566  L_67;
		memset(&L_67, 0, sizeof(L_67));
		Vector3__ctor_m2926210380(&L_67, L_60, L_63, L_66, /*hidden argument*/NULL);
		NullCheck(L_58);
		Transform_set_position_m3111394108(L_58, L_67, /*hidden argument*/NULL);
	}

IL_01fb:
	{
		Transform_t1659122786 * L_68 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_68);
		Vector3_t4282066566  L_69 = Transform_get_position_m2211398607(L_68, /*hidden argument*/NULL);
		V_15 = L_69;
		float L_70 = (&V_15)->get_z_3();
		Vector3_t4282066566 * L_71 = __this->get_address_of_boundary_11();
		float L_72 = L_71->get_z_3();
		if ((!(((float)L_70) < ((float)((-L_72))))))
		{
			goto IL_0264;
		}
	}
	{
		Transform_t1659122786 * L_73 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_74 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_74);
		Vector3_t4282066566  L_75 = Transform_get_position_m2211398607(L_74, /*hidden argument*/NULL);
		V_16 = L_75;
		float L_76 = (&V_16)->get_x_1();
		Transform_t1659122786 * L_77 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_77);
		Vector3_t4282066566  L_78 = Transform_get_position_m2211398607(L_77, /*hidden argument*/NULL);
		V_17 = L_78;
		float L_79 = (&V_17)->get_y_2();
		Vector3_t4282066566 * L_80 = __this->get_address_of_boundary_11();
		float L_81 = L_80->get_z_3();
		Vector3_t4282066566  L_82;
		memset(&L_82, 0, sizeof(L_82));
		Vector3__ctor_m2926210380(&L_82, L_76, L_79, ((-L_81)), /*hidden argument*/NULL);
		NullCheck(L_73);
		Transform_set_position_m3111394108(L_73, L_82, /*hidden argument*/NULL);
	}

IL_0264:
	{
		Transform_t1659122786 * L_83 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_83);
		Vector3_t4282066566  L_84 = Transform_get_position_m2211398607(L_83, /*hidden argument*/NULL);
		V_18 = L_84;
		float L_85 = (&V_18)->get_z_3();
		Vector3_t4282066566 * L_86 = __this->get_address_of_boundary_11();
		float L_87 = L_86->get_z_3();
		if ((!(((float)L_85) > ((float)L_87))))
		{
			goto IL_02d1;
		}
	}
	{
		Transform_t1659122786 * L_88 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_89 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_89);
		Vector3_t4282066566  L_90 = Transform_get_position_m2211398607(L_89, /*hidden argument*/NULL);
		V_19 = L_90;
		float L_91 = (&V_19)->get_x_1();
		Transform_t1659122786 * L_92 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_92);
		Vector3_t4282066566  L_93 = Transform_get_position_m2211398607(L_92, /*hidden argument*/NULL);
		V_20 = L_93;
		float L_94 = (&V_20)->get_y_2();
		Vector3_t4282066566 * L_95 = __this->get_address_of_boundary_11();
		float L_96 = L_95->get_z_3();
		Vector3_t4282066566  L_97;
		memset(&L_97, 0, sizeof(L_97));
		Vector3__ctor_m2926210380(&L_97, L_91, L_94, ((float)((float)L_96/(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_88);
		Transform_set_position_m3111394108(L_88, L_97, /*hidden argument*/NULL);
	}

IL_02d1:
	{
		return;
	}
}
// System.Void Move::Update()
extern "C"  void Move_Update_m3187790467 (Move_t2404337 * __this, const MethodInfo* method)
{
	{
		Move_move_m3903435179(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Move::Left()
extern "C"  void Move_Left_m2948975745 (Move_t2404337 * __this, const MethodInfo* method)
{
	{
		__this->set_movingLeft_20((bool)1);
		return;
	}
}
// System.Void Move::Right()
extern "C"  void Move_Right_m2369145348 (Move_t2404337 * __this, const MethodInfo* method)
{
	{
		__this->set_movingRight_19((bool)1);
		return;
	}
}
// System.Void Move::resetMove()
extern "C"  void Move_resetMove_m3748710728 (Move_t2404337 * __this, const MethodInfo* method)
{
	{
		__this->set_traslationH_9((0.0f));
		__this->set_movingRight_19((bool)0);
		__this->set_movingLeft_20((bool)0);
		return;
	}
}
// System.Void Move::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Move_setCameras_m1043826816 (Move_t2404337 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___Perspe0;
		__this->set_Perspec_14(L_0);
		Camera_t2727095145 * L_1 = ___Ortho1;
		__this->set_Ortho_15(L_1);
		Camera_t2727095145 * L_2 = ___Iso2;
		__this->set_Iso_16(L_2);
		Camera_t2727095145 * L_3 = ___Interface3;
		__this->set_Interface_17(L_3);
		return;
	}
}
// System.Void MoveBall::.ctor()
extern "C"  void MoveBall__ctor_m3909728011 (MoveBall_t4254648720 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveBall::Start()
extern "C"  void MoveBall_Start_m2856865803 (MoveBall_t4254648720 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MoveBall::SetBoundary(UnityEngine.Vector3)
extern "C"  void MoveBall_SetBoundary_m3003401188 (MoveBall_t4254648720 * __this, Vector3_t4282066566  ___boundary0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___boundary0;
		__this->set_boundary_3(L_0);
		return;
	}
}
// System.Void MoveBall::Update()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t MoveBall_Update_m2669346146_MetadataUsageId;
extern "C"  void MoveBall_Update_m2669346146 (MoveBall_t4254648720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MoveBall_Update_m2669346146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		bool L_0 = __this->get_isReady_4();
		if (!L_0)
		{
			goto IL_028b;
		}
	}
	{
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566 * L_3 = (&V_0);
		float L_4 = L_3->get_x_1();
		Vector3_t4282066566 * L_5 = __this->get_address_of_velocidad_2();
		float L_6 = L_5->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_6, (-0.3f), (0.3f), /*hidden argument*/NULL);
		float L_8 = ((float)((float)L_4+(float)L_7));
		V_2 = L_8;
		L_3->set_x_1(L_8);
		float L_9 = V_2;
		Vector3_t4282066566 * L_10 = (&V_0);
		float L_11 = L_10->get_z_3();
		Vector3_t4282066566 * L_12 = __this->get_address_of_velocidad_2();
		float L_13 = L_12->get_z_3();
		float L_14 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_13, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_15 = ((float)((float)L_11+(float)L_14));
		V_2 = L_15;
		L_10->set_z_3(L_15);
		float L_16 = V_2;
		Vector3__ctor_m2926210380((&V_1), L_9, (0.0f), L_16, /*hidden argument*/NULL);
		Transform_t1659122786 * L_17 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_18 = V_1;
		NullCheck(L_17);
		Transform_set_position_m3111394108(L_17, L_18, /*hidden argument*/NULL);
		Transform_t1659122786 * L_19 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = Transform_get_position_m2211398607(L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = (&V_3)->get_x_1();
		Vector3_t4282066566 * L_22 = __this->get_address_of_boundary_3();
		float L_23 = L_22->get_x_1();
		if ((!(((float)L_21) < ((float)((-L_23))))))
		{
			goto IL_00db;
		}
	}
	{
		Vector3_t4282066566 * L_24 = __this->get_address_of_velocidad_2();
		float L_25 = L_24->get_x_1();
		Vector3_t4282066566 * L_26 = __this->get_address_of_velocidad_2();
		float L_27 = L_26->get_y_2();
		Vector3_t4282066566 * L_28 = __this->get_address_of_velocidad_2();
		float L_29 = L_28->get_z_3();
		Vector3_t4282066566  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m2926210380(&L_30, ((float)((float)L_25*(float)(-1.0f))), L_27, L_29, /*hidden argument*/NULL);
		__this->set_velocidad_2(L_30);
	}

IL_00db:
	{
		Transform_t1659122786 * L_31 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		float L_33 = (&V_4)->get_x_1();
		Vector3_t4282066566 * L_34 = __this->get_address_of_boundary_3();
		float L_35 = L_34->get_x_1();
		if ((!(((float)L_33) > ((float)L_35))))
		{
			goto IL_0131;
		}
	}
	{
		Vector3_t4282066566 * L_36 = __this->get_address_of_velocidad_2();
		float L_37 = L_36->get_x_1();
		Vector3_t4282066566 * L_38 = __this->get_address_of_velocidad_2();
		float L_39 = L_38->get_y_2();
		Vector3_t4282066566 * L_40 = __this->get_address_of_velocidad_2();
		float L_41 = L_40->get_z_3();
		Vector3_t4282066566  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Vector3__ctor_m2926210380(&L_42, ((float)((float)L_37*(float)(-1.0f))), L_39, L_41, /*hidden argument*/NULL);
		__this->set_velocidad_2(L_42);
	}

IL_0131:
	{
		Transform_t1659122786 * L_43 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_t4282066566  L_44 = Transform_get_position_m2211398607(L_43, /*hidden argument*/NULL);
		V_5 = L_44;
		float L_45 = (&V_5)->get_y_2();
		Vector3_t4282066566 * L_46 = __this->get_address_of_boundary_3();
		float L_47 = L_46->get_y_2();
		if ((!(((float)L_45) < ((float)((-L_47))))))
		{
			goto IL_0188;
		}
	}
	{
		Vector3_t4282066566 * L_48 = __this->get_address_of_velocidad_2();
		float L_49 = L_48->get_x_1();
		Vector3_t4282066566 * L_50 = __this->get_address_of_velocidad_2();
		float L_51 = L_50->get_y_2();
		Vector3_t4282066566 * L_52 = __this->get_address_of_velocidad_2();
		float L_53 = L_52->get_z_3();
		Vector3_t4282066566  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Vector3__ctor_m2926210380(&L_54, L_49, ((float)((float)L_51*(float)(-1.0f))), L_53, /*hidden argument*/NULL);
		__this->set_velocidad_2(L_54);
	}

IL_0188:
	{
		Transform_t1659122786 * L_55 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Vector3_t4282066566  L_56 = Transform_get_position_m2211398607(L_55, /*hidden argument*/NULL);
		V_6 = L_56;
		float L_57 = (&V_6)->get_y_2();
		Vector3_t4282066566 * L_58 = __this->get_address_of_boundary_3();
		float L_59 = L_58->get_y_2();
		if ((!(((float)L_57) > ((float)L_59))))
		{
			goto IL_01de;
		}
	}
	{
		Vector3_t4282066566 * L_60 = __this->get_address_of_velocidad_2();
		float L_61 = L_60->get_x_1();
		Vector3_t4282066566 * L_62 = __this->get_address_of_velocidad_2();
		float L_63 = L_62->get_y_2();
		Vector3_t4282066566 * L_64 = __this->get_address_of_velocidad_2();
		float L_65 = L_64->get_z_3();
		Vector3_t4282066566  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Vector3__ctor_m2926210380(&L_66, L_61, ((float)((float)L_63*(float)(-1.0f))), L_65, /*hidden argument*/NULL);
		__this->set_velocidad_2(L_66);
	}

IL_01de:
	{
		Transform_t1659122786 * L_67 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		Vector3_t4282066566  L_68 = Transform_get_position_m2211398607(L_67, /*hidden argument*/NULL);
		V_7 = L_68;
		float L_69 = (&V_7)->get_z_3();
		Vector3_t4282066566 * L_70 = __this->get_address_of_boundary_3();
		float L_71 = L_70->get_z_3();
		if ((!(((float)L_69) < ((float)((-L_71))))))
		{
			goto IL_0235;
		}
	}
	{
		Vector3_t4282066566 * L_72 = __this->get_address_of_velocidad_2();
		float L_73 = L_72->get_x_1();
		Vector3_t4282066566 * L_74 = __this->get_address_of_velocidad_2();
		float L_75 = L_74->get_y_2();
		Vector3_t4282066566 * L_76 = __this->get_address_of_velocidad_2();
		float L_77 = L_76->get_z_3();
		Vector3_t4282066566  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector3__ctor_m2926210380(&L_78, L_73, L_75, ((float)((float)L_77*(float)(-1.0f))), /*hidden argument*/NULL);
		__this->set_velocidad_2(L_78);
	}

IL_0235:
	{
		Transform_t1659122786 * L_79 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_79);
		Vector3_t4282066566  L_80 = Transform_get_position_m2211398607(L_79, /*hidden argument*/NULL);
		V_8 = L_80;
		float L_81 = (&V_8)->get_z_3();
		Vector3_t4282066566 * L_82 = __this->get_address_of_boundary_3();
		float L_83 = L_82->get_z_3();
		if ((!(((float)L_81) > ((float)L_83))))
		{
			goto IL_028b;
		}
	}
	{
		Vector3_t4282066566 * L_84 = __this->get_address_of_velocidad_2();
		float L_85 = L_84->get_x_1();
		Vector3_t4282066566 * L_86 = __this->get_address_of_velocidad_2();
		float L_87 = L_86->get_y_2();
		Vector3_t4282066566 * L_88 = __this->get_address_of_velocidad_2();
		float L_89 = L_88->get_z_3();
		Vector3_t4282066566  L_90;
		memset(&L_90, 0, sizeof(L_90));
		Vector3__ctor_m2926210380(&L_90, L_85, L_87, ((float)((float)L_89*(float)(-1.0f))), /*hidden argument*/NULL);
		__this->set_velocidad_2(L_90);
	}

IL_028b:
	{
		return;
	}
}
// System.Void Plants::.ctor()
extern "C"  void Plants__ctor_m2365783571 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Plants::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Plants_setCameras_m1784206025 (Plants_t2393071496 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___Perspe0;
		__this->set_Perspec_7(L_0);
		Camera_t2727095145 * L_1 = ___Ortho1;
		__this->set_Ortho_8(L_1);
		Camera_t2727095145 * L_2 = ___Iso2;
		__this->set_Iso_9(L_2);
		Camera_t2727095145 * L_3 = ___Interface3;
		__this->set_Interface_10(L_3);
		return;
	}
}
// System.Void Plants::SetWeather(System.Int32)
extern "C"  void Plants_SetWeather_m4171024980 (Plants_t2393071496 * __this, int32_t ___fase0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___fase0;
		Plants_GenerateLandscape_m3010235720(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Plants::CreateSet(System.Int32,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1170771901;
extern Il2CppCodeGenString* _stringLiteral80238310;
extern Il2CppCodeGenString* _stringLiteral64452641;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral3312290682;
extern const uint32_t Plants_CreateSet_m585282447_MetadataUsageId;
extern "C"  void Plants_CreateSet_m585282447 (Plants_t2393071496 * __this, int32_t ___fase0, int32_t ___room1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreateSet_m585282447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Quaternion_t1553702882  V_4;
	memset(&V_4, 0, sizeof(V_4));
	GameObject_t3674682005 * V_5 = NULL;
	RaycastHit_t4003175726  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Bounds_t2711641849  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Bounds_t2711641849  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t4282066566  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Bounds_t2711641849  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t4282066566  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Bounds_t2711641849  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t4282066566  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		Renderer_t3076687687 * L_0 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_0);
		Bounds_t2711641849  L_1 = Renderer_get_bounds_m1533373851(L_0, /*hidden argument*/NULL);
		V_7 = L_1;
		Vector3_t4282066566  L_2 = Bounds_get_size_m3666348432((&V_7), /*hidden argument*/NULL);
		V_8 = L_2;
		float L_3 = (&V_8)->get_x_1();
		V_0 = ((int32_t)((int32_t)0-(int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)L_3)))/(int32_t)2))));
		goto IL_01e3;
	}

IL_0028:
	{
		Renderer_t3076687687 * L_4 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_4);
		Bounds_t2711641849  L_5 = Renderer_get_bounds_m1533373851(L_4, /*hidden argument*/NULL);
		V_9 = L_5;
		Vector3_t4282066566  L_6 = Bounds_get_size_m3666348432((&V_9), /*hidden argument*/NULL);
		V_10 = L_6;
		float L_7 = (&V_10)->get_z_3();
		V_1 = ((int32_t)((int32_t)0-(int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)L_7)))/(int32_t)2))));
		goto IL_01b5;
	}

IL_0050:
	{
		int32_t L_8 = Random_Range_m75452833(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_01b1;
		}
	}
	{
		int32_t L_10 = V_0;
		int32_t L_11 = Random_Range_m75452833(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		Transform_t1659122786 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		V_11 = L_13;
		float L_14 = (&V_11)->get_y_2();
		int32_t L_15 = V_1;
		int32_t L_16 = Random_Range_m75452833(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_3), (((float)((float)((int32_t)((int32_t)L_10+(int32_t)L_11))))), ((float)((float)L_14+(float)(1.0f))), (((float)((float)((int32_t)((int32_t)L_15+(int32_t)L_16))))), /*hidden argument*/NULL);
		int32_t L_17 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)359), /*hidden argument*/NULL);
		Vector3_t4282066566  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, (0.0f), (((float)((float)L_17))), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_19 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		Vector3_t4282066566  L_20 = V_3;
		Quaternion_t1553702882  L_21 = V_4;
		int32_t L_22 = ___fase0;
		GameObject_t3674682005 * L_23 = Plants_CreatePlantsItems_m3446816263(__this, L_20, L_21, L_22, /*hidden argument*/NULL);
		V_5 = L_23;
		GameObject_t3674682005 * L_24 = V_5;
		NullCheck(L_24);
		Renderer_t3076687687 * L_25 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_24, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		Material_t3870600107 * L_26 = Plants_GetMaterial_m1752811087(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Renderer_set_material_m1012580896(L_25, L_26, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_27 = V_5;
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		Transform_t1659122786 * L_29 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_parent_m3231272063(L_28, L_29, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_30 = V_5;
		NullCheck(L_30);
		GameObject_set_tag_m859036203(L_30, _stringLiteral1170771901, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_31 = V_5;
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = GameObject_get_transform_m1278640159(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t4282066566  L_33 = Transform_get_position_m2211398607(L_32, /*hidden argument*/NULL);
		Vector3_t4282066566  L_34 = Vector3_get_down_m1397301612(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_35 = Physics_Raycast_m2482317716(NULL /*static, unused*/, L_33, L_34, (&V_6), /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_01b1;
		}
	}
	{
		GameObject_t3674682005 * L_36 = V_5;
		NullCheck(L_36);
		Transform_t1659122786 * L_37 = GameObject_get_transform_m1278640159(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t4282066566  L_38 = Transform_get_position_m2211398607(L_37, /*hidden argument*/NULL);
		Vector3_t4282066566  L_39 = Vector3_get_down_m1397301612(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t4194546905  L_40 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_DrawRay_m123146114(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		Collider_t2939674232 * L_41 = RaycastHit_get_collider_m3116882274((&V_6), /*hidden argument*/NULL);
		NullCheck(L_41);
		GameObject_t3674682005 * L_42 = Component_get_gameObject_m1170635899(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		String_t* L_43 = GameObject_get_tag_m211612200(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_44 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_43, _stringLiteral80238310, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_01aa;
		}
	}
	{
		Collider_t2939674232 * L_45 = RaycastHit_get_collider_m3116882274((&V_6), /*hidden argument*/NULL);
		NullCheck(L_45);
		GameObject_t3674682005 * L_46 = Component_get_gameObject_m1170635899(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		String_t* L_47 = GameObject_get_tag_m211612200(L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_48 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_47, _stringLiteral64452641, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01aa;
		}
	}
	{
		Collider_t2939674232 * L_49 = RaycastHit_get_collider_m3116882274((&V_6), /*hidden argument*/NULL);
		NullCheck(L_49);
		GameObject_t3674682005 * L_50 = Component_get_gameObject_m1170635899(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		String_t* L_51 = GameObject_get_tag_m211612200(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_51, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (L_52)
		{
			goto IL_01aa;
		}
	}
	{
		Collider_t2939674232 * L_53 = RaycastHit_get_collider_m3116882274((&V_6), /*hidden argument*/NULL);
		NullCheck(L_53);
		GameObject_t3674682005 * L_54 = Component_get_gameObject_m1170635899(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		String_t* L_55 = GameObject_get_tag_m211612200(L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_56 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_55, _stringLiteral3312290682, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01b1;
		}
	}

IL_01aa:
	{
		GameObject_t3674682005 * L_57 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
	}

IL_01b1:
	{
		int32_t L_58 = V_1;
		V_1 = ((int32_t)((int32_t)L_58+(int32_t)4));
	}

IL_01b5:
	{
		int32_t L_59 = V_1;
		Renderer_t3076687687 * L_60 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_60);
		Bounds_t2711641849  L_61 = Renderer_get_bounds_m1533373851(L_60, /*hidden argument*/NULL);
		V_12 = L_61;
		Vector3_t4282066566  L_62 = Bounds_get_size_m3666348432((&V_12), /*hidden argument*/NULL);
		V_13 = L_62;
		float L_63 = (&V_13)->get_z_3();
		if ((((float)(((float)((float)L_59)))) < ((float)((float)((float)L_63/(float)(2.0f))))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_64 = V_0;
		V_0 = ((int32_t)((int32_t)L_64+(int32_t)4));
	}

IL_01e3:
	{
		int32_t L_65 = V_0;
		Renderer_t3076687687 * L_66 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_66);
		Bounds_t2711641849  L_67 = Renderer_get_bounds_m1533373851(L_66, /*hidden argument*/NULL);
		V_14 = L_67;
		Vector3_t4282066566  L_68 = Bounds_get_size_m3666348432((&V_14), /*hidden argument*/NULL);
		V_15 = L_68;
		float L_69 = (&V_15)->get_x_1();
		if ((((float)(((float)((float)L_65)))) < ((float)((float)((float)L_69/(float)(2.0f))))))
		{
			goto IL_0028;
		}
	}
	{
		return;
	}
}
// UnityEngine.GameObject Plants::CreatePlantsItems(UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const uint32_t Plants_CreatePlantsItems_m3446816263_MetadataUsageId;
extern "C"  GameObject_t3674682005 * Plants_CreatePlantsItems_m3446816263 (Plants_t2393071496 * __this, Vector3_t4282066566  ___pos0, Quaternion_t1553702882  ___rot1, int32_t ___fase2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreatePlantsItems_m3446816263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	GameObject_t3674682005 * V_2 = NULL;
	GameObject_t3674682005 * V_3 = NULL;
	GameObject_t3674682005 * V_4 = NULL;
	GameObject_t3674682005 * V_5 = NULL;
	GameObject_t3674682005 * V_6 = NULL;
	GameObject_t3674682005 * V_7 = NULL;
	{
		int32_t L_0 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0029;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_1 = __this->get_plantas_3();
		int32_t L_2 = Random_Range_m75452833(NULL /*static, unused*/, 0, 4, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		GameObject_t3674682005 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		Vector3_t4282066566  L_5 = ___pos0;
		Quaternion_t1553702882  L_6 = ___rot1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_7 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = ((GameObject_t3674682005 *)IsInstSealed(L_7, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_8 = V_0;
		return L_8;
	}

IL_0029:
	{
		int32_t L_9 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_9) == ((uint32_t)6))))
		{
			goto IL_0059;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_10 = __this->get_plantas_3();
		MaterialU5BU5D_t170856778* L_11 = __this->get_materiales_2();
		NullCheck(L_11);
		int32_t L_12 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_12);
		int32_t L_13 = L_12;
		GameObject_t3674682005 * L_14 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		Vector3_t4282066566  L_15 = ___pos0;
		Quaternion_t1553702882  L_16 = ___rot1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_17 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_1 = ((GameObject_t3674682005 *)IsInstSealed(L_17, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_18 = V_1;
		return L_18;
	}

IL_0059:
	{
		int32_t L_19 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_19) == ((uint32_t)4))))
		{
			goto IL_007c;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_20 = __this->get_plantas_3();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 8);
		int32_t L_21 = 8;
		GameObject_t3674682005 * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		Vector3_t4282066566  L_23 = ___pos0;
		Quaternion_t1553702882  L_24 = ___rot1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_25 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_2 = ((GameObject_t3674682005 *)IsInstSealed(L_25, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_26 = V_2;
		return L_26;
	}

IL_007c:
	{
		int32_t L_27 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_27) == ((uint32_t)5))))
		{
			goto IL_00a5;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_28 = __this->get_plantas_3();
		int32_t L_29 = Random_Range_m75452833(NULL /*static, unused*/, 2, 4, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		GameObject_t3674682005 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		Vector3_t4282066566  L_32 = ___pos0;
		Quaternion_t1553702882  L_33 = ___rot1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_34 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/NULL);
		V_3 = ((GameObject_t3674682005 *)IsInstSealed(L_34, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_35 = V_3;
		return L_35;
	}

IL_00a5:
	{
		int32_t L_36 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_36) == ((uint32_t)2))))
		{
			goto IL_00d7;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_37 = __this->get_plantas_3();
		MaterialU5BU5D_t170856778* L_38 = __this->get_materiales_2();
		NullCheck(L_38);
		int32_t L_39 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_39);
		int32_t L_40 = L_39;
		GameObject_t3674682005 * L_41 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		Vector3_t4282066566  L_42 = ___pos0;
		Quaternion_t1553702882  L_43 = ___rot1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_44 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_41, L_42, L_43, /*hidden argument*/NULL);
		V_4 = ((GameObject_t3674682005 *)IsInstSealed(L_44, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_45 = V_4;
		return L_45;
	}

IL_00d7:
	{
		int32_t L_46 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_46) == ((uint32_t)1))))
		{
			goto IL_0103;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_47 = __this->get_plantas_3();
		int32_t L_48 = Random_Range_m75452833(NULL /*static, unused*/, 8, ((int32_t)11), /*hidden argument*/NULL);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		int32_t L_49 = L_48;
		GameObject_t3674682005 * L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		Vector3_t4282066566  L_51 = ___pos0;
		Quaternion_t1553702882  L_52 = ___rot1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_53 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_50, L_51, L_52, /*hidden argument*/NULL);
		V_5 = ((GameObject_t3674682005 *)IsInstSealed(L_53, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_54 = V_5;
		return L_54;
	}

IL_0103:
	{
		int32_t L_55 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_55) == ((uint32_t)7))))
		{
			goto IL_012e;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_56 = __this->get_plantas_3();
		int32_t L_57 = Random_Range_m75452833(NULL /*static, unused*/, 4, 5, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		int32_t L_58 = L_57;
		GameObject_t3674682005 * L_59 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		Vector3_t4282066566  L_60 = ___pos0;
		Quaternion_t1553702882  L_61 = ___rot1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_62 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_59, L_60, L_61, /*hidden argument*/NULL);
		V_6 = ((GameObject_t3674682005 *)IsInstSealed(L_62, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_63 = V_6;
		return L_63;
	}

IL_012e:
	{
		GameObjectU5BU5D_t2662109048* L_64 = __this->get_plantas_3();
		Int32U5BU5D_t3230847821* L_65 = __this->get_indicesEntrada_4();
		int32_t L_66 = ___fase2;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, L_66);
		int32_t L_67 = L_66;
		int32_t L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		Int32U5BU5D_t3230847821* L_69 = __this->get_indicesSalida_5();
		int32_t L_70 = ___fase2;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		int32_t L_71 = L_70;
		int32_t L_72 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		int32_t L_73 = Random_Range_m75452833(NULL /*static, unused*/, L_68, L_72, /*hidden argument*/NULL);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_73);
		int32_t L_74 = L_73;
		GameObject_t3674682005 * L_75 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		Vector3_t4282066566  L_76 = ___pos0;
		Quaternion_t1553702882  L_77 = ___rot1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_78 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_75, L_76, L_77, /*hidden argument*/NULL);
		V_7 = ((GameObject_t3674682005 *)IsInstSealed(L_78, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_79 = V_7;
		return L_79;
	}
}
// UnityEngine.Material Plants::GetMaterial()
extern const MethodInfo* Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var;
extern const uint32_t Plants_GetMaterial_m1752811087_MetadataUsageId;
extern "C"  Material_t3870600107 * Plants_GetMaterial_m1752811087 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_GetMaterial_m1752811087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t3870600107 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		MaterialU5BU5D_t170856778* L_0 = __this->get_materiales_2();
		MaterialU5BU5D_t170856778* L_1 = __this->get_materiales_2();
		NullCheck(L_1);
		int32_t L_2 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_2);
		int32_t L_3 = L_2;
		Material_t3870600107 * L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		int32_t L_5 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_0075;
		}
	}
	{
		MaterialU5BU5D_t170856778* L_6 = __this->get_materiales_2();
		MaterialU5BU5D_t170856778* L_7 = __this->get_materiales_2();
		NullCheck(L_7);
		int32_t L_8 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
		int32_t L_9 = L_8;
		Material_t3870600107 * L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		Camera_t2727095145 * L_11 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_12 = __this->get_colors_11();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		NullCheck(L_11);
		Camera_set_backgroundColor_m501006344(L_11, (*(Color_t4194546905 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))), /*hidden argument*/NULL);
		Renderer_t3076687687 * L_13 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_13);
		Material_t3870600107 * L_14 = Renderer_get_material_m2720864603(L_13, /*hidden argument*/NULL);
		ColorU5BU5D_t2441545636* L_15 = __this->get_colors_11();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 6);
		NullCheck(L_14);
		Material_set_color_m3296857020(L_14, (*(Color_t4194546905 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))), /*hidden argument*/NULL);
	}

IL_0075:
	{
		int32_t L_16 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_16) == ((uint32_t)6))))
		{
			goto IL_00c7;
		}
	}
	{
		MaterialU5BU5D_t170856778* L_17 = __this->get_materiales_2();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		Material_t3870600107 * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_0 = L_19;
		Camera_t2727095145 * L_20 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_21 = __this->get_colors_11();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 5);
		NullCheck(L_20);
		Camera_set_backgroundColor_m501006344(L_20, (*(Color_t4194546905 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)))), /*hidden argument*/NULL);
		Renderer_t3076687687 * L_22 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_22);
		Material_t3870600107 * L_23 = Renderer_get_material_m2720864603(L_22, /*hidden argument*/NULL);
		ColorU5BU5D_t2441545636* L_24 = __this->get_colors_11();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 5);
		NullCheck(L_23);
		Material_set_color_m3296857020(L_23, (*(Color_t4194546905 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)))), /*hidden argument*/NULL);
	}

IL_00c7:
	{
		int32_t L_25 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_25) == ((uint32_t)4))))
		{
			goto IL_0134;
		}
	}
	{
		MaterialU5BU5D_t170856778* L_26 = __this->get_materiales_2();
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		int32_t L_27 = 0;
		Material_t3870600107 * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		V_0 = L_28;
		int32_t L_29 = Random_Range_m75452833(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		V_1 = L_29;
		Plants_CreateReflejosMar_m2151477137(__this, /*hidden argument*/NULL);
		int32_t L_30 = V_1;
		if ((((int32_t)L_30) <= ((int32_t)(-1))))
		{
			goto IL_00f7;
		}
	}
	{
		Plants_CreateNiebla_m3669633466(__this, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		Camera_t2727095145 * L_31 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_32 = __this->get_colors_11();
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 4);
		NullCheck(L_31);
		Camera_set_backgroundColor_m501006344(L_31, (*(Color_t4194546905 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))), /*hidden argument*/NULL);
		Renderer_t3076687687 * L_33 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_33);
		Material_t3870600107 * L_34 = Renderer_get_material_m2720864603(L_33, /*hidden argument*/NULL);
		ColorU5BU5D_t2441545636* L_35 = __this->get_colors_11();
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 4);
		NullCheck(L_34);
		Material_set_color_m3296857020(L_34, (*(Color_t4194546905 *)((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))), /*hidden argument*/NULL);
	}

IL_0134:
	{
		int32_t L_36 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_36) == ((uint32_t)5))))
		{
			goto IL_0186;
		}
	}
	{
		MaterialU5BU5D_t170856778* L_37 = __this->get_materiales_2();
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 6);
		int32_t L_38 = 6;
		Material_t3870600107 * L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		V_0 = L_39;
		Camera_t2727095145 * L_40 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_41 = __this->get_colors_11();
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 6);
		NullCheck(L_40);
		Camera_set_backgroundColor_m501006344(L_40, (*(Color_t4194546905 *)((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))), /*hidden argument*/NULL);
		Renderer_t3076687687 * L_42 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_42);
		Material_t3870600107 * L_43 = Renderer_get_material_m2720864603(L_42, /*hidden argument*/NULL);
		ColorU5BU5D_t2441545636* L_44 = __this->get_colors_11();
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 6);
		NullCheck(L_43);
		Material_set_color_m3296857020(L_43, (*(Color_t4194546905 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))), /*hidden argument*/NULL);
	}

IL_0186:
	{
		int32_t L_45 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_45) == ((uint32_t)2))))
		{
			goto IL_01e4;
		}
	}
	{
		MaterialU5BU5D_t170856778* L_46 = __this->get_materiales_2();
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 8);
		int32_t L_47 = 8;
		Material_t3870600107 * L_48 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		V_0 = L_48;
		Camera_t2727095145 * L_49 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_50 = __this->get_colors_11();
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 0);
		NullCheck(L_49);
		Camera_set_backgroundColor_m501006344(L_49, (*(Color_t4194546905 *)((L_50)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		Renderer_t3076687687 * L_51 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_51);
		Material_t3870600107 * L_52 = Renderer_get_material_m2720864603(L_51, /*hidden argument*/NULL);
		ColorU5BU5D_t2441545636* L_53 = __this->get_colors_11();
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
		NullCheck(L_52);
		Material_set_color_m3296857020(L_52, (*(Color_t4194546905 *)((L_53)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		Plants_CreateLluvia_m3322971302(__this, /*hidden argument*/NULL);
		Plants_CreateTrueno_m1100102140(__this, /*hidden argument*/NULL);
	}

IL_01e4:
	{
		int32_t L_54 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_54) == ((uint32_t)1))))
		{
			goto IL_0236;
		}
	}
	{
		MaterialU5BU5D_t170856778* L_55 = __this->get_materiales_2();
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 7);
		int32_t L_56 = 7;
		Material_t3870600107 * L_57 = (L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		V_0 = L_57;
		Camera_t2727095145 * L_58 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_59 = __this->get_colors_11();
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 7);
		NullCheck(L_58);
		Camera_set_backgroundColor_m501006344(L_58, (*(Color_t4194546905 *)((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(7)))), /*hidden argument*/NULL);
		Renderer_t3076687687 * L_60 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_60);
		Material_t3870600107 * L_61 = Renderer_get_material_m2720864603(L_60, /*hidden argument*/NULL);
		ColorU5BU5D_t2441545636* L_62 = __this->get_colors_11();
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 7);
		NullCheck(L_61);
		Material_set_color_m3296857020(L_61, (*(Color_t4194546905 *)((L_62)->GetAddressAt(static_cast<il2cpp_array_size_t>(7)))), /*hidden argument*/NULL);
	}

IL_0236:
	{
		int32_t L_63 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_63) == ((uint32_t)7))))
		{
			goto IL_0295;
		}
	}
	{
		MaterialU5BU5D_t170856778* L_64 = __this->get_materiales_2();
		MaterialU5BU5D_t170856778* L_65 = __this->get_materiales_2();
		NullCheck(L_65);
		int32_t L_66 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_65)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_66);
		int32_t L_67 = L_66;
		Material_t3870600107 * L_68 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		V_0 = L_68;
		Camera_t2727095145 * L_69 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_70 = __this->get_colors_11();
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 8);
		NullCheck(L_69);
		Camera_set_backgroundColor_m501006344(L_69, (*(Color_t4194546905 *)((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(8)))), /*hidden argument*/NULL);
		Renderer_t3076687687 * L_71 = Component_GetComponent_TisRenderer_t3076687687_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t3076687687_m500377675_MethodInfo_var);
		NullCheck(L_71);
		Material_t3870600107 * L_72 = Renderer_get_material_m2720864603(L_71, /*hidden argument*/NULL);
		ColorU5BU5D_t2441545636* L_73 = __this->get_colors_11();
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 8);
		NullCheck(L_72);
		Material_set_color_m3296857020(L_72, (*(Color_t4194546905 *)((L_73)->GetAddressAt(static_cast<il2cpp_array_size_t>(8)))), /*hidden argument*/NULL);
	}

IL_0295:
	{
		Material_t3870600107 * L_74 = V_0;
		return L_74;
	}
}
// System.Void Plants::DestroyWeatherElements()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3249282829;
extern Il2CppCodeGenString* _stringLiteral3195290873;
extern Il2CppCodeGenString* _stringLiteral790436604;
extern Il2CppCodeGenString* _stringLiteral3429849039;
extern Il2CppCodeGenString* _stringLiteral1385283029;
extern const uint32_t Plants_DestroyWeatherElements_m997798466_MetadataUsageId;
extern "C"  void Plants_DestroyWeatherElements_m997798466 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_DestroyWeatherElements_m997798466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral3249282829, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral3195290873, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral790436604, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral3429849039, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_4 = GameObject_FindGameObjectWithTag_m2635560165(NULL /*static, unused*/, _stringLiteral1385283029, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_5 = __this->get_nieve_20();
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0067;
		}
	}
	{
		GameObject_t3674682005 * L_7 = __this->get_nieve_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void Plants::CreateLineasFlotacion()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1385283029;
extern const uint32_t Plants_CreateLineasFlotacion_m3334133290_MetadataUsageId;
extern "C"  void Plants_CreateLineasFlotacion_m3334133290 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreateLineasFlotacion_m3334133290_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_lineasFlotacion_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_LineasFlotacionObj_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_3 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		__this->set_lineasFlotacion_24(L_3);
		GameObject_t3674682005 * L_4 = __this->get_lineasFlotacion_24();
		NullCheck(L_4);
		GameObject_set_tag_m859036203(L_4, _stringLiteral1385283029, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Plants::CreateNiebla()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3249282829;
extern const uint32_t Plants_CreateNiebla_m3669633466_MetadataUsageId;
extern "C"  void Plants_CreateNiebla_m3669633466 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreateNiebla_m3669633466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_niebla_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_NieblaObj_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_3 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		__this->set_niebla_21(L_3);
		GameObject_t3674682005 * L_4 = __this->get_niebla_21();
		NullCheck(L_4);
		GameObject_set_tag_m859036203(L_4, _stringLiteral3249282829, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Plants::CreateNieve()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern const uint32_t Plants_CreateNieve_m3582645902_MetadataUsageId;
extern "C"  void Plants_CreateNieve_m3582645902 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreateNieve_m3582645902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_nieve_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_NieveObj_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_3 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		__this->set_nieve_20(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void Plants::CreateLluvia()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3195290873;
extern const uint32_t Plants_CreateLluvia_m3322971302_MetadataUsageId;
extern "C"  void Plants_CreateLluvia_m3322971302 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreateLluvia_m3322971302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_lluvia_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_LluviaObj_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_3 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		__this->set_lluvia_19(L_3);
		GameObject_t3674682005 * L_4 = __this->get_lluvia_19();
		NullCheck(L_4);
		GameObject_set_tag_m859036203(L_4, _stringLiteral3195290873, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Plants::CreateReflejosMar()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral790436604;
extern const uint32_t Plants_CreateReflejosMar_m2151477137_MetadataUsageId;
extern "C"  void Plants_CreateReflejosMar_m2151477137 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreateReflejosMar_m2151477137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_reflejosMar_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_ReflejosMarObj_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_3 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		__this->set_reflejosMar_22(L_3);
		GameObject_t3674682005 * L_4 = __this->get_reflejosMar_22();
		NullCheck(L_4);
		GameObject_set_tag_m859036203(L_4, _stringLiteral790436604, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Plants::CreateTrueno()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3429849039;
extern const uint32_t Plants_CreateTrueno_m1100102140_MetadataUsageId;
extern "C"  void Plants_CreateTrueno_m1100102140 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreateTrueno_m1100102140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_trueno_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t3674682005 * L_2 = __this->get_TruenoObj_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_3 = Object_Instantiate_TisGameObject_t3674682005_m3917608929(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t3674682005_m3917608929_MethodInfo_var);
		__this->set_trueno_23(L_3);
		GameObject_t3674682005 * L_4 = __this->get_trueno_23();
		NullCheck(L_4);
		GameObject_set_tag_m859036203(L_4, _stringLiteral3429849039, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Plants::GenerateLandscape(System.Int32)
extern Il2CppClass* Tiempo_t1322696995_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2061439202;
extern Il2CppCodeGenString* _stringLiteral2888093652;
extern const uint32_t Plants_GenerateLandscape_m3010235720_MetadataUsageId;
extern "C"  void Plants_GenerateLandscape_m3010235720 (Plants_t2393071496 * __this, int32_t ___fase0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_GenerateLandscape_m3010235720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObjectU5BU5D_t2662109048* V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	GameObjectU5BU5D_t2662109048* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		Camera_t2727095145 * L_0 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_1 = __this->get_colors_11();
		int32_t L_2 = ___fase0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		NullCheck(L_0);
		Camera_set_backgroundColor_m501006344(L_0, (*(Color_t4194546905 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))), /*hidden argument*/NULL);
		Plants_DestroyWeatherElements_m997798466(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___fase0;
		__this->set_currentTiempo_12(L_3);
		int32_t L_4 = __this->get_currentTiempo_12();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Tiempo_t1322696995_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2061439202, L_6, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_8 = GameObject_FindGameObjectsWithTag_m3058873418(NULL /*static, unused*/, _stringLiteral2888093652, /*hidden argument*/NULL);
		V_0 = L_8;
		GameObjectU5BU5D_t2662109048* L_9 = V_0;
		V_2 = L_9;
		V_3 = 0;
		goto IL_0065;
	}

IL_0057:
	{
		GameObjectU5BU5D_t2662109048* L_10 = V_2;
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		GameObject_t3674682005 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
		GameObject_t3674682005 * L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0065:
	{
		int32_t L_16 = V_3;
		GameObjectU5BU5D_t2662109048* L_17 = V_2;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_18 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_18) == ((uint32_t)3))))
		{
			goto IL_0080;
		}
	}
	{
		Plants_CreateLluvia_m3322971302(__this, /*hidden argument*/NULL);
	}

IL_0080:
	{
		int32_t L_19 = __this->get_currentTiempo_12();
		if (L_19)
		{
			goto IL_00ad;
		}
	}
	{
		Plants_CreateNiebla_m3669633466(__this, /*hidden argument*/NULL);
		Camera_t2727095145 * L_20 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_21 = __this->get_colors_11();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
		NullCheck(L_20);
		Camera_set_backgroundColor_m501006344(L_20, (*(Color_t4194546905 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))), /*hidden argument*/NULL);
	}

IL_00ad:
	{
		int32_t L_22 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_22) == ((uint32_t)6))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_23 = Random_Range_m75452833(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		V_4 = L_23;
		Plants_CreateNiebla_m3669633466(__this, /*hidden argument*/NULL);
		int32_t L_24 = V_4;
		if ((((int32_t)L_24) <= ((int32_t)(-1))))
		{
			goto IL_00d6;
		}
	}
	{
		Plants_CreateNieve_m3582645902(__this, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		Camera_t2727095145 * L_25 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_26 = __this->get_colors_11();
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		NullCheck(L_25);
		Camera_set_backgroundColor_m501006344(L_25, (*(Color_t4194546905 *)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)))), /*hidden argument*/NULL);
	}

IL_00f2:
	{
		int32_t L_27 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_27) == ((uint32_t)4))))
		{
			goto IL_0137;
		}
	}
	{
		Plants_CreateLineasFlotacion_m3334133290(__this, /*hidden argument*/NULL);
		int32_t L_28 = Random_Range_m75452833(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		V_5 = L_28;
		int32_t L_29 = V_5;
		if ((((int32_t)L_29) <= ((int32_t)(-1))))
		{
			goto IL_011b;
		}
	}
	{
		Plants_CreateNiebla_m3669633466(__this, /*hidden argument*/NULL);
	}

IL_011b:
	{
		Camera_t2727095145 * L_30 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_31 = __this->get_colors_11();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 4);
		NullCheck(L_30);
		Camera_set_backgroundColor_m501006344(L_30, (*(Color_t4194546905 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))), /*hidden argument*/NULL);
	}

IL_0137:
	{
		int32_t L_32 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_32) == ((uint32_t)2))))
		{
			goto IL_0165;
		}
	}
	{
		Camera_t2727095145 * L_33 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_34 = __this->get_colors_11();
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 4);
		NullCheck(L_33);
		Camera_set_backgroundColor_m501006344(L_33, (*(Color_t4194546905 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))), /*hidden argument*/NULL);
		Plants_CreateLluvia_m3322971302(__this, /*hidden argument*/NULL);
	}

IL_0165:
	{
		int32_t L_35 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_35) == ((uint32_t)1))))
		{
			goto IL_018d;
		}
	}
	{
		Camera_t2727095145 * L_36 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_37 = __this->get_colors_11();
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 4);
		NullCheck(L_36);
		Camera_set_backgroundColor_m501006344(L_36, (*(Color_t4194546905 *)((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))), /*hidden argument*/NULL);
	}

IL_018d:
	{
		int32_t L_38 = __this->get_currentTiempo_12();
		if ((!(((uint32_t)L_38) == ((uint32_t)7))))
		{
			goto IL_01b5;
		}
	}
	{
		Camera_t2727095145 * L_39 = __this->get_Iso_9();
		ColorU5BU5D_t2441545636* L_40 = __this->get_colors_11();
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 4);
		NullCheck(L_39);
		Camera_set_backgroundColor_m501006344(L_39, (*(Color_t4194546905 *)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))), /*hidden argument*/NULL);
	}

IL_01b5:
	{
		return;
	}
}
// System.Void Plants::CreateMap(System.Int32,System.Int32)
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1170771901;
extern const uint32_t Plants_CreateMap_m3058892373_MetadataUsageId;
extern "C"  void Plants_CreateMap_m3058892373 (Plants_t2393071496 * __this, int32_t ___fase0, int32_t ___room1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_CreateMap_m3058892373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Quaternion_t1553702882  V_5;
	memset(&V_5, 0, sizeof(V_5));
	GameObject_t3674682005 * V_6 = NULL;
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		V_0 = 0;
		goto IL_00d7;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_00b8;
	}

IL_000e:
	{
		V_2 = 0;
		goto IL_00ab;
	}

IL_0015:
	{
		int32_t L_0 = Random_Range_m75452833(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		V_3 = L_0;
		int32_t L_1 = V_3;
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_2 = V_1;
		int32_t L_3 = Random_Range_m75452833(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_7 = L_5;
		float L_6 = (&V_7)->get_y_2();
		int32_t L_7 = V_2;
		int32_t L_8 = Random_Range_m75452833(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_4), (((float)((float)((int32_t)((int32_t)L_2+(int32_t)L_3))))), ((float)((float)L_6+(float)(1.0f))), (((float)((float)((int32_t)((int32_t)L_7+(int32_t)L_8))))), /*hidden argument*/NULL);
		int32_t L_9 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)359), /*hidden argument*/NULL);
		Vector3_t4282066566  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, (0.0f), (((float)((float)L_9))), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_11 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		Vector3_t4282066566  L_12 = V_4;
		Quaternion_t1553702882  L_13 = V_5;
		int32_t L_14 = V_0;
		GameObject_t3674682005 * L_15 = Plants_CreatePlantsItems_m3446816263(__this, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_6 = L_15;
		GameObject_t3674682005 * L_16 = V_6;
		NullCheck(L_16);
		Renderer_t3076687687 * L_17 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_16, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		Material_t3870600107 * L_18 = Plants_GetMaterial_m1752811087(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Renderer_set_material_m1012580896(L_17, L_18, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_19 = V_6;
		NullCheck(L_19);
		GameObject_set_tag_m859036203(L_19, _stringLiteral1170771901, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)((int32_t)10)));
	}

IL_00ab:
	{
		int32_t L_21 = V_2;
		if ((((int32_t)L_21) < ((int32_t)((int32_t)100))))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)((int32_t)10)));
	}

IL_00b8:
	{
		int32_t L_23 = V_1;
		Transform_t1659122786 * L_24 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_localScale_m3886572677(L_24, /*hidden argument*/NULL);
		V_8 = L_25;
		float L_26 = (&V_8)->get_x_1();
		if ((((float)(((float)((float)L_23)))) < ((float)L_26)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_27 = V_0;
		V_0 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = ___fase0;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Plants::BackToMenu()
extern Il2CppCodeGenString* _stringLiteral1201105850;
extern const uint32_t Plants_BackToMenu_m2087043794_MetadataUsageId;
extern "C"  void Plants_BackToMenu_m2087043794 (Plants_t2393071496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plants_BackToMenu_m2087043794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral1201105850, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Progresion::.ctor()
extern "C"  void Progresion__ctor_m419370873 (Progresion_t2029956962 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Room::.ctor()
extern "C"  void Room__ctor_m1273823232 (Room_t2553083 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RoomGenerator::.ctor()
extern "C"  void RoomGenerator__ctor_m4112043091 (RoomGenerator_t3606720536 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RoomGenerator::.cctor()
extern Il2CppClass* RoomGenerator_t3606720536_il2cpp_TypeInfo_var;
extern const uint32_t RoomGenerator__cctor_m2437188026_MetadataUsageId;
extern "C"  void RoomGenerator__cctor_m2437188026 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RoomGenerator__cctor_m2437188026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->set_pasaFaseFactor_26(3);
		return;
	}
}
// System.Void RoomGenerator::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var;
extern const uint32_t RoomGenerator_Update_m351179034_MetadataUsageId;
extern "C"  void RoomGenerator_Update_m351179034 (RoomGenerator_t3606720536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RoomGenerator_Update_m351179034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)103), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_t3674682005 * L_1 = __this->get_Controller_11();
		NullCheck(L_1);
		Controller_t2630893500 * L_2 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_1, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		NullCheck(L_2);
		Controller_PasaFase_m306660593(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void RoomGenerator::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void RoomGenerator_setCameras_m2717393161 (RoomGenerator_t3606720536 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___Perspe0;
		__this->set_Perspec_19(L_0);
		Camera_t2727095145 * L_1 = ___Ortho1;
		__this->set_Ortho_20(L_1);
		Camera_t2727095145 * L_2 = ___Iso2;
		__this->set_Iso_21(L_2);
		Camera_t2727095145 * L_3 = ___Interface3;
		__this->set_Interface_22(L_3);
		return;
	}
}
// UnityEngine.Vector3 RoomGenerator::SetRoom(System.Int32)
extern "C"  Vector3_t4282066566  RoomGenerator_SetRoom_m4237980505 (RoomGenerator_t3606720536 * __this, int32_t ___fase0, const MethodInfo* method)
{
	{
		__this->set_alto_28(4);
		int32_t L_0 = __this->get_minAncho_32();
		int32_t L_1 = ___fase0;
		__this->set_ancho_29(((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))));
		int32_t L_2 = __this->get_minAlto_31();
		int32_t L_3 = ___fase0;
		__this->set_fondo_30(((int32_t)((int32_t)L_2+(int32_t)((int32_t)((int32_t)L_3*(int32_t)4)))));
		int32_t L_4 = __this->get_alto_28();
		int32_t L_5 = __this->get_ancho_29();
		int32_t L_6 = __this->get_fondo_30();
		Vector3_t4282066566  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2926210380(&L_7, (((float)((float)L_4))), (((float)((float)L_5))), (((float)((float)L_6))), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void RoomGenerator::increaseFase(System.Int32)
extern Il2CppClass* RoomGenerator_t3606720536_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4223663123;
extern const uint32_t RoomGenerator_increaseFase_m279346609_MetadataUsageId;
extern "C"  void RoomGenerator_increaseFase_m279346609 (RoomGenerator_t3606720536 * __this, int32_t ___room0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RoomGenerator_increaseFase_m279346609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___room0;
		IL2CPP_RUNTIME_CLASS_INIT(RoomGenerator_t3606720536_il2cpp_TypeInfo_var);
		int32_t L_1 = ((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->get_pasaFaseFactor_26();
		if (((int32_t)((int32_t)L_0%(int32_t)L_1)))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RoomGenerator_t3606720536_il2cpp_TypeInfo_var);
		int32_t L_2 = ((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->get_fase_25();
		((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->set_fase_25(((int32_t)((int32_t)L_2+(int32_t)1)));
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RoomGenerator_t3606720536_il2cpp_TypeInfo_var);
		int32_t L_3 = ((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->get_fase_25();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral4223663123, L_5, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RoomGenerator::GenerateRoom()
extern Il2CppClass* RoomGenerator_t3606720536_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBallShadow_t3766914079_m2067993822_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlants_t2393071496_m619420629_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1593011297;
extern Il2CppCodeGenString* _stringLiteral2364455;
extern Il2CppCodeGenString* _stringLiteral78959100;
extern Il2CppCodeGenString* _stringLiteral115029;
extern Il2CppCodeGenString* _stringLiteral1995605579;
extern Il2CppCodeGenString* _stringLiteral80238310;
extern Il2CppCodeGenString* _stringLiteral68152841;
extern Il2CppCodeGenString* _stringLiteral2062599;
extern Il2CppCodeGenString* _stringLiteral3312290682;
extern const uint32_t RoomGenerator_GenerateRoom_m2374567553_MetadataUsageId;
extern "C"  void RoomGenerator_GenerateRoom_m2374567553 (RoomGenerator_t3606720536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RoomGenerator_GenerateRoom_m2374567553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Quaternion_t1553702882  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Quaternion_t1553702882  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t4282066566  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Quaternion_t1553702882  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t4282066566  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Quaternion_t1553702882  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t4282066566  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t4282066566  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Quaternion_t1553702882  V_17;
	memset(&V_17, 0, sizeof(V_17));
	GameObject_t3674682005 * V_18 = NULL;
	Vector3_t4282066566  V_19;
	memset(&V_19, 0, sizeof(V_19));
	GameObject_t3674682005 * V_20 = NULL;
	Vector3_t4282066566  V_21;
	memset(&V_21, 0, sizeof(V_21));
	GameObject_t3674682005 * V_22 = NULL;
	Vector3_t4282066566  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector3_t4282066566  V_24;
	memset(&V_24, 0, sizeof(V_24));
	GameObject_t3674682005 * V_25 = NULL;
	Vector3_t4282066566  V_26;
	memset(&V_26, 0, sizeof(V_26));
	GameObject_t3674682005 * V_27 = NULL;
	Vector3_t4282066566  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector3_t4282066566  V_29;
	memset(&V_29, 0, sizeof(V_29));
	GameObject_t3674682005 * V_30 = NULL;
	Vector3_t4282066566  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t4282066566  V_32;
	memset(&V_32, 0, sizeof(V_32));
	Vector3_t4282066566  V_33;
	memset(&V_33, 0, sizeof(V_33));
	Vector3_t4282066566  V_34;
	memset(&V_34, 0, sizeof(V_34));
	GameObject_t3674682005 * V_35 = NULL;
	Vector3_t4282066566  V_36;
	memset(&V_36, 0, sizeof(V_36));
	Vector3_t4282066566  V_37;
	memset(&V_37, 0, sizeof(V_37));
	Vector3_t4282066566  V_38;
	memset(&V_38, 0, sizeof(V_38));
	Vector3_t4282066566  V_39;
	memset(&V_39, 0, sizeof(V_39));
	Vector3_t4282066566  V_40;
	memset(&V_40, 0, sizeof(V_40));
	Vector3_t4282066566  V_41;
	memset(&V_41, 0, sizeof(V_41));
	Vector3_t4282066566  V_42;
	memset(&V_42, 0, sizeof(V_42));
	Vector3_t4282066566  V_43;
	memset(&V_43, 0, sizeof(V_43));
	{
		IL2CPP_RUNTIME_CLASS_INIT(RoomGenerator_t3606720536_il2cpp_TypeInfo_var);
		int32_t L_0 = ((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->get_fase_25();
		RoomGenerator_SetRoom_m4237980505(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_ancho_29();
		Vector3__ctor_m2926210380((&V_0), (((float)((float)((int32_t)((int32_t)((-L_1))/(int32_t)2))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2926210380(&L_2, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_3 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = __this->get_alto_28();
		int32_t L_5 = __this->get_fondo_30();
		Vector3__ctor_m2926210380((&V_2), (1.0f), (((float)((float)L_4))), (((float)((float)L_5))), /*hidden argument*/NULL);
		int32_t L_6 = __this->get_ancho_29();
		Vector3__ctor_m2926210380((&V_3), (((float)((float)((int32_t)((int32_t)L_6/(int32_t)2))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2926210380(&L_7, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_8 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		int32_t L_9 = __this->get_alto_28();
		int32_t L_10 = __this->get_fondo_30();
		Vector3__ctor_m2926210380((&V_5), (1.0f), (((float)((float)L_9))), (((float)((float)L_10))), /*hidden argument*/NULL);
		int32_t L_11 = __this->get_fondo_30();
		Vector3__ctor_m2926210380((&V_6), (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_11/(int32_t)2))))), /*hidden argument*/NULL);
		int32_t L_12 = __this->get_alto_28();
		int32_t L_13 = __this->get_ancho_29();
		Vector3__ctor_m2926210380((&V_7), (1.0f), (((float)((float)L_12))), (((float)((float)L_13))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2926210380(&L_14, (0.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_15 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_8 = L_15;
		int32_t L_16 = __this->get_fondo_30();
		Vector3__ctor_m2926210380((&V_9), (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)((-L_16))/(int32_t)2))))), /*hidden argument*/NULL);
		int32_t L_17 = __this->get_alto_28();
		int32_t L_18 = __this->get_ancho_29();
		Vector3__ctor_m2926210380((&V_10), (1.0f), (((float)((float)L_17))), (((float)((float)L_18))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m2926210380(&L_19, (0.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_20 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_11 = L_20;
		int32_t L_21 = __this->get_alto_28();
		Vector3__ctor_m2926210380((&V_12), (0.0f), (((float)((float)((int32_t)((int32_t)((-L_21))/(int32_t)2))))), (0.0f), /*hidden argument*/NULL);
		int32_t L_22 = __this->get_ancho_29();
		int32_t L_23 = __this->get_fondo_30();
		Vector3__ctor_m2926210380((&V_13), (1.0f), (((float)((float)L_22))), (((float)((float)L_23))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m2926210380(&L_24, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_25 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		V_14 = L_25;
		int32_t L_26 = __this->get_alto_28();
		Vector3__ctor_m2926210380((&V_15), (0.0f), (((float)((float)((int32_t)((int32_t)L_26/(int32_t)2))))), (0.0f), /*hidden argument*/NULL);
		int32_t L_27 = __this->get_ancho_29();
		int32_t L_28 = __this->get_fondo_30();
		Vector3__ctor_m2926210380((&V_16), (1.0f), (((float)((float)L_27))), (((float)((float)L_28))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector3__ctor_m2926210380(&L_29, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_30 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		V_17 = L_30;
		GameObject_t3674682005 * L_31 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m845034556(L_31, /*hidden argument*/NULL);
		__this->set_Container_23(L_31);
		GameObject_t3674682005 * L_32 = __this->get_Container_23();
		NullCheck(L_32);
		Object_set_name_m1123518500(L_32, _stringLiteral1593011297, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_33 = __this->get_Controller_11();
		NullCheck(L_33);
		Controller_t2630893500 * L_34 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_33, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		GameObject_t3674682005 * L_35 = __this->get_Container_23();
		NullCheck(L_34);
		L_34->set_Room_7(L_35);
		GameObject_t3674682005 * L_36 = __this->get_LadrilloObj_2();
		Vector3_t4282066566  L_37 = V_0;
		Quaternion_t1553702882  L_38 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_39 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_36, L_37, L_38, /*hidden argument*/NULL);
		V_18 = ((GameObject_t3674682005 *)IsInstSealed(L_39, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_40 = V_18;
		NullCheck(L_40);
		Object_set_name_m1123518500(L_40, _stringLiteral2364455, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_41 = V_18;
		NullCheck(L_41);
		Transform_t1659122786 * L_42 = GameObject_get_transform_m1278640159(L_41, /*hidden argument*/NULL);
		Vector3_t4282066566  L_43 = V_2;
		NullCheck(L_42);
		Transform_set_localScale_m310756934(L_42, L_43, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_44 = V_18;
		NullCheck(L_44);
		Transform_t1659122786 * L_45 = GameObject_get_transform_m1278640159(L_44, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_46 = __this->get_Container_23();
		NullCheck(L_46);
		Transform_t1659122786 * L_47 = GameObject_get_transform_m1278640159(L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_SetParent_m3449663462(L_45, L_47, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_19), (-5.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_48 = V_18;
		NullCheck(L_48);
		BoxCollider_t2538127765 * L_49 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_48, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_50 = V_19;
		NullCheck(L_49);
		BoxCollider_set_center_m1759609917(L_49, L_50, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_51 = __this->get_LadrilloObj_2();
		Vector3_t4282066566  L_52 = V_3;
		Quaternion_t1553702882  L_53 = V_4;
		Object_t3071478659 * L_54 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_51, L_52, L_53, /*hidden argument*/NULL);
		V_20 = ((GameObject_t3674682005 *)IsInstSealed(L_54, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_55 = V_20;
		NullCheck(L_55);
		Object_set_name_m1123518500(L_55, _stringLiteral78959100, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_56 = V_20;
		NullCheck(L_56);
		Transform_t1659122786 * L_57 = GameObject_get_transform_m1278640159(L_56, /*hidden argument*/NULL);
		Vector3_t4282066566  L_58 = V_5;
		NullCheck(L_57);
		Transform_set_localScale_m310756934(L_57, L_58, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_59 = V_20;
		NullCheck(L_59);
		Transform_t1659122786 * L_60 = GameObject_get_transform_m1278640159(L_59, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_61 = __this->get_Container_23();
		NullCheck(L_61);
		Transform_t1659122786 * L_62 = GameObject_get_transform_m1278640159(L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		Transform_SetParent_m3449663462(L_60, L_62, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_21), (5.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_63 = V_20;
		NullCheck(L_63);
		BoxCollider_t2538127765 * L_64 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_63, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_65 = V_21;
		NullCheck(L_64);
		BoxCollider_set_center_m1759609917(L_64, L_65, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_66 = __this->get_LadrilloObj_2();
		Vector3_t4282066566  L_67 = V_15;
		Quaternion_t1553702882  L_68 = V_17;
		Object_t3071478659 * L_69 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_66, L_67, L_68, /*hidden argument*/NULL);
		V_22 = ((GameObject_t3674682005 *)IsInstSealed(L_69, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_70 = V_22;
		NullCheck(L_70);
		Object_set_name_m1123518500(L_70, _stringLiteral115029, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_71 = V_22;
		NullCheck(L_71);
		Transform_t1659122786 * L_72 = GameObject_get_transform_m1278640159(L_71, /*hidden argument*/NULL);
		Vector3_t4282066566  L_73 = V_16;
		NullCheck(L_72);
		Transform_set_localScale_m310756934(L_72, L_73, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_74 = V_22;
		NullCheck(L_74);
		Transform_t1659122786 * L_75 = GameObject_get_transform_m1278640159(L_74, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_76 = __this->get_Container_23();
		NullCheck(L_76);
		Transform_t1659122786 * L_77 = GameObject_get_transform_m1278640159(L_76, /*hidden argument*/NULL);
		NullCheck(L_75);
		Transform_SetParent_m3449663462(L_75, L_77, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_78 = V_22;
		NullCheck(L_78);
		MeshRenderer_t2804666580 * L_79 = GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910(L_78, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910_MethodInfo_var);
		Material_t3870600107 * L_80 = __this->get_Cristal_5();
		NullCheck(L_79);
		Renderer_set_material_m1012580896(L_79, L_80, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_23), (10.0f), (1.0f), (10.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_81 = V_22;
		NullCheck(L_81);
		BoxCollider_t2538127765 * L_82 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_81, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_83 = V_23;
		NullCheck(L_82);
		BoxCollider_set_size_m2674293393(L_82, L_83, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_24), (5.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_84 = V_22;
		NullCheck(L_84);
		BoxCollider_t2538127765 * L_85 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_84, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_86 = V_24;
		NullCheck(L_85);
		BoxCollider_set_center_m1759609917(L_85, L_86, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_87 = __this->get_LadrilloObj_2();
		Vector3_t4282066566  L_88 = V_12;
		Quaternion_t1553702882  L_89 = V_14;
		Object_t3071478659 * L_90 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_87, L_88, L_89, /*hidden argument*/NULL);
		V_25 = ((GameObject_t3674682005 *)IsInstSealed(L_90, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_91 = V_25;
		NullCheck(L_91);
		Object_set_name_m1123518500(L_91, _stringLiteral1995605579, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_92 = V_25;
		NullCheck(L_92);
		GameObject_set_tag_m859036203(L_92, _stringLiteral80238310, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_93 = V_25;
		NullCheck(L_93);
		Transform_t1659122786 * L_94 = GameObject_get_transform_m1278640159(L_93, /*hidden argument*/NULL);
		Vector3_t4282066566  L_95 = V_13;
		NullCheck(L_94);
		Transform_set_localScale_m310756934(L_94, L_95, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_96 = V_25;
		NullCheck(L_96);
		Transform_t1659122786 * L_97 = GameObject_get_transform_m1278640159(L_96, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_98 = __this->get_Container_23();
		NullCheck(L_98);
		Transform_t1659122786 * L_99 = GameObject_get_transform_m1278640159(L_98, /*hidden argument*/NULL);
		NullCheck(L_97);
		Transform_SetParent_m3449663462(L_97, L_99, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_26), (-5.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_100 = V_25;
		NullCheck(L_100);
		BoxCollider_t2538127765 * L_101 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_100, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_102 = V_26;
		NullCheck(L_101);
		BoxCollider_set_center_m1759609917(L_101, L_102, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_103 = __this->get_LadrilloObj_2();
		Vector3_t4282066566  L_104 = V_6;
		Quaternion_t1553702882  L_105 = V_8;
		Object_t3071478659 * L_106 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_103, L_104, L_105, /*hidden argument*/NULL);
		V_27 = ((GameObject_t3674682005 *)IsInstSealed(L_106, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_107 = V_27;
		NullCheck(L_107);
		Object_set_name_m1123518500(L_107, _stringLiteral68152841, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_108 = V_27;
		NullCheck(L_108);
		Transform_t1659122786 * L_109 = GameObject_get_transform_m1278640159(L_108, /*hidden argument*/NULL);
		Vector3_t4282066566  L_110 = V_7;
		NullCheck(L_109);
		Transform_set_localScale_m310756934(L_109, L_110, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_111 = V_27;
		NullCheck(L_111);
		Transform_t1659122786 * L_112 = GameObject_get_transform_m1278640159(L_111, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_113 = __this->get_Container_23();
		NullCheck(L_113);
		Transform_t1659122786 * L_114 = GameObject_get_transform_m1278640159(L_113, /*hidden argument*/NULL);
		NullCheck(L_112);
		Transform_SetParent_m3449663462(L_112, L_114, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_115 = V_27;
		NullCheck(L_115);
		Transform_t1659122786 * L_116 = GameObject_get_transform_m1278640159(L_115, /*hidden argument*/NULL);
		NullCheck(L_116);
		Vector3_t4282066566  L_117 = Transform_get_position_m2211398607(L_116, /*hidden argument*/NULL);
		V_36 = L_117;
		float L_118 = (&V_36)->get_x_1();
		int32_t L_119 = __this->get_ancho_29();
		GameObject_t3674682005 * L_120 = V_27;
		NullCheck(L_120);
		Transform_t1659122786 * L_121 = GameObject_get_transform_m1278640159(L_120, /*hidden argument*/NULL);
		NullCheck(L_121);
		Vector3_t4282066566  L_122 = Transform_get_position_m2211398607(L_121, /*hidden argument*/NULL);
		V_37 = L_122;
		float L_123 = (&V_37)->get_y_2();
		GameObject_t3674682005 * L_124 = V_27;
		NullCheck(L_124);
		Transform_t1659122786 * L_125 = GameObject_get_transform_m1278640159(L_124, /*hidden argument*/NULL);
		NullCheck(L_125);
		Vector3_t4282066566  L_126 = Transform_get_position_m2211398607(L_125, /*hidden argument*/NULL);
		V_38 = L_126;
		float L_127 = (&V_38)->get_z_3();
		Vector3_t4282066566  L_128;
		memset(&L_128, 0, sizeof(L_128));
		Vector3__ctor_m2926210380(&L_128, ((float)((float)L_118-(float)(((float)((float)((int32_t)((int32_t)L_119/(int32_t)3))))))), ((float)((float)L_123-(float)(1.0f))), ((float)((float)L_127-(float)(4.0f))), /*hidden argument*/NULL);
		__this->set_WallPos_24(L_128);
		Vector3__ctor_m2926210380((&V_28), (10.0f), (1.0f), (10.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_129 = V_27;
		NullCheck(L_129);
		BoxCollider_t2538127765 * L_130 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_129, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_131 = V_28;
		NullCheck(L_130);
		BoxCollider_set_size_m2674293393(L_130, L_131, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_29), (-5.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_132 = V_27;
		NullCheck(L_132);
		BoxCollider_t2538127765 * L_133 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_132, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_134 = V_29;
		NullCheck(L_133);
		BoxCollider_set_center_m1759609917(L_133, L_134, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_135 = __this->get_LadrilloObj_2();
		Vector3_t4282066566  L_136 = V_9;
		Quaternion_t1553702882  L_137 = V_11;
		Object_t3071478659 * L_138 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_135, L_136, L_137, /*hidden argument*/NULL);
		V_30 = ((GameObject_t3674682005 *)IsInstSealed(L_138, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_139 = V_30;
		NullCheck(L_139);
		Object_set_name_m1123518500(L_139, _stringLiteral2062599, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_140 = V_30;
		NullCheck(L_140);
		GameObject_set_tag_m859036203(L_140, _stringLiteral3312290682, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_141 = V_30;
		NullCheck(L_141);
		Transform_t1659122786 * L_142 = GameObject_get_transform_m1278640159(L_141, /*hidden argument*/NULL);
		Vector3_t4282066566  L_143 = V_10;
		NullCheck(L_142);
		Transform_set_localScale_m310756934(L_142, L_143, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_144 = V_30;
		NullCheck(L_144);
		Transform_t1659122786 * L_145 = GameObject_get_transform_m1278640159(L_144, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_146 = __this->get_Container_23();
		NullCheck(L_146);
		Transform_t1659122786 * L_147 = GameObject_get_transform_m1278640159(L_146, /*hidden argument*/NULL);
		NullCheck(L_145);
		Transform_SetParent_m3449663462(L_145, L_147, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_148 = V_30;
		NullCheck(L_148);
		MeshRenderer_t2804666580 * L_149 = GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910(L_148, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910_MethodInfo_var);
		Material_t3870600107 * L_150 = __this->get_Cristal_5();
		NullCheck(L_149);
		Renderer_set_material_m1012580896(L_149, L_150, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_31), (10.0f), (1.0f), (10.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_151 = V_30;
		NullCheck(L_151);
		BoxCollider_t2538127765 * L_152 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_151, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_153 = V_31;
		NullCheck(L_152);
		BoxCollider_set_size_m2674293393(L_152, L_153, /*hidden argument*/NULL);
		Vector3__ctor_m2926210380((&V_32), (5.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_154 = V_30;
		NullCheck(L_154);
		BoxCollider_t2538127765 * L_155 = GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457(L_154, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t2538127765_m3222626457_MethodInfo_var);
		Vector3_t4282066566  L_156 = V_32;
		NullCheck(L_155);
		BoxCollider_set_center_m1759609917(L_155, L_156, /*hidden argument*/NULL);
		int32_t L_157 = __this->get_alturaCam_14();
		GameObject_t3674682005 * L_158 = V_30;
		NullCheck(L_158);
		Transform_t1659122786 * L_159 = GameObject_get_transform_m1278640159(L_158, /*hidden argument*/NULL);
		NullCheck(L_159);
		Vector3_t4282066566  L_160 = Transform_get_position_m2211398607(L_159, /*hidden argument*/NULL);
		V_39 = L_160;
		float L_161 = (&V_39)->get_z_3();
		int32_t L_162 = __this->get_distanciaZCam_13();
		Vector3_t4282066566  L_163;
		memset(&L_163, 0, sizeof(L_163));
		Vector3__ctor_m2926210380(&L_163, (0.0f), (((float)((float)L_157))), ((float)((float)L_161-(float)(((float)((float)L_162))))), /*hidden argument*/NULL);
		__this->set_LockCam_16(L_163);
		float L_164 = (&V_12)->get_y_2();
		float L_165 = (&V_9)->get_z_3();
		Vector3_t4282066566  L_166;
		memset(&L_166, 0, sizeof(L_166));
		Vector3__ctor_m2926210380(&L_166, (0.0f), ((float)((float)L_164+(float)(3.0f))), ((float)((float)L_165+(float)(4.0f))), /*hidden argument*/NULL);
		__this->set_LockPlayer_17(L_166);
		Vector3_t4282066566 * L_167 = __this->get_address_of_LockPlayer_17();
		float L_168 = L_167->get_x_1();
		float L_169 = (&V_12)->get_y_2();
		Vector3_t4282066566 * L_170 = __this->get_address_of_LockPlayer_17();
		float L_171 = L_170->get_z_3();
		Vector3_t4282066566  L_172;
		memset(&L_172, 0, sizeof(L_172));
		Vector3__ctor_m2926210380(&L_172, L_168, ((float)((float)L_169+(float)(1.0f))), L_171, /*hidden argument*/NULL);
		__this->set_LockBall_18(L_172);
		GameObject_t3674682005 * L_173 = __this->get_Ball_10();
		NullCheck(L_173);
		Transform_t1659122786 * L_174 = GameObject_get_transform_m1278640159(L_173, /*hidden argument*/NULL);
		NullCheck(L_174);
		Transform_t1659122786 * L_175 = Transform_GetChild_m4040462992(L_174, 0, /*hidden argument*/NULL);
		NullCheck(L_175);
		GameObject_t3674682005 * L_176 = Component_get_gameObject_m1170635899(L_175, /*hidden argument*/NULL);
		__this->set_Shadow_7(L_176);
		GameObject_t3674682005 * L_177 = __this->get_Shadow_7();
		NullCheck(L_177);
		BallShadow_t3766914079 * L_178 = GameObject_GetComponent_TisBallShadow_t3766914079_m2067993822(L_177, /*hidden argument*/GameObject_GetComponent_TisBallShadow_t3766914079_m2067993822_MethodInfo_var);
		float L_179 = (&V_12)->get_y_2();
		NullCheck(L_178);
		L_178->set_alturaSombra_4(((float)((float)L_179+(float)(0.5f))));
		Camera_t2727095145 * L_180 = __this->get_CameraMan_8();
		NullCheck(L_180);
		Transform_t1659122786 * L_181 = Component_get_transform_m4257140443(L_180, /*hidden argument*/NULL);
		Vector3_t4282066566  L_182 = __this->get_LockCam_16();
		NullCheck(L_181);
		Transform_set_position_m3111394108(L_181, L_182, /*hidden argument*/NULL);
		int32_t L_183 = __this->get_ancho_29();
		GameObject_t3674682005 * L_184 = __this->get_Player_9();
		NullCheck(L_184);
		Transform_t1659122786 * L_185 = GameObject_get_transform_m1278640159(L_184, /*hidden argument*/NULL);
		NullCheck(L_185);
		Vector3_t4282066566  L_186 = Transform_get_localScale_m3886572677(L_185, /*hidden argument*/NULL);
		V_40 = L_186;
		float L_187 = (&V_40)->get_x_1();
		float L_188 = __this->get_umbralRebotebounds_15();
		int32_t L_189 = __this->get_alto_28();
		int32_t L_190 = __this->get_fondo_30();
		float L_191 = __this->get_umbralRebotebounds_15();
		Vector3__ctor_m2926210380((&V_33), ((float)((float)((float)((float)(((float)((float)((int32_t)((int32_t)L_183/(int32_t)2)))))-(float)L_187))-(float)L_188)), (((float)((float)((int32_t)((int32_t)L_189/(int32_t)2))))), ((float)((float)(((float)((float)((int32_t)((int32_t)L_190/(int32_t)2)))))-(float)L_191)), /*hidden argument*/NULL);
		GameObject_t3674682005 * L_192 = __this->get_Player_9();
		NullCheck(L_192);
		Transform_t1659122786 * L_193 = GameObject_get_transform_m1278640159(L_192, /*hidden argument*/NULL);
		Vector3_t4282066566  L_194 = __this->get_LockPlayer_17();
		NullCheck(L_193);
		Transform_set_position_m3111394108(L_193, L_194, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_195 = __this->get_Player_9();
		NullCheck(L_195);
		Move_t2404337 * L_196 = GameObject_GetComponent_TisMove_t2404337_m307893836(L_195, /*hidden argument*/GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var);
		Vector3_t4282066566  L_197 = V_33;
		NullCheck(L_196);
		Move_SetBoundary_m976619013(L_196, L_197, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_198 = __this->get_Player_9();
		NullCheck(L_198);
		Move_t2404337 * L_199 = GameObject_GetComponent_TisMove_t2404337_m307893836(L_198, /*hidden argument*/GameObject_GetComponent_TisMove_t2404337_m307893836_MethodInfo_var);
		int32_t L_200 = __this->get_ancho_29();
		NullCheck(L_199);
		Move_setAncho_m3968134854(L_199, L_200, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_201 = __this->get_Ball_10();
		NullCheck(L_201);
		MoveBall_t4254648720 * L_202 = GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405(L_201, /*hidden argument*/GameObject_GetComponent_TisMoveBall_t4254648720_m3360515405_MethodInfo_var);
		Vector3_t4282066566  L_203 = V_33;
		NullCheck(L_202);
		MoveBall_SetBoundary_m3003401188(L_202, L_203, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_204 = __this->get_Ball_10();
		NullCheck(L_204);
		Transform_t1659122786 * L_205 = GameObject_get_transform_m1278640159(L_204, /*hidden argument*/NULL);
		Vector3_t4282066566  L_206 = __this->get_LockBall_18();
		NullCheck(L_205);
		Transform_set_position_m3111394108(L_205, L_206, /*hidden argument*/NULL);
		int32_t L_207 = __this->get_ancho_29();
		int32_t L_208 = __this->get_fondo_30();
		RoomGenerator_CreateWall_m4117356815(__this, L_207, L_208, /*hidden argument*/NULL);
		Transform_t1659122786 * L_209 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_209);
		Vector3_t4282066566  L_210 = Transform_get_position_m2211398607(L_209, /*hidden argument*/NULL);
		V_41 = L_210;
		float L_211 = (&V_41)->get_x_1();
		Transform_t1659122786 * L_212 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_212);
		Vector3_t4282066566  L_213 = Transform_get_position_m2211398607(L_212, /*hidden argument*/NULL);
		V_42 = L_213;
		float L_214 = (&V_42)->get_y_2();
		Transform_t1659122786 * L_215 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_215);
		Vector3_t4282066566  L_216 = Transform_get_position_m2211398607(L_215, /*hidden argument*/NULL);
		V_43 = L_216;
		float L_217 = (&V_43)->get_z_3();
		Vector3__ctor_m2926210380((&V_34), L_211, ((float)((float)L_214-(float)(5.0f))), L_217, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_218 = __this->get_PlantaciUF3n_12();
		Vector3_t4282066566  L_219 = V_34;
		Quaternion_t1553702882  L_220 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t3071478659 * L_221 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_218, L_219, L_220, /*hidden argument*/NULL);
		V_35 = ((GameObject_t3674682005 *)IsInstSealed(L_221, GameObject_t3674682005_il2cpp_TypeInfo_var));
		GameObject_t3674682005 * L_222 = V_35;
		NullCheck(L_222);
		Transform_t1659122786 * L_223 = GameObject_get_transform_m1278640159(L_222, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_224 = __this->get_Container_23();
		NullCheck(L_224);
		Transform_t1659122786 * L_225 = GameObject_get_transform_m1278640159(L_224, /*hidden argument*/NULL);
		NullCheck(L_223);
		Transform_SetParent_m3449663462(L_223, L_225, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_226 = V_35;
		NullCheck(L_226);
		Plants_t2393071496 * L_227 = GameObject_GetComponent_TisPlants_t2393071496_m619420629(L_226, /*hidden argument*/GameObject_GetComponent_TisPlants_t2393071496_m619420629_MethodInfo_var);
		Camera_t2727095145 * L_228 = __this->get_Perspec_19();
		Camera_t2727095145 * L_229 = __this->get_Ortho_20();
		Camera_t2727095145 * L_230 = __this->get_Iso_21();
		Camera_t2727095145 * L_231 = __this->get_Interface_22();
		NullCheck(L_227);
		Plants_setCameras_m1784206025(L_227, L_228, L_229, L_230, L_231, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_232 = V_35;
		NullCheck(L_232);
		Plants_t2393071496 * L_233 = GameObject_GetComponent_TisPlants_t2393071496_m619420629(L_232, /*hidden argument*/GameObject_GetComponent_TisPlants_t2393071496_m619420629_MethodInfo_var);
		int32_t L_234 = ((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->get_fase_25();
		NullCheck(L_233);
		Plants_SetWeather_m4171024980(L_233, L_234, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_235 = V_35;
		NullCheck(L_235);
		Plants_t2393071496 * L_236 = GameObject_GetComponent_TisPlants_t2393071496_m619420629(L_235, /*hidden argument*/GameObject_GetComponent_TisPlants_t2393071496_m619420629_MethodInfo_var);
		int32_t L_237 = ((RoomGenerator_t3606720536_StaticFields*)RoomGenerator_t3606720536_il2cpp_TypeInfo_var->static_fields)->get_fase_25();
		int32_t L_238 = __this->get_room_27();
		NullCheck(L_236);
		Plants_CreateSet_m585282447(L_236, L_237, L_238, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RoomGenerator::CreateWall(System.Int32,System.Int32)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2688490;
extern const uint32_t RoomGenerator_CreateWall_m4117356815_MetadataUsageId;
extern "C"  void RoomGenerator_CreateWall_m4117356815 (RoomGenerator_t3606720536 * __this, int32_t ___ancho0, int32_t ___fondo1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RoomGenerator_CreateWall_m4117356815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_WallObj_3();
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_4 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		__this->set_Wall_4(((GameObject_t3674682005 *)IsInstSealed(L_4, GameObject_t3674682005_il2cpp_TypeInfo_var)));
		GameObject_t3674682005 * L_5 = __this->get_Wall_4();
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		Vector3_t4282066566  L_7 = __this->get_WallPos_24();
		NullCheck(L_6);
		Transform_set_position_m3111394108(L_6, L_7, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_8 = __this->get_Controller_11();
		NullCheck(L_8);
		Controller_t2630893500 * L_9 = GameObject_GetComponent_TisController_t2630893500_m3221217313(L_8, /*hidden argument*/GameObject_GetComponent_TisController_t2630893500_m3221217313_MethodInfo_var);
		GameObject_t3674682005 * L_10 = __this->get_Wall_4();
		NullCheck(L_9);
		L_9->set_FrontWall_6(L_10);
		GameObject_t3674682005 * L_11 = __this->get_Wall_4();
		NullCheck(L_11);
		Object_set_name_m1123518500(L_11, _stringLiteral2688490, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_12 = __this->get_Wall_4();
		NullCheck(L_12);
		FrontWall_t2131806067 * L_13 = GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678(L_12, /*hidden argument*/GameObject_GetComponent_TisFrontWall_t2131806067_m2028209678_MethodInfo_var);
		int32_t L_14 = ___ancho0;
		int32_t L_15 = ___ancho0;
		int32_t L_16 = ___fondo1;
		NullCheck(L_13);
		FrontWall_CreateBrickWall_m1068921921(L_13, ((int32_t)((int32_t)L_14-(int32_t)((int32_t)((int32_t)L_15/(int32_t)3)))), ((int32_t)((int32_t)L_16/(int32_t)3)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SelfDestroy::.ctor()
extern "C"  void SelfDestroy__ctor_m4251083837 (SelfDestroy_t1472365294 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SelfDestroy::Start()
extern "C"  void SelfDestroy_Start_m3198221629 (SelfDestroy_t1472365294 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SelfDestroy::Update()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872_MethodInfo_var;
extern const uint32_t SelfDestroy_Update_m366474864_MetadataUsageId;
extern "C"  void SelfDestroy_Update_m366474864 (SelfDestroy_t1472365294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SelfDestroy_Update_m366474864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ParticleSystem_t381473177 * L_1 = GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872(L_0, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872_MethodInfo_var);
		NullCheck(L_1);
		bool L_2 = ParticleSystem_IsAlive_m3733722867(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Shake::.ctor()
extern "C"  void Shake__ctor_m3472411973 (Shake_t79847142 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shake::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Shake_setCameras_m4002115067 (Shake_t79847142 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = ___Perspe0;
		__this->set_Perspec_13(L_0);
		Camera_t2727095145 * L_1 = ___Ortho1;
		__this->set_Ortho_14(L_1);
		Camera_t2727095145 * L_2 = ___Iso2;
		__this->set_Iso_15(L_2);
		Camera_t2727095145 * L_3 = ___Interface3;
		__this->set_Interface_16(L_3);
		return;
	}
}
// System.Void Shake::Start()
extern "C"  void Shake_Start_m2419549765 (Shake_t79847142 * __this, const MethodInfo* method)
{
	{
		Camera_t2727095145 * L_0 = __this->get_Perspec_13();
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set_initPosP_7(L_2);
		Camera_t2727095145 * L_3 = __this->get_Perspec_13();
		NullCheck(L_3);
		float L_4 = Camera_get_fieldOfView_m65126887(L_3, /*hidden argument*/NULL);
		__this->set_shakeSizeP_10(L_4);
		Camera_t2727095145 * L_5 = __this->get_Ortho_14();
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		__this->set_initPosO_8(L_7);
		Camera_t2727095145 * L_8 = __this->get_Ortho_14();
		NullCheck(L_8);
		float L_9 = Camera_get_orthographicSize_m3215515490(L_8, /*hidden argument*/NULL);
		__this->set_shakeSizeO_11(L_9);
		Camera_t2727095145 * L_10 = __this->get_Iso_15();
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = Component_get_transform_m4257140443(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = Transform_get_position_m2211398607(L_11, /*hidden argument*/NULL);
		__this->set_initPosI_9(L_12);
		Camera_t2727095145 * L_13 = __this->get_Iso_15();
		NullCheck(L_13);
		float L_14 = Camera_get_orthographicSize_m3215515490(L_13, /*hidden argument*/NULL);
		__this->set_shakeSizeI_12(L_14);
		return;
	}
}
// System.Void Shake::ShakeThatCamera(System.Single)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109399814;
extern const uint32_t Shake_ShakeThatCamera_m1815182614_MetadataUsageId;
extern "C"  void Shake_ShakeThatCamera_m1815182614 (Shake_t79847142 * __this, float ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Shake_ShakeThatCamera_m1815182614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m3858025161(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		bool L_2 = __this->get_shaking_2();
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = ___t0;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_4);
		MonoBehaviour_StartCoroutine_m2964903975(__this, _stringLiteral109399814, L_5, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Collections.IEnumerator Shake::shake(System.Single)
extern Il2CppClass* U3CshakeU3Ec__Iterator7_t570686417_il2cpp_TypeInfo_var;
extern const uint32_t Shake_shake_m2392108282_MetadataUsageId;
extern "C"  Il2CppObject * Shake_shake_m2392108282 (Shake_t79847142 * __this, float ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Shake_shake_m2392108282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CshakeU3Ec__Iterator7_t570686417 * V_0 = NULL;
	{
		U3CshakeU3Ec__Iterator7_t570686417 * L_0 = (U3CshakeU3Ec__Iterator7_t570686417 *)il2cpp_codegen_object_new(U3CshakeU3Ec__Iterator7_t570686417_il2cpp_TypeInfo_var);
		U3CshakeU3Ec__Iterator7__ctor_m4289725626(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CshakeU3Ec__Iterator7_t570686417 * L_1 = V_0;
		float L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CshakeU3Ec__Iterator7_t570686417 * L_3 = V_0;
		float L_4 = ___t0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Et_6(L_4);
		U3CshakeU3Ec__Iterator7_t570686417 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_7(__this);
		U3CshakeU3Ec__Iterator7_t570686417 * L_6 = V_0;
		return L_6;
	}
}
// System.Void Shake/<shake>c__Iterator7::.ctor()
extern "C"  void U3CshakeU3Ec__Iterator7__ctor_m4289725626 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Shake/<shake>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CshakeU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m828775192 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Shake/<shake>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CshakeU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m4171321004 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Boolean Shake/<shake>c__Iterator7::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const uint32_t U3CshakeU3Ec__Iterator7_MoveNext_m2349314042_MetadataUsageId;
extern "C"  bool U3CshakeU3Ec__Iterator7_MoveNext_m2349314042 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CshakeU3Ec__Iterator7_MoveNext_m2349314042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	bool V_10 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0336;
		}
	}
	{
		goto IL_040a;
	}

IL_0021:
	{
		Shake_t79847142 * L_2 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_2);
		L_2->set_shaking_2((bool)1);
		goto IL_0336;
	}

IL_0032:
	{
		Shake_t79847142 * L_3 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_3);
		Camera_t2727095145 * L_4 = L_3->get_Perspec_13();
		Shake_t79847142 * L_5 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_5);
		float L_6 = L_5->get_shakeSizeP_10();
		Shake_t79847142 * L_7 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_7);
		float L_8 = L_7->get_shakeSizeP_10();
		Shake_t79847142 * L_9 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_9);
		float L_10 = L_9->get_maxShakeField_6();
		float L_11 = Random_Range_m3362417303(NULL /*static, unused*/, L_6, ((float)((float)L_8+(float)L_10)), /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_fieldOfView_m809388684(L_4, (((float)((float)(((int32_t)((int32_t)L_11)))))), /*hidden argument*/NULL);
		Shake_t79847142 * L_12 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_12);
		Camera_t2727095145 * L_13 = L_12->get_Ortho_14();
		Shake_t79847142 * L_14 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_14);
		float L_15 = L_14->get_shakeSizeO_11();
		Shake_t79847142 * L_16 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_16);
		float L_17 = L_16->get_shakeSizeO_11();
		Shake_t79847142 * L_18 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_18);
		float L_19 = L_18->get_maxShakeField_6();
		float L_20 = Random_Range_m3362417303(NULL /*static, unused*/, L_15, ((float)((float)L_17+(float)L_19)), /*hidden argument*/NULL);
		NullCheck(L_13);
		Camera_set_orthographicSize_m3910539041(L_13, (((float)((float)(((int32_t)((int32_t)L_20)))))), /*hidden argument*/NULL);
		Shake_t79847142 * L_21 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_21);
		Camera_t2727095145 * L_22 = L_21->get_Iso_15();
		Shake_t79847142 * L_23 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_23);
		float L_24 = L_23->get_shakeSizeI_12();
		Shake_t79847142 * L_25 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_25);
		float L_26 = L_25->get_shakeSizeI_12();
		Shake_t79847142 * L_27 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_27);
		float L_28 = L_27->get_maxShakeField_6();
		float L_29 = Random_Range_m3362417303(NULL /*static, unused*/, L_24, ((float)((float)L_26+(float)L_28)), /*hidden argument*/NULL);
		NullCheck(L_22);
		Camera_set_orthographicSize_m3910539041(L_22, (((float)((float)(((int32_t)((int32_t)L_29)))))), /*hidden argument*/NULL);
		Shake_t79847142 * L_30 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_30);
		Camera_t2727095145 * L_31 = L_30->get_Perspec_13();
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = Component_get_transform_m4257140443(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t4282066566  L_33 = Transform_get_position_m2211398607(L_32, /*hidden argument*/NULL);
		V_1 = L_33;
		float L_34 = (&V_1)->get_x_1();
		Shake_t79847142 * L_35 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_35);
		float L_36 = L_35->get_positionRange_5();
		Shake_t79847142 * L_37 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_37);
		float L_38 = L_37->get_positionRange_5();
		float L_39 = Random_Range_m3362417303(NULL /*static, unused*/, ((-L_36)), L_38, /*hidden argument*/NULL);
		Shake_t79847142 * L_40 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_40);
		Camera_t2727095145 * L_41 = L_40->get_Perspec_13();
		NullCheck(L_41);
		Transform_t1659122786 * L_42 = Component_get_transform_m4257140443(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Vector3_t4282066566  L_43 = Transform_get_position_m2211398607(L_42, /*hidden argument*/NULL);
		V_2 = L_43;
		float L_44 = (&V_2)->get_y_2();
		Shake_t79847142 * L_45 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_45);
		float L_46 = L_45->get_positionRange_5();
		Shake_t79847142 * L_47 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_47);
		float L_48 = L_47->get_positionRange_5();
		float L_49 = Random_Range_m3362417303(NULL /*static, unused*/, ((-L_46)), L_48, /*hidden argument*/NULL);
		Shake_t79847142 * L_50 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_50);
		Camera_t2727095145 * L_51 = L_50->get_Perspec_13();
		NullCheck(L_51);
		Transform_t1659122786 * L_52 = Component_get_transform_m4257140443(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Vector3_t4282066566  L_53 = Transform_get_position_m2211398607(L_52, /*hidden argument*/NULL);
		V_3 = L_53;
		float L_54 = (&V_3)->get_z_3();
		Vector3_t4282066566  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Vector3__ctor_m2926210380(&L_55, ((float)((float)L_34+(float)L_39)), ((float)((float)L_44+(float)L_49)), L_54, /*hidden argument*/NULL);
		__this->set_U3CpPU3E__0_1(L_55);
		Shake_t79847142 * L_56 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_56);
		Camera_t2727095145 * L_57 = L_56->get_Ortho_14();
		NullCheck(L_57);
		Transform_t1659122786 * L_58 = Component_get_transform_m4257140443(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		Vector3_t4282066566  L_59 = Transform_get_position_m2211398607(L_58, /*hidden argument*/NULL);
		V_4 = L_59;
		float L_60 = (&V_4)->get_x_1();
		Shake_t79847142 * L_61 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_61);
		float L_62 = L_61->get_positionRange_5();
		Shake_t79847142 * L_63 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_63);
		float L_64 = L_63->get_positionRange_5();
		float L_65 = Random_Range_m3362417303(NULL /*static, unused*/, ((-L_62)), L_64, /*hidden argument*/NULL);
		Shake_t79847142 * L_66 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_66);
		Camera_t2727095145 * L_67 = L_66->get_Ortho_14();
		NullCheck(L_67);
		Transform_t1659122786 * L_68 = Component_get_transform_m4257140443(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		Vector3_t4282066566  L_69 = Transform_get_position_m2211398607(L_68, /*hidden argument*/NULL);
		V_5 = L_69;
		float L_70 = (&V_5)->get_y_2();
		Shake_t79847142 * L_71 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_71);
		float L_72 = L_71->get_positionRange_5();
		Shake_t79847142 * L_73 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_73);
		float L_74 = L_73->get_positionRange_5();
		float L_75 = Random_Range_m3362417303(NULL /*static, unused*/, ((-L_72)), L_74, /*hidden argument*/NULL);
		Shake_t79847142 * L_76 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_76);
		Camera_t2727095145 * L_77 = L_76->get_Ortho_14();
		NullCheck(L_77);
		Transform_t1659122786 * L_78 = Component_get_transform_m4257140443(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		Vector3_t4282066566  L_79 = Transform_get_position_m2211398607(L_78, /*hidden argument*/NULL);
		V_6 = L_79;
		float L_80 = (&V_6)->get_z_3();
		Vector3_t4282066566  L_81;
		memset(&L_81, 0, sizeof(L_81));
		Vector3__ctor_m2926210380(&L_81, ((float)((float)L_60+(float)L_65)), ((float)((float)L_70+(float)L_75)), L_80, /*hidden argument*/NULL);
		__this->set_U3CpOU3E__1_2(L_81);
		Shake_t79847142 * L_82 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_82);
		Camera_t2727095145 * L_83 = L_82->get_Iso_15();
		NullCheck(L_83);
		Transform_t1659122786 * L_84 = Component_get_transform_m4257140443(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		Vector3_t4282066566  L_85 = Transform_get_position_m2211398607(L_84, /*hidden argument*/NULL);
		V_7 = L_85;
		float L_86 = (&V_7)->get_x_1();
		Shake_t79847142 * L_87 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_87);
		float L_88 = L_87->get_positionRange_5();
		Shake_t79847142 * L_89 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_89);
		float L_90 = L_89->get_positionRange_5();
		float L_91 = Random_Range_m3362417303(NULL /*static, unused*/, ((-L_88)), L_90, /*hidden argument*/NULL);
		Shake_t79847142 * L_92 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_92);
		Camera_t2727095145 * L_93 = L_92->get_Iso_15();
		NullCheck(L_93);
		Transform_t1659122786 * L_94 = Component_get_transform_m4257140443(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		Vector3_t4282066566  L_95 = Transform_get_position_m2211398607(L_94, /*hidden argument*/NULL);
		V_8 = L_95;
		float L_96 = (&V_8)->get_y_2();
		Shake_t79847142 * L_97 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_97);
		float L_98 = L_97->get_positionRange_5();
		Shake_t79847142 * L_99 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_99);
		float L_100 = L_99->get_positionRange_5();
		float L_101 = Random_Range_m3362417303(NULL /*static, unused*/, ((-L_98)), L_100, /*hidden argument*/NULL);
		Shake_t79847142 * L_102 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_102);
		Camera_t2727095145 * L_103 = L_102->get_Iso_15();
		NullCheck(L_103);
		Transform_t1659122786 * L_104 = Component_get_transform_m4257140443(L_103, /*hidden argument*/NULL);
		NullCheck(L_104);
		Vector3_t4282066566  L_105 = Transform_get_position_m2211398607(L_104, /*hidden argument*/NULL);
		V_9 = L_105;
		float L_106 = (&V_9)->get_z_3();
		Vector3_t4282066566  L_107;
		memset(&L_107, 0, sizeof(L_107));
		Vector3__ctor_m2926210380(&L_107, ((float)((float)L_86+(float)L_91)), ((float)((float)L_96+(float)L_101)), L_106, /*hidden argument*/NULL);
		__this->set_U3CpIU3E__2_3(L_107);
		Shake_t79847142 * L_108 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_108);
		Camera_t2727095145 * L_109 = L_108->get_Perspec_13();
		NullCheck(L_109);
		Transform_t1659122786 * L_110 = Component_get_transform_m4257140443(L_109, /*hidden argument*/NULL);
		Vector3_t4282066566  L_111 = __this->get_U3CpPU3E__0_1();
		NullCheck(L_110);
		Transform_set_position_m3111394108(L_110, L_111, /*hidden argument*/NULL);
		Shake_t79847142 * L_112 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_112);
		Camera_t2727095145 * L_113 = L_112->get_Ortho_14();
		NullCheck(L_113);
		Transform_t1659122786 * L_114 = Component_get_transform_m4257140443(L_113, /*hidden argument*/NULL);
		Vector3_t4282066566  L_115 = __this->get_U3CpOU3E__1_2();
		NullCheck(L_114);
		Transform_set_position_m3111394108(L_114, L_115, /*hidden argument*/NULL);
		Shake_t79847142 * L_116 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_116);
		Camera_t2727095145 * L_117 = L_116->get_Iso_15();
		NullCheck(L_117);
		Transform_t1659122786 * L_118 = Component_get_transform_m4257140443(L_117, /*hidden argument*/NULL);
		Vector3_t4282066566  L_119 = __this->get_U3CpIU3E__2_3();
		NullCheck(L_118);
		Transform_set_position_m3111394108(L_118, L_119, /*hidden argument*/NULL);
		float L_120 = __this->get_t_0();
		__this->set_t_0(((float)((float)L_120-(float)(1.0f))));
		WaitForSeconds_t1615819279 * L_121 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_121, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_5(L_121);
		__this->set_U24PC_4(1);
		goto IL_040c;
	}

IL_0336:
	{
		float L_122 = __this->get_t_0();
		if ((((float)L_122) > ((float)(0.0f))))
		{
			goto IL_0032;
		}
	}
	{
		Shake_t79847142 * L_123 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_123);
		Camera_t2727095145 * L_124 = L_123->get_Perspec_13();
		Shake_t79847142 * L_125 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_125);
		float L_126 = L_125->get_shakeSizeP_10();
		NullCheck(L_124);
		Camera_set_fieldOfView_m809388684(L_124, L_126, /*hidden argument*/NULL);
		Shake_t79847142 * L_127 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_127);
		Camera_t2727095145 * L_128 = L_127->get_Ortho_14();
		Shake_t79847142 * L_129 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_129);
		float L_130 = L_129->get_shakeSizeO_11();
		NullCheck(L_128);
		Camera_set_orthographicSize_m3910539041(L_128, L_130, /*hidden argument*/NULL);
		Shake_t79847142 * L_131 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_131);
		Camera_t2727095145 * L_132 = L_131->get_Iso_15();
		Shake_t79847142 * L_133 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_133);
		float L_134 = L_133->get_shakeSizeI_12();
		NullCheck(L_132);
		Camera_set_orthographicSize_m3910539041(L_132, L_134, /*hidden argument*/NULL);
		Shake_t79847142 * L_135 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_135);
		Camera_t2727095145 * L_136 = L_135->get_Perspec_13();
		NullCheck(L_136);
		Transform_t1659122786 * L_137 = Component_get_transform_m4257140443(L_136, /*hidden argument*/NULL);
		Shake_t79847142 * L_138 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_138);
		Vector3_t4282066566  L_139 = L_138->get_initPosP_7();
		NullCheck(L_137);
		Transform_set_position_m3111394108(L_137, L_139, /*hidden argument*/NULL);
		Shake_t79847142 * L_140 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_140);
		Camera_t2727095145 * L_141 = L_140->get_Ortho_14();
		NullCheck(L_141);
		Transform_t1659122786 * L_142 = Component_get_transform_m4257140443(L_141, /*hidden argument*/NULL);
		Shake_t79847142 * L_143 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_143);
		Vector3_t4282066566  L_144 = L_143->get_initPosO_8();
		NullCheck(L_142);
		Transform_set_position_m3111394108(L_142, L_144, /*hidden argument*/NULL);
		Shake_t79847142 * L_145 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_145);
		Camera_t2727095145 * L_146 = L_145->get_Iso_15();
		NullCheck(L_146);
		Transform_t1659122786 * L_147 = Component_get_transform_m4257140443(L_146, /*hidden argument*/NULL);
		Shake_t79847142 * L_148 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_148);
		Vector3_t4282066566  L_149 = L_148->get_initPosI_9();
		NullCheck(L_147);
		Transform_set_position_m3111394108(L_147, L_149, /*hidden argument*/NULL);
		Shake_t79847142 * L_150 = __this->get_U3CU3Ef__this_7();
		NullCheck(L_150);
		L_150->set_shaking_2((bool)0);
		__this->set_U24PC_4((-1));
	}

IL_040a:
	{
		return (bool)0;
	}

IL_040c:
	{
		return (bool)1;
	}
	// Dead block : IL_040e: ldloc.s V_10
}
// System.Void Shake/<shake>c__Iterator7::Dispose()
extern "C"  void U3CshakeU3Ec__Iterator7_Dispose_m3450972791 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Shake/<shake>c__Iterator7::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CshakeU3Ec__Iterator7_Reset_m1936158567_MetadataUsageId;
extern "C"  void U3CshakeU3Ec__Iterator7_Reset_m1936158567 (U3CshakeU3Ec__Iterator7_t570686417 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CshakeU3Ec__Iterator7_Reset_m1936158567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Telefono::.ctor()
extern "C"  void Telefono__ctor_m3358828039 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var;
extern const uint32_t Telefono_Start_m2305965831_MetadataUsageId;
extern "C"  void Telefono_Start_m2305965831 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Telefono_Start_m2305965831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_currentNumber_16(L_0);
		Telefono_UpdateDialHUD_m2371369475(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_buttonigual_14();
		NullCheck(L_1);
		Button_t3896396478 * L_2 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_1, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_2);
		Selectable_set_interactable_m2686686419(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_fotoDisplay_2();
		NullCheck(L_3);
		Image_t538875265 * L_4 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_5 = __this->get_pictures_18();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		int32_t L_6 = 0;
		Sprite_t3199167241 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_4);
		Image_set_sprite_m572551402(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add1()
extern "C"  void Telefono_add1_m1291726317 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add2()
extern "C"  void Telefono_add2_m1291727278 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add3()
extern "C"  void Telefono_add3_m1291728239 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add4()
extern "C"  void Telefono_add4_m1291729200 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add5()
extern "C"  void Telefono_add5_m1291730161 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add6()
extern "C"  void Telefono_add6_m1291731122 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add7()
extern "C"  void Telefono_add7_m1291732083 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add8()
extern "C"  void Telefono_add8_m1291733044 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add9()
extern "C"  void Telefono_add9_m1291734005 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, ((int32_t)9), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::add0()
extern "C"  void Telefono_add0_m1291725356 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_AddNumber_m2955638976(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::AddNumber(System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern const uint32_t Telefono_AddNumber_m2955638976_MetadataUsageId;
extern "C"  void Telefono_AddNumber_m2955638976 (Telefono_t2999111444 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Telefono_AddNumber_m2955638976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_isDialing_17();
		if (L_0)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_1 = __this->get_currentNumber_16();
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2979997331(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)7)))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_3 = __this->get_currentNumber_16();
		String_t* L_4 = Int32_ToString_m1286526384((&___i0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_currentNumber_16(L_5);
	}

IL_0034:
	{
		String_t* L_6 = __this->get_currentNumber_16();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m2979997331(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)7))))
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t3674682005 * L_8 = __this->get_buttonigual_14();
		NullCheck(L_8);
		Button_t3896396478 * L_9 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_8, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_9);
		Selectable_set_interactable_m2686686419(L_9, (bool)1, /*hidden argument*/NULL);
	}

IL_0056:
	{
		String_t* L_10 = __this->get_currentNumber_16();
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Telefono_UpdateDialHUD_m2371369475(__this, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void Telefono::Delete()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1655019122;
extern const uint32_t Telefono_Delete_m2268432968_MetadataUsageId;
extern "C"  void Telefono_Delete_m2268432968 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Telefono_Delete_m2268432968_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_currentNumber_16(L_0);
		Telefono_UpdateDialHUD_m2371369475(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_buttonigual_14();
		NullCheck(L_1);
		Button_t3896396478 * L_2 = GameObject_GetComponent_TisButton_t3896396478_m2812094092(L_1, /*hidden argument*/GameObject_GetComponent_TisButton_t3896396478_m2812094092_MethodInfo_var);
		NullCheck(L_2);
		Selectable_set_interactable_m2686686419(L_2, (bool)0, /*hidden argument*/NULL);
		MonoBehaviour_StopCoroutine_m2790918991(__this, _stringLiteral1655019122, /*hidden argument*/NULL);
		__this->set_isDialing_17((bool)0);
		GameObject_t3674682005 * L_3 = __this->get_fotoDisplay_2();
		NullCheck(L_3);
		Image_t538875265 * L_4 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_5 = __this->get_pictures_18();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		int32_t L_6 = 0;
		Sprite_t3199167241 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_4);
		Image_set_sprite_m572551402(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::Call()
extern "C"  void Telefono_Call_m430376251 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	{
		Telefono_CallingHUD_m112899632(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono::UpdateDialHUD()
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var;
extern const uint32_t Telefono_UpdateDialHUD_m2371369475_MetadataUsageId;
extern "C"  void Telefono_UpdateDialHUD_m2371369475 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Telefono_UpdateDialHUD_m2371369475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_DialHUD_15();
		NullCheck(L_0);
		Text_t9039225 * L_1 = GameObject_GetComponent_TisText_t9039225_m202917489(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		String_t* L_2 = __this->get_currentNumber_16();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		return;
	}
}
// System.Void Telefono::CallingHUD()
extern Il2CppCodeGenString* _stringLiteral1655019122;
extern const uint32_t Telefono_CallingHUD_m112899632_MetadataUsageId;
extern "C"  void Telefono_CallingHUD_m112899632 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Telefono_CallingHUD_m112899632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral1655019122, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Telefono::dialing()
extern Il2CppClass* U3CdialingU3Ec__Iterator8_t3806020820_il2cpp_TypeInfo_var;
extern const uint32_t Telefono_dialing_m411091183_MetadataUsageId;
extern "C"  Il2CppObject * Telefono_dialing_m411091183 (Telefono_t2999111444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Telefono_dialing_m411091183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CdialingU3Ec__Iterator8_t3806020820 * V_0 = NULL;
	{
		U3CdialingU3Ec__Iterator8_t3806020820 * L_0 = (U3CdialingU3Ec__Iterator8_t3806020820 *)il2cpp_codegen_object_new(U3CdialingU3Ec__Iterator8_t3806020820_il2cpp_TypeInfo_var);
		U3CdialingU3Ec__Iterator8__ctor_m1365083719(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CdialingU3Ec__Iterator8_t3806020820 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CdialingU3Ec__Iterator8_t3806020820 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Telefono::CheckCall(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3832195318;
extern Il2CppCodeGenString* _stringLiteral1070509616;
extern Il2CppCodeGenString* _stringLiteral1987596753;
extern Il2CppCodeGenString* _stringLiteral2904683890;
extern Il2CppCodeGenString* _stringLiteral3821771027;
extern Il2CppCodeGenString* _stringLiteral443890868;
extern const uint32_t Telefono_CheckCall_m2092514743_MetadataUsageId;
extern "C"  void Telefono_CheckCall_m2092514743 (Telefono_t2999111444 * __this, String_t* ___currentNumber0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Telefono_CheckCall_m2092514743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___currentNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3832195318, L_0, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___currentNumber0;
		bool L_3 = String_Equals_m1002918753(NULL /*static, unused*/, L_2, _stringLiteral1070509616, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		GameObject_t3674682005 * L_4 = __this->get_fotoDisplay_2();
		NullCheck(L_4);
		Image_t538875265 * L_5 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_4, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_6 = __this->get_pictures_18();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		Sprite_t3199167241 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_5);
		Image_set_sprite_m572551402(L_5, L_8, /*hidden argument*/NULL);
	}

IL_0038:
	{
		String_t* L_9 = ___currentNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_9, _stringLiteral1987596753, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0060;
		}
	}
	{
		GameObject_t3674682005 * L_11 = __this->get_fotoDisplay_2();
		NullCheck(L_11);
		Image_t538875265 * L_12 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_11, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_13 = __this->get_pictures_18();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		Sprite_t3199167241 * L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_12);
		Image_set_sprite_m572551402(L_12, L_15, /*hidden argument*/NULL);
	}

IL_0060:
	{
		String_t* L_16 = ___currentNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_16, _stringLiteral2904683890, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0088;
		}
	}
	{
		GameObject_t3674682005 * L_18 = __this->get_fotoDisplay_2();
		NullCheck(L_18);
		Image_t538875265 * L_19 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_18, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_20 = __this->get_pictures_18();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		int32_t L_21 = 2;
		Sprite_t3199167241 * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_19);
		Image_set_sprite_m572551402(L_19, L_22, /*hidden argument*/NULL);
	}

IL_0088:
	{
		String_t* L_23 = ___currentNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_23, _stringLiteral3821771027, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b0;
		}
	}
	{
		GameObject_t3674682005 * L_25 = __this->get_fotoDisplay_2();
		NullCheck(L_25);
		Image_t538875265 * L_26 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_25, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_27 = __this->get_pictures_18();
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 3);
		int32_t L_28 = 3;
		Sprite_t3199167241 * L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_26);
		Image_set_sprite_m572551402(L_26, L_29, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		String_t* L_30 = ___currentNumber0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_30, _stringLiteral443890868, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00d8;
		}
	}
	{
		GameObject_t3674682005 * L_32 = __this->get_fotoDisplay_2();
		NullCheck(L_32);
		Image_t538875265 * L_33 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_32, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_34 = __this->get_pictures_18();
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 4);
		int32_t L_35 = 4;
		Sprite_t3199167241 * L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_33);
		Image_set_sprite_m572551402(L_33, L_36, /*hidden argument*/NULL);
	}

IL_00d8:
	{
		Telefono_UpdateDialHUD_m2371369475(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Telefono/<dialing>c__Iterator8::.ctor()
extern "C"  void U3CdialingU3Ec__Iterator8__ctor_m1365083719 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Telefono/<dialing>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdialingU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3838339893 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Telefono/<dialing>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdialingU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m442071241 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean Telefono/<dialing>c__Iterator8::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1266623652;
extern Il2CppCodeGenString* _stringLiteral32210;
extern const uint32_t U3CdialingU3Ec__Iterator8_MoveNext_m82314037_MetadataUsageId;
extern "C"  bool U3CdialingU3Ec__Iterator8_MoveNext_m82314037 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CdialingU3Ec__Iterator8_MoveNext_m82314037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ee;
		}
	}
	{
		goto IL_011b;
	}

IL_0021:
	{
		Telefono_t2999111444 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		L_2->set_isDialing_17((bool)1);
		Telefono_t2999111444 * L_3 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = L_3->get_DialHUD_15();
		NullCheck(L_4);
		Text_t9039225 * L_5 = GameObject_GetComponent_TisText_t9039225_m202917489(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral1266623652);
		__this->set_U3CtimeU3E__0_0((5.0f));
		goto IL_00ee;
	}

IL_0057:
	{
		float L_6 = __this->get_U3CtimeU3E__0_0();
		__this->set_U3CtimeU3E__0_0(((float)((float)L_6-(float)(1.0f))));
		float L_7 = __this->get_U3CtimeU3E__0_0();
		if ((((float)(fmodf(L_7, (2.0f)))) == ((float)(0.0f))))
		{
			goto IL_00b8;
		}
	}
	{
		Telefono_t2999111444 * L_8 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = L_8->get_DialHUD_15();
		NullCheck(L_9);
		Text_t9039225 * L_10 = GameObject_GetComponent_TisText_t9039225_m202917489(L_9, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		Telefono_t2999111444 * L_11 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		GameObject_t3674682005 * L_12 = L_11->get_DialHUD_15();
		NullCheck(L_12);
		Text_t9039225 * L_13 = GameObject_GetComponent_TisText_t9039225_m202917489(L_12, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(73 /* System.String UnityEngine.UI.Text::get_text() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m138640077(NULL /*static, unused*/, L_14, _stringLiteral32210, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_15);
		goto IL_00d2;
	}

IL_00b8:
	{
		Telefono_t2999111444 * L_16 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = L_16->get_DialHUD_15();
		NullCheck(L_17);
		Text_t9039225 * L_18 = GameObject_GetComponent_TisText_t9039225_m202917489(L_17, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m202917489_MethodInfo_var);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, _stringLiteral1266623652);
	}

IL_00d2:
	{
		WaitForSeconds_t1615819279 * L_19 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_19, (0.9f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_19);
		__this->set_U24PC_1(1);
		goto IL_011d;
	}

IL_00ee:
	{
		float L_20 = __this->get_U3CtimeU3E__0_0();
		if ((((float)L_20) > ((float)(0.0f))))
		{
			goto IL_0057;
		}
	}
	{
		Telefono_t2999111444 * L_21 = __this->get_U3CU3Ef__this_3();
		Telefono_t2999111444 * L_22 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_22);
		String_t* L_23 = L_22->get_currentNumber_16();
		NullCheck(L_21);
		Telefono_CheckCall_m2092514743(L_21, L_23, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_011b:
	{
		return (bool)0;
	}

IL_011d:
	{
		return (bool)1;
	}
	// Dead block : IL_011f: ldloc.1
}
// System.Void Telefono/<dialing>c__Iterator8::Dispose()
extern "C"  void U3CdialingU3Ec__Iterator8_Dispose_m1778711748 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void Telefono/<dialing>c__Iterator8::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CdialingU3Ec__Iterator8_Reset_m3306483956_MetadataUsageId;
extern "C"  void U3CdialingU3Ec__Iterator8_Reset_m3306483956 (U3CdialingU3Ec__Iterator8_t3806020820 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CdialingU3Ec__Iterator8_Reset_m3306483956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Thunder::.ctor()
extern "C"  void Thunder__ctor_m1982240551 (Thunder_t329757892 * __this, const MethodInfo* method)
{
	{
		__this->set_minTime_2((0.9f));
		__this->set_threshold_3((0.5f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Thunder::Start()
extern "C"  void Thunder_Start_m929378343 (Thunder_t329757892 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Thunder::Update()
extern "C"  void Thunder_Update_m3046777030 (Thunder_t329757892 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_lastTime_5();
		float L_2 = __this->get_minTime_2();
		if ((!(((float)((float)((float)L_0-(float)L_1))) > ((float)L_2))))
		{
			goto IL_004f;
		}
	}
	{
		float L_3 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_threshold_3();
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0038;
		}
	}
	{
		Light_t4202674828 * L_5 = __this->get_light_4();
		NullCheck(L_5);
		Behaviour_set_enabled_m2046806933(L_5, (bool)1, /*hidden argument*/NULL);
		goto IL_004f;
	}

IL_0038:
	{
		Light_t4202674828 * L_6 = __this->get_light_4();
		NullCheck(L_6);
		Behaviour_set_enabled_m2046806933(L_6, (bool)0, /*hidden argument*/NULL);
		float L_7 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastTime_5(L_7);
	}

IL_004f:
	{
		return;
	}
}
// System.Void TimeLight::.ctor()
extern "C"  void TimeLight__ctor_m3113265506 (TimeLight_t2022754857 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimeLight::Start()
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2505164499;
extern const uint32_t TimeLight_Start_m2060403298_MetadataUsageId;
extern "C"  void TimeLight_Start_m2060403298 (TimeLight_t2022754857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimeLight_Start_m2060403298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t4283661327  V_2;
	memset(&V_2, 0, sizeof(V_2));
	TimeSpan_t413522987  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_0 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_0;
		TimeSpan_t413522987  L_1 = DateTime_get_TimeOfDay_m299590820((&V_2), /*hidden argument*/NULL);
		V_3 = L_1;
		double L_2 = TimeSpan_get_TotalMinutes_m854833447((&V_3), /*hidden argument*/NULL);
		V_0 = (((float)((float)L_2)));
		float L_3 = V_0;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2505164499, L_5, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		Quaternion_t1553702882  L_8 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)L_7*(float)(360.0f)))/(float)(1440.0f)))+(float)(180.0f))), (115.0f), (0.0f), /*hidden argument*/NULL);
		V_1 = L_8;
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_10 = V_1;
		NullCheck(L_9);
		Transform_set_localRotation_m3719981474(L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
