﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Move
struct Move_t2404337;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void Move::.ctor()
extern "C"  void Move__ctor_m324221386 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Start()
extern "C"  void Move_Start_m3566326474 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::setAncho(System.Int32)
extern "C"  void Move_setAncho_m3968134854 (Move_t2404337 * __this, int32_t ___ancho0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::SetBoundary(UnityEngine.Vector3)
extern "C"  void Move_SetBoundary_m976619013 (Move_t2404337 * __this, Vector3_t4282066566  ___boundary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::move()
extern "C"  void Move_move_m3903435179 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Update()
extern "C"  void Move_Update_m3187790467 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Left()
extern "C"  void Move_Left_m2948975745 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Right()
extern "C"  void Move_Right_m2369145348 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::resetMove()
extern "C"  void Move_resetMove_m3748710728 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::setCameras(UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Move_setCameras_m1043826816 (Move_t2404337 * __this, Camera_t2727095145 * ___Perspe0, Camera_t2727095145 * ___Ortho1, Camera_t2727095145 * ___Iso2, Camera_t2727095145 * ___Interface3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
