﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Examples.Scenes.TouchpadCamera.RotateCamera
struct RotateCamera_t1717774348;

#include "codegen/il2cpp-codegen.h"

// System.Void Examples.Scenes.TouchpadCamera.RotateCamera::.ctor()
extern "C"  void RotateCamera__ctor_m2159699684 (RotateCamera_t1717774348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Examples.Scenes.TouchpadCamera.RotateCamera::Update()
extern "C"  void RotateCamera_Update_m4253042857 (RotateCamera_t1717774348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
