﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Controller_State2540969054.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller
struct  Controller_t2630893500  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject Controller::Map
	GameObject_t3674682005 * ___Map_2;
	// UnityEngine.GameObject Controller::PlayerObj
	GameObject_t3674682005 * ___PlayerObj_3;
	// UnityEngine.GameObject Controller::BallObj
	GameObject_t3674682005 * ___BallObj_4;
	// UnityEngine.GameObject Controller::RoomGen
	GameObject_t3674682005 * ___RoomGen_5;
	// UnityEngine.GameObject Controller::FrontWall
	GameObject_t3674682005 * ___FrontWall_6;
	// UnityEngine.GameObject Controller::Room
	GameObject_t3674682005 * ___Room_7;
	// UnityEngine.GameObject Controller::UIPanelMenu
	GameObject_t3674682005 * ___UIPanelMenu_8;
	// UnityEngine.GameObject Controller::UIPanelGOver
	GameObject_t3674682005 * ___UIPanelGOver_9;
	// UnityEngine.GameObject Controller::UIPanelJuego
	GameObject_t3674682005 * ___UIPanelJuego_10;
	// UnityEngine.GameObject Controller::UIPanelTransicion
	GameObject_t3674682005 * ___UIPanelTransicion_11;
	// UnityEngine.GameObject Controller::BotesText
	GameObject_t3674682005 * ___BotesText_12;
	// UnityEngine.GameObject Controller::RoomText
	GameObject_t3674682005 * ___RoomText_13;
	// UnityEngine.GameObject Controller::BricksText
	GameObject_t3674682005 * ___BricksText_14;
	// UnityEngine.GameObject Controller::BotesTextGameOver
	GameObject_t3674682005 * ___BotesTextGameOver_15;
	// UnityEngine.GameObject Controller::RoomTextGameOver
	GameObject_t3674682005 * ___RoomTextGameOver_16;
	// UnityEngine.GameObject Controller::BricksTextGameOver
	GameObject_t3674682005 * ___BricksTextGameOver_17;
	// UnityEngine.GameObject Controller::BotesBuenosText
	GameObject_t3674682005 * ___BotesBuenosText_18;
	// UnityEngine.GameObject Controller::TimerTransition
	GameObject_t3674682005 * ___TimerTransition_19;
	// UnityEngine.GameObject Controller::BricksTextTransicion
	GameObject_t3674682005 * ___BricksTextTransicion_20;
	// UnityEngine.GameObject Controller::RoomTextTransicion
	GameObject_t3674682005 * ___RoomTextTransicion_21;
	// UnityEngine.GameObject Controller::TotalRoomMenuText
	GameObject_t3674682005 * ___TotalRoomMenuText_22;
	// UnityEngine.GameObject Controller::TotalBricksMenuText
	GameObject_t3674682005 * ___TotalBricksMenuText_23;
	// UnityEngine.GameObject Controller::Telefono
	GameObject_t3674682005 * ___Telefono_24;
	// UnityEngine.GameObject Controller::MinigameButton
	GameObject_t3674682005 * ___MinigameButton_25;
	// UnityEngine.GameObject Controller::EnterButton
	GameObject_t3674682005 * ___EnterButton_26;
	// UnityEngine.GameObject Controller::ball
	GameObject_t3674682005 * ___ball_27;
	// UnityEngine.GameObject Controller::player
	GameObject_t3674682005 * ___player_28;
	// UnityEngine.GameObject Controller::plants
	GameObject_t3674682005 * ___plants_29;
	// UnityEngine.Camera Controller::Perspec
	Camera_t2727095145 * ___Perspec_30;
	// UnityEngine.Camera Controller::Ortho
	Camera_t2727095145 * ___Ortho_31;
	// UnityEngine.Camera Controller::Iso
	Camera_t2727095145 * ___Iso_32;
	// UnityEngine.Camera Controller::Interface
	Camera_t2727095145 * ___Interface_33;
	// System.Int32 Controller::rollerCamara
	int32_t ___rollerCamara_34;
	// System.Single Controller::speedCamAnim
	float ___speedCamAnim_35;
	// System.Boolean Controller::gameRulesOn
	bool ___gameRulesOn_36;
	// System.Boolean Controller::isEnterButtonReady
	bool ___isEnterButtonReady_37;
	// System.Int32 Controller::NRoom
	int32_t ___NRoom_38;
	// System.Int32 Controller::NBricks
	int32_t ___NBricks_39;
	// System.Single Controller::tiempo
	float ___tiempo_42;
	// System.Single Controller::tiempoPausa
	float ___tiempoPausa_43;
	// System.Int32 Controller::numMultiplyBalls
	int32_t ___numMultiplyBalls_44;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Controller::Multibolas
	List_1_t747900261 * ___Multibolas_45;
	// UnityEngine.GameObject Controller::audio
	GameObject_t3674682005 * ___audio_46;

public:
	inline static int32_t get_offset_of_Map_2() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___Map_2)); }
	inline GameObject_t3674682005 * get_Map_2() const { return ___Map_2; }
	inline GameObject_t3674682005 ** get_address_of_Map_2() { return &___Map_2; }
	inline void set_Map_2(GameObject_t3674682005 * value)
	{
		___Map_2 = value;
		Il2CppCodeGenWriteBarrier(&___Map_2, value);
	}

	inline static int32_t get_offset_of_PlayerObj_3() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___PlayerObj_3)); }
	inline GameObject_t3674682005 * get_PlayerObj_3() const { return ___PlayerObj_3; }
	inline GameObject_t3674682005 ** get_address_of_PlayerObj_3() { return &___PlayerObj_3; }
	inline void set_PlayerObj_3(GameObject_t3674682005 * value)
	{
		___PlayerObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerObj_3, value);
	}

	inline static int32_t get_offset_of_BallObj_4() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___BallObj_4)); }
	inline GameObject_t3674682005 * get_BallObj_4() const { return ___BallObj_4; }
	inline GameObject_t3674682005 ** get_address_of_BallObj_4() { return &___BallObj_4; }
	inline void set_BallObj_4(GameObject_t3674682005 * value)
	{
		___BallObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___BallObj_4, value);
	}

	inline static int32_t get_offset_of_RoomGen_5() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___RoomGen_5)); }
	inline GameObject_t3674682005 * get_RoomGen_5() const { return ___RoomGen_5; }
	inline GameObject_t3674682005 ** get_address_of_RoomGen_5() { return &___RoomGen_5; }
	inline void set_RoomGen_5(GameObject_t3674682005 * value)
	{
		___RoomGen_5 = value;
		Il2CppCodeGenWriteBarrier(&___RoomGen_5, value);
	}

	inline static int32_t get_offset_of_FrontWall_6() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___FrontWall_6)); }
	inline GameObject_t3674682005 * get_FrontWall_6() const { return ___FrontWall_6; }
	inline GameObject_t3674682005 ** get_address_of_FrontWall_6() { return &___FrontWall_6; }
	inline void set_FrontWall_6(GameObject_t3674682005 * value)
	{
		___FrontWall_6 = value;
		Il2CppCodeGenWriteBarrier(&___FrontWall_6, value);
	}

	inline static int32_t get_offset_of_Room_7() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___Room_7)); }
	inline GameObject_t3674682005 * get_Room_7() const { return ___Room_7; }
	inline GameObject_t3674682005 ** get_address_of_Room_7() { return &___Room_7; }
	inline void set_Room_7(GameObject_t3674682005 * value)
	{
		___Room_7 = value;
		Il2CppCodeGenWriteBarrier(&___Room_7, value);
	}

	inline static int32_t get_offset_of_UIPanelMenu_8() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___UIPanelMenu_8)); }
	inline GameObject_t3674682005 * get_UIPanelMenu_8() const { return ___UIPanelMenu_8; }
	inline GameObject_t3674682005 ** get_address_of_UIPanelMenu_8() { return &___UIPanelMenu_8; }
	inline void set_UIPanelMenu_8(GameObject_t3674682005 * value)
	{
		___UIPanelMenu_8 = value;
		Il2CppCodeGenWriteBarrier(&___UIPanelMenu_8, value);
	}

	inline static int32_t get_offset_of_UIPanelGOver_9() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___UIPanelGOver_9)); }
	inline GameObject_t3674682005 * get_UIPanelGOver_9() const { return ___UIPanelGOver_9; }
	inline GameObject_t3674682005 ** get_address_of_UIPanelGOver_9() { return &___UIPanelGOver_9; }
	inline void set_UIPanelGOver_9(GameObject_t3674682005 * value)
	{
		___UIPanelGOver_9 = value;
		Il2CppCodeGenWriteBarrier(&___UIPanelGOver_9, value);
	}

	inline static int32_t get_offset_of_UIPanelJuego_10() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___UIPanelJuego_10)); }
	inline GameObject_t3674682005 * get_UIPanelJuego_10() const { return ___UIPanelJuego_10; }
	inline GameObject_t3674682005 ** get_address_of_UIPanelJuego_10() { return &___UIPanelJuego_10; }
	inline void set_UIPanelJuego_10(GameObject_t3674682005 * value)
	{
		___UIPanelJuego_10 = value;
		Il2CppCodeGenWriteBarrier(&___UIPanelJuego_10, value);
	}

	inline static int32_t get_offset_of_UIPanelTransicion_11() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___UIPanelTransicion_11)); }
	inline GameObject_t3674682005 * get_UIPanelTransicion_11() const { return ___UIPanelTransicion_11; }
	inline GameObject_t3674682005 ** get_address_of_UIPanelTransicion_11() { return &___UIPanelTransicion_11; }
	inline void set_UIPanelTransicion_11(GameObject_t3674682005 * value)
	{
		___UIPanelTransicion_11 = value;
		Il2CppCodeGenWriteBarrier(&___UIPanelTransicion_11, value);
	}

	inline static int32_t get_offset_of_BotesText_12() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___BotesText_12)); }
	inline GameObject_t3674682005 * get_BotesText_12() const { return ___BotesText_12; }
	inline GameObject_t3674682005 ** get_address_of_BotesText_12() { return &___BotesText_12; }
	inline void set_BotesText_12(GameObject_t3674682005 * value)
	{
		___BotesText_12 = value;
		Il2CppCodeGenWriteBarrier(&___BotesText_12, value);
	}

	inline static int32_t get_offset_of_RoomText_13() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___RoomText_13)); }
	inline GameObject_t3674682005 * get_RoomText_13() const { return ___RoomText_13; }
	inline GameObject_t3674682005 ** get_address_of_RoomText_13() { return &___RoomText_13; }
	inline void set_RoomText_13(GameObject_t3674682005 * value)
	{
		___RoomText_13 = value;
		Il2CppCodeGenWriteBarrier(&___RoomText_13, value);
	}

	inline static int32_t get_offset_of_BricksText_14() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___BricksText_14)); }
	inline GameObject_t3674682005 * get_BricksText_14() const { return ___BricksText_14; }
	inline GameObject_t3674682005 ** get_address_of_BricksText_14() { return &___BricksText_14; }
	inline void set_BricksText_14(GameObject_t3674682005 * value)
	{
		___BricksText_14 = value;
		Il2CppCodeGenWriteBarrier(&___BricksText_14, value);
	}

	inline static int32_t get_offset_of_BotesTextGameOver_15() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___BotesTextGameOver_15)); }
	inline GameObject_t3674682005 * get_BotesTextGameOver_15() const { return ___BotesTextGameOver_15; }
	inline GameObject_t3674682005 ** get_address_of_BotesTextGameOver_15() { return &___BotesTextGameOver_15; }
	inline void set_BotesTextGameOver_15(GameObject_t3674682005 * value)
	{
		___BotesTextGameOver_15 = value;
		Il2CppCodeGenWriteBarrier(&___BotesTextGameOver_15, value);
	}

	inline static int32_t get_offset_of_RoomTextGameOver_16() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___RoomTextGameOver_16)); }
	inline GameObject_t3674682005 * get_RoomTextGameOver_16() const { return ___RoomTextGameOver_16; }
	inline GameObject_t3674682005 ** get_address_of_RoomTextGameOver_16() { return &___RoomTextGameOver_16; }
	inline void set_RoomTextGameOver_16(GameObject_t3674682005 * value)
	{
		___RoomTextGameOver_16 = value;
		Il2CppCodeGenWriteBarrier(&___RoomTextGameOver_16, value);
	}

	inline static int32_t get_offset_of_BricksTextGameOver_17() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___BricksTextGameOver_17)); }
	inline GameObject_t3674682005 * get_BricksTextGameOver_17() const { return ___BricksTextGameOver_17; }
	inline GameObject_t3674682005 ** get_address_of_BricksTextGameOver_17() { return &___BricksTextGameOver_17; }
	inline void set_BricksTextGameOver_17(GameObject_t3674682005 * value)
	{
		___BricksTextGameOver_17 = value;
		Il2CppCodeGenWriteBarrier(&___BricksTextGameOver_17, value);
	}

	inline static int32_t get_offset_of_BotesBuenosText_18() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___BotesBuenosText_18)); }
	inline GameObject_t3674682005 * get_BotesBuenosText_18() const { return ___BotesBuenosText_18; }
	inline GameObject_t3674682005 ** get_address_of_BotesBuenosText_18() { return &___BotesBuenosText_18; }
	inline void set_BotesBuenosText_18(GameObject_t3674682005 * value)
	{
		___BotesBuenosText_18 = value;
		Il2CppCodeGenWriteBarrier(&___BotesBuenosText_18, value);
	}

	inline static int32_t get_offset_of_TimerTransition_19() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___TimerTransition_19)); }
	inline GameObject_t3674682005 * get_TimerTransition_19() const { return ___TimerTransition_19; }
	inline GameObject_t3674682005 ** get_address_of_TimerTransition_19() { return &___TimerTransition_19; }
	inline void set_TimerTransition_19(GameObject_t3674682005 * value)
	{
		___TimerTransition_19 = value;
		Il2CppCodeGenWriteBarrier(&___TimerTransition_19, value);
	}

	inline static int32_t get_offset_of_BricksTextTransicion_20() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___BricksTextTransicion_20)); }
	inline GameObject_t3674682005 * get_BricksTextTransicion_20() const { return ___BricksTextTransicion_20; }
	inline GameObject_t3674682005 ** get_address_of_BricksTextTransicion_20() { return &___BricksTextTransicion_20; }
	inline void set_BricksTextTransicion_20(GameObject_t3674682005 * value)
	{
		___BricksTextTransicion_20 = value;
		Il2CppCodeGenWriteBarrier(&___BricksTextTransicion_20, value);
	}

	inline static int32_t get_offset_of_RoomTextTransicion_21() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___RoomTextTransicion_21)); }
	inline GameObject_t3674682005 * get_RoomTextTransicion_21() const { return ___RoomTextTransicion_21; }
	inline GameObject_t3674682005 ** get_address_of_RoomTextTransicion_21() { return &___RoomTextTransicion_21; }
	inline void set_RoomTextTransicion_21(GameObject_t3674682005 * value)
	{
		___RoomTextTransicion_21 = value;
		Il2CppCodeGenWriteBarrier(&___RoomTextTransicion_21, value);
	}

	inline static int32_t get_offset_of_TotalRoomMenuText_22() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___TotalRoomMenuText_22)); }
	inline GameObject_t3674682005 * get_TotalRoomMenuText_22() const { return ___TotalRoomMenuText_22; }
	inline GameObject_t3674682005 ** get_address_of_TotalRoomMenuText_22() { return &___TotalRoomMenuText_22; }
	inline void set_TotalRoomMenuText_22(GameObject_t3674682005 * value)
	{
		___TotalRoomMenuText_22 = value;
		Il2CppCodeGenWriteBarrier(&___TotalRoomMenuText_22, value);
	}

	inline static int32_t get_offset_of_TotalBricksMenuText_23() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___TotalBricksMenuText_23)); }
	inline GameObject_t3674682005 * get_TotalBricksMenuText_23() const { return ___TotalBricksMenuText_23; }
	inline GameObject_t3674682005 ** get_address_of_TotalBricksMenuText_23() { return &___TotalBricksMenuText_23; }
	inline void set_TotalBricksMenuText_23(GameObject_t3674682005 * value)
	{
		___TotalBricksMenuText_23 = value;
		Il2CppCodeGenWriteBarrier(&___TotalBricksMenuText_23, value);
	}

	inline static int32_t get_offset_of_Telefono_24() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___Telefono_24)); }
	inline GameObject_t3674682005 * get_Telefono_24() const { return ___Telefono_24; }
	inline GameObject_t3674682005 ** get_address_of_Telefono_24() { return &___Telefono_24; }
	inline void set_Telefono_24(GameObject_t3674682005 * value)
	{
		___Telefono_24 = value;
		Il2CppCodeGenWriteBarrier(&___Telefono_24, value);
	}

	inline static int32_t get_offset_of_MinigameButton_25() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___MinigameButton_25)); }
	inline GameObject_t3674682005 * get_MinigameButton_25() const { return ___MinigameButton_25; }
	inline GameObject_t3674682005 ** get_address_of_MinigameButton_25() { return &___MinigameButton_25; }
	inline void set_MinigameButton_25(GameObject_t3674682005 * value)
	{
		___MinigameButton_25 = value;
		Il2CppCodeGenWriteBarrier(&___MinigameButton_25, value);
	}

	inline static int32_t get_offset_of_EnterButton_26() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___EnterButton_26)); }
	inline GameObject_t3674682005 * get_EnterButton_26() const { return ___EnterButton_26; }
	inline GameObject_t3674682005 ** get_address_of_EnterButton_26() { return &___EnterButton_26; }
	inline void set_EnterButton_26(GameObject_t3674682005 * value)
	{
		___EnterButton_26 = value;
		Il2CppCodeGenWriteBarrier(&___EnterButton_26, value);
	}

	inline static int32_t get_offset_of_ball_27() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___ball_27)); }
	inline GameObject_t3674682005 * get_ball_27() const { return ___ball_27; }
	inline GameObject_t3674682005 ** get_address_of_ball_27() { return &___ball_27; }
	inline void set_ball_27(GameObject_t3674682005 * value)
	{
		___ball_27 = value;
		Il2CppCodeGenWriteBarrier(&___ball_27, value);
	}

	inline static int32_t get_offset_of_player_28() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___player_28)); }
	inline GameObject_t3674682005 * get_player_28() const { return ___player_28; }
	inline GameObject_t3674682005 ** get_address_of_player_28() { return &___player_28; }
	inline void set_player_28(GameObject_t3674682005 * value)
	{
		___player_28 = value;
		Il2CppCodeGenWriteBarrier(&___player_28, value);
	}

	inline static int32_t get_offset_of_plants_29() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___plants_29)); }
	inline GameObject_t3674682005 * get_plants_29() const { return ___plants_29; }
	inline GameObject_t3674682005 ** get_address_of_plants_29() { return &___plants_29; }
	inline void set_plants_29(GameObject_t3674682005 * value)
	{
		___plants_29 = value;
		Il2CppCodeGenWriteBarrier(&___plants_29, value);
	}

	inline static int32_t get_offset_of_Perspec_30() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___Perspec_30)); }
	inline Camera_t2727095145 * get_Perspec_30() const { return ___Perspec_30; }
	inline Camera_t2727095145 ** get_address_of_Perspec_30() { return &___Perspec_30; }
	inline void set_Perspec_30(Camera_t2727095145 * value)
	{
		___Perspec_30 = value;
		Il2CppCodeGenWriteBarrier(&___Perspec_30, value);
	}

	inline static int32_t get_offset_of_Ortho_31() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___Ortho_31)); }
	inline Camera_t2727095145 * get_Ortho_31() const { return ___Ortho_31; }
	inline Camera_t2727095145 ** get_address_of_Ortho_31() { return &___Ortho_31; }
	inline void set_Ortho_31(Camera_t2727095145 * value)
	{
		___Ortho_31 = value;
		Il2CppCodeGenWriteBarrier(&___Ortho_31, value);
	}

	inline static int32_t get_offset_of_Iso_32() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___Iso_32)); }
	inline Camera_t2727095145 * get_Iso_32() const { return ___Iso_32; }
	inline Camera_t2727095145 ** get_address_of_Iso_32() { return &___Iso_32; }
	inline void set_Iso_32(Camera_t2727095145 * value)
	{
		___Iso_32 = value;
		Il2CppCodeGenWriteBarrier(&___Iso_32, value);
	}

	inline static int32_t get_offset_of_Interface_33() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___Interface_33)); }
	inline Camera_t2727095145 * get_Interface_33() const { return ___Interface_33; }
	inline Camera_t2727095145 ** get_address_of_Interface_33() { return &___Interface_33; }
	inline void set_Interface_33(Camera_t2727095145 * value)
	{
		___Interface_33 = value;
		Il2CppCodeGenWriteBarrier(&___Interface_33, value);
	}

	inline static int32_t get_offset_of_rollerCamara_34() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___rollerCamara_34)); }
	inline int32_t get_rollerCamara_34() const { return ___rollerCamara_34; }
	inline int32_t* get_address_of_rollerCamara_34() { return &___rollerCamara_34; }
	inline void set_rollerCamara_34(int32_t value)
	{
		___rollerCamara_34 = value;
	}

	inline static int32_t get_offset_of_speedCamAnim_35() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___speedCamAnim_35)); }
	inline float get_speedCamAnim_35() const { return ___speedCamAnim_35; }
	inline float* get_address_of_speedCamAnim_35() { return &___speedCamAnim_35; }
	inline void set_speedCamAnim_35(float value)
	{
		___speedCamAnim_35 = value;
	}

	inline static int32_t get_offset_of_gameRulesOn_36() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___gameRulesOn_36)); }
	inline bool get_gameRulesOn_36() const { return ___gameRulesOn_36; }
	inline bool* get_address_of_gameRulesOn_36() { return &___gameRulesOn_36; }
	inline void set_gameRulesOn_36(bool value)
	{
		___gameRulesOn_36 = value;
	}

	inline static int32_t get_offset_of_isEnterButtonReady_37() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___isEnterButtonReady_37)); }
	inline bool get_isEnterButtonReady_37() const { return ___isEnterButtonReady_37; }
	inline bool* get_address_of_isEnterButtonReady_37() { return &___isEnterButtonReady_37; }
	inline void set_isEnterButtonReady_37(bool value)
	{
		___isEnterButtonReady_37 = value;
	}

	inline static int32_t get_offset_of_NRoom_38() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___NRoom_38)); }
	inline int32_t get_NRoom_38() const { return ___NRoom_38; }
	inline int32_t* get_address_of_NRoom_38() { return &___NRoom_38; }
	inline void set_NRoom_38(int32_t value)
	{
		___NRoom_38 = value;
	}

	inline static int32_t get_offset_of_NBricks_39() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___NBricks_39)); }
	inline int32_t get_NBricks_39() const { return ___NBricks_39; }
	inline int32_t* get_address_of_NBricks_39() { return &___NBricks_39; }
	inline void set_NBricks_39(int32_t value)
	{
		___NBricks_39 = value;
	}

	inline static int32_t get_offset_of_tiempo_42() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___tiempo_42)); }
	inline float get_tiempo_42() const { return ___tiempo_42; }
	inline float* get_address_of_tiempo_42() { return &___tiempo_42; }
	inline void set_tiempo_42(float value)
	{
		___tiempo_42 = value;
	}

	inline static int32_t get_offset_of_tiempoPausa_43() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___tiempoPausa_43)); }
	inline float get_tiempoPausa_43() const { return ___tiempoPausa_43; }
	inline float* get_address_of_tiempoPausa_43() { return &___tiempoPausa_43; }
	inline void set_tiempoPausa_43(float value)
	{
		___tiempoPausa_43 = value;
	}

	inline static int32_t get_offset_of_numMultiplyBalls_44() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___numMultiplyBalls_44)); }
	inline int32_t get_numMultiplyBalls_44() const { return ___numMultiplyBalls_44; }
	inline int32_t* get_address_of_numMultiplyBalls_44() { return &___numMultiplyBalls_44; }
	inline void set_numMultiplyBalls_44(int32_t value)
	{
		___numMultiplyBalls_44 = value;
	}

	inline static int32_t get_offset_of_Multibolas_45() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___Multibolas_45)); }
	inline List_1_t747900261 * get_Multibolas_45() const { return ___Multibolas_45; }
	inline List_1_t747900261 ** get_address_of_Multibolas_45() { return &___Multibolas_45; }
	inline void set_Multibolas_45(List_1_t747900261 * value)
	{
		___Multibolas_45 = value;
		Il2CppCodeGenWriteBarrier(&___Multibolas_45, value);
	}

	inline static int32_t get_offset_of_audio_46() { return static_cast<int32_t>(offsetof(Controller_t2630893500, ___audio_46)); }
	inline GameObject_t3674682005 * get_audio_46() const { return ___audio_46; }
	inline GameObject_t3674682005 ** get_address_of_audio_46() { return &___audio_46; }
	inline void set_audio_46(GameObject_t3674682005 * value)
	{
		___audio_46 = value;
		Il2CppCodeGenWriteBarrier(&___audio_46, value);
	}
};

struct Controller_t2630893500_StaticFields
{
public:
	// System.Int32 Controller::NTotalBricks
	int32_t ___NTotalBricks_40;
	// System.Int32 Controller::NTotalRooms
	int32_t ___NTotalRooms_41;
	// Controller/State Controller::gameState
	int32_t ___gameState_47;

public:
	inline static int32_t get_offset_of_NTotalBricks_40() { return static_cast<int32_t>(offsetof(Controller_t2630893500_StaticFields, ___NTotalBricks_40)); }
	inline int32_t get_NTotalBricks_40() const { return ___NTotalBricks_40; }
	inline int32_t* get_address_of_NTotalBricks_40() { return &___NTotalBricks_40; }
	inline void set_NTotalBricks_40(int32_t value)
	{
		___NTotalBricks_40 = value;
	}

	inline static int32_t get_offset_of_NTotalRooms_41() { return static_cast<int32_t>(offsetof(Controller_t2630893500_StaticFields, ___NTotalRooms_41)); }
	inline int32_t get_NTotalRooms_41() const { return ___NTotalRooms_41; }
	inline int32_t* get_address_of_NTotalRooms_41() { return &___NTotalRooms_41; }
	inline void set_NTotalRooms_41(int32_t value)
	{
		___NTotalRooms_41 = value;
	}

	inline static int32_t get_offset_of_gameState_47() { return static_cast<int32_t>(offsetof(Controller_t2630893500_StaticFields, ___gameState_47)); }
	inline int32_t get_gameState_47() const { return ___gameState_47; }
	inline int32_t* get_address_of_gameState_47() { return &___gameState_47; }
	inline void set_gameState_47(int32_t value)
	{
		___gameState_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
