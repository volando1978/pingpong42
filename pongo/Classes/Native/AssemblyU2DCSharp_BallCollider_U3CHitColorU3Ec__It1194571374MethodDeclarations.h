﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BallCollider/<HitColor>c__Iterator1
struct U3CHitColorU3Ec__Iterator1_t1194571374;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BallCollider/<HitColor>c__Iterator1::.ctor()
extern "C"  void U3CHitColorU3Ec__Iterator1__ctor_m3621972157 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BallCollider/<HitColor>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CHitColorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m98097461 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BallCollider/<HitColor>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CHitColorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m369485513 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BallCollider/<HitColor>c__Iterator1::MoveNext()
extern "C"  bool U3CHitColorU3Ec__Iterator1_MoveNext_m12638743 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallCollider/<HitColor>c__Iterator1::Dispose()
extern "C"  void U3CHitColorU3Ec__Iterator1_Dispose_m1690016186 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BallCollider/<HitColor>c__Iterator1::Reset()
extern "C"  void U3CHitColorU3Ec__Iterator1_Reset_m1268405098 (U3CHitColorU3Ec__Iterator1_t1194571374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
