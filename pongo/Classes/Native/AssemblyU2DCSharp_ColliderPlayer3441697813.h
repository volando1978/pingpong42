﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColliderPlayer
struct  ColliderPlayer_t3441697813  : public MonoBehaviour_t667441552
{
public:
	// System.Single ColliderPlayer::AVG_x
	float ___AVG_x_2;
	// System.Single ColliderPlayer::AVG_y
	float ___AVG_y_3;
	// System.Single ColliderPlayer::AVG_z
	float ___AVG_z_4;
	// System.Single ColliderPlayer::AVG_Multiplier
	float ___AVG_Multiplier_5;
	// System.Single ColliderPlayer::fuerza
	float ___fuerza_6;
	// UnityEngine.Vector3 ColliderPlayer::sc
	Vector3_t4282066566  ___sc_7;
	// UnityEngine.Camera ColliderPlayer::Perspec
	Camera_t2727095145 * ___Perspec_8;
	// UnityEngine.Camera ColliderPlayer::Ortho
	Camera_t2727095145 * ___Ortho_9;
	// UnityEngine.Camera ColliderPlayer::Iso
	Camera_t2727095145 * ___Iso_10;
	// UnityEngine.Camera ColliderPlayer::Interface
	Camera_t2727095145 * ___Interface_11;
	// UnityEngine.GameObject ColliderPlayer::Controller
	GameObject_t3674682005 * ___Controller_12;

public:
	inline static int32_t get_offset_of_AVG_x_2() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___AVG_x_2)); }
	inline float get_AVG_x_2() const { return ___AVG_x_2; }
	inline float* get_address_of_AVG_x_2() { return &___AVG_x_2; }
	inline void set_AVG_x_2(float value)
	{
		___AVG_x_2 = value;
	}

	inline static int32_t get_offset_of_AVG_y_3() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___AVG_y_3)); }
	inline float get_AVG_y_3() const { return ___AVG_y_3; }
	inline float* get_address_of_AVG_y_3() { return &___AVG_y_3; }
	inline void set_AVG_y_3(float value)
	{
		___AVG_y_3 = value;
	}

	inline static int32_t get_offset_of_AVG_z_4() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___AVG_z_4)); }
	inline float get_AVG_z_4() const { return ___AVG_z_4; }
	inline float* get_address_of_AVG_z_4() { return &___AVG_z_4; }
	inline void set_AVG_z_4(float value)
	{
		___AVG_z_4 = value;
	}

	inline static int32_t get_offset_of_AVG_Multiplier_5() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___AVG_Multiplier_5)); }
	inline float get_AVG_Multiplier_5() const { return ___AVG_Multiplier_5; }
	inline float* get_address_of_AVG_Multiplier_5() { return &___AVG_Multiplier_5; }
	inline void set_AVG_Multiplier_5(float value)
	{
		___AVG_Multiplier_5 = value;
	}

	inline static int32_t get_offset_of_fuerza_6() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___fuerza_6)); }
	inline float get_fuerza_6() const { return ___fuerza_6; }
	inline float* get_address_of_fuerza_6() { return &___fuerza_6; }
	inline void set_fuerza_6(float value)
	{
		___fuerza_6 = value;
	}

	inline static int32_t get_offset_of_sc_7() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___sc_7)); }
	inline Vector3_t4282066566  get_sc_7() const { return ___sc_7; }
	inline Vector3_t4282066566 * get_address_of_sc_7() { return &___sc_7; }
	inline void set_sc_7(Vector3_t4282066566  value)
	{
		___sc_7 = value;
	}

	inline static int32_t get_offset_of_Perspec_8() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___Perspec_8)); }
	inline Camera_t2727095145 * get_Perspec_8() const { return ___Perspec_8; }
	inline Camera_t2727095145 ** get_address_of_Perspec_8() { return &___Perspec_8; }
	inline void set_Perspec_8(Camera_t2727095145 * value)
	{
		___Perspec_8 = value;
		Il2CppCodeGenWriteBarrier(&___Perspec_8, value);
	}

	inline static int32_t get_offset_of_Ortho_9() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___Ortho_9)); }
	inline Camera_t2727095145 * get_Ortho_9() const { return ___Ortho_9; }
	inline Camera_t2727095145 ** get_address_of_Ortho_9() { return &___Ortho_9; }
	inline void set_Ortho_9(Camera_t2727095145 * value)
	{
		___Ortho_9 = value;
		Il2CppCodeGenWriteBarrier(&___Ortho_9, value);
	}

	inline static int32_t get_offset_of_Iso_10() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___Iso_10)); }
	inline Camera_t2727095145 * get_Iso_10() const { return ___Iso_10; }
	inline Camera_t2727095145 ** get_address_of_Iso_10() { return &___Iso_10; }
	inline void set_Iso_10(Camera_t2727095145 * value)
	{
		___Iso_10 = value;
		Il2CppCodeGenWriteBarrier(&___Iso_10, value);
	}

	inline static int32_t get_offset_of_Interface_11() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___Interface_11)); }
	inline Camera_t2727095145 * get_Interface_11() const { return ___Interface_11; }
	inline Camera_t2727095145 ** get_address_of_Interface_11() { return &___Interface_11; }
	inline void set_Interface_11(Camera_t2727095145 * value)
	{
		___Interface_11 = value;
		Il2CppCodeGenWriteBarrier(&___Interface_11, value);
	}

	inline static int32_t get_offset_of_Controller_12() { return static_cast<int32_t>(offsetof(ColliderPlayer_t3441697813, ___Controller_12)); }
	inline GameObject_t3674682005 * get_Controller_12() const { return ___Controller_12; }
	inline GameObject_t3674682005 ** get_address_of_Controller_12() { return &___Controller_12; }
	inline void set_Controller_12(GameObject_t3674682005 * value)
	{
		___Controller_12 = value;
		Il2CppCodeGenWriteBarrier(&___Controller_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
